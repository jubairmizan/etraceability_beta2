@extends('layouts.admin.master')
@section('main-content')
	<div class="container-fluid">
		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				@if($create==1)
					<h5 class="txt-dark">Create New User</h5>
				@else
					<h5 class="txt-dark">Edit User</h5>
				@endif
			</div>
		
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
					<li><a href="#"><span>Users</span></a></li>
					<li class="active"><span>@if($create==1) Create @else Edit @endif</span></li>
				</ol>
			</div>
			<!-- /Breadcrumb -->
		
		</div>
		<!-- /Title -->
		
		<!-- Row -->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default card-view">
										<div class="panel-wrapper collapse in">
											<div class="panel-body">
												<div class="row">
													<div class="col-md-12">
														<div class="form-wrap">
															{{ Form::open(['files'=>true, 'class'=>'form-horizontal']) }}
																<div class="form-body">
																	<h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Basic Information</h6>
																	<hr class="light-grey-hr"/>
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('type')?' has-error':''}}">
																				{{ Form::label('type_id','Type',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::select('type_id',$user_types,null,['class'=>'form-control','required','placeholder'=>'Select Type','id'=>'type_id']) }}
																					<span class="help-block with-errors">{{$errors->first('type_id')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('name')?' has-error':''}}">
																				{{ Form::label('name','Name',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::text('name',null,['class'=>'form-control','required','placeholder'=>'Enter Name','id'=>'name']) }}
																					<span class="help-block with-errors">{{$errors->first('name')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																	<!-- /Row -->
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('gender')?' has-error':''}}">
																				{{ Form::label('gender','Gender',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					<div class="radio-list">
																						<div class="radio-inline pl-0">
																							<span class="radio radio-info">
																								{{ Form::radio('gender','male',false,['class'=>'','id'=>'male','required']) }}
																								{{ Form::label('male','Male') }}
																							</span>
																						</div>
																						<div class="radio-inline pl-0">
																							<span class="radio radio-info">
																								{{ Form::radio('gender','female',false,['class'=>'','id'=>'female','required']) }}
																								{{ Form::label('female','Female') }}
																							</span>
																						</div>
																						<div class="radio-inline pl-0">
																							<span class="radio radio-info">
																								{{ Form::radio('gender','other',false,['class'=>'','id'=>'other','required']) }}
																								{{ Form::label('other','Other') }}
																							</span>
																						</div>
																					</div>
																					<span class="help-block with-errors">{{$errors->first('gender')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('dob')?' has-error':''}}">
																				{{ Form::label('dob','Date of Birth',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					<div class='input-group date datetimepicker1'>
																						{{  Form::text('dob',null,['class'=>'form-control']) }}
																						<span class="input-group-addon">
																							<span class="fa fa-calendar"></span>
																						</span>
																					</div>
																					<span class="help-block with-errors">{{$errors->first('dob')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																	<!-- /Row -->
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('email')?' has-error':''}}">
																				{{ Form::label('email','Email',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::text('email',null,['class'=>'form-control','required','placeholder'=>'Enter Email','id'=>'email']) }}
																					<span class="help-block with-errors">{{$errors->first('email')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('cell_no')?' has-error':''}}">
																				{{ Form::label('cell_no','Cell Number',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::text('cell_no',null,['class'=>'form-control','required','placeholder'=>'Enter Cell Number','id'=>'cell_no']) }}
																					<span class="help-block with-errors">{{$errors->first('cell_no')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																	<!-- /Row -->
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('nid')?' has-error':''}}">
																				{{ Form::label('nid','National ID',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::text('nid',null,['class'=>'form-control','placeholder'=>'Enter NID','id'=>'nid']) }}
																					<span class="help-block with-errors">{{$errors->first('nid')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('status')?' has-error':''}}">
																				{{ Form::label('status','Status',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					<div class="radio-list">
																						<div class="radio-inline pl-0">
																							<span class="radio radio-info">
																								{{ Form::radio('status',1,false,['class'=>'','id'=>'active','required']) }}
																								{{ Form::label('active','Active') }}
																							</span>
																						</div>
																						<div class="radio-inline pl-0">
																							<span class="radio radio-info">
																								{{ Form::radio('status',0,false,['class'=>'','id'=>'inactive','required']) }}
																								{{ Form::label('inactive','Inactive') }}
																							</span>
																						</div>
																					</div>
																					<span class="help-block with-errors">{{$errors->first('status')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																	<!-- /Row -->
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('password')?' has-error':''}}">
																				{{ Form::label('password','Password',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::password('password',['class'=>'form-control','required','placeholder'=>'Enter Password','id'=>'password']) }}
																					<span class="help-block with-errors">{{$errors->first('password')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																		<div class="col-md-6">
																			<div class="form-group">
																				{{ Form::label('confirm_password','Confirm Password',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::password('password_confirmation',['class'=>'form-control','required','placeholder'=>'Confirm Password','id'=>'confirm_password']) }}
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																	<!-- /Row -->
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('farming_type_id')?' has-error':''}}">
																				{{ Form::label('farming_type_id','Type',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::select('farming_type_id',$farming_type,null,['class'=>'form-control','placeholder'=>'Select Farming Type','id'=>'farming_type_id']) }}
																					<span class="help-block with-errors">{{$errors->first('farming_type_id')}}</span>
																				</div>
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('photo')?' has-error':''}}">
																				{{ Form::label('photo','Photo',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::file('photo',null,['class'=>'form-control','id'=>'photo']) }}
																					<span class="help-block with-errors">{{$errors->first('photo')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																	<!-- /Row -->
																	<div class="seprator-block"></div>

																	<h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>address</h6>
																	<hr class="light-grey-hr"/>
																	<!-- /Row -->
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('division_id')?' has-error':''}}">
																				{{ Form::label('division_id','Division',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::select('division_id',$divisions,null,['class'=>'form-control','required','placeholder'=>'Select Division','id'=>'division_id']) }}
																					<span class="help-block with-errors">{{$errors->first('division_id')}}</span>
																				</div>
																			</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-{{$errors->has('district_id')?' has-error':''}}">
																				{{ Form::label('district_id','District',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::select('district_id',$districts,null,['class'=>'form-control','required','placeholder'=>'Select District','id'=>'district_id']) }}
																					<span class="help-block with-errors">{{$errors->first('district_id')}}</span>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('upazila_id')?' has-error':''}}">
																				{{ Form::label('upazila_id','Upazila',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::select('upazila_id',$upazilas,null,['class'=>'form-control','required','placeholder'=>'Select Upazila','id'=>'upazila_id']) }}
																					<span class="help-block with-errors">{{$errors->first('upazila_id')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																		<div class="col-md-6">
																			<div class="form-group{{$errors->has('union_id')?' has-error':''}}">
																				{{ Form::label('union_id','Union',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::select('union_id',$unions,null,['class'=>'form-control','required','placeholder'=>'Select Union','id'=>'union_id']) }}
																					<span class="help-block with-errors">{{$errors->first('union_id')}}</span>
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																	<!-- /Row -->
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				{{ Form::label('mouja','Mouja',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::text('mouja',null,['class'=>'form-control','placeholder'=>'Enter Mouja','id'=>'mouja']) }}
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																		<div class="col-md-6">
																			<div class="form-group">
																				{{ Form::label('post_office','Post Office',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::text('post_office',null,['class'=>'form-control','placeholder'=>'Enter Post Office','id'=>'post_office']) }}
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																	<!-- /Row -->
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																				{{ Form::label('post_code','Post Code',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::text('post_code',null,['class'=>'form-control','placeholder'=>'Enter Post Code','id'=>'post_code']) }}
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																		<div class="col-md-6">
																			<div class="form-group">
																				{{ Form::label('ward_no','Ward No.',['class'=>'control-label col-md-3']) }}
																				<div class="col-md-9">
																					{{ Form::text('ward_no',null,['class'=>'form-control','placeholder'=>'Enter Ward No.','id'=>'ward_no']) }}
																				</div>
																			</div>
																		</div>
																		<!--/span-->
																	</div>
																	<!-- /Row -->
																</div>
																<div class="form-actions mt-10">
																	<div class="row">
																		<div class="col-md-6">
																			<div class="row">
																				<div class="col-md-offset-3 col-md-9">
																					<button type="submit" class="btn btn-success  mr-10">Submit</button>
																					<button type="button" class="btn btn-default">Cancel</button>
																				</div>
																			</div>
																		</div>
																		<div class="col-md-6"> </div>
																	</div>
																</div>
															{{ Form::close() }}
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Row -->
	</div>
@endsection