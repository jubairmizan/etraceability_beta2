@extends('layouts.admin.master')
@section('main-content')
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
	<div class="container-fluid">	
		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			  <h5 class="txt-dark">Users</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			  <ol class="breadcrumb">
				<li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
				<li><a href="#"><span>Users</span></a></li>
				<li class="active"><span>View</span></li>
			  </ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->
		
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive">
									<table id="example" class="table table-hover display  pb-30" >
										<thead>
											<tr>
												<th>SL</th>
												<th>Name</th>
												<th>User ID</th>
												<th>Email</th>
												<th>Type</th>
												<th>Photo</th>
												<th>Status</th>
												<th>Created At</th>
											</tr>
										</thead>
										
										<tbody>
											<?php $sl = 1; ?>
											@foreach($users as $value)
												<tr>
													<td>{{ $sl }}</td>
													<td>{{ $value->name }}</td>
													<td>{{ $value->user_id }}</td>
													<td>{{ $value->email }}</td>
													<td>{{ $value->type->name }}</td>
													<td><img class="img-responsive" width="30" src="@if(!is_null($value->photo)){{asset($value->photo)}} @else {{asset('image/avatar.png')}}@endif"></td>
													<td>
														@if($value->status==1)
															Active
														@else
															In-active
														@endif
													</td>
													<td>{{ date('Y, F d',strtotime($value->created_at)) }}</td>
												</tr>
												<?php $sl++; ?>
											@endforeach
										</tbody>
									
										<tfoot>
											<tr>
												<th>SL</th>
												<th>Name</th>
												<th>Email</th>
												<th>Position</th>
												<th>Role</th>
												<th>Photo</th>
												<th>Status</th>
												<th>Created At</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->
	</div>
@endsection