@extends('layouts.processing_plant.master')
@section('main-content')
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
	<div class="container-fluid">	
		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				@if($type_of_farming==1)
					<h5 class="txt-dark">Clusters Pond List</h5>
				@else
					<h5 class="txt-dark">Semi-Intensive Ponds List</h5>
				@endif
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			  <ol class="breadcrumb">
				<li><a href="{{ route('processing_plant.dashboard') }}">Dashboard</a></li>
{{--				  <li> <a href="{{ route('processing_plant.supplierList',[$type_of_farming]) }}">@if($type_farming==1)Suppliers (Cluster-Perspective) @else Suppliers (Semi-Intensive) @endif</a></li>--}}
				 {{--@if($type_of_farming==1)--}}
					  {{--<li><a href="{{ route('processing_plant.clusters',[$type_of_farming,$cluster_or_farm_id]) }}"><span>Clusters</span></a></li>--}}
				{{--@else--}}
					  {{--<li><a href="{{ route('processing_plant.farms',[$type_of_farming,$cluster_or_farm_id]) }}"><span>Farms</span></a></li>--}}
				{{--@endif--}}
				<li><a href="#"><span>Ponds</span></a></li>
			  </ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->
		
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive">
									<table id="example" class="table table-hover display  pb-30" >
										{!! Form::hidden('client_id',$client_id) !!}
										<thead>
											<tr>
												<th>SL</th>
												<th>Name</th>
												<th>Total Capacity</th>
												<th>Average Body  Weight</th>
												<th>Expected Price</th>
												<th>Status</th>
												<th>RFQ</th>
												<th>Rating</th>
											</tr>
										</thead>
										
										<tbody>
											<?php $sl = 1; ?>
											@foreach($pondsList as $value)
												<tr>
													<td>{{ $sl }}</td>
													<td>{{ $value->name }}</td>
													<td>--</td>
													<td>
														@if($value->id==2)
															20.00 g
														@elseif($value->id==3)
															12.29 g
														@elseif($value->id==4)
															14.29 g
														@elseif($value->id==5)
															20.00 g
														@elseif($value->id==6)
															20.00 g
														@elseif($value->id==7)
															31.08 g
														@elseif($value->id==8)
															33.39 g
														@elseif($value->id==9)
															32.08 g
														@elseif($value->id==10)
															38.50 g
														@elseif($value->id==11)
															32.61 g
														@elseif($value->id==12)
															34.00 g
														@elseif($value->id==13)
															12.13 g
														@elseif($value->id==14)
															12.29 g
														@elseif($value->id==15)
															12.39 g
														@elseif($value->id==16)
															18.32 g
														@elseif($value->id==17)
															12.11 g
														@else
															--
														@endif
													</td>
													<td>--</td>
													<td><a href="{{route('processing_plant.ponds.history',[$type_of_farming, $value->id,$client_id])}}">Click to see the Pond's Status</a></td>

													{{--@if($value->id >= 2 && $value->id <= 18)--}}
														{{--<td><a href="{{route('processing_plant.ponds.history',[$type_of_farming, $value->id,$client_id])}}">Click to see the Pond's Status</a></td>--}}
													{{--@else--}}
														{{--<td><a href="#">Click to see the Pond's Status</a></td>--}}
													{{--@endif--}}
													<td>--</td>
													<td>(5.0) <span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
													</td>
												</tr>
												<?php $sl++; ?>
											@endforeach
										</tbody>
									
										<tfoot>
											<tr>
												<th>SL</th>
												<th>Name</th>
												<th>Total Capacity</th>
												<th>Average Body  Weight</th>
												<th>Expected Price</th>
												<th>Status</th>
												<th>RFQ</th>
												<th>Rating</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->
	</div>
@endsection