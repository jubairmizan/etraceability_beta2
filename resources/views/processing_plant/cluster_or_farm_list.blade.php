@extends('layouts.processing_plant.master')
@section('main-content')
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
	<div class="container-fluid">	
		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				@if($type_of_farming==1)
					<h5 class="txt-dark">Clusters</h5>
				@else
					<h5 class="txt-dark">Farms</h5>
				@endif
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			  <ol class="breadcrumb">
				<li><a href="{{ route('processing_plant.dashboard') }}">Dashboard</a></li>
				@if($type_of_farming==1)
					  <li><a href="#"><span>Clusters</span></a></li>
				@else
					  <li><a href="#"><span>Farms</span></a></li>
				@endif
			  </ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive">
									<table id="example" class="table table-hover display  pb-30" >
										<thead>
											<tr>
												<th>SL</th>
												@if($type_of_farming==1)
													<th>Clusters' Name</th>
													<th>Map</th>
													<th>Cluster Ponds</th>
												@else
													<th>Farms' Name</th>
													<th>Map</th>
													<th>Semi-Intensive Ponds</th>
												@endif
												<th>Total Capacity</th>
												<th>RFQ</th>
												<th>Rating</th>
											</tr>
										</thead>
										
										<tbody>
											<?php $sl = 1; ?>
											@foreach($cluster_or_farm as $value)
												<tr>
													<td>{{ $sl }}</td>
													<td>{{ $value->name }}</td>
													@if($type_of_farming==1)
														<td><a href="https://www.google.com/maps/d/u/0/viewer?ll=22.692393220851287%2C89.41166279999993&z=17&mid=18uC9F-5A8cfqIhA8Ql1EL5lCRitm--td">See the Google Map</a></td>
													@else
														@if($value->id==2)
															<td><a href="https://www.google.com/maps/d/u/0/viewer?ll=22.582558629042502,89.55279535&z=11&mid=1aa3XCs0zdV6HrZIRuiLHliNCVDIqaew4">See the Google Map</a></td>
														@else
														<td><a href="https://www.google.com/maps/d/u/0/viewer?ll=21.12756645810412,92.15467749999993&z=11&mid=1M17MO6eCTuYJj1ESK8iRiWoeryAzwfiI">See the Google Map</a></td>
													@endif
													@endif
													<td><a href="{{ url('processing_plant/ponds',[$type_of_farming,$value->id,$client_id]) }}">Click to see the Ponds</a></td>
													<td>--</td>
													<td>--</td>
													<td>(5.0) <span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
													</td>
												</tr>
												<?php $sl++; ?>
											@endforeach
										</tbody>
										<tfoot>
											<tr>
												<th>SL</th>
												@if($type_of_farming==1)
													<th>Clusters' Name</th>
													<th>Cluster Ponds</th>
												@else
													<th>Farms' Name</th>
													<th>Semi-Intensive Ponds</th>
												@endif
												<th>Total Capacity</th>
												<th>RFQ</th>
												<th>Rating</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->
	</div>
@endsection