@extends('layouts.shrimp_culture.processing_plant.master')
@section('main_content')
	<div class="container-fluid">
		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			  <h5 class="txt-dark">Suppliers</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			  <ol class="breadcrumb">
				<li><a href="{{ route('shrimp_culture.processing_plant.dashboard') }}">Dashboard</a></li>
				<li><a href="#"><span>Suppliers</span></a></li>
			  </ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->
		
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive">
									<table id="example" class="table table-hover display  pb-30" >
										@if($type_of_farming==1)
											<thead>
											<tr>
												<th>SL</th>
												<th>Suppliers' Name</th>
												<th>Clusters</th>
												<th>Total Capacity</th>
												<th>RFQ</th>
												<th>Rating</th>
											</tr>
											</thead>

											<tbody>
                                            <?php $sl = 1; ?>
											@foreach($supplier as $value)
												<tr>
													<td>{{ $sl }}</td>
													<td>{{ $value->name }}</td>
													<td><a href="{{ route('shrimp_culture.processing_plant.clusters',[$type_of_farming,$value->id]) }}">Click to see the Cluster</a></td>
													<td>--</td>
													<td>--</td>
													<td>(5.0) <span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
													</td>
												</tr>
                                                <?php $sl++; ?>
											@endforeach
											</tbody>

											<tfoot>
											<tr>
												<th>SL</th>
												<th>Name</th>
												<th>Farms</th>
												<th>Total Capacity</th>
												<th>RFQ</th>
												<th>Rating</th>
											</tr>
											</tfoot>
										@else
											<thead>
											<tr>
												<th>SL</th>
												<th>Suppliers' Name</th>
												<th>Farms</th>
												<th>Total Capacity</th>
												<th>RFQ</th>
												<th>Rating</th>
											</tr>
											</thead>

											<tbody>
                                            <?php $sl = 1; ?>
											@foreach($supplier as $value)
												<tr>
													<td>{{ $sl }}</td>
													<td>{{ $value->name }}</td>
													<td><a href="{{ route('shrimp_culture.processing_plant.farms',[$type_of_farming,$value->id]) }}">Click to see the Farms</a></td>
													<td>--</td>
													<td>--</td>
													<td>(5.0) <span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
														<span class="fa fa-star checked"></span>
													</td>
												</tr>
                                                <?php $sl++; ?>
											@endforeach
											</tbody>

											<tfoot>
											<tr>
												<th>SL</th>
												<th>Name</th>
												<th>Farms</th>
												<th>Total Capacity</th>
												<th>RFQ</th>
												<th>Rating</th>
											</tr>
											</tfoot>
										@endif
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->
	</div>
@endsection