@extends('layouts.processing_plant.master')
@section('main-content')
    <div class="container-fluid pt-25">
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row" style="padding: 20px">
                                        <h4>Welcome, {{Auth()->user()->name}}!</h4>
                                        <h5>Profile Information</h5>
                                        <p>Company: {{Auth()->user()->name}}</p>
                                        <p>District: {{$userData->district->name}}</p>
                                        <p>Upazila: {{$userData->upazila->name}}</p>
                                        <p>Email: {{Auth()->user()->email}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
@endsection