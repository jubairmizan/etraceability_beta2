@extends('layouts.processing_plant.master')
@section('main-content')
    <div class="container-fluid pt-25">
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Pond's Status</h5>
            </div>
            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('processing_plant.dashboard') }}">Dashboard</a></li>
                    <li><a href="#"><span>Pond's Status</span></a></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
        <!-- /Title -->
        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Status</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-responsive mt-40">
                                <table class="table table-bordered table-striped" style="border: 1px solid #000000">
                                    @if($type_of_farming==1)
                                    <thead>
                                    <tr>
                                        <th>Cluster Pond Name and Map Link</th>
                                        <td colspan="4">Name: {{$pond->ponds->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Cluster</th>
                                        <td colspan="4">{{$pond->cluster->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Owner</th>
                                        <td colspan="4">{{$client->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Total Capacity</th>
                                        <td colspan="4">--</td>
                                    </tr>
                                    <tr>
                                        <th>Target Harvest Weight</th>
                                        <td colspan="4">--</td>
                                    </tr>
                                    <tr>
                                        <th>Current Average Body  Weight</th>
                                        <td colspan="4">
                                            @if($pond->ponds->id==2)
                                                20.00 g
                                            @elseif($pond->ponds->id==3)
                                                12.29 g
                                            @elseif($pond->ponds->id==4)
                                                14.29 g
                                            @elseif($pond->ponds->id==5)
                                                20.00 g
                                            @elseif($pond->ponds->id==6)
                                                20.00 g
                                            @elseif($pond->ponds->id==7)
                                                31.08 g
                                            @elseif($pond->ponds->id==8)
                                                33.39 g
                                            @elseif($pond->ponds->id==9)
                                                32.08 g
                                            @elseif($pond->ponds->id==10)
                                                38.50 g
                                            @elseif($pond->ponds->id==11)
                                                32.61 g
                                            @elseif($pond->ponds->id==12)
                                                34.00 g
                                            @elseif($pond->ponds->id==13)
                                                12.13 g
                                            @elseif($pond->ponds->id==14)
                                                12.29 g
                                            @elseif($pond->ponds->id==15)
                                                12.39 g
                                            @elseif($pond->ponds->id==16)
                                                18.32 g
                                            @elseif($pond->ponds->id==17)
                                                12.11 g
                                            @else
                                                --
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Total (Current) Biomass</th>
                                        <td colspan="4">
                                            @if($pond->ponds->id==2)
                                                1,548 kg
                                            @elseif($pond->ponds->id==3)
                                                --
                                            @elseif($pond->ponds->id==4)
                                                --
                                            @elseif($pond->ponds->id==5)
                                                --
                                            @elseif($pond->ponds->id==6)
                                                1,836 kg
                                            @elseif($pond->ponds->id==7)
                                                2,028 kg
                                            @elseif($pond->ponds->id==8)
                                                1,791 kg
                                            @elseif($pond->ponds->id==9)
                                                1,419 kg
                                            @elseif($pond->ponds->id==10)
                                                2,327 kg
                                            @elseif($pond->ponds->id==11)
                                                1,654 kg
                                            @elseif($pond->ponds->id==12)
                                                1,408 kg
                                            @elseif($pond->ponds->id==13)
                                                466 kg
                                            @elseif($pond->ponds->id==14)
                                                472 kg
                                            @elseif($pond->ponds->id==15)
                                                515 kg
                                            @elseif($pond->ponds->id==16)
                                                645 kg
                                            @elseif($pond->ponds->id==17)
                                                465 kg
                                            @else
                                                --
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Days of Culture</th>
                                        <td colspan="4">--</td>
                                    </tr>
                                    <tr>
                                        <th>Expected Price</th>
                                        <td colspan="4">--</td>
                                    </tr>
                                    </thead>
                                    @else
                                        <thead>
                                        <tr>
                                            <th>Pond Name and Map Link</th>
                                            <td colspan="4">Name: {{$pond->ponds->name}}</td>
                                        </tr>
                                        <tr>
                                            <th>Farm</th>
                                            <td colspan="4">{{$pond->farm->name}}</td>
                                        </tr>
                                        <tr>
                                            <th>Farm Owner</th>
                                            <td colspan="4">{{$client->name}}</td>
                                        </tr>
                                        <tr>
                                            <th>Total Capacity</th>
                                            <td colspan="4">--</td>
                                        </tr>
                                        <tr>
                                            <th>Target Harvest Weight</th>
                                            <td colspan="4">Coming Soon</td>
                                        </tr>
                                        <tr>
                                            <th>Current Average Body  Weight</th>
                                            <td colspan="4">
                                                @if($pond->ponds->id==2)
                                                    20.00 g
                                                @elseif($pond->ponds->id==3)
                                                    12.29 g
                                                @elseif($pond->ponds->id==4)
                                                    14.29 g
                                                @elseif($pond->ponds->id==5)
                                                    20.00 g
                                                @elseif($pond->ponds->id==6)
                                                    20.00 g
                                                @elseif($pond->ponds->id==7)
                                                    31.08 g
                                                @elseif($pond->ponds->id==8)
                                                    33.39 g
                                                @elseif($pond->ponds->id==9)
                                                    32.08 g
                                                @elseif($pond->ponds->id==10)
                                                    38.50 g
                                                @elseif($pond->ponds->id==11)
                                                    32.61 g
                                                @elseif($pond->ponds->id==12)
                                                    34.00 g
                                                @elseif($pond->ponds->id==13)
                                                    12.13 g
                                                @elseif($pond->ponds->id==14)
                                                    12.29 g
                                                @elseif($pond->ponds->id==15)
                                                    12.39 g
                                                @elseif($pond->ponds->id==16)
                                                    18.32 g
                                                @elseif($pond->ponds->id==17)
                                                    12.11 g
                                                @else
                                                    --
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Total (Current) Biomass</th>
                                            <td colspan="4">
                                                @if($pond->ponds->id==2)
                                                    1,548 kg
                                                @elseif($pond->ponds->id==3)
                                                @elseif($pond->ponds->id==4)
                                                @elseif($pond->ponds->id==5)
                                                @elseif($pond->ponds->id==6)
                                                    1,836 kg
                                                @elseif($pond->ponds->id==7)
                                                    2,028 kg
                                                @elseif($pond->ponds->id==8)
                                                    1,791 kg
                                                @elseif($pond->ponds->id==9)
                                                    1,419 kg
                                                @elseif($pond->ponds->id==10)
                                                    2,327 kg
                                                @elseif($pond->ponds->id==11)
                                                    1,654 kg
                                                @elseif($pond->ponds->id==12)
                                                    1,408 kg
                                                @elseif($pond->ponds->id==13)
                                                    466 kg
                                                @elseif($pond->ponds->id==14)
                                                    472 kg
                                                @elseif($pond->ponds->id==15)
                                                    515 kg
                                                @elseif($pond->ponds->id==16)
                                                    645 kg
                                                @elseif($pond->ponds->id==17)
                                                    465 kg
                                                @else
                                                    --
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Days of Culture</th>
                                            <td colspan="4">--</td>
                                        </tr>
                                        <tr>
                                            <th>Expected Price</th>
                                            <td colspan="4">--</td>
                                        </tr>
                                        </thead>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div  class="panel-body">
                            <div class="table-responsive mt-40">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th colspan="5">Pond's Input</th>
                                    </tr>
                                    <tr>
                                        <th>Feeds</th>
                                        <th>Probiotics</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            @foreach($feed_details as $feed)
                                                <p>{{$feed->name}}</p>
                                            @endforeach
                                        </td>
                                        <td>
                                            @foreach($remark_details as $remark)
                                                <p>{{$remark->name}}</p>
                                            @endforeach
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="col-sm-4 col-sm-offset-4 col-xs-12 mt-15 text-center">
                                    {{--<button class="btn btn-primary btn-rounded btn-block">Make an RFQ</button>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
@endsection