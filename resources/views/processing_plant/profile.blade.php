@extends('layouts.processing_plant.master')
@section('main-content')
	<div class="row">
		<div class="col-md-12">
			<div class="button-list pull-right">
				<a class="btn btn-primary btn-outline btn-icon left-icon" style="border: none" onclick='printDiv()'> 
					<i class="fa fa-print"></i><span> Print</span> 
				</a>
			</div>
		</div>
	</div>
	@foreach($userData as $userDataValue)
		<div class="row" id="invoice">
			<div class="col-md-12">
				<div class="panel panel-default card-view">
					<div class="panel-heading" style="border-bottom: 1px black solid;float: left;width: 100%">
						<div class="pull-left" style="float: left;width: 100%">
							<h6 class="panel-title txt-dark" style="font-size: 20px;font-weight: bold;padding-left: 10px;">User Profile</h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="row" style="float: left;width: 100%">
								<div class="col-md-12 col-xs-12" style="width: 100%;float: left;">
									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">Name : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											@if($userDataValue->name != '')
												<span class="address-head mb-5">{{ $userDataValue->name }}</span>
											@endif
										</div>
									</div>
									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">User Id : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											<span class="address-head mb-5">{{ $userDataValue->user_id }}</span>
										</div>
									</div>
									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">Email : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											<span class="address-head mb-5">{{ $userDataValue->email }}</span>
										</div>
									</div>	

									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">Mobile Number : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											<span class="address-head mb-5">{{ $userDataValue->cell_no }}</span>
										</div>
									</div>	

									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">NID : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											<span class="address-head mb-5">{{ $userDataValue->nid }}</span>
										</div>
									</div>	

									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">Gender : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											<span class="address-head mb-5">{{ $userDataValue->gender }}</span>
										</div>
									</div>	

									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">Date Of Birth : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											<span class="address-head mb-5">{{ $userDataValue->dob }}</span>
										</div>
									</div>	

									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">Mouja : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											<span class="address-head mb-5">{{ $userDataValue->mouja }}</span>
										</div>
									</div>	


									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">Post Office : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											<span class="address-head mb-5">{{ $userDataValue->post_office }}</span>
										</div>
									</div>	

									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">Post Code : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											<span class="address-head mb-5">{{ $userDataValue->psot_code }}</span>
										</div>
									</div>
									<div class="row"  style="float: left;width: 100%;border-bottom: 1px gray solid;padding: 10px;">
										<div class="col-md-3" style="width: 17%;float: left;">
											<span class="txt-dark head-font inline-block capitalize-font mb-5">word no : </span>
										</div>
										<div class="col-md-9" style="width: 83%;float: right;">
											<span class="address-head mb-5">{{ $userDataValue->word_no }}</span>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach	

	<script>
        function printDiv()
        {

            var divToPrint=document.getElementById('invoice');

            var newWin=window.open('','Print-Window');

            newWin.document.open();

            newWin.document.write('<html><body style="margin:0px;padding:0px;" onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

            newWin.document.close();

            // setTimeout(function(){newWin.close();},10);
        }
    </script>
@endsection