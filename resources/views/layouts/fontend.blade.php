<!DOCTYPE html>
<html>
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">	

	<title>@yield('title') | Traceability</title>	

	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Porto - Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<!-- Favicon -->
	<link rel="shortcut icon" href="" type="image/x-icon" />
	<link rel="apple-touch-icon" href="./fontend/img/apple-touch-icon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Web Fonts  -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

	@yield('stylesheet')
	<!-- Vendor CSS -->
	
</head>
<body>

	<div class="body">
		<header id="header" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 57, "stickySetTop": "-57px", "stickyChangeLogo": true}'>
			<div class="header-body">
				<div class="header-container container">
					<div class="header-row">
						<div class="header-column">
							<div class="header-logo">
								<a href="{{ url('/') }}">
									<img alt="e-Traceability" width="222" height="108" data-sticky-width="" data-sticky-height="" data-sticky-top="33"  src="./fontend/img/222X108.png">
								</a>
							</div>
						</div>
						<div class="header-column">
							<div class="header-row">
								<nav class="header-nav-top">
									<ul class="nav nav-pills">
									</ul>
								</nav>
							</div>
							<div class="header-row">
								<div class="header-nav">
									<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
										<i class="fa fa-bars"></i>
									</button>
									<ul class="header-social-icons social-icons hidden-xs">
										<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
										<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
										<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
									</ul>
									<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
										<nav>
											<ul class="nav nav-pills" id="mainNav">
											{{-- 	<li class="">
													<a class="" href="{{ url('/') }}">
														Home
													</a>

												</li> --}}

												<li class="">
													<a class="" href="{{ route('about') }}">
														About US
													</a>
												</li>
												<li class="">
													<a class="" href="{{ route('partners') }}">
														Partners
													</a>
												</li>		
												{{-- <li class="">
													<a class="" href="#">
														Vendors
													</a>
												</li>	 --}}
												{{-- <li class="">
													<a class="" href="#">
														Product
													</a>
												</li>		 --}}
												
												<li class="dropdown">
													<a class="dropdown-toggle" href="#">
														Services
													</a>
													<ul class="dropdown-menu">
														<li><a href="{{ route('fishing') }}">Fishing</a></li>
														<li><a href="{{ route('farming')}}">Farming</a></li>
														{{-- <li><a href="{{ route('support')}}">Support</a></li> --}}
													</ul>
												</li>
												<li class="">
													<a class="" href="#">
														Support
													</a>
												</li>

												<li class="">
													<a class="" href="{{ route('contact') }}">
														Contact Us
													</a>

												</li>
												<li class="">
													<a class="" href="{{ route('login') }}">
														Login
													</a>

												</li>
												{{-- <li class="dropdown">
													<a class="dropdown-toggle" href="#">
														Login
													</a>
													<ul class="dropdown-menu">
														<li><a href="{{ url('/login') }}">Super Admin</a></li>
														
														<li><a href="{{ route('hatchery.login') }}">Hatchery Login</a></li>
														<li><a href="{{ route('si_pond.login') }}">Semi-Intensive Pond Login</a></li>

														<li><a href="{{ route('farmer.login') }}">Farmer</a></li>
														<li><a href="{{ route('cluster.login') }}">Cluster</a></li>
														<li><a href="{{ route('collection_center.login') }}">Collection Center</a></li>
													</ul>
												</li> --}}											
											</ul>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		@yield('content')

		@include('partials.fontend.footer')
	</div>

	@yield('script')

</body>
</html>
