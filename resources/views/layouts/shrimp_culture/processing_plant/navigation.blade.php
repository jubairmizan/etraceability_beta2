<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span>Welcome, {{auth()->user()->name}}</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='processing_plant' && request()->segment(3)=='dashboard'?'active':null}}" href="{{ route('shrimp_culture.processing_plant.dashboard') }}"><div class="pull-left"><i class="fa fa-dashboard mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a href="javascript:void(0);" data-toggle="collapse" data-target="#users"><div class="pull-left"><i class="fa fa-user mr-20"></i><span class="right-nav-text">Suppliers</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="users" class="collapse collapse-level-1 {{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='processing_plant' && (request()->segment(4)=='extensive' || request()->segment(4)=='semi_intensive')?'in':null}}">
                <li>
                    <a href="{{ route('shrimp_culture.processing_plant.suppliers',['extensive']) }}" class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='processing_plant' && request()->segment(3)=='suppliers' && request()->segment(4)=='extensive'?'active':null}}">Cluster Perspective</a>
                </li>
                <li>
                    <a href="{{ route('shrimp_culture.processing_plant.suppliers',['semi_intensive']) }}" class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='processing_plant' && request()->segment(3)=='suppliers' && request()->segment(4)=='semi_intensive'?'active':null}}">Semi-Intensive</a>
                </li>
            </ul>
        </li>
    </ul>
</div>