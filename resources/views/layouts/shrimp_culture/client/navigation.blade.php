<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        <li class="navigation-header">
            <span>Welcome, {{auth()->user()->name}}</span>
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='dashboard'?'active':null}}" href="{{route('shrimp_culture.client.dashboard')}}"><div class="pull-left"><i class="fa fa-dashboard mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='semi_intensive'?'active':null}}" href="javascript:void(0);" data-toggle="collapse" data-target="#semi_intensive"><div class="pull-left"><i class="fa fa-institution mr-20"></i><span class="right-nav-text">Semi-Intensive Farms</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="semi_intensive" class="collapse collapse-level-1 {{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='semi_intensive' && (request()->segment(4)=='pond_management' || request()->segment(4)=='sampling' || request()->segment(4)=='harvest')?'in':null}}">
                <li>
                    <a href="{{route('shrimp_culture.client.semi_intensive.pond_management')}}" class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='semi_intensive' && request()->segment(4)=='pond_management'?'active':null}}">Pond Management</a>
                </li>
                <li>
                    <a href="{{route('shrimp_culture.client.semi_intensive.sampling')}}" class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='semi_intensive' && request()->segment(4)=='sampling'?'active':null}}">Sampling</a>
                </li>
                <li>
                    <a href="{{route('shrimp_culture.client.semi_intensive.harvest')}}" class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='semi_intensive' && request()->segment(4)=='harvest'?'active':null}}">Harvest</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='extensive'?'active':null}}" href="javascript:void(0);" data-toggle="collapse" data-target="#extensive"><div class="pull-left"><i class="fa fa-institution mr-20"></i><span class="right-nav-text">Extensive Farms</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="extensive" class="collapse collapse-level-1 {{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='extensive' && request()->segment(4)=='pond_management'?'in':null}}">
                <li>
                    <a  href="{{route('shrimp_culture.client.extensive.pond_management')}}" class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='extensive' && request()->segment(4)=='pond_management'?'active':null}}">Pond Management</a>
                </li>
                <li>
                    <a  href="{{route('shrimp_culture.client.extensive.harvest')}}" class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='extensive' && request()->segment(4)=='harvest'?'active':null}}">Sampling</a>
                </li>
                <li>
                    <a  href="{{route('shrimp_culture.client.extensive.harvest')}}" class="{{request()->segment(1)=='shrimp_culture' && request()->segment(2)=='client' && request()->segment(3)=='extensive' && request()->segment(4)=='harvest'?'active':null}}">Harvest</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="" href="{{route('shrimp_culture.client.hatchery')}}"><div class="pull-left"><i class="fa fa-institution mr-20"></i><span class="right-nav-text">Hatcheries</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="" href="{{route('shrimp_culture.client.inventory')}}"><div class="pull-left"><i class="fa fa-flask mr-20"></i><span class="right-nav-text">Inventories</span></div><div class="pull-right"></div><div class="clearfix"></div></a>
        </li>
        <li>
            <a class="" href="javascript:void(0);" data-toggle="collapse" data-target="#inventory"><div class="pull-left"><i class="fa fa-list mr-20"></i><span class="right-nav-text">Reports</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="inventory" class="collapse collapse-level-1">
                <li>
                    <a class="active" href="#">Coming Soon</a>
                </li>
            </ul>
        </li>
        <li>
            {{--<a class="@if(isset($menu)) @if($menu=='inventory') active @endif @endif" href="javascript:void(0);" data-toggle="collapse" data-target="#Inventory"><div class="pull-left"><i class="zmdi zmdi-chart-donut mr-20"></i><span class="right-nav-text">Inventory In</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>--}}
            {{--<ul id="Inventory" class="collapse collapse-level-1 @if(isset($menu)) @if($menu=='inventory') in @endif @endif">--}}
            {{--<li>--}}
            {{--<a class="@if(isset($submenu)) @if($submenu=='inventory_management') active @endif @endif" href="{{ route('WhiteFish.client.inventory_management') }}">Management</a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a class="@if(isset($submenu)) @if($submenu=='pona') active @endif @endif" href="{{ route('WhiteFish.client.pona') }}">Pona</a>--}}
            {{--</li>--}}
            {{--@foreach($inventoryType as $menuVal)--}}
            {{--<li>--}}
            {{--<a class="@if(isset($submenu)) @if($submenu==str_replace(' ','_',$menuVal->name)) active @endif @endif" href="{{ route('WhiteFish.client.inventoryTypes',$menuVal->id) }}">{{ $menuVal->name }}</a>--}}
            {{--</li>--}}
            {{--@endforeach--}}
            {{--</ul>--}}
        </li>
        <li>
            {{--<a class="@if(isset($menu)) @if($menu=='inventoryStockList') active @endif @endif" href="javascript:void(0);" data-toggle="collapse" data-target="#inventoryStockList"><div class="pull-left"><i class="zmdi zmdi-chart-donut mr-20"></i><span class="right-nav-text">Inventory Out</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>--}}
            {{--<ul id="inventoryStockList" class="collapse collapse-level-1 @if(isset($menu)) @if($menu=='inventoryStockList') in @endif @endif">--}}
            {{--@foreach($inventoryType as $menuVal)--}}
            {{--<li>--}}
            {{--<a class="@if(isset($submenu)) @if($submenu==str_replace(' ','_',$menuVal->name)) active @endif @endif" href="{{ route('WhiteFish.client.inventoryStockData',$menuVal->id) }}">{{ $menuVal->name }}</a>--}}
            {{--</li>--}}
            {{--@endforeach--}}
            {{--</ul>--}}
        </li>
        <li>
            {{--<a class=" @if(isset($menu)) @if($menu=='ponds') active @endif @endif" href="javascript:void(0);" data-toggle="collapse" data-target="#ponds"><div class="pull-left"><i class="fa fa-list mr-20"></i><span class="right-nav-text">Ponds</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>--}}
            {{--<ul id="ponds" class="collapse collapse-level-1 @if(isset($menu)) @if($menu=='ponds') in @endif @endif">--}}
            {{--<li>--}}
            {{--<a class="@if(isset($submenu)) @if($submenu=='create') active @endif @endif" href="{{ route('WhiteFish.client.ponds.create') }}">Create</a>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<a class="@if(isset($submenu)) @if($submenu=='view') active @endif @endif" href="{{ route('WhiteFish.client.ponds') }}">View</a>--}}
            {{--</li>--}}
            {{--</ul>--}}
        </li>
        <li>
        {{--<a class=" @if(isset($menu)) @if($menu=='system_setting') active @endif @endif" href="javascript:void(0);" data-toggle="collapse" data-target="#systemSetting"><div class="pull-left"><i class="zmdi zmdi-filter-list mr-20"></i><span class="right-nav-text">System Settings</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>--}}
        {{--<ul id="systemSetting" class="collapse collapse-level-1 @if(isset($menu)) @if($menu=='system_setting') in @endif @endif">--}}
        {{--<li>--}}
        {{--<a class="@if(isset($submenu)) @if($submenu=='farms') active @endif @endif" href="{{ route('WhiteFish.client.farms') }}">Farms</a>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--<a class="@if(isset($submenu)) @if($submenu=='typesOfFish') active @endif @endif" href="{{ route('WhiteFish.client.typesOfFish') }}">Types Of Fish</a>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--<a class="@if(isset($submenu)) @if($submenu=='block') active @endif @endif" href="{{ route('WhiteFish.client.blocks') }}">Blocks</a>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--<a class="@if(isset($submenu)) @if($submenu=='inventory_brands') active @endif @endif" href="{{ route('WhiteFish.client.inventory_brands') }}">Brands</a>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--<a class="@if(isset($submenu)) @if($submenu=='inventory_types') active @endif @endif" href="{{ route('WhiteFish.client.inventory_types') }}">Types</a>--}}
        {{--</li>--}}
        {{--<li>--}}
        {{--<a class="@if(isset($submenu)) @if($submenu=='inventories') active @endif @endif" href="{{ route('WhiteFish.client.inventories') }}">Product</a>--}}
        {{--</li>--}}
    </ul>
    </li>
    </ul>
</div>