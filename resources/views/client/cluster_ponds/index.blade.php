@extends('layouts.client.master')
@section('main-content')
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
    <div class="container-fluid">
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">{{$title}}</h5>
            </div>
            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('client.dashboard') }}">Dashboard</a></li>
                    <li><a href="#"><span>Cluster Ponds</span></a></li>
                    <li class="active"><span>View</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="example" class="table table-hover display  pb-30" >
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Pond ID</th>
                                            <th>Pond Name</th>
                                            <th>Map</th>
                                            <th>Cluster</th>
                                            <th>Cluster Leader</th>
                                            <th>Area</th>
                                            <th>Depth</th>
                                            <th>Type of Soil</th>
                                            <th>Farming Cycle</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php $sl = 1; ?>
                                        @if(count($cluster_ponds)>0)
                                        @foreach($cluster_ponds as $value)
                                            <tr>
                                                <td>{{ $sl }}</td>
                                                <td>{{ $value->pond_id }}</td>
                                                <td>{{ $value->name }}</td>
                                                <td>
                                                    <a target="_blank" href="https://www.google.com/maps/d/u/0/viewer?ll=22.690557100000014%2C89.41116929999998&z=17&mid=18uC9F-5A8cfqIhA8Ql1EL5lCRitm--td">See the Google Map</a>
                                                </td>
                                                <td>{{ $value->cluster->name }}</td>
                                                <td>{{ $value->cluster_manager->name }}</td>
                                                <td>{{ $value->area }}</td>
                                                <td>{{ $value->depth }}</td>
                                                <td>{{ $value->type_of_soil }}</td>
                                                <td>{{ $value->farming_cycle }}</td>
                                                <td>
                                                    @if($value->status==1)
                                                        Active
                                                    @else
                                                        In-active
                                                    @endif
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        @endforeach
                                        @else
                                            <tr class="warning"><td colspan="11" align="center">No information found</td></tr>
                                        @endif
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>SL</th>
                                            <th>Pond ID</th>
                                            <th>Pond Name</th>
                                            <th>Map</th>
                                            <th>Cluster</th>
                                            <th>Cluster Leader</th>
                                            <th>Area</th>
                                            <th>Depth</th>
                                            <th>Type of Soil</th>
                                            <th>Farming Cycle</th>
                                            <th>Status</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
@endsection