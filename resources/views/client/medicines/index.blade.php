@extends('layouts.client.master')
@section('main-content')
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
    <div class="container-fluid">
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">{{$title}}</h5>
            </div>
            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('client.dashboard') }}">Dashboard</a></li>
                    <li><a href="#"><span>Medicines</span></a></li>
                    <li class="active"><span>View</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="example" class="table table-hover display  pb-30" >
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Brand</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php $sl = 1; ?>
                                        @if(count($medicines)>0)
                                        @foreach($medicines as $value)
                                            <tr>
                                                <td>{{ $sl }}</td>
                                                <td>{{ $value->name }}</td>
                                                <td>{{ isset($value->brand)?$value->brand:'N/A' }}</td>
                                                <td>
                                                    @if($value->status==1)
                                                        Active
                                                    @else
                                                        In-active
                                                    @endif
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        @endforeach
                                        @else
                                            <tr class="warning"><td colspan="5" align="center">No information found</td></tr>
                                        @endif
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Brand</th>
                                            <th>Status</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
@endsection