@extends('layouts.client.master')
@section('main-content')
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
	<div class="container-fluid">	
		<!-- Title -->
		<div class="row heading-bg">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			  <h5 class="txt-dark">{{$title}}</h5>
			</div>
			<!-- Breadcrumb -->
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
			  <ol class="breadcrumb">
				<li><a href="{{ route('client.dashboard') }}">Dashboard</a></li>
				<li><a href="#"><span>Farms</span></a></li>
				<li class="active"><span>View</span></li>
			  </ol>
			</div>
			<!-- /Breadcrumb -->
		</div>
		<!-- /Title -->
		
		<!-- Row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div class="table-wrap">
								<div class="table-responsive">
									<table id="example" class="table table-hover display  pb-30" >
										<thead>
											<tr>
												<th>SL</th>
												<th>Farm Name</th>
												<th>Map</th>
												<th>Farm Manager</th>
												<th>License Number</th>
												<th>Area (In acre)</th>
												<th>District</th>
												<th>Upazila</th>
												<th>Union</th>
												<th>Status</th>
											</tr>
										</thead>
										
										<tbody>
											<?php $sl = 1; ?>
											@if(count($farms)>0)
											@foreach($farms as $value)
												<tr>
													<td>{{ $sl }}</td>
													<td>{{ $value->name }}</td>
													<td>
														<a target="_blank" href="https://www.google.com/maps/d/u/0/viewer?ll=21.127566458104123%2C92.15467749999993&z=11&mid=1M17MO6eCTuYJj1ESK8iRiWoeryAzwfiI">See the Google Map</a>
													</td>
													<td>{{ $value->farm_manager->name }}</td>
													<td>{{ $value->license_number }}</td>
													<td>{{ $value->area }}</td>
													<td>{{ $value->district->name }}</td>
													<td>{{ $value->upazila->name }}</td>
													<td>{{ $value->union->name }}</td>
													<td>
														@if($value->status==1)
															Active
														@else
															In-active
														@endif
													</td>
												</tr>
												<?php $sl++; ?>
											@endforeach
											@else
												<tr class="warning"><td colspan="10" align="center">No information found</td></tr>
											@endif
										</tbody>
										<tfoot>
											<tr>
												<th>SL</th>
												<th>Farm Name</th>
												<th>Map</th>
												<th>Farm Manager</th>
												<th>License Number</th>
												<th>Area (In acre)</th>
												<th>District</th>
												<th>Upazila</th>
												<th>Union</th>
												<th>Status</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<!-- /Row -->
	</div>
@endsection