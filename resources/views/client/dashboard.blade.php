@extends('layouts.shrimp_culture.client.master')
@section('main-content')
    <div class="container-fluid pt-25">
        <!-- Row -->
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-darks block counter"><span class="counter-anim">{{$hatcheries}}</span></span>
                                            <span class="weight-500 uppercase-font txt-darks block font-13">Hatcheries</span>
                                        </div>
                                        <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                            <i class="icon-layers data-right-rep-icon txt-light-grey"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-darks block counter"><span class="counter-anim">{{$clusters}}</span></span>
                                            <span class="weight-500 uppercase-font txt-darks block">Clusters</span>
                                        </div>
                                        <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                            <i class="icon-layers data-right-rep-icon txt-light-grey"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-darks block counter"><span class="counter-anim">{{$cluster_ponds}}</span></span>
                                            <span class="weight-500 uppercase-font txt-darks block">Cluster Ponds</span>
                                        </div>
                                        <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                            <i class="icon-layers data-right-rep-icon txt-light-grey"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-darks block counter"><span class="counter-anim">{{$farms}}</span></span>
                                            <span class="weight-500 uppercase-font txt-darks block">Semi-Intensive  Farms</span>
                                        </div>
                                        <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                            <i class="icon-layers data-right-rep-icon txt-light-grey"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-default card-view pa-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="txt-darks block counter"><span class="counter-anim">{{$si_ponds}}</span></span>
                                            <span class="weight-500 uppercase-font txt-darks block">Semi-Intensive Ponds</span>
                                        </div>
                                        <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                            <i class="icon-layers data-right-rep-icon txt-light-grey"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- Main Content -->
        <div class="container-fluid">
            <!-- Row -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="panel panel-default card-view panel-refresh">
                            <div class="refresh-container">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="col-md-3">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-dark">Pond Details</h6>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <select class="form-control">
                                        <option value="">Choose Pond</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <div class="pull-right">
                                        <a href="#" class="pull-left inline-block refresh">
                                            <i class="zmdi zmdi-replay"></i>
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div id="dashboardChart" class="" style="height:294px;"></div>
                                    <div class="label-chatrs mt-15">
                                        <div class="inline-block mr-15">
                                            <span class="clabels inline-block bg-blue mr-5"></span>
                                            <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond</span>
                                        </div>
                                        <div class="inline-block">
                                            <span class="clabels inline-block bg-light-blue mr-5"></span>
                                            <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Feed</span>
                                        </div>                                  
                                    </div>
                                </div>  
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                if( $('#dashboardChart').length > 0 ){
                    var eChart_3 = echarts.init(document.getElementById('dashboardChart'));
                    var colors = ['#0FC5BB ', '#92F2EF'];
                    var option2 = {
                        color: colors,

                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(33,33,33,1)',
                            borderRadius:0,
                            padding:10,
                            axisPointer: {
                                type: 'cross',
                                label: {
                                    backgroundColor: 'rgba(33,33,33,1)'
                                }
                            },
                            textStyle: {
                                color: '#fff',
                                fontStyle: 'normal',
                                fontWeight: 'normal',
                                fontFamily: "'Open Sans', sans-serif",
                                fontSize: 12
                            }   
                        },
                        grid:{
                            show:false,
                            top: 30,
                            bottom: 10,
                            containLabel: true,
                        },
                        xAxis: [
                            {
                                type: 'category',
                                axisTick: {
                                    alignWithLabel: true
                                },
                                axisLine: {
                                    show:false
                                },
                                axisLabel: {
                                    textStyle: {
                                        color: '#878787'
                                    }
                                },
                                axisPointer: {
                                    label: {
                                        formatter: function (params) {
                                            return params.value
                                                + (params.seriesData.length ? '：' + params.seriesData[0].data : '');
                                        }
                                    }
                                },
                                data: ["2016-1", "2016-2", "2016-3", "2016-4", "2016-5", "2016-6", "2016-7", "2016-8", "2016-9", "2016-10", "2016-11", "2016-12"]
                            },
                            {
                                type: 'category',
                                axisTick: {
                                    alignWithLabel: true
                                },
                                axisLine: {
                                    show:false
                                },
                                axisLabel: {
                                    textStyle: {
                                        color: '#878787'
                                    }
                                },
                                axisPointer: {
                                    label: {
                                        formatter: function (params) {
                                            return  params.value
                                                + (params.seriesData.length ? '：' + params.seriesData[0].data : '');
                                        }
                                    }
                                },
                                data: ["2015-1", "2015-2", "2015-3", "2015-4", "2015-5", "2015-6", "2015-7", "2015-8", "2015-9", "2015-10", "2015-11", "2015-12"]
                            }
                        ],
                        yAxis: [
                            {
                                type: 'value',
                                axisLine: {
                                    show:false
                                },
                                axisLabel: {
                                    textStyle: {
                                        color: '#878787'
                                    }
                                },
                                splitLine: {
                                    show: false,
                                }
                            }
                        ],
                        series: [
                            {
                                name:'2015',
                                type:'line',
                                xAxisIndex: 1,
                                smooth: true,
                                data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
                            },
                            {
                                name:'2016',
                                type:'line',
                                smooth: true,
                                data: [3.9, 5.9, 11.1, 18.7, 48.3, 69.2, 231.6, 46.6, 55.4, 18.4, 10.3, 0.7]
                            }
                        ]
                    };

                    eChart_3.setOption(option2);
                    eChart_3.resize();
                }
            })
        </script>
        <!-- /Main Content -->
        {{--<div class="row">--}}
            {{--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">--}}
                {{--<div class="panel panel-default card-view">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<div class="pull-left">--}}
                            {{--<h6 class="panel-title txt-darks">Yearly Revenue</h6>--}}
                        {{--</div>--}}
                        {{--<div class="pull-right">--}}
                            {{--<div class="pull-left form-group mb-0 sm-bootstrap-select mr-15">--}}
                                {{--<select class="selectpicker" data-style="form-control">--}}
                                    {{--<option selected value='1'>Janaury</option>--}}
                                    {{--<option value='2'>February</option>--}}
                                    {{--<option value='3'>March</option>--}}
                                    {{--<option value='4'>April</option>--}}
                                    {{--<option value='5'>May</option>--}}
                                    {{--<option value='6'>June</option>--}}
                                    {{--<option value='7'>July</option>--}}
                                    {{--<option value='8'>August</option>--}}
                                    {{--<option value='9'>September</option>--}}
                                    {{--<option value='10'>October</option>--}}
                                    {{--<option value='11'>November</option>--}}
                                    {{--<option value='12'>December</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            {{--<a href="#" class="pull-left inline-block full-screen">--}}
                                {{--<i class="zmdi zmdi-fullscreen"></i>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-wrapper collapse in">--}}
                        {{--<div class="panel-body">--}}
                            {{--<ul class="flex-stat mb-10 ml-15">--}}
                                {{--<li class="text-left auto-width mr-60">--}}
                                    {{--<span class="block">Last Year</span>--}}
                                    {{--<span class="block txt-darks weight-500 font-18"><span class="counter-anim">3,24,222</span></span>--}}
                                    {{--<span class="block txt-success mt-5">--}}
										{{--<i class="zmdi zmdi-caret-up pr-5 font-20"></i><span class="weight-500">+15%</span>--}}
									{{--</span>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</li>--}}
                                {{--<li class="text-left auto-width mr-60">--}}
                                    {{--<span class="block">This Year</span>--}}
                                    {{--<span class="block txt-darks weight-500 font-18"><span class="counter-anim">1,23,432</span></span>--}}
                                    {{--<span class="block txt-success mt-5">--}}
										{{--<i class="zmdi zmdi-caret-up pr-5 font-20"></i><span class="weight-500">+4%</span>--}}
									{{--</span>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</li>--}}
                                {{--<li class="text-left auto-width">--}}
                                    {{--<span class="block">Revenue</span>--}}
                                    {{--<span class="block txt-darks weight-500 font-18">$<span class="counter-anim">324,222</span></span>--}}
                                    {{--<span class="block txt-danger mt-5">--}}
										{{--<i class="zmdi zmdi-caret-down pr-5 font-20"></i><span class="weight-500">-5%</span>--}}
									{{--</span>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                            {{--<div id="e_chart_1" class="" style="height:280px;"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">--}}
                {{--<div class="panel panel-default card-view">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<div class="pull-left">--}}
                            {{--<h6 class="panel-title txt-darks">sales analytics</h6>--}}
                        {{--</div>--}}
                        {{--<div class="pull-right">--}}
                            {{--<div class="pull-left form-group mb-0 sm-bootstrap-select mr-15">--}}
                                {{--<select class="selectpicker" data-style="form-control">--}}
                                    {{--<option selected value='1'>Janaury</option>--}}
                                    {{--<option value='2'>February</option>--}}
                                    {{--<option value='3'>March</option>--}}
                                    {{--<option value='4'>April</option>--}}
                                    {{--<option value='5'>May</option>--}}
                                    {{--<option value='6'>June</option>--}}
                                    {{--<option value='7'>July</option>--}}
                                    {{--<option value='8'>August</option>--}}
                                    {{--<option value='9'>September</option>--}}
                                    {{--<option value='10'>October</option>--}}
                                    {{--<option value='11'>November</option>--}}
                                    {{--<option value='12'>December</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            {{--<a href="#" class="pull-left inline-block full-screen">--}}
                                {{--<i class="zmdi zmdi-fullscreen"></i>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-wrapper collapse in">--}}
                        {{--<div class="panel-body">--}}
                            {{--<ul class="flex-stat mb-10 ml-15">--}}
                                {{--<li class="text-left auto-width mr-60">--}}
                                    {{--<span class="block">Traffic</span>--}}
                                    {{--<span class="block txt-darks weight-500 font-18"><span class="counter-anim">3,24,222</span></span>--}}
                                    {{--<span class="block txt-success mt-5">--}}
										{{--<i class="zmdi zmdi-caret-up pr-5 font-20"></i><span class="weight-500">+15%</span>--}}
									{{--</span>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</li>--}}
                                {{--<li class="text-left auto-width mr-60">--}}
                                    {{--<span class="block">Orders</span>--}}
                                    {{--<span class="block txt-darks weight-500 font-18"><span class="counter-anim">1,23,432</span></span>--}}
                                    {{--<span class="block txt-success mt-5">--}}
										{{--<i class="zmdi zmdi-caret-up pr-5 font-20"></i><span class="weight-500">+4%</span>--}}
									{{--</span>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</li>--}}
                                {{--<li class="text-left auto-width">--}}
                                    {{--<span class="block">Revenue</span>--}}
                                    {{--<span class="block txt-darks weight-500 font-18">$<span class="counter-anim">324,222</span></span>--}}
                                    {{--<span class="block txt-danger mt-5">--}}
										{{--<i class="zmdi zmdi-caret-down pr-5 font-20"></i><span class="weight-500">-5%</span>--}}
									{{--</span>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                            {{--<div id="e_chart_sales" class="" style="height:280px;"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-md-12 col-xs-12 col-sm-12">--}}
                        {{--<div class="panel panel-default card-view pt-0">--}}
                            {{--<div class="panel-wrapper collapse in">--}}
                                {{--<div class="panel-body pa-0">--}}
                                    {{--<div class="sm-data-box bg-white">--}}
                                        {{--<div class="container-fluid">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">--}}
                                                    {{--<span class="txt-darks block counter"><span class="counter-anim">46.41</span>%</span>--}}
                                                    {{--<span class="block"><span class="weight-500 uppercase-font txt-grey font-13">population</span><i class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i></span>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-xs-6 text-left  pl-0 pr-0 pt-25 data-wrap-right">--}}
                                                    {{--<div id="sparkline_5" class="sp-small-chart" ></div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-md-12 col-xs-12 col-sm-12">--}}
                        {{--<div class="panel card-view">--}}
                            {{--<div class="panel-heading small-panel-heading relative">--}}
                                {{--<div class="pull-left">--}}
                                    {{--<h6 class="panel-title">BSFF Revenue</h6>--}}
                                {{--</div>--}}
                                {{--<div class="clearfix"></div>--}}
                                {{--<div class="head-overlay"></div>--}}
                            {{--</div>--}}
                            {{--<div class="panel-wrapper collapse in">--}}
                                {{--<div class="panel-body row pa-0">--}}
                                    {{--<div class="sm-data-box">--}}
                                        {{--<div class="container-fluid">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">--}}
                                                    {{--<span class="block"><i class="zmdi zmdi-trending-up txt-success font-18 mr-5"></i><span class="weight-500 uppercase-font">growth</span></span>--}}
                                                    {{--<span class="txt-darks block counter">$<span class="counter-anim">15,678</span></span>--}}
                                                {{--</div>--}}
                                                {{--<div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">--}}
                                                    {{--<div id="sparkline_1" class="sp-small-chart" ></div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">--}}
                {{--<div class="panel panel-default card-view">--}}
                    {{--<div class="panel-wrapper collapse in">--}}
                        {{--<div class="panel-body sm-data-box-1">--}}
                            {{--<span class="uppercase-font weight-500 font-14 block text-center txt-darks">success index this year</span>--}}
                            {{--<div class="cus-sat-stat weight-500 txt-primary text-center mt-5">--}}
                                {{--<span class="counter-anim">40.13</span><span>%</span>--}}
                            {{--</div>--}}
                            {{--<div class="progress-anim mt-20">--}}
                                {{--<div class="progress">--}}
                                    {{--<div class="progress-bar progress-bar-primary wow animated progress-animated" role="progressbar" aria-valuenow="40.13" aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<ul class="flex-stat mt-5">--}}
                                {{--<li class="half-width">--}}
                                    {{--<span class="block">joined shcool</span>--}}
                                    {{--<span class="block txt-darks weight-500 font-15">--}}
										{{--<i class="zmdi zmdi-trending-up txt-success font-20 mr-10"></i>52--}}
									{{--</span>--}}
                                {{--</li>--}}
                                {{--<li class="half-width">--}}
                                    {{--<span class="block">dropped shcool</span>--}}
                                    {{--<span class="block txt-darks weight-500 font-15">+14.29</span>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- /Row -->--}}

        {{--<!-- Row -->--}}
        {{--<div class="row">--}}
            {{--<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">--}}
                {{--<div class="panel panel-default card-view">--}}
                    {{--<div class="panel-wrapper collapse in">--}}
                        {{--<div class="panel-body">--}}
                            {{--<div class="calendar-wrap">--}}
                                {{--<div id="calendar_small" class="small-calendar"></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">--}}
                {{--<div class="panel panel-default card-view panel-refresh">--}}
                    {{--<div class="refresh-container">--}}
                        {{--<div class="la-anim-1"></div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-heading">--}}
                        {{--<div class="pull-left">--}}
                            {{--<h6 class="panel-title txt-darks">Departmental Patients</h6>--}}
                        {{--</div>--}}
                        {{--<div class="pull-right">--}}
                            {{--<a href="#" class="pull-left inline-block refresh mr-15">--}}
                                {{--<i class="zmdi zmdi-replay"></i>--}}
                            {{--</a>--}}
                            {{--<a href="#" class="pull-left inline-block full-screen mr-15">--}}
                                {{--<i class="zmdi zmdi-fullscreen"></i>--}}
                            {{--</a>--}}
                            {{--<div class="pull-left inline-block dropdown">--}}
                                {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="zmdi zmdi-more-vert"></i></a>--}}
                                {{--<ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">--}}
                                    {{--<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Edit</a></li>--}}
                                    {{--<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i>Delete</a></li>--}}
                                    {{--<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-trash" aria-hidden="true"></i>New</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-wrapper collapse in">--}}
                        {{--<div class="panel-body">--}}
                            {{--<div id="e_chart_3" class="" style="height:346px;"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">--}}
                {{--<div class="panel panel-default card-view panel-refresh">--}}
                    {{--<div class="refresh-container">--}}
                        {{--<div class="la-anim-1"></div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-heading">--}}
                        {{--<div class="pull-left">--}}
                            {{--<h6 class="panel-title txt-darks">General Appoinments</h6>--}}
                        {{--</div>--}}
                        {{--<div class="pull-right">--}}
                            {{--<a href="#" class="pull-left inline-block refresh mr-15">--}}
                                {{--<i class="zmdi zmdi-replay"></i>--}}
                            {{--</a>--}}
                            {{--<div class="pull-left inline-block dropdown mr-15">--}}
                                {{--<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="zmdi zmdi-more-vert"></i></a>--}}
                                {{--<ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">--}}
                                    {{--<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Devices</a></li>--}}
                                    {{--<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-share" aria-hidden="true"></i>General</a></li>--}}
                                    {{--<li role="presentation"><a href="javascript:void(0)" role="menuitem"><i class="icon wb-trash" aria-hidden="true"></i>Referral</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<a class="pull-left inline-block close-panel" href="#" data-effect="fadeOut">--}}
                                {{--<i class="zmdi zmdi-close"></i>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-wrapper collapse in">--}}
                        {{--<div class="panel-body">--}}
                            {{--<div id="e_chart_2" class="" style="height:346px;"></div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">--}}
                {{--<div class="panel panel-default card-view panel-refresh">--}}
                    {{--<div class="refresh-container">--}}
                        {{--<div class="la-anim-1"></div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-heading">--}}
                        {{--<div class="pull-left">--}}
                            {{--<h6 class="panel-title txt-darks">Visit by Traffic Types</h6>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-wrapper collapse in">--}}
                        {{--<div class="panel-body">--}}
                            {{--<div>--}}
                                {{--<canvas id="chart_6" height="191"></canvas>--}}
                            {{--</div>--}}
                            {{--<hr class="light-grey-hr row mt-10 mb-15"/>--}}
                            {{--<div class="label-chatrs">--}}
                                {{--<div class="">--}}
                                    {{--<span class="clabels clabels-lg inline-block bg-blue mr-10 pull-left"></span>--}}
                                    {{--<span class="clabels-text font-12 inline-block txt-darks capitalize-font pull-left"><span class="block font-15 weight-500 mb-5">44.46% organic</span><span class="block txt-grey">356 visits</span></span>--}}
                                    {{--<div id="sparkline_organic" class="sp-small-chart pull-right"  ></div>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<hr class="light-grey-hr row mt-10 mb-15"/>--}}
                            {{--<div class="label-chatrs">--}}
                                {{--<div class="">--}}
                                    {{--<span class="clabels clabels-lg inline-block bg-green mr-10 pull-left"></span>--}}
                                    {{--<span class="clabels-text font-12 inline-block txt-darks capitalize-font pull-left"><span class="block font-15 weight-500 mb-5">5.54% Refrral</span><span class="block txt-grey">36 visits</span></span>--}}
                                    {{--<div id="sparkline_2" class="sp-small-chart pull-right" ></div>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<hr class="light-grey-hr row mt-10 mb-15"/>--}}
                            {{--<div class="label-chatrs">--}}
                                {{--<div class="">--}}
                                    {{--<span class="clabels clabels-lg inline-block bg-yellow mr-10 pull-left"></span>--}}
                                    {{--<span class="clabels-text font-12 inline-block txt-darks capitalize-font pull-left"><span class="block font-15 weight-500 mb-5">50% Refrral</span><span class="block txt-grey">36 visits</span></span>--}}
                                    {{--<div id="sparkline_3" class="sp-small-chart pull-right" ></div>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<!-- <div class="col-lg-4 col-md-6 col-xs-12">--}}
                {{--<div class="panel panel-default border-panel card-view">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<div class="pull-left">--}}
                            {{--<h6 class="panel-title txt-darks">recent activity</h6>--}}
                        {{--</div>--}}
                        {{--<a class="txt-danger pull-right right-float-sub-text inline-block" href="javascript:void(0)"> clear all </a>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-wrapper task-panel collapse in">--}}
                        {{--<div class="panel-body row pa-0">--}}
                            {{--<div class="list-group mb-0">--}}
                                {{--<a href="#" class="list-group-item">--}}
                                    {{--<span class="badge transparent-badge badge-info capitalize-font">just now</span>--}}
                                    {{--<i class="zmdi zmdi-calendar-check pull-left"></i><p class="pull-left">Calendar updated</p>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</a>--}}
                                {{--<a href="#" class="list-group-item">--}}
                                    {{--<span class="badge transparent-badge badge-success capitalize-font">4 min</span>--}}
                                    {{--<i class="zmdi zmdi-comment-alert pull-left"></i><p class=" pull-left">Commented on a post</p>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</a>--}}
                                {{--<a href="#" class="list-group-item">--}}
                                    {{--<span class="badge transparent-badge badge-warning capitalize-font">23 min </span>--}}
                                    {{--<i class="zmdi zmdi-truck pull-left"></i><p class=" pull-left">Order 392 shipped</p>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</a>--}}
                                {{--<a href="#" class="list-group-item">--}}
                                    {{--<span class="badge transparent-badge badge-success capitalize-font">46 min</span>--}}
                                    {{--<i class="zmdi zmdi-money pull-left"></i><p class=" pull-left">Invoice 653 has been paid</p>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</a>--}}
                                {{--<a href="#" class="list-group-item">--}}
                                    {{--<span class="badge transparent-badge badge-danger capitalize-font">1 hr</span>--}}
                                    {{--<i class="zmdi zmdi-account pull-left"></i><p class="pull-left">A new user has been added</p>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</a>--}}
                                {{--<a href="#" class="list-group-item">--}}
                                    {{--<span class="badge transparent-badge badge-warning capitalize-font">just now</span>--}}
                                    {{--<i class="zmdi zmdi-picture-in-picture pull-left"></i><p class=" pull-left">Finance report has been released</p>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</a>--}}
                                {{--<a href="#" class="list-group-item">--}}
                                    {{--<span class="badge transparent-badge badge-success capitalize-font">1 hr</span>--}}
                                    {{--<i class="zmdi zmdi-device-hub pull-left"></i><p class="pull-left">Web server hardware updated</p>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</a>--}}
                                {{--<a href="#" class="list-group-item">--}}
                                    {{--<span class="badge transparent-badge badge-success capitalize-font">1 hr</span>--}}
                                    {{--<i class="zmdi zmdi-device-hub pull-left"></i><p class="pull-left">Web server hardware updated</p>--}}
                                    {{--<div class="clearfix"></div>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div> -->--}}

            {{--<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">--}}
                {{--<div class="panel panel-default card-view">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<div class="pull-left">--}}
                            {{--<h6 class="panel-title txt-darks">customer support</h6>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="panel-wrapper collapse in">--}}
                        {{--<div class="panel-body row pa-0">--}}
                            {{--<div class="table-wrap">--}}
                                {{--<div class="table-responsive">--}}
                                    {{--<table class="table display product-overview border-none" id="support_table">--}}
                                        {{--<thead>--}}
                                        {{--<tr>--}}
                                            {{--<th>ticket ID</th>--}}
                                            {{--<th>Customer</th>--}}
                                            {{--<th>issue</th>--}}
                                            {{--<th>Date</th>--}}
                                            {{--<th>Status</th>--}}
                                            {{--<th>Actions</th>--}}
                                        {{--</tr>--}}
                                        {{--</thead>--}}
                                        {{--<tbody>--}}
                                        {{--<tr>--}}
                                            {{--<td>#85457895</td>--}}
                                            {{--<td>David Perry</td>--}}
                                            {{--<td>Felix PSD</td>--}}
                                            {{--<td>Oct 25</td>--}}
                                            {{--<td>--}}
                                                {{--<span class="label label-danger">pending</span>--}}
                                            {{--</td>--}}
                                            {{--<td><a href="javascript:void(0)" class="pr-10" data-toggle="tooltip" title="completed" ><i class="zmdi zmdi-check"></i></a> <a href="javascript:void(0)" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i></a></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>#85457896</td>--}}
                                            {{--<td>Anthony Davie</td>--}}
                                            {{--<td>Cinnabar</td>--}}
                                            {{--<td>Oct 25</td>--}}
                                            {{--<td>--}}
                                                {{--<span class="label label-success ">done</span>--}}
                                            {{--</td>--}}
                                            {{--<td><a href="javascript:void(0)" class="pr-10" data-toggle="tooltip" title="completed" ><i class="zmdi zmdi-check"></i></a> <a href="javascript:void(0)" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i></a></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>#85457894</td>--}}
                                            {{--<td>Anthony Davie</td>--}}
                                            {{--<td>Beryl iphone</td>--}}
                                            {{--<td>Oct 25</td>--}}
                                            {{--<td>--}}
                                                {{--<span class="label label-success ">done</span>--}}
                                            {{--</td>--}}
                                            {{--<td><a href="javascript:void(0)" class="pr-10" data-toggle="tooltip" title="completed" ><i class="zmdi zmdi-check"></i></a> <a href="javascript:void(0)" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i></a></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>#85457893</td>--}}
                                            {{--<td>Alan Gilchrist</td>--}}
                                            {{--<td>Pogody button</td>--}}
                                            {{--<td>Oct 22</td>--}}
                                            {{--<td>--}}
                                                {{--<span class="label label-warning ">Pending</span>--}}
                                            {{--</td>--}}
                                            {{--<td><a href="javascript:void(0)" class="pr-10" data-toggle="tooltip" title="completed" ><i class="zmdi zmdi-check"></i></a> <a href="javascript:void(0)" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i></a></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>#85457892</td>--}}
                                            {{--<td>Mark Hay</td>--}}
                                            {{--<td>Beavis sidebar</td>--}}
                                            {{--<td>Oct 18</td>--}}
                                            {{--<td>--}}
                                                {{--<span class="label label-success ">done</span>--}}
                                            {{--</td>--}}
                                            {{--<td><a href="javascript:void(0)" class="pr-10" data-toggle="tooltip" title="completed" ><i class="zmdi zmdi-check"></i></a> <a href="javascript:void(0)" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i></a></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>#85457891</td>--}}
                                            {{--<td>Sue Woodger</td>--}}
                                            {{--<td>Pogody header</td>--}}
                                            {{--<td>Oct 17</td>--}}
                                            {{--<td>--}}
                                                {{--<span class="label label-danger">pending</span>--}}
                                            {{--</td>--}}
                                            {{--<td><a href="javascript:void(0)" class="pr-10" data-toggle="tooltip" title="completed" ><i class="zmdi zmdi-check"></i></a> <a href="javascript:void(0)" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i></a></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td>#85457891</td>--}}
                                            {{--<td>Sue Woodger</td>--}}
                                            {{--<td>Pogody header</td>--}}
                                            {{--<td>Oct 17</td>--}}
                                            {{--<td>--}}
                                                {{--<span class="label label-danger">pending</span>--}}
                                            {{--</td>--}}
                                            {{--<td><a href="javascript:void(0)" class="pr-10" data-toggle="tooltip" title="completed" ><i class="zmdi zmdi-check"></i></a> <a href="javascript:void(0)" class="text-inverse" title="Delete" data-toggle="tooltip"><i class="zmdi zmdi-delete"></i></a></td>--}}
                                        {{--</tr>--}}
                                        {{--</tbody>--}}
                                    {{--</table>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- /Row -->--}}
    </div>
@endsection