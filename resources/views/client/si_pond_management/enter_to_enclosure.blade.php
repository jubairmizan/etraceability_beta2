@extends('layouts.client.master')
@section('main-content')
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
    <div class="container-fluid">
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">{{$title}}</h5>
            </div>
            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('client.dashboard') }}">Dashboard</a></li>
                    <li><a href="#"><span>Enter to Enclosure</span></a></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                    <table id="example" class="table table-hover display  pb-30" >
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Hatchery</th>
                                            <th>Farm</th>
                                            <th>Farm Manager</th>
                                            <th>Pond</th>
                                            <th>PL Quantity</th>
                                            <th>Price</th>
                                            <th>Created At</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php $sl = 1; ?>
                                        @if(count($enter_to_enclosure)>0)
                                            @foreach($enter_to_enclosure as $value)
                                                <tr>
                                                    <td>{{ $sl }}</td>
                                                    <td>{{ $value->hatchery->name }}</td>
                                                    <td>{{ $value->farm->name }}</td>
                                                    <td>{{ $value->farm_manager->name }}</td>
                                                    <td>
                                                        Name: {{ $value->ponds->name }}<br>
                                                        ID: {{ $value->ponds->pond_id }}
                                                    </td>
                                                    <td>{{ $value->pl_quantity }}</td>
                                                    <td>{{ $value->price }}</td>
                                                    <td>{{ date('Y, F d',strtotime($value->created_at)) }}</td>
                                                </tr>
                                                <?php $sl++; ?>
                                            @endforeach
                                        @else
                                            <tr class="warning"><td colspan="8" align="center">No information found</td></tr>
                                        @endif
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>SL</th>
                                            <th>Hatchery</th>
                                            <th>Farm</th>
                                            <th>Farm Manager</th>
                                            <th>Pond</th>
                                            <th>PL Quantity</th>
                                            <th>Price</th>
                                            <th>Created At</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
@endsection