@extends('layouts.client.master')
@section('main-content')
    <style>
        .nav-pills > li {
             margin-right: 0;
        }
    </style>
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
    <div class="container-fluid">
        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">{{$title}}</h5>
            </div>
            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('client.dashboard') }}">Dashboard</a></li>
                    <li><a href="#"><span>During Enclosure</span></a></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">During Enclosure Details</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="row">
                                {{ Form::open(['method'=>'get']) }}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('farm_id','Select Farm',['class'=>'control-label','style'=>'margin-bottom:5px']) }}
                                        {{ Form::select('farm_id',$farms,$farm_id,['class'=>'form-control','required','placeholder'=>'Select Farm','id'=>'farm_id']) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('pond_id','Select Pond',['class'=>'control-label','style'=>'margin-bottom:5px']) }}
                                        {{ Form::select('pond_id',$ponds,$pond_id,['class'=>'form-control','required','placeholder'=>'Select Pond','id'=>'pond_id']) }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        {{ Form::label('pond_id','Select Pond',['class'=>'control-label','style'=>'margin-bottom:5px;color:#212121']) }}
                                        {{ Form::submit('See the Information',['class'=>'form-control btn btn-primary']) }}
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                            <div  class="pills-struct mt-40">
                                <ul role="tablist" class="nav nav-pills" id="myTabs_9">
                                    <li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="pond_overview_tab" href="#pond_overview_content">Pond Overview</a></li>
                                    <li role="presentation" class=""><a  data-toggle="tab" id="feed_tab" role="tab" href="#feed_content" aria-expanded="false">Feeds</a></li>
                                    <li role="presentation" class=""><a  data-toggle="tab" id="probiotics_tab" role="tab" href="#probiotics_content" aria-expanded="false">Probiotics</a></li>
                                    <li role="presentation" class=""><a  data-toggle="tab" id="ph_tab" role="tab" href="#ph_content" aria-expanded="false">pH</a></li>
                                    <li role="presentation" class=""><a  data-toggle="tab" id="dissolved_oxygen_tab" role="tab" href="#dissolved_oxygen_content" aria-expanded="false">Dissolved Oxygen</a></li>
                                    <li role="presentation" class=""><a  data-toggle="tab" id="temperature_tab" role="tab" href="#temperature_content" aria-expanded="false">Temperature</a></li>
                                    <li role="presentation" class=""><a  data-toggle="tab" id="parameters_tab" role="tab" href="#parameters_content" aria-expanded="false">Other Parameters</a></li>
                                    <li role="presentation" class=""><a  data-toggle="tab" id="projection_tab" role="tab" href="#projection_content" aria-expanded="false">Projections</a></li>
                                </ul>
                                <div class="tab-content" id="myTabContent_9">
                                    <div  id="pond_overview_content" class="tab-pane fade active in" role="tabpanel">
                                        <div class="panel-wrapper collapse in">
                                            <div  class="panel-body">
                                                <div class="table-responsive mt-40">
                                                    <table class="table table-bordered table-striped">
                                                        <thead>
                                                        <tr style="margin: 0 auto">
                                                            <th colspan="5">Pond's Basic Information</th>
                                                        </tr>
                                                        <tr>
                                                            <th>Pond Name</th>
                                                            <td colspan="4">{{isset($pond_info->name)?$pond_info->name:''}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Google Map</th>
                                                            <td colspan="4"><a target="_blank" href="https://www.google.com/maps/d/u/0/viewer?ll=22.58255862904249%2C89.55279535&z=11&mid=1aa3XCs0zdV6HrZIRuiLHliNCVDIqaew4">Click to see the Pond at Google Maps</a></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Pond ID</th>
                                                            <td colspan="4">{{isset($pond_info->pond_id)?$pond_info->pond_id:''}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Farm</th>
                                                            <td colspan="4">{{isset($pond_info->farm)?$pond_info->farm->name:''}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Farm Manager</th>
                                                            <td colspan="4">{{isset($pond_info->farm_manager)?$pond_info->farm_manager->name:''}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Farm Owner</th>
                                                            <td colspan="4">{{isset($pond_info->client)?$pond_info->client->name:''}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Area</th>
                                                            <td colspan="4">{{isset($pond_info->area)?$pond_info->area.' Feet':''}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Type of Soil</th>
                                                            <td colspan="4">{{isset($pond_info->type_of_soil)?$pond_info->type_of_soil:''}}</td>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="feed_content" class="tab-pane fade" role="tabpanel">
                                        <div class="table-wrap">
                                            <div class="table-responsive">
                                                <table id="feed_datatable" class="table table-hover display pb-30" >
                                                    <thead>
                                                    <tr>
                                                        <th>Feed Name</th>
                                                        <th>Timing</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($during_enclosure)>0)
                                                    @foreach($during_enclosure as $de)
                                                        @foreach($de->feed as $feed)
                                                            <tr>
                                                                <td>{{$feed->feed->name}}</td>
                                                                <td>{{$feed->timing}}</td>
                                                                <td>{{$feed->quantity}}</td>
                                                                <td>{{$feed->created_at}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Feed Name</th>
                                                        <th>Timing</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="probiotics_content" class="tab-pane fade " role="tabpanel">
                                        <div class="table-wrap">
                                            <div class="table-responsive">
                                                <table id="probiotic_datatable" class="table table-hover display pb-30" >
                                                    <thead>
                                                    <tr>
                                                        <th>Probiotic Name</th>
                                                        <th>Timing</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($during_enclosure)>0)
                                                    @foreach($during_enclosure as $de)
                                                        @foreach($de->remark as $remark)
                                                            <tr>
                                                                <td>{{$remark->remark->name}}</td>
                                                                <td>{{$remark->timing}}</td>
                                                                <td>{{$remark->quantity}}</td>
                                                                <td>{{$remark->created_at}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Probiotic Name</th>
                                                        <th>Timing</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="ph_content" class="tab-pane fade" role="tabpanel">
                                        <div class="table-wrap">
                                            <div class="table-responsive">
                                                <table id="ph_datatable" class="table table-hover display pb-30" >
                                                    <thead>
                                                    <tr>
                                                        <th>Timing</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                    @if(count($during_enclosure)>0)
                                                    @foreach($during_enclosure as $de)
                                                        @foreach($de->ph as $ph)
                                                            <tr>
                                                                <td>{{$ph->timing}}</td>
                                                                <td>{{$ph->quantity}}</td>
                                                                <td>{{$ph->created_at}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Timing</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="dissolved_oxygen_content" class="tab-pane fade" role="tabpanel">
                                        <div class="table-wrap">
                                            <div class="table-responsive">
                                                <table id="dissolved_oxygen_datatable" class="table table-hover display pb-30" >
                                                    <thead>
                                                    <tr>
                                                        <th>Timing</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                    @if(count($during_enclosure)>0)
                                                    @foreach($during_enclosure as $de)
                                                        @foreach($de->dissolved_oxygen as $dissolved_oxygen)
                                                            <tr>
                                                                <td>{{$dissolved_oxygen->timing}}</td>
                                                                <td>{{$dissolved_oxygen->quantity}}</td>
                                                                <td>{{$dissolved_oxygen->created_at}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Timing</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="temperature_content" class="tab-pane fade" role="tabpanel">
                                        <div class="table-wrap">
                                            <div class="table-responsive">
                                                <table id="temperature_oxygen_datatable" class="table table-hover display pb-30" >
                                                    <thead>
                                                    <tr>
                                                        <th>Timing</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </thead>

                                                    <tbody>
                                                    @if(count($during_enclosure)>0)
                                                    @foreach($during_enclosure as $de)
                                                        @foreach($de->temperature as $temperature)
                                                            <tr>
                                                                <td>{{$temperature->timing}}</td>
                                                                <td>{{$temperature->quantity}}</td>
                                                                <td>{{$temperature->created_at}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Timing</th>
                                                        <th>Quantity</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="parameters_content" class="tab-pane fade" role="tabpanel">
                                        <div class="table-wrap">
                                            <div class="table-responsive">
                                                <table id="parameter_datatable" class="table table-hover display pb-30" >
                                                    <thead>
                                                    <tr>
                                                        <th>HCO3</th>
                                                        <th>CO3</th>
                                                        <th>NH3</th>
                                                        <th>NO3</th>
                                                        <th>Salinity</th>
                                                        <th>Transparency</th>
                                                        <th>Plankton Type</th>
                                                        <th>Green</th>
                                                        <th>Yellow</th>
                                                        <th>Vibrio Count</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($during_enclosure)>0)
                                                    @foreach($during_enclosure as $de)
                                                        @foreach($de->water_parameter as $water_parameter)
                                                            <tr>
                                                                <td>{{$water_parameter->hco3}}</td>
                                                                <td>{{$water_parameter->co3}}</td>
                                                                <td>{{$water_parameter->nh3}}</td>
                                                                <td>{{$water_parameter->n03}}</td>
                                                                <td>{{$water_parameter->salinity}}</td>
                                                                <td>{{$water_parameter->transparency}}</td>
                                                                <td>{{$water_parameter->plankton_type}}</td>
                                                                <td>{{$water_parameter->green}}</td>
                                                                <td>{{$water_parameter->yellow}}</td>
                                                                <td>{{$water_parameter->vibrio_count}}</td>
                                                                <td>{{$water_parameter->created_at}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                    @endif
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>HCO3</th>
                                                        <th>CO3</th>
                                                        <th>NH3</th>
                                                        <th>NO3</th>
                                                        <th>Salinity</th>
                                                        <th>Transparency</th>
                                                        <th>Plankton Type</th>
                                                        <th>Green</th>
                                                        <th>Yellow</th>
                                                        <th>Vibrio Count</th>
                                                        <th>Date</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="projection_content" class="tab-pane fade" role="tabpanel">
                                        <p>No information found</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->
    </div>
@endsection