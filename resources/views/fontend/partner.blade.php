@extends('layouts.fontend')

@section('title', 'Partners')

@section('stylesheet')

{!! Html::style('fontend/vendor/bootstrap/css/bootstrap.css') !!}

<!-- <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"> -->
{!! Html::style('fontend/vendor/font-awesome/css/font-awesome.css') !!}

<!-- <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.css"> -->
{!! Html::style('fontend/vendor/simple-line-icons/css/simple-line-icons.css') !!}

<!-- <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.css"> -->
{!! Html::style('fontend/vendor/owl.carousel/assets/owl.carousel.min.css') !!}

<!-- <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css"> -->
{!! Html::style('fontend/vendor/owl.carousel/assets/owl.theme.default.min.css') !!}

<!-- <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css"> -->
{!! Html::style('fontend/vendor/magnific-popup/magnific-popup.css') !!}

<!-- <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.css"> -->

<!-- Theme CSS -->
<!-- <link rel="stylesheet" href="css/theme.css"> -->
{!! Html::style('fontend/css/theme.css') !!}

<!-- <link rel="stylesheet" href="css/theme-elements.css"> -->
{!! Html::style('fontend/css/theme-elements.css') !!}

<!-- <link rel="stylesheet" href="css/theme-blog.css"> -->
{!! Html::style('fontend/css/theme-blog.css') !!}

<!-- <link rel="stylesheet" href="css/theme-shop.css"> -->
{!! Html::style('fontend/css/theme-shop.css') !!}

<!-- <link rel="stylesheet" href="css/theme-animate.css"> -->
{!! Html::style('fontend/css/theme-animate.css') !!}

<!-- Skin CSS -->
{!! Html::style('fontend/css/skins/default.css') !!}

<!-- <link rel="stylesheet" href="css/skins/default.css"> -->

<!-- Theme Custom CSS -->
{!! Html::style('fontend/css/custom.css') !!}

{!! Html::style('/fontend/css/skins/skin-corporate-4.css') !!}

<!-- <link rel="stylesheet" href="css/custom.css"> -->

<!-- Head Libs -->
{!! Html::script('fontend/vendor/modernizr/modernizr.js') !!}

<!-- <script src="vendor/modernizr/modernizr.js"></script> -->

@endsection

@section('content')
<div role="main" class="main">

	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li class="active">Partners</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Partners</h1>
				</div>
			</div>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="feature-box feature-box-style-2">
					<div class="feature-box-icon">
						<i class="fa fa-group"></i>
					</div>
					<div class="feature-box-info">
						<h2 class="mb-none" style="color: #5BC8F2">BSFF</h2>
						<p class="tall" style="color: #fff">BSFF has a mission especially scripted to facilitate realization of its broad

							objective of promoting growth and development of the fisheries sector and shrimp

							industries in Bangladesh which is supportive of inclusive growth, income and

							employment generation, trade and compliance with relevant international norms,

							standards and social, economic and environmental sustainability imperatives.

							An area of major interest of BSFF is to facilitate introduction of Good Aquaculture

							Practices (GAqP), safe seed and feed as well as Better Management Practices

							(BMP). Effectively introducing Codes of Conducts in all segments of the value

							chain in the sector is another BSFF priority. So the objective to improve the quality

							of the final product from the sector. Ensuring compliance with the environmental

							and social performance of the industry is also an important area of interest and

							targeted activities of BSFF. Recent efforts to assess the economic, social and

							environmental impacts of the shrimp industry have greatly improved. The work in

							this sector is however still taking place without adequate coordination. BSFF is

							trying to overcome some of these problems.</p>
						</div>
					</div>

				</div>

				{{-- <div class="col-md-12">
					<div class="feature-box feature-box-style-2">
						<div class="feature-box-icon">
							<i class="fa fa-group"></i>
						</div>
						<div class="feature-box-info">
							<h2 class="mb-none" style="color: #5BC8F2">Solidaridad</h2>
							<p class="tall" style="color: #fff">Solidaridad South and South-East Asia provides scalable and economically

								effective sustainability solutions in agriculture and mining sectors in collaboration

								with governments, businesses and the community. Solidaridad aims to drive

								sustainability from niche to norm in Asian markets and among Asian producers in

								a way that can sustain people, planet and profits. Solidaridad provides

								sustainability solutions at scale to large and small businesses, governments and

								producers across all critical sectors of the economy. Many businesses from across

								Asia are joining hands with us to make their supply chains more sustainable or for

								implementing their corporate social responsibility programmes. We want to

								combine our scale, influence and resources to make a real difference for change

								that matters.</p>
							</div>
						</div>

					</div>

				</div> --}}
			</div>

			<section class="section section-primary mb-none">
				<div class="container">
					<div class="row">
						<div class="counters counters-text-light">
							<div class="col-md-3 col-sm-6">
								<div class="counter">
									<strong data-to="19000" data-append="+">0</strong>
									<label>Happy Clients</label>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="counter">
									<strong data-to="15">0</strong>
									<label>Years in Business</label>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="counter">
									<strong data-to="352">0</strong>
									<label>Cups of Coffee</label>
								</div>
							</div>
							<div class="col-md-3 col-sm-6">
								<div class="counter">
									<strong data-to="178">0</strong>
									<label>High Score</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="video section section-text-light section-video section-center mt-none" data-video-path="./fontend/video/dark" data-plugin-video-background data-plugin-options='{"posterType": "jpg", "position": "50% 50%", "overlay": true}'>
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="owl-carousel owl-theme nav-bottom rounded-nav mt-lg mb-none" data-plugin-options='{"items": 1, "loop": false}'>
								<div>
									<div class="col-md-12">
										<div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
											<blockquote>
												<p>Hello, I want to thank you for creating a great template and for the excellent and quick support and help that you have been providing to me as I begin to work with it.</p>
											</blockquote>
											<div class="testimonial-author">
												<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
											</div>
										</div>
									</div>
								</div>
								<div>
									<div class="col-md-12">
										<div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
											<blockquote>
												<p>Just want to say Okler RULES. Provide great tech service for each template and allows me to become more knowledgeable as a designer.</p>
											</blockquote>
											<div class="testimonial-author">
												<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<div class="container">
				<div class="row mt-xlg">
					<div class="col-md-5">
						<h2><strong>Who</strong> We Are</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In eu libero ligula. </p>
					</div>
					<div class="col-md-7">
						<div class="progress-bars">
							<div class="progress-label">
								<span>HTML/CSS</span>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-primary" data-appear-progress-animation="100%">
									<span class="progress-bar-tooltip">100%</span>
								</div>
							</div>
							<div class="progress-label">
								<span>Design</span>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-primary" data-appear-progress-animation="85%" data-appear-animation-delay="300">
									<span class="progress-bar-tooltip">85%</span>
								</div>
							</div>
							<div class="progress-label">
								<span>WordPress</span>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-primary" data-appear-progress-animation="75%" data-appear-animation-delay="600">
									<span class="progress-bar-tooltip">75%</span>
								</div>
							</div>
							<div class="progress-label">
								<span>Photoshop</span>
							</div>
							<div class="progress">
								<div class="progress-bar progress-bar-primary" data-appear-progress-animation="85%" data-appear-animation-delay="900">
									<span class="progress-bar-tooltip">85%</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row mt-xlg mb-xlg">
					<div class="col-md-3 col-sm-6 col-xs-12">
						<span class="thumb-info thumb-info-hide-wrapper-bg">
							<span class="thumb-info-wrapper">
								<a href="about-me.html">
									<!-- <img src="img/team/team-1.jpg" class="img-responsive" alt=""> -->
									{!! Html::image('./fontend/img/team/team.team-1.jpg', 'team image', ['class' => 'img-responsive']) !!}
									<span class="thumb-info-title">
										<span class="thumb-info-inner">John Doe</span>
										<span class="thumb-info-type">CEO</span>
									</span>
								</a>
							</span>
							<span class="thumb-info-caption">
								<span class="thumb-info-caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan</span>
								<span class="thumb-info-social-icons">
									<a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i><span>Facebook</span></a>
									<a href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
									<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
								</span>
							</span>
						</span>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<span class="thumb-info thumb-info-hide-wrapper-bg">
							<span class="thumb-info-wrapper">
								<a href="about-me.html">
									{!! Html::image('./fontend/img/team/team-2.jpg', 'team image', ['class'=> 'img-responsive']) !!}
									<!-- <img src="img/team/team-2.jpg" class="img-responsive" alt=""> -->
									<span class="thumb-info-title">
										<span class="thumb-info-inner">Jessica Doe</span>
										<span class="thumb-info-type">Marketing</span>
									</span>
								</a>
							</span>
							<span class="thumb-info-caption">
								<span class="thumb-info-caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan</span>
								<span class="thumb-info-social-icons">
									<a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i><span>Facebook</span></a>
									<a href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
									<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
								</span>
							</span>
						</span>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<span class="thumb-info thumb-info-hide-wrapper-bg">
							<span class="thumb-info-wrapper">
								<a href="about-me.html">
									{!! Html::image('./fontend/img/team/team-3.jpg', 'team image', ['class'=> 'img-responsive']) !!}
									<!-- <img src="img/team/team-3.jpg" class="img-responsive" alt=""> -->
									<span class="thumb-info-title">
										<span class="thumb-info-inner">Rick Edward Doe</span>
										<span class="thumb-info-type">Developer</span>
									</span>
								</a>
							</span>
							<span class="thumb-info-caption">
								<span class="thumb-info-caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan</span>
								<span class="thumb-info-social-icons">
									<a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i><span>Facebook</span></a>
									<a href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
									<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
								</span>
							</span>
						</span>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<span class="thumb-info thumb-info-hide-wrapper-bg">
							<span class="thumb-info-wrapper">
								<a href="about-me.html">
									{!! Html::image('./fontend/img/team/team-4.jpg', 'team image', ['class'=> 'img-responsive']) !!}
									<!-- <img src="img/team/team-4.jpg" class="img-responsive" alt=""> -->
									<span class="thumb-info-title">
										<span class="thumb-info-inner">Melinda Wolosky</span>
										<span class="thumb-info-type">Design</span>
									</span>
								</a>
							</span>
							<span class="thumb-info-caption">
								<span class="thumb-info-caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan</span>
								<span class="thumb-info-social-icons">
									<a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i><span>Facebook</span></a>
									<a href="http://www.twitter.com"><i class="fa fa-twitter"></i><span>Twitter</span></a>
									<a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i><span>Linkedin</span></a>
								</span>
							</span>
						</span>
					</div>
				</div>
			</div>

			<section class="call-to-action call-to-action-default with-button-arrow call-to-action-in-footer">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="call-to-action-content">
								<h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website!</h3>
								<p>The <strong>#1 Selling</strong> HTML Site Template on ThemeForest</p>
							</div>
							<div class="call-to-action-btn">
								<a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary">Buy Now!</a><span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -12px;"></span>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		@endsection


		@section('script')
		{!! Html::script('/fontend/vendor/jquery/jquery.js') !!}
		<!-- <script src="vendor/jquery/jquery.js"></script> -->
		{!! Html::script('/fontend/vendor/jquery.appear/jquery.appear.js') !!}

		<!-- <script src="vendor/jquery.appear/jquery.appear.js"></script> -->
		{!! Html::script('/fontend/vendor/jquery.easing/jquery.easing.js') !!}

		<!-- <script src="vendor/jquery.easing/jquery.easing.js"></script> -->
		{!! Html::script('/fontend/vendor/jquery-cookie/jquery-cookie.js') !!}

		<!-- <script src="vendor/jquery-cookie/jquery-cookie.js"></script> -->
		{!! Html::script('/fontend/vendor/bootstrap/js/bootstrap.js') !!}

		<!-- <script src="vendor/bootstrap/js/bootstrap.js"></script> -->
		<!-- <script src="vendor/common/common.js"></script> -->
		{!! Html::script('/fontend/vendor/common/common.js') !!}

		{!! Html::script('/fontend/vendor/jquery.validation/jquery.validation.js') !!}

		<!-- <script src="vendor/jquery.validation/jquery.validation.js"></script> -->
		{!! Html::script('/fontend/vendor/jquery.stellar/jquery.stellar.js') !!}

		<!-- <script src="vendor/jquery.stellar/jquery.stellar.js"></script> -->
		{!! Html::script('/fontend/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js') !!}

		<!-- <script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script> -->
		{!! Html::script('/fontend/vendor/jquery.gmap/jquery.gmap.js') !!}

		<!-- <script src="vendor/jquery.gmap/jquery.gmap.js"></script> -->
		{!! Html::script('/fontend/vendor/jquery.lazyload/jquery.lazyload.js') !!}

		<!-- <script src="vendor/jquery.lazyload/jquery.lazyload.js"></script> -->
		{!! Html::script('/fontend/vendor/isotope/jquery.isotope.js') !!}

		<!-- <script src="vendor/isotope/jquery.isotope.js"></script> -->
		{!! Html::script('/fontend/vendor/owl.carousel/owl.carousel.js') !!}

		<!-- <script src="vendor/owl.carousel/owl.carousel.js"></script> -->
		{!! Html::script('/fontend/vendor/magnific-popup/jquery.magnific-popup.js') !!}

		<!-- <script src="vendor/magnific-popup/jquery.magnific-popup.js"></script> -->
		{!! Html::script('/fontend/vendor/vide/vide.js') !!}

		<!-- <script src="vendor/vide/vide.js"></script> -->

		<!-- Theme Base, Components and Settings -->
		{!! Html::script('/fontend/js/theme.js') !!}

		<!-- <script src="js/theme.js"></script> -->
		{!! Html::script('/fontend/js/custom.js') !!}


		<!-- Theme Custom -->
		<!-- <script src="js/custom.js"></script> -->

		<!-- Theme Initialization Files -->
		{!! Html::script('/fontend/js/theme.init.js') !!}

		<!-- <script src="js/theme.init.js"></script> -->

		@endsection
