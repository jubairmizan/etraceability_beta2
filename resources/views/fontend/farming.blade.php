@extends('layouts.fontend')

@section('title', 'Home')


@section('stylesheet')
{!! Html::style('/fontend/vendor/bootstrap/css/bootstrap.css') !!}

<!-- <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css"> -->
{!! Html::style('/fontend/vendor/font-awesome/css/font-awesome.css') !!}
<!-- <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.css"> -->
<!-- <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.css"> -->
{!! Html::style('/fontend/vendor/simple-line-icons/css/simple-line-icons.css') !!}
{!! Html::style('/fontend/vendor//owl.carousel/assets/owl.carousel.min.css') !!}

<!-- <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css"> -->
{!! Html::style('/fontend/vendor/owl.carousel/assets/owl.theme.default.min.css') !!}

<!-- <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css"> -->
{!! Html::style('/fontend/vendor/magnific-popup/magnific-popup.css') !!}

<!-- <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.css"> -->

<!-- Theme CSS -->
{!! Html::style('/fontend/css/theme.css') !!}

<!-- <link rel="stylesheet" href="css/theme.css"> -->
{!! Html::style('/fontend/css/theme-elements.css') !!}

<!-- <link rel="stylesheet" href="css/theme-elements.css"> -->
{!! Html::style('/fontend/css/theme-blog.css') !!}

<!-- <link rel="stylesheet" href="css/theme-blog.css"> -->
{!! Html::style('/fontend/css/theme-shop.css') !!}

<!-- <link rel="stylesheet" href="css/theme-shop.css"> -->
{!! Html::style('/fontend/css/theme-animate.css') !!}

<!-- <link rel="stylesheet" href="css/theme-animate.css"> -->

<!-- Current Page CSS -->
{!! Html::style('/fontend/vendor/rs-plugin/css/settings.css') !!}

<!-- <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css" media="screen"> -->
{!! Html::style('/fontend/vendor/rs-plugin/css/layers.css') !!}

<!-- <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css" media="screen"> -->
{!! Html::style('/fontend/vendor/rs-plugin/css/navigation.css') !!}

<!-- <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css" media="screen"> -->
{!! Html::style('/fontend/vendor/circle-flip-slideshow/css/component.css') !!}

<!-- <link rel="stylesheet" href="vendor/circle-flip-slideshow/css/component.css" media="screen"> -->

<!-- Skin CSS -->
{!! Html::style('/fontend/css/skins/skin-corporate-4.css') !!}

<!-- <link rel="stylesheet" href="css/skins/skin-corporate-4.css"> -->

<!-- Theme Custom CSS -->
{!! Html::style('/fontend/css/custom.css') !!}

<!-- <link rel="stylesheet" href="css/custom.css"> -->

<!-- Head Libs -->
{!! Html::script('/fontend/vendor/modernizr/modernizr.js') !!}

<!-- <script src="vendor/modernizr/modernizr.js"></script> -->

@endsection


@section('content')
<div role="main" class="main">

	<div class="slider-container rev_slider_wrapper">
		<div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"gridwidth": 1170, "gridheight": 500}'>
			<ul>
				<li data-transition="fade">

					<img src="./fontend/img/slides/1920f.jpg"  
					


					alt=""
					data-bgposition="center center" 
					data-bgfit="cover" 
					data-bgrepeat="no-repeat" 
					class="rev-slidebg">

					<div class="tp-caption"
					data-x="177"
					data-y="180"
					data-start="1000"
					data-transform_in="x:[-300%];opacity:0;s:500;"><img src="./fontend/img/slides/slide-title-border-light.png" alt=""></div>

					<div class="tp-caption top-label"
					data-x="217"
					data-y="180"
					data-start="500"
					data-transform_in="y:[-300%];opacity:0;s:500;">First Time In Bangladesh </div>

					<div class="tp-caption"
					data-x="480"
					data-y="180"
					data-start="1000"
					data-transform_in="x:[300%];opacity:0;s:500;"><img src="./fontend/img/slides/slide-title-border-light.png" alt=""></div>

					<div class="tp-caption main-label"
					data-x="135"
					data-y="210"
					data-start="1500"
					data-whitespace="nowrap"						 
					data-transform_in="y:[100%];s:500;"
					data-transform_out="opacity:0;s:500;"
					data-mask_in="x:0px;y:0px;" style="color: #1C58A6;">e-Traceability</div>

					<div class="tp-caption bottom-label"
					data-x="185"
					data-y="280"
					data-start="2000"
					data-transform_in="y:[100%];opacity:0;s:500;">In mobile application.</div>

				</li>
				<li data-transition="fade">

					<img src="./fontend/img/slides/1920b.jpg"  
					alt=""
					data-bgposition="center center" 
					data-bgfit="cover" 
					data-bgrepeat="no-repeat" 
					class="rev-slidebg">

				</li>
				<li data-transition="fade">

					<img src="./fontend/img/slides/1920a.jpg"  
					alt=""
					data-bgposition="center center" 
					data-bgfit="cover" 
					data-bgrepeat="no-repeat" 
					class="rev-slidebg">
				</li>
			</ul>
		</div>
	</div>

	<div class="home-intro" id="home-intro">
		<div class="container">

			<div class="row">
				<div class="col-md-8">
					<p>
						Want to download our <strong> <em>App ?</em></strong>
						<!-- <span>Check out our options and features included.</span> -->
					</p>
				</div>
				<div class="col-md-4">
					<div class="get-started">
						<a href="#" class="btn btn-lg btn-primary">Download Here</a>
						<!-- <div class="learn-more">or <a href="index.html">learn more.</a></div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 style="color : #5BC8F2">Our <strong>Services</strong></h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="feature-box feature-box-style-2 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
					<div class="feature-box-icon">
						<i class="icon-user-following icons" style="color: #5BC8F2"></i>
					</div>
					<div class="feature-box-info">
						<h4 class="mb-sm" style="color: #5BC8F2">Fishing</h4>
						{{-- <p class="mb-lg" style="color: #fff">
							Is an online market place that allows consumers or aquaculture/ Agriculture  value chain, (Hatchery – Nursery – Famers and Processing plants) to trade under auction model simply using mobile application.
							It allows paperless cross boarder trade among large Volume buyers and gives transparency until the button of the value chain.</p> --}}
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature-box feature-box-style-2 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
						<div class="feature-box-icon">
							<i class="icon-layers icons" style="color: #5BC8F2"></i>
						</div>
						<div class="feature-box-info">
							<h4 class="mb-sm" style="color: #5BC8F2">Farming</h4>
							{{-- <p class="mb-lg" style="color: #fff">“e-traceability is not an end in itself but it is a means to an end”  
								Trace My Fish is all about e-traceability. E-traceability is tracing back food in all stages of production, processing and distribution. 

								E traceability helps optimize the limited resources, boosts up yield and ensures fair price for the farmers. 

								eTraceability ensured  improved productivity of Aquaculture and Agriculture, and eventually creates scope for a huge international market potential.
							</p> --}}
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="feature-box feature-box-style-2 appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">
						<div class="feature-box-icon">
							<i class="icon-calculator icons" style="color: #5BC8F2"></i>
						</div>
						<div class="feature-box-info">
							<h4 class="mb-sm" style="color: #5BC8F2">Support</h4>
							{{-- <p class="mb-lg" style="color: #fff">A project in development, we will surely maintain food safety at all costs and track the shipment of our products in real time for the buyers.</p> --}}
						</div>
					</div>
				</div>
			</div>
		</div>

		<section class="parallax section section-text-light section-parallax section-center mt-xl mb-none" data-stellar-background-ratio="0.5" style="background-image: url(./fontend/img/parallax-people.jpg);">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options='{"items": 1, "loop": false}'>
							<div>
								<div class="col-md-12">
									<div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
										<div class="testimonial-author">
											<img src="./fontend/img/clients/client-1.jpg" class="img-responsive img-circle" alt="">
										</div>
										<blockquote>
											<p>e-traceability is not an end in itself but it is a means to an end.</p>
										</blockquote>
										<div class="testimonial-author">
											<p><strong>Syed Mahmudul Huq</strong><span>Honorable Chairman BSFF</span></p>
										</div>
									</div>
								</div>
							</div>
							<div>
								<div class="col-md-12">
									<div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
										<div class="testimonial-author">
											<img src="./fontend/img/clients/client-1.jpg" class="img-responsive img-circle" alt="">
										</div>
										<blockquote>
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
										</blockquote>
										<div class="testimonial-author">
											<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
<!-- 
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 p-none">
					<section class="section section-secondary match-height mt-none">
						<div class="container-fluid p-none mr-xl">
							<div class="row">
								<div class="col-md-11 col-md-offset-1 col-lg-6 col-lg-offset-5 p-none col-xs-offset-1 col-sm-offset-1">

									<h2 class="heading-dark">Latest <strong>Posts</strong></h2>
									<div class="row">
										<div class="col-md-6">
											<div class="recent-posts">
												<article class="post">
													<div class="date">
														<span class="day">15</span>
														<span class="month">Jan</span>
													</div>
													<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur</a></h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
												</article>
											</div>
										</div>
										<div class="col-md-6">
											<div class="recent-posts">
												<article class="post">
													<div class="date">
														<span class="day">15</span>
														<span class="month">Jan</span>
													</div>
													<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur</a></h4>
													<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
												</article>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="col-md-6 p-none">
					<section class="section section-tertiary match-height mt-none">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-11 col-md-offset-1 col-lg-5">
									<h2 class="heading-dark">Our <strong>Clients</strong></h2>

									<div class="content-grid content-grid-dashed mt-xlg mb-lg">
										<div class="row content-grid-row">
											<div class="content-grid-item col-md-6 center">
												<img class="img-responsive" src="img/logos/logo-1.png" alt="">
											</div>
											<div class="content-grid-item col-md-6 center">
												<img class="img-responsive" src="img/logos/logo-2.png" alt="">
											</div>
										</div>
										<div class="row content-grid-row">
											<div class="content-grid-item col-md-6 center">
												<img class="img-responsive" src="img/logos/logo-5.png" alt="">
											</div>
											<div class="content-grid-item col-md-6 center">
												<img class="img-responsive" src="img/logos/logo-6.png" alt="">
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div> -->

<!-- 		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 class="mt-xl">Our <strong>Portfolio</strong></h2>

					<div class="owl-carousel owl-theme mb-none" data-plugin-options='{"items": 4, "margin": 0, "loop": false}'>
						<div>
							<a href="portfolio-single.html">
								<img src="img/projects/project-9.jpg" class="img-responsive" alt="">
							</a>
						</div>
						<div>
							<a href="portfolio-single.html">
								<img src="img/projects/project-12.jpg" class="img-responsive" alt="">
							</a>
						</div>
						<div>
							<a href="portfolio-single.html">
								<img src="img/projects/project-11.jpg" class="img-responsive" alt="">
							</a>
						</div>
						<div>
							<a href="portfolio-single.html">
								<img src="img/projects/project-8.jpg" class="img-responsive" alt="">
							</a>
						</div>
						<div>
							<a href="portfolio-single.html">
								<img src="img/projects/project-9.jpg" class="img-responsive" alt="">
							</a>
						</div>
						<div>
							<a href="portfolio-single.html">
								<img src="img/projects/project-12.jpg" class="img-responsive" alt="">
							</a>
						</div>
					</div>

				</div>
			</div>
		</div> -->

	</div>
	@endsection


	
	@section('script')
	<!-- Vendor -->
	{!! Html::script('fontend/vendor/jquery/jquery.js') !!}
	<!-- <script src="vendor/jquery/jquery.js"></script> -->
	{!! Html::script('fontend/vendor/jquery.appear/jquery.appear.js') !!}

	<!-- <script src="vendor/jquery.appear/jquery.appear.js"></script> -->
	{!! Html::script('fontend/vendor/jquery.easing/jquery.easing.js') !!}

	<!-- <script src="vendor/jquery.easing/jquery.easing.js"></script> -->
	{!! Html::script('fontend/vendor/jquery-cookie/jquery-cookie.js') !!}

	<!-- <script src="vendor/jquery-cookie/jquery-cookie.js"></script> -->
	{!! Html::script('fontend/vendor/bootstrap/js/bootstrap.js') !!}

	<!-- <script src="vendor/bootstrap/js/bootstrap.js"></script> -->
	{!! Html::script('fontend/vendor/common/common.js') !!}

	<!-- <script src="vendor/common/common.js"></script> -->
	{!! Html::script('fontend/vendor/jquery.validation/jquery.validation.js') !!}

	<!-- <script src="vendor/jquery.validation/jquery.validation.js"></script> -->
	{!! Html::script('fontend/vendor/jquery.stellar/jquery.stellar.js') !!}

	<!-- <script src="vendor/jquery.stellar/jquery.stellar.js"></script> -->

	{!! Html::script('fontend/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js') !!}

	<!-- <script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script> -->
	{!! Html::script('fontend/vendor/jquery.gmap/jquery.gmap.js') !!}

	<!-- <script src="vendor/jquery.gmap/jquery.gmap.js"></script> -->
	{!! Html::script('fontend/vendor/jquery.lazyload/jquery.lazyload.js') !!}

	<!-- <script src="vendor/jquery.lazyload/jquery.lazyload.js"></script> -->
	{!! Html::script('fontend/vendor/isotope/jquery.isotope.js') !!}

	<!-- <script src="vendor/isotope/jquery.isotope.js"></script> -->
	{!! Html::script('fontend/vendor/owl.carousel/owl.carousel.js') !!}

	<!-- <script src="vendor/owl.carousel/owl.carousel.js"></script> -->
	{!! Html::script('fontend/vendor/magnific-popup/jquery.magnific-popup.js') !!}

	<!-- <script src="vendor/magnific-popup/jquery.magnific-popup.js"></script> -->
	{!! Html::script('fontend/vendor/vide/vide.js') !!}

	<!-- <script src="vendor/vide/vide.js"></script> -->

	<!-- Theme Base, Components and Settings -->
	{!! Html::script('fontend/js/theme.js') !!}

	<!-- <script src="js/theme.js"></script> -->

	<!-- Current Page Vendor and Views -->
	{!! Html::script('fontend/vendor/rs-plugin/js/jquery.themepunch.tools.min.js') !!}

	<!-- <script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script> -->
	{!! Html::script('fontend/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') !!}

	<!-- <script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> -->
	{!! Html::script('fontend/vendor/circle-flip-slideshow/js/jquery.flipshow.js') !!}

	<!-- <script src="vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script> -->

	{!! Html::script('fontend/js/views/view.home.js') !!}

	<!-- <script src="vendor/vide/vide.js"></script> -->

	<!-- <script src="js/views/view.home.js"></script> -->

	<!-- Theme Custom -->
	{!! Html::script('fontend/js/custom.js') !!}

	<!-- <script src="js/custom.js"></script> -->

	<!-- Theme Initialization Files -->
	{!! Html::script('fontend/js/theme.init.js') !!}

	<!-- <script src="js/theme.init.js"></script> -->

	@endsection