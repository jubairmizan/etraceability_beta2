@extends('layouts.WhiteFish.client.master')
@section('main-content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<a class="btn btn-primary" href="{{ route('WhiteFish.client.farms.create') }}" style="background-color: #03A9F4;margin-bottom: 20px;">Add New</a>
						</div>
						<div class="col-md-7 col-xs-12">
							<h3>Farms Lists </h3>
						</div>
					</div>
					<hr>
					
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="example" class="table table-hover display  pb-30" >
								<thead>
									<tr>
										<th>Sl</th>
										<th>Name</th>
										{{-- <th>Fisheries Officer</th> --}}
										<th>Area</th>
										<th>Created At</th>
										{{-- <th>Action</th> --}}
									</tr>
								</thead>
								
								<tbody>
									<?php $sl = 1; ?>
									@foreach($getData as $value)
										<tr>
											<td>{{ $sl++ }}</td>
											<td>{{ $value->name }}</td>
											{{-- <td>{{ $value->rel_farm_manager->name }}</td> --}}
											<td>{{ $value->area }}</td>
											<td>@if($value->created_at <>''){{ $value->created_at->toDateString() }}@endif</td>
											{{-- <td>
												<a href="javascript:void(0)" class="pr-10" data-toggle="tooltip" title="completed" >
													<i class="fa fa-pencil-square-o"></i>
												</a> 
												<a href="javascript:void(0)" class="text-inverse" title="Delete" data-toggle="tooltip">
													<i class="zmdi zmdi-delete" style="color: red"></i>
												</a>
											</td> --}}
										</tr>
									@endforeach
								</tbody>
							
								<tfoot>
									<tr>
										<th>Sl</th>
										<th>Name</th>
										<th>Area</th>
										<th>Created At</th>
										{{-- <th>Action</th> --}}
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection