@extends('layouts.WhiteFish.client.master')
@section('main-content')
    <div class="container-fluid pt-25">
		<!-- Row -->
		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
                        <div class="panel-body sm-data-box-1">
							<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total Invest</span>	
							<div class="cus-sat-stat weight-500 txt-primary text-center mt-5">
								<span class="counter-anim">94000</span>
							</div>
							<div class="progress-anim mt-20">
								<div class="progress">
									<div class="progress-bar progress-bar-primary
									wow animated progress-animated" role="progressbar" aria-valuenow="93.12" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<ul class="flex-stat mt-5">
								<li>
									<span class="block">Feeds</span>
									<span class="block weight-500 txt-dark font-15">23200</span>
								</li>
								<li>
									<span class="block">Medicine</span>
									<span class="block weight-500 txt-dark font-15">30000</span>
								</li>
								<li>
									<span class="block">Others</span>
									<span class="block weight-500 txt-dark font-15">40800</span>
								</li>
							</ul>
						</div>
                    </div>
                </div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
                        <div class="panel-body sm-data-box-1">
							<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total Revenue</span>	
							<div class="cus-sat-stat weight-500 txt-primary text-center mt-5">
								<span class="counter-anim">120000</span>
							</div>
							<div class="progress-anim mt-20">
								<div class="progress">
									<div class="progress-bar progress-bar-primary
									wow animated progress-animated" role="progressbar" aria-valuenow="93.12" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<ul class="flex-stat mt-5">
								<li>
									<span class="block">Ruhi</span>
									<span class="block weight-500 txt-dark font-15">40000</span>
								</li>
								<li>
									<span class="block">Pangash</span>
									<span class="block weight-500 txt-dark font-15">30000</span>
								</li>
								<li>
									<span class="block">Silver Corpe</span>
									<span class="block weight-500 txt-dark font-15">50000</span>
								</li>
							</ul>
						</div>
                    </div>
                </div>
			</div>
		</div>
	</div>
@endsection