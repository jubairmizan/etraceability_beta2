@extends('layouts.WhiteFish.client.master')
@section('main-content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<a class="btn btn-primary" href="{{ route('WhiteFish.client.inventory_management.create') }}" style="background-color: #03A9F4;margin-bottom: 20px;">Add New</a>
						</div>
						<div class="col-md-7 col-xs-12">
							<h3>Inventory Stock Out List </h3>
						</div>
					</div>
					<hr>
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="datatableWithFooterCount" class="table table-hover display  pb-30" >
								<thead>
									<tr>
										<th>Sl</th>
										<th>Inventory Name</th>
										<th>Block</th>
										<th>Pond</th>
										<th>Quantity</th>
										<th>Price</th>
										<th>Total Price</th>
										<th>Created At</th>
									</tr>
								</thead>
								
								<tbody>
									<?php $sl = 1; ?>
									@foreach($getData as $value)
										<tr>
											<td>{{ $sl++ }}</td>
											<td>{{ $value->inventory->name }}</td>
											<td>{{ $value->wf_during->block->name }}</td>
											<td>{{ $value->wf_during->pond->name }}</td>
											<td>{{ $value->quantity }}</td>
											<td>{{ $value->price }}</td>
											<td>{{ $value->total_price }}</td>
											<td>@if($value->created_at){{ $value->created_at->diffForHumans() }} @endif</td>
										</tr>
									@endforeach
								</tbody>
							
								<tfoot>
									<tr>
										<th>Sl</th>
										<th>Inventory Name</th>
										<th>Block</th>
										<th>Pond</th>
										<th>Quantity</th>
										<th>Price</th>
										<th>Total Price</th>
										<th>Created At</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#datatableWithFooterCount').DataTable( {
	        "footerCallback": function ( row, data, start, end, display ) {
	            var api = this.api(), data;
	 
	            // Remove the formatting to get integer data for summation
	            var intVal = function ( i ) {
	                return typeof i === 'string' ?
	                    i.replace(/[\$,]/g, '')*1 :
	                    typeof i === 'number' ?
	                        i : 0;
	            };
	 
	            // Total over all pages
	            total = api
	                .column( 6 )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Total over this page
	            pageTotal = api
	                .column( 6, { page: 'current'} )
	                .data()
	                .reduce( function (a, b) {
	                    return intVal(a) + intVal(b);
	                }, 0 );
	 
	            // Update footer
	            $( api.column( 6 ).footer() ).html(
	                // pageTotal +' ('+ total+')'
	                total
	            );
	        }
	    } );
	})
</script>
@endsection