@extends('layouts.WhiteFish.client.master')
@section('main-content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<a class="btn btn-primary" href="{{ route('WhiteFish.client.ponds.create') }}" style="background-color: #03A9F4;margin-bottom: 20px;">Add New</a>
						</div>
						<div class="col-md-7 col-xs-12">
							<h3>Ponds Lists </h3>
						</div>
					</div>
					<hr>
					
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="example" class="table table-hover display  pb-30" >
								<thead>
									<tr>
										<th>Sl</th>
										<th>Quick Links</th>
										<th>Block Name</th>
										<th>Pond ID</th>
										<th>Pond Name</th>
										<th>Pond Area</th>
										<th>Staff Last Day</th>
										<th>Cultivation Fishes</th>
										<th>Fisheries Officer Name</th>
										<th>Pond Superviser Name</th>
										<th>Cultivation Type</th>
										<th>Culvation Process</th>
										<th>Cultivation Period</th>
										<th>Flood Free</th>
										<th>Establishment Date</th>
										<th>Created At</th>
									</tr>
								</thead>
								
								<tbody>
									<?php $sl = 1; ?>
									@foreach($getData as $value)
										<tr>
											<td>{{ $sl++ }}</td>
											<td>
												<div class="btn-group">
													<div class="dropdown">
														<button aria-expanded="false" data-toggle="dropdown" class="btn btn-primary dropdown-toggle " type="button">Quick Links <span class="caret"></span></button>
														<ul role="menu" class="dropdown-menu">
															<li><a href="{{ route('WhiteFish.client.staffList',$value->id) }}">Staff</a></li>
															<li><a href="{{ route('WhiteFish.client.ponaStockList',$value->id) }}">Pona Stockes</a></li>
															<li><a href="{{ route('WhiteFish.client.ponaSampling',$value->id) }}">Pona Sampling</a></li>
															<li><a href="{{ route('WhiteFish.client.waterParameterDetails',$value->id) }}">Monitoring Parameter Details</a></li>
														</ul>
													</div>
												</div>
											</td>
											<td>{{ $value->block->name }}</td>
											<td>{{ $value->pond_id }}</td>
											<td>{{ $value->name }}</td>
											<td>{{ $value->area }}</td>
											<td>@if($value->no_off_staff <>'') <a href="{{ route('WhiteFish.client.staffList',$value->id) }}">{{ $value->no_off_staff->no_of_staff }}</a>@endif</td>
											<td>
												@if(count($value->relPondWiseFish)>0)
													@foreach($value->relPondWiseFish as $fishLists)
														<span>{{ $fishLists->name }}</span> ,
													@endforeach
												@endif
											</td>
											<td>@if($value->fisheriesOfficer <>''){{ $value->fisheriesOfficer->name }}@endif</td>
											<td>@if($value->fisheriesOfficer <>''){{ $value->pondSuperviser->name }}@endif</td>
											<td>{{ $value->farmingType->name }}</td>
											<td>{{ $value->culvationProcess->name }}</td>
											<td>{{ $value->cultivationPeriod->name }}</td>
											<td>
												@if($value->flood_free == 1)
												    {{ 'Yes' }}
												@else
												  {{ 'No' }}
												@endif
											</td>


											<td>{{ $value->establishment_date }}</td>
											<td>@if($value->created_at<>''){{ $value->created_at->toDateString() }}@endif</td>
										</tr>
									@endforeach
								</tbody>
							
								<tfoot>
									<tr>
										<th>Sl</th>
										<th>Block Name</th>
										<th>Pond ID</th>
										<th>Pond Name</th>
										<th>Pond Area</th>
										<th>Staff Last Day</th>
										<th>Cultivation Fishes</th>
										<th>Fisheries Officer Name</th>
										<th>Pond Superviser Name</th>
										<th>Cultivation Type</th>
										<th>Culvation Process</th>
										<th>Cultivation Period</th>
										<th>Flood Free</th>
										<th>Establishment Date</th>
										<th>Created At</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection