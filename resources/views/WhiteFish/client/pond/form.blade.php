@extends('layouts.WhiteFish.client.master')
@section('main-content')
	<div class="row" style="margin-top: 30px;">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Ponds Entry</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									{{ Form::open(['method' => 'POST','route' => array('WhiteFish.client.ponds.store'),'role'=>'form','data-toggle'=>'validator']) }}
										{{ csrf_field() }}
										<div class="form-group">
											<label for="farm_id" class="control-label mb-10">Farm <span style="color:red">*</span></label>
											{!! Form::select('farm_id', $farms,null, ['class' => 'form-control','required'=>'required','id' => 'farm_id']) !!}
										</div>
										<div class="form-group">
											<label for="block_id" class="control-label mb-10">Block <span style="color:red">*</span></label>

											{!! Form::select('block_id', $block,null, ['placeholder' => 'Choose Block', 'class' => 'form-control','required'=>'required','id' => 'block_id']) !!}
										</div>
										<div class="form-group">
											<label for="fisheries_officer_id" class="control-label mb-10">Fisheries Officer</label>
											{!! Form::select('fisheries_officer_id', $fisheries_officer,null, ['placeholder' => 'Choose Fisheries Officer', 'class' => 'form-control','id' => 'fisheries_officer_id']) !!}
										</div>
										<div class="form-group">
											<label for="pond_superviser_id" class="control-label mb-10">Pond Supervisor</label>
											{!! Form::select('pond_superviser_id', $pond_superviser,null, ['placeholder' => 'Choose Pond Supervisor', 'class' => 'form-control','id' => 'pond_superviser_id']) !!}
										</div>
										<div class="form-group">
											<label for="farming_type_id" class="control-label mb-10">Farming Type <span style="color:red">*</span></label>
											{!! Form::select('farming_type_id', $farming_type,null, ['placeholder' => 'Choose Farming Type', 'class' => 'form-control','required'=>'required','id' => 'farming_type_id']) !!}
										</div>
										<div class="form-group">
											<label for="culvation_process_id" class="control-label mb-10">Cultivation Process <span style="color:red">*</span></label>
											{!! Form::select('culvation_process_id', $culvation_process,null, ['placeholder' => 'Choose Cultivation Process', 'class' => 'form-control','required'=>'required','id' => 'culvation_process_id']) !!}
										</div>
										<div class="form-group">
											<label for="cultivation_period_id" class="control-label mb-10">Cultivation Period <span style="color:red">*</span></label>
											{!! Form::select('cultivation_period_id', $cultivation_period,null, ['placeholder' => 'Choose Cultivation Period', 'class' => 'form-control','required'=>'required','id' => 'cultivation_period_id']) !!}
										</div>
										<div class="form-group">
											<label for="name" class="control-label mb-10">Name</label>
											{{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Pond Number','id'=>'name']) }}
										</div>
										<div class="form-group">
											<label for="pond_number" class="control-label mb-10">Pond Number <span style="color:red">*</span></label>
											{{ Form::text('pond_number',null,['class'=>'form-control','required','placeholder'=>'Pond Number','id'=>'pond_number']) }}
										</div>
										<div class="form-group">
											<label for="area" class="control-label mb-10">Area</label>
											{{ Form::text('area',null,['class'=>'form-control','placeholder'=>'Pond Area','id'=>'area']) }}
										</div>
										<div class="form-group">
											<label for="area" class="control-label mb-10">Cultivation Fish</label><br>
											@foreach($fishes as $fishesValue)
												<input type="checkbox" name="cultivation_fish[]" value="{{ $fishesValue->id }}"> <label class="control-label mb-10">{{ $fishesValue->name }}</label>
											@endforeach
										</div>
										<div class="form-group">
											<label for="establishment_date" class="control-label mb-10">Establishment Date</label>
											{{ Form::text('establishment_date',null,['class'=>'form-control datepicker','placeholder'=>'Establishment Date','id'=>'establishment_date']) }}
										</div>
										<div class="form-group">
											<label for="flood_free" class="control-label mb-10">Flood Free</label>
											<input type="radio" name="flood_free" value="1"> Yes
											<input type="radio" name="flood_free" value="0"> No
										</div>
										<div class="form-group mb-0">
											<button type="submit" class="btn btn-primary btn-anim"><i class="icon-rocket"></i><span class="btn-text">submit</span></button>
										</div>
									{{ Form::close() }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection