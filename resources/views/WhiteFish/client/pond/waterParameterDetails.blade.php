@extends('layouts.WhiteFish.client.master')
@section('main-content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-7 col-xs-12">
							<h3>Monitoring Parameter Lists </h3>
						</div>
					</div>
					<hr>
					
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="example" class="table table-hover display  pb-30" >
								<thead>
									<tr>
										<th>Sl</th>
										<th>Alkanitity HCO3</th>
										<th>Carbonate CO3</th>
										<th>Calcium CA</th>
										<th>Chlorine CL2</th>
										<th>Magnesium MG</th>
										<th>Potassium</th>
										<th>Ammonia NH3</th>
										<th>NO3</th>
										<th>Ammonium NH4</th>
										<th>Salinity</th>
										<th>Transparency</th>
										<th>Plankton Type</th>
										<th>Green</th>
										<th>Yellow</th>
										<th>Vibrio Count</th>
										<th>Created At</th>
									</tr>
								</thead>
								
								<tbody>
									<?php $sl = 1; ?>
									@foreach($getData as $value)
										<tr>
											<td>{{ $sl++ }}</td>
											<td>{{ $value->alkanitity_hco3 }}</td>
											<td>{{ $value->carbonate_co3 }}</td>
											<td>{{ $value->calcium_ca }}</td>
											<td>{{ $value->chlorine_cl2 }}</td>
											<td>{{ $value->magnesium_mg }}</td>
											<td>{{ $value->potassium_k }}</td>
											<td>{{ $value->ammonia_nh3 }}</td>
											<td>{{ $value->nitrate_no3 }}</td>
											<td>{{ $value->ammonium_nh4 }}</td>
											<td>{{ $value->salinity }}</td>
											<td>{{ $value->transparency }}</td>
											<td>{{ $value->plankton_type }}</td>
											<td>{{ $value->green }}</td>
											<td>{{ $value->yellow }}</td>
											<td>{{ $value->vibrio_count }}</td>
											<td>@if($value->created_at <>''){{ $value->created_at->toDateString() }}@endif</td>
										</tr>
									@endforeach
								</tbody>
							
								<tfoot>
									<tr>
										<th>Sl</th>
										<th>Alkanitity HCO3</th>
										<th>Carbonate CO3</th>
										<th>Calcium CA</th>
										<th>Chlorine CL2</th>
										<th>Magnesium MG</th>
										<th>Potassium</th>
										<th>Ammonia NH3</th>
										<th>NO3</th>
										<th>Ammonium NH4</th>
										<th>Salinity</th>
										<th>Transparency</th>
										<th>Plankton Type</th>
										<th>Green</th>
										<th>Yellow</th>
										<th>Vibrio Count</th>
										<th>Created At</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection