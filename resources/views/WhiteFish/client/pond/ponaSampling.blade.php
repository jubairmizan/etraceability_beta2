@extends('layouts.WhiteFish.client.master')
@section('main-content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-7 col-xs-12">
							<h3>Lists </h3>
						</div>
					</div>
					<hr>
					
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="example" class="table table-hover display  pb-30" >
								<thead>
									<tr>
										<th>Sl</th>
										<th>Block</th>
										<th>Pond Name</th>
										<th>Pond Number</th>
										<th>Fish</th>
										<th>Number Of Sample</th>
										<th>Average Body Weight</th>
										<th>Current Biomas</th>
										<th>Survival Rate</th>
										<th>Total Weight</th>
										<th>Pl Price</th>
										<th>Created At</th>
									</tr>
								</thead>
								
								<tbody>
									<?php $sl = 1; ?>
									@foreach($getData as $value)
										<tr>
											<td>{{ $sl++ }}</td>
											<td>{{ $value->block->name }}</td>
											<td>{{ $value->pond->name }}</td>
											<td>{{ $value->pond->pond_number }}</td>
											<td>{{ $value->fish->name }}</td>
											<td>{{ $value->number_of_sample }}</td>
											<td>{{ $value->average_body_weight }}</td>
											<td>{{ $value->current_biomass }}</td>
											<td>{{ $value->survival_rate }}</td>
											<td>{{ $value->total_weight }}</td>
											<td>{{ $value->pl_price }}</td>
											<td>@if($value->created_at <>''){{ $value->created_at->toDateString() }}@endif</td>
										</tr>
									@endforeach
								</tbody>
							
								<tfoot>
									<tr>
										<th>Sl</th>
										<th>Block</th>
										<th>Pond Name</th>
										<th>Pond Number</th>
										<th>Fish</th>
										<th>Quantity</th>
										<th>Cost</th>
										<th>Nursing During</th>
										<th>Stock Date</th>
										<th>Created At</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection