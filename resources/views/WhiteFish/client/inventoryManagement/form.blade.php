@extends('layouts.WhiteFish.client.master')
@section('main-content')
	<div class="row" style="margin-top: 30px;">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Inventory Management Entry</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									{{ Form::open(['method' => 'POST','route' => array('WhiteFish.client.inventory_management.store'),'role'=>'form','data-toggle'=>'validator']) }}
										{{ csrf_field() }}
										<div class="form-group">
											<label for="inventoryType" class="control-labal mb-10">Inventory Type</label>
											<select class="form-control" id="inventoryType" required="required">
												<option value="">Choose Inventory Type</option>
												@foreach($inventoryType as $value)
													<option value="{{ $value->id }}">{{ $value->name }}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<label for="inventory" class="control-labal mb-10">Inventory</label>
											<select class="form-control" id="inventory" required="required" name="inventory_id">
												<option value="">Choose Inventory</option>
											</select>
										</div>
										<div class="form-group">
											<label for="quantity" class="control-label mb-10">Quantity</label>
											{{ Form::text('quantity',null,['class'=>'form-control','required','placeholder'=>'Quantity','id'=>'quantity']) }}
										</div>
										<div class="form-group">
											<label for="price" class="control-label mb-10">Price</label>
											{{ Form::text('price',null,['class'=>'form-control','required','placeholder'=>'Price','id'=>'price']) }}
										</div>
										<div class="form-group mb-0">
											<button type="submit" class="btn btn-primary btn-anim"><i class="icon-rocket"></i><span class="btn-text">submit</span></button>
										</div>
									{{ Form::close() }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		$(document).ready(function(){
			$('#inventoryType').on('change',function(){
				$.ajax({
					url:'{{ route('WhiteFish.client.getInventory') }}'+'?id='+$(this).val(),
				}).done(function(data){
					$('#inventory').html(data);
				}).fail(function(data){
					alert('fail');
				});
			});
		});
	</script>
@endsection