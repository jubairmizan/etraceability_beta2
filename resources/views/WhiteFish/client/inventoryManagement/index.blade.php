@extends('layouts.WhiteFish.client.master')
@section('main-content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<a class="btn btn-primary" href="{{ route('WhiteFish.client.inventory_management.create') }}" style="background-color: #03A9F4;margin-bottom: 20px;">Add New</a>
						</div>
						<div class="col-md-7 col-xs-12">
							<h3>Inventory Management </h3>
						</div>
					</div>
					<hr>
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="example" class="table table-hover display  pb-30" >
								<thead>
									<tr>
										<th>Sl</th>
										<th>Type</th>
										<th>Inventory</th>
										<th>Quantity</th>
										<th>Remaining Quantity</th>
										<th>Price</th>
										<th>Total Price</th>
										<th>Created At</th>
									</tr>
								</thead>
								
								<tbody>
									<?php $sl = 1; ?>
									@foreach($getData as $value)
										{{-- <tr>
											<td>{{ $sl++ }}</td>
											<td>
												{{ $value->inventoryType['0']->name }}
											</td>
											<td>{{ $value->rel_inventory->name }}</td>
											<td>{{ $value->quantity }}</td>
											<td>{{ $value->remaining_quantity }}</td>
											<td>{{ $value->price }}</td>
											<td>{{ $value->total_price }}</td>
											<td>{{ $value->created_at }}</td>
										</tr> --}}
										<tr>
											<td>{{ $sl++ }}</td>
											<td>{{ $value->typeName }}</td>
											<td>{{ $value->inventoryName }}</td>
											<td>{{ $value->quantity }}</td>
											<td>{{ $value->remaining_quantity }}</td>
											<td>{{ $value->price }}</td>
											<td>{{ $value->total_price }}</td>
											<td>{{ $value->created_at }}</td>
										</tr>
									@endforeach
								</tbody>
							
								<tfoot>
									<tr>
										<th>Sl</th>
										<th>Type</th>
										<th>Inventory</th>
										<th>Quantity</th>
										<th>Remaining Quantity</th>
										<th>Price</th>
										<th>Total Price</th>
										<th>Created At</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection