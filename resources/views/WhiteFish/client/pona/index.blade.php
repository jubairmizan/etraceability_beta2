@extends('layouts.WhiteFish.client.master')
@section('main-content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-5 col-xs-12">
						</div>
						<div class="col-md-7 col-xs-12">
							<h3>Pona Lists</h3>
						</div>
						<div class="col-md-12">
							{{ Form::open(['method' => 'POST','route' => array('WhiteFish.client.pona.search'),'role'=>'form']) }}
								{{ csrf_field() }}
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<label for="block_id" class="control-label mb-10">Block </label>
											{!! Form::select('block_id', $block,(isset($block_id)?$block_id:null), ['placeholder' => 'Choose Block', 'class' => 'form-control','id' => 'block_id']) !!}
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="pond_id" class="control-label mb-10">Pond </label>
											{!! Form::select('pond_id', $pond,(isset($pond_id)?$pond_id:null), ['placeholder' => 'Choose Pond', 'class' => 'form-control','id' => 'pond_id']) !!}
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group mb-0" style="margin-top: 30px;">
											<button type="submit" class="btn btn-primary btn-anim"><i class="icon-rocket"></i><span class="btn-text">submit</span></button>
										</div>
									</div>
								</div>
							{{ Form::close() }}
						</div>
					</div>
					<hr>
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="example" class="table table-hover display  pb-30" >
								<thead>
									<tr>
										<th>Sl</th>
										<th>Farm</th>
										<th>Block</th>
										<th>Pond</th>
										<th>Stock Date</th>
										<th>Fish</th>
										<th>Qty</th>
										<th>Cost</th>
										<th>Source</th>
										<th>Nursery</th>
										<th>Nursing During</th>
										<th>Created At</th>
									</tr>
								</thead>
								
								<tbody>
									<?php $sl = 1; ?>
									@foreach($getData as $value)
										<tr>
											<td>{{ $sl++ }}</td>
											<td>{{ $value->farm->name }}</td>
											<td>{{ $value->block->name }}</td>
											<td>{{ $value->pond->pond_id }}</td>
											<td>{{ $value->stock_date }}</td>
											<td>{{ $value->fish->name }}</td>
											<td>{{ $value->quantity }}</td>
											<td>{{ $value->cost }}</td>
											<td>{{ $value->source }}</td>
											<td>
												@if($value->establish_nursery == 1)
													Yes
												@else
													No
												@endif
											</td>
											<td>{{ $value->nursing_during }}</td>
											<td>@if($value->created_at <>''){{ $value->created_at->toDateString() }}@endif</td>
										</tr>
									@endforeach
								</tbody>
							
								<tfoot>
									<tr>
										<th>Sl</th>
										<th>Farm</th>
										<th>Block</th>
										<th>Pond</th>
										<th>Stock Date</th>
										<th>Fish</th>
										<th>Qty</th>
										<th>Cost</th>
										<th>Source</th>
										<th>Nursery</th>
										<th>Nursing During</th>
										<th>Created At</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection