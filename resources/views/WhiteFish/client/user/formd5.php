@extends('layouts.WhiteFish.client.master')
@section('main-content')
<link href="{{ URL::asset('themeAssets/bower_components/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet" type="text/css"/>
    <div class="container-fluid pt-25">
		<!-- Row -->
		<div class="row">
			<div class="col-md-3">
				<div style="height: 270px" class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
                        <div class="panel-body sm-data-box-1">
							<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total investment<span style="color:#878787; font-size: 12px" class="block">Last harvest</span></span>	
							<div class="cus-sat-stat weight-500 txt-primary text-center mt-5">
								<span class="counter-anim">{{ $investTotalAmount->totalPrice }}</span>
							</div>
							<div class="progress-anim mt-20">
								<div class="progress">
									<div class="progress-bar progress-bar-primary
									wow animated progress-animated" role="progressbar" aria-valuenow="93.12" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<ul class="flex-stat mt-5">
								@foreach($investInventoryTypeList as $key=>$value)
									@if($key<3)
										<li>
											<span class="block">{{ $value->name }}</span>
											<span class="block weight-500 txt-dark font-15">{{ $value->inventoriesTotalPrice }}</span>
										</li>
									@endif
								@endforeach
							</ul>
						</div>
                    </div>
                </div>
			</div>
			<div class="col-md-3">
				<div style="height: 270px" class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
                        <div class="panel-body sm-data-box-1">
							<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total revenue<span style="color:#878787; font-size: 12px" class="block">Last harvest</span></span>
							<div class="cus-sat-stat weight-500 txt-primary text-center mt-5">
								<span class="counter-anim">{{ $harvestTotalAmount->totalPrice }}</span>
							</div>
							<div class="progress-anim mt-20">
								<div class="progress">
									<div class="progress-bar progress-bar-primary
									wow animated progress-animated" role="progressbar" aria-valuenow="93.12" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<ul class="flex-stat mt-5">
								@foreach($harvestFishWise as $key=>$value)
									@if($key<3)
										<li>
											<span class="block">{{ $value->name }}</span>
											<span class="block weight-500 txt-dark font-15">{{ $value->totalPrice }}</span>
										</li>
									@endif
								@endforeach
							</ul>
						</div>
                    </div>
                </div>
			</div>
			<div class="col-md-3">
				<div style="height: 270px" class="panel panel-default card-view">
					<div class="panel-heading">
						<div class="pull-left">
							<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Return on Investment<span style="color:#878787; font-size: 12px" class="block">Lifetime</span></span>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">



					  <div class="panel-wrapper collapse in">
				       {{--  <div class="panel-body">
				          <div id="chart_6" class="" style="height:165px;"></div>
				        </div> --}}
				        <div class="panel-body">
				          <canvas id="roi" height="200"></canvas>
				        </div>
				      </div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div style="height: 130px" class="panel card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body row pa-0">
							<div class="sm-data-box">
								<div class="container-fluid">
									<div class="row">
										<div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
											<span class="weight-500 uppercase-font block">Ponds</span>
											<span class="block counter txt-dark"><span class="counter-anim">{{ count($pond) }}</span></span>
										</div>
										<div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
											<span class="weight-500 uppercase-font block">Blocks</span>
											<span class="block counter txt-dark"><span class="counter-anim">{{ count($block) }}</span></span>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="height: 130px" class="panel card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body row pa-0">
							<div class="sm-data-box">
								<div class="container-fluid">
									<div class="row">
										<div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
											<span class="weight-500 uppercase-font block">Cultivation Fishes</span>
											<span class="block counter txt-dark"><span class="counter-anim">{{ count($typesOfFish) }}</span></span>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			{{-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-default card-view panel-refresh">
						<div class="refresh-container">
							<div class="la-anim-1"></div>
						</div>
						<div class="panel-heading">
							<div class="pull-left">
								<h6 class="panel-title txt-dark">Ponds Invest</h6>
							</div>
							<div class="pull-right">
								<a href="#" class="pull-left inline-block refresh">
									<i class="zmdi zmdi-replay"></i>
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-wrapper collapse in">
							<div class="panel-body">
								<div id="pondsInvest" class="" style="height:294px;"></div>
								<div class="label-chatrs mt-15">
									<div class="inline-block mr-15">
										<span class="clabels inline-block bg-blue mr-5" style="background-color: #0FC5BB !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 1</span>
									</div>
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #92F2EF !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 2</span>
									</div>
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #1B76BC !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 3</span>
									</div>		
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2B3991 !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 4</span>
									</div>		
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2CB474 !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 5</span>
									</div>									
								</div>
							</div>	
						</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-default card-view panel-refresh">
						<div class="refresh-container">
							<div class="la-anim-1"></div>
						</div>
						<div class="panel-heading">
							<div class="pull-left">
								<h6 class="panel-title txt-dark">Ponds Revenue</h6>
							</div>
							<div class="pull-right">
								<a href="#" class="pull-left inline-block refresh">
									<i class="zmdi zmdi-replay"></i>
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-wrapper collapse in">
							<div class="panel-body">
								<div id="pondsRevenue" class="" style="height:294px;"></div>
								<div class="label-chatrs mt-15">
									<div class="inline-block mr-15">
										<span class="clabels inline-block bg-blue mr-5" style="background-color: #0FC5BB !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 1</span>
									</div>
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #92F2EF !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 2</span>
									</div>
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #1B76BC !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 3</span>
									</div>		
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2B3991 !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 4</span>
									</div>		
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2CB474 !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 5</span>
									</div>									
								</div>
							</div>	
						</div>
				</div>
			</div> --}}

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 _hidden">
				<div style="height: 508px; max-height: 598px;" class="panel panel-default card-view panel-refresh">

					<div class="refresh-container">
						<div class="la-anim-1"></div>
					</div>
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Pond Invest Day Wise</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block refresh">
								<i class="zmdi zmdi-replay"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="box box-primary">
			            <div class="box-body no-padding">
			              <div style="padding:20px" id="dashboardCalendar"></div>
			            </div>
			        </div>

			        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
					  <div class="modal-dialog modal-lg" role="document">
					    <div class="modal-content">
					      
					    </div>
					  </div>
					</div>



		         </div>
	        </div>

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 _hidden">
				<div style="height: 508px; max-height: 598px;" class="panel panel-default card-view panel-refresh">

					<div class="refresh-container">
						<div class="la-anim-1"></div>
					</div>
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Pond Harvest Day Wise</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block refresh">
								<i class="zmdi zmdi-replay"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="box box-primary">
			            <div class="box-body no-padding">
			              <div style="padding:20px" id="dashboardHarvestCalendar"></div>
			            </div>
			        </div>

			        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
					  <div class="modal-dialog modal-lg" role="document">
					    <div class="modal-content">
					      
					    </div>
					  </div>
					</div>



		         </div>
	        </div>
	        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hidden">
				<div style="height: 508px; max-height: 598px;" class="panel panel-default card-view panel-refresh">

					<div class="refresh-container">
						<div class="la-anim-1"></div>
					</div>
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Current Investment - {{ $investTotalAmount->totalPrice }}</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block refresh">
								<i class="zmdi zmdi-replay"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div  class="panel-body">
						@foreach($investInventoryTypeList as $value)
							<span class="font-12 head-font txt-dark">{{ $value->name }} <span class="pull-right">{{ $value->inventoriesTotalPrice }}</span></span>
							<div class="progress mt-10 mb-30">
								<div class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%" role="progressbar"> <span class="sr-only">85% Complete (success)</span> </div>
							</div>
						@endforeach
					</div>
		         </div>
	        </div>
		</div>
		@if(!isset($pond_details))
			<div class="row">
				{{-- Pond Cost --}}
				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
					<div class="panel panel-default card-view panel-refresh">
						<div class="refresh-container">
							<div class="la-anim-1"></div>
						</div>
						<div class="panel-heading">
							<div class="pull-left">
								<h6 class="panel-title txt-dark">Pond Invest</h6>
							</div>
							<div class="pull-right">
								<a href="#" class="pull-left inline-block refresh">
									<i class="zmdi zmdi-replay"></i>
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-wrapper collapse in">
							<div class="panel-body">
								<div id="pond_invest" class="" style="height:330px;"></div>
							</div>	
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
					<div class="panel panel-default card-view panel-refresh">
						<div class="refresh-container">
							<div class="la-anim-1"></div>
						</div>
						<div class="panel-heading">
							<div class="pull-left">
								<h6 class="panel-title txt-dark">Pond Revenue</h6>
							</div>
							<div class="pull-right">
								<a href="#" class="pull-left inline-block refresh">
									<i class="zmdi zmdi-replay"></i>
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-wrapper collapse in">
							<div class="panel-body">
								<div id="pondsRevenue" class="" style="height:330px;"></div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		@endif
		<style type="text/css">
			#exTab3 .nav-pills > li > a {
			  border-radius: 4px 4px 0 0 ;
			}

			#exTab3 .tab-content {
			  color : white;
			  background-color: _rgba(15, 197, 187, .7);
			  border: 2px solid rgba(15, 197, 187, 1);
			  padding : 5px 15px;
			  box-shadow: 2px 2px 2px rgba(15, 197, 187, 1);
			  border-radius: 0px 0px 5px 5px;
			}
		</style>

		@if(isset($pond_details))
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="panel panel-default card-view panel-refresh">
						<div class="refresh-container">
							<div class="la-anim-1"></div>
						</div>
						<div class="panel-heading">
							<div class="pull-left">
								<h6 class="panel-title txt-dark">Pond Details</h6>
							</div>
							<div class="pull-right">
								<a href="#" class="pull-left inline-block refresh">
									<i class="zmdi zmdi-replay"></i>
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
						
						<div class="box box-primary">
				            <div  style="padding:20px 0;" class="box-body no-padding">
				            	<div id="exTab3" class="">
									<ul  class="nav nav-pills">
										<li class="active">
											<a  href="#1b" data-toggle="tab">Overview</a>
										</li>
										<li>
											<a href="#2b" data-toggle="tab">Staff</a>
										</li>
										<li>
											<a href="#3b" data-toggle="tab">Pona Stock</a>
										</li>
										<li>
											<a href="#4b" data-toggle="tab">Pona Sampling</a>
										</li>
										<li>
											<a href="#5b" data-toggle="tab">Monitoring Parameter</a>
										</li>
									</ul>
									<div style="color:#878787;" class="tab-content clearfix">
										<div class="tab-pane active" id="1b">
											<div class="row">
												<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="border-right: 1px solid #02fbfb;">
													<div class="_panel _panel-default">
														<div class="panel-wrapper collapse in">
															<div class="panel-body">
																<p class="">Block Name : <span class=""> {{ $pond_details->block->name }}</span></p>
																<p class="">Pond ID : <span class=""> {{ $pond_details->pond_id }}</span></p>
																<p class="">Pond Name : <span class=""> {{ $pond_details->name }}</span></p>
																<p class="">Pond Area : <span class=""> {{ $pond_details->area }}</span></p>
																<p class="">Fisheries Officer Name : 
																	<span class="">
																		@if($pond_details->fisheriesOfficer != '')
																			@foreach($pond_details->fisheriesOfficer as $value)
																				{{ $value->name.',' }}
																			@endforeach
																		@endif
																	</span>
																</p>
																<p class="">Pond Superviser Name : 
																	<span class="">
																		@if($pond_details->pondSuperviser != '')
																			@foreach($pond_details->pondSuperviser as $value)
																				{{ $value->name.',' }}
																			@endforeach
																		@endif
																	</span>
																</p>
																<p class="">Cultivation Fishes  : 
																	<span class="">
																		@if($pond_details->relPondWiseFish != '')
																			@foreach($pond_details->relPondWiseFish as $value)
																				{{ $value->name.',' }}
																			@endforeach
																		@endif
																	</span>
																</p>
																<p class="">Cultivation Type : <span class="">{{ $pond_details->farmingType->name }}</span></p>
																<p class="">Cultivation Period : <span class="">{{ $pond_details->cultivationPeriod->name }}</span></p>
															</div>	
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
													<div class="_panel _panel-default">
														<div class="panel-wrapper collapse in">
															<div class="panel-body">
																<p class="">Last Staff Work : <span class="">@if($pond_details->no_off_staff !=''){{ $pond_details->no_off_staff->id }} on {{ $pond_details->no_off_staff->created_at->diffForHumans() }}@endif</span></p>
																<p class="">Last Pona Stock : <span class="">@if($pond_details->lastPonaStockeDetails !=''){{ $pond_details->lastPonaStockeDetails->quantity }} On ){{ $pond_details->lastPonaStockeDetails->created_at->diffForHumans() }}@endif</span></p>
																<p class="">Last Pona Sampling : <span class="">@if($pond_details->lastPonaSampling){{ $pond_details->lastPonaSampling->number_of_sample  }} On {{ $pond_details->lastPonaSampling->created_at }} @endif</span></p>
															</div>	
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="2b">
											<div class="table-wrap">
												<div class="table-responsive">
													<table class="table table-hover display  pb-30 datatable" >
														<thead>
															<tr>
																<th>Sl</th>
																<th>Block</th>
																<th>Pond Name</th>
																<th>Pond Number</th>
																<th>Staff</th>
																<th>Created At</th>
															</tr>
														</thead>
														
														<tbody>
															<?php $sl = 1; ?>
															@foreach($pond_staff as $value)
																<tr>
																	<td>{{ $sl++ }}</td>
																	<td>{{ $value->block->name }}</td>
																	<td>{{ $value->pond->name }}</td>
																	<td>{{ $value->pond->pond_number }}</td>
																	<td>{{ $value->no_of_staff }}</td>
																	<td>{{ $value->created_at->toDateString() }}</td>
																</tr>
															@endforeach
														</tbody>
													
														<tfoot>
															<tr>
																<th>Sl</th>
																<th>Block</th>
																<th>Pond Name</th>
																<th>Pond Number</th>
																<th>Staff</th>
																<th>Created At</th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="3b">
											<div class="table-wrap">
												<div class="table-responsive">
													<table class="table table-hover display  pb-30 datatable" >
														<thead>
															<tr>
																<th>Sl</th>
																<th>Block</th>
																<th>Pond Name</th>
																<th>Pond Number</th>
																<th>Fish</th>
																<th>Quantity</th>
																<th>Cost</th>
																<th>Nursing During</th>
																<th>Stock Date</th>
																<th>Created At</th>
															</tr>
														</thead>
														
														<tbody>
															<?php $sl = 1; ?>
															@foreach($pond_stock as $value)
																<tr>
																	<td>{{ $sl++ }}</td>
																	<td>{{ $value->block->name }}</td>
																	<td>{{ $value->pond->name }}</td>
																	<td>{{ $value->pond->pond_number }}</td>
																	<td>{{ $value->fish->name }}</td>
																	<td>{{ $value->quantity }}</td>
																	<td>{{ $value->cost }}</td>
																	<td>{{ $value->nursing_during }}</td>
																	<td>{{ $value->stock_date }}</td>
																	<td>{{ $value->created_at->toDateString() }}</td>
																</tr>
															@endforeach
														</tbody>
													
														<tfoot>
															<tr>
																<th>Sl</th>
																<th>Block</th>
																<th>Pond Name</th>
																<th>Pond Number</th>
																<th>Fish</th>
																<th>Quantity</th>
																<th>Cost</th>
																<th>Nursing During</th>
																<th>Stock Date</th>
																<th>Created At</th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="4b">
											<div class="table-wrap">
												<div class="table-responsive">
													<table class="table table-hover display  pb-30 datatable" >
														<thead>
															<tr>
																<th>Sl</th>
																<th>Block</th>
																<th>Pond Name</th>
																<th>Pond Number</th>
																<th>Fish</th>
																<th>Number Of Sample</th>
																<th>Average Body Weight</th>
																<th>Current Biomas</th>
																<th>Survival Rate</th>
																<th>Total Weight</th>
																<th>Pl Price</th>
																<th>Created At</th>
															</tr>
														</thead>
														
														<tbody>
															<?php $sl = 1; ?>
															@foreach($pond_pona_sampling as $value)
																<tr>
																	<td>{{ $sl++ }}</td>
																	<td>{{ $value->block->name }}</td>
																	<td>{{ $value->pond->name }}</td>
																	<td>{{ $value->pond->pond_number }}</td>
																	<td>{{ $value->fish->name }}</td>
																	<td>{{ $value->number_of_sample }}</td>
																	<td>{{ $value->average_body_weight }}</td>
																	<td>{{ $value->current_biomass }}</td>
																	<td>{{ $value->survival_rate }}</td>
																	<td>{{ $value->total_weight }}</td>
																	<td>{{ $value->pl_price }}</td>
																	<td>@if($value->created_at <>''){{ $value->created_at->toDateString() }}@endif</td>
																</tr>
															@endforeach
														</tbody>
													
														<tfoot>
															<tr>
																<th>Sl</th>
																<th>Block</th>
																<th>Pond Name</th>
																<th>Pond Number</th>
																<th>Fish</th>
																<th>Number Of Sample</th>
																<th>Average Body Weight</th>
																<th>Current Biomas</th>
																<th>Survival Rate</th>
																<th>Total Weight</th>
																<th>Pl Price</th>
																<th>Created At</th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
										<div class="tab-pane" id="5b">
											<div class="table-wrap">
												<div class="table-responsive">
													<table class="table table-hover display  pb-30 datatable" >
														<thead>
															<tr>
																<th>Sl</th>
																<th>Alkanitity HCO3</th>
																<th>Carbonate CO3</th>
																<th>Calcium CA</th>
																<th>Chlorine CL2</th>
																<th>Magnesium MG</th>
																<th>Potassium</th>
																<th>Ammonia NH3</th>
																<th>NO3</th>
																<th>Ammonium NH4</th>
																<th>Salinity</th>
																<th>Transparency</th>
																<th>Plankton Type</th>
																<th>Green</th>
																<th>Yellow</th>
																<th>Vibrio Count</th>
																<th>Created At</th>
															</tr>
														</thead>
														
														<tbody>
															<?php $sl = 1; ?>
															@foreach($pond_monitoring_parameter as $value)
																<tr>
																	<td>{{ $sl++ }}</td>
																	<td>{{ $value->alkanitity_hco3 }}</td>
																	<td>{{ $value->carbonate_co3 }}</td>
																	<td>{{ $value->calcium_ca }}</td>
																	<td>{{ $value->chlorine_cl2 }}</td>
																	<td>{{ $value->magnesium_mg }}</td>
																	<td>{{ $value->potassium_k }}</td>
																	<td>{{ $value->ammonia_nh3 }}</td>
																	<td>{{ $value->nitrate_no3 }}</td>
																	<td>{{ $value->ammonium_nh4 }}</td>
																	<td>{{ $value->salinity }}</td>
																	<td>{{ $value->transparency }}</td>
																	<td>{{ $value->plankton_type }}</td>
																	<td>{{ $value->green }}</td>
																	<td>{{ $value->yellow }}</td>
																	<td>{{ $value->vibrio_count }}</td>
																	<td>@if($value->created_at <>''){{ $value->created_at->toDateString() }}@endif</td>
																</tr>
															@endforeach
														</tbody>
													
														<tfoot>
															<tr>
																<th>Sl</th>
																<th>Alkanitity HCO3</th>
																<th>Carbonate CO3</th>
																<th>Calcium CA</th>
																<th>Chlorine CL2</th>
																<th>Magnesium MG</th>
																<th>Potassium</th>
																<th>Ammonia NH3</th>
																<th>NO3</th>
																<th>Ammonium NH4</th>
																<th>Salinity</th>
																<th>Transparency</th>
																<th>Plankton Type</th>
																<th>Green</th>
																<th>Yellow</th>
																<th>Vibrio Count</th>
																<th>Created At</th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
				            </div>
				        </div>
					</div>
				</div>
			</div>
		@endif
		<!-- /Row -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view panel-refresh">
					<iframe src="https://www.google.com/maps/d/embed?mid=1QElAMwhfDO0frK8O0yzfr7GkHr4_LPlc" width="100%" height="650"></iframe>
				</div>
			</div>
		</div>
		<!-- /Row -->
	</div>
	<script src="{{ URL::asset('themeAssets/bower_components/fullcalendar/dist/adminlte.min.js') }}"></script>
	<script src="{{ URL::asset('themeAssets/bower_components/fullcalendar/dist/moment.js') }}"></script>
	<script src="{{ URL::asset('themeAssets/bower_components/fullcalendar/dist/fullcalendar.min.js') }}"></script>
	<script src="{{ URL::asset('themeAssets/bower_components/chartist/dist/Chart.min.js') }}"></script>
	{{-- <script src="{{ URL::asset('themeAssets/js/calendar.js') }}"></script> --}}

	@include('layouts.WhiteFish.client.dashboardJS')
@endsection