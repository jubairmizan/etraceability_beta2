@extends('layouts.WhiteFish.client.master')
@section('main-content')
<link href="{{ URL::asset('themeAssets/bower_components/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet" type="text/css"/>
    <div class="container-fluid pt-25">
		<!-- Row -->
		<div class="row">
			<div class="col-md-3">
				<div style="height: 270px" class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
                        <div class="panel-body sm-data-box-1">
							<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total investment<span style="color:#878787; font-size: 12px" class="block">Last harvest</span></span>	
							<div class="cus-sat-stat weight-500 txt-primary text-center mt-5">
								<span class="counter-anim">94000</span>
							</div>
							<div class="progress-anim mt-20">
								<div class="progress">
									<div class="progress-bar progress-bar-primary
									wow animated progress-animated" role="progressbar" aria-valuenow="93.12" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<ul class="flex-stat mt-5">
								<li>
									<span class="block">Feeds</span>
									<span class="block weight-500 txt-dark font-15">23200</span>
								</li>
								<li>
									<span class="block">Medicine</span>
									<span class="block weight-500 txt-dark font-15">30000</span>
								</li>
								<li>
									<span class="block">Others</span>
									<span class="block weight-500 txt-dark font-15">40800</span>
								</li>
							</ul>
						</div>
                    </div>
                </div>
			</div>
			<div style="height: 270px" class="col-md-3">
				<div class="panel panel-default card-view">
					<div class="panel-wrapper collapse in">
                        <div class="panel-body sm-data-box-1">
							<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total revenue<span style="color:#878787; font-size: 12px" class="block">Last harvest</span></span>
							<div class="cus-sat-stat weight-500 txt-primary text-center mt-5">
								<span class="counter-anim">120000</span>
							</div>
							<div class="progress-anim mt-20">
								<div class="progress">
									<div class="progress-bar progress-bar-primary
									wow animated progress-animated" role="progressbar" aria-valuenow="93.12" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
							</div>
							<ul class="flex-stat mt-5">
								<li>
									<span class="block">Ruhi</span>
									<span class="block weight-500 txt-dark font-15">40000</span>
								</li>
								<li>
									<span class="block">Pangash</span>
									<span class="block weight-500 txt-dark font-15">30000</span>
								</li>
								<li>
									<span class="block">Silver Corpe</span>
									<span class="block weight-500 txt-dark font-15">50000</span>
								</li>
							</ul>
						</div>
                    </div>
                </div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
				<div style="height: 270px" class="panel panel-default card-view">
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Return on Investment</h6>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">



					  <div class="panel-wrapper collapse in">
				       {{--  <div class="panel-body">
				          <div id="chart_6" class="" style="height:165px;"></div>
				        </div> --}}
				        <div class="panel-body">
				          <canvas id="roi" height="220"></canvas>
				        </div>
				      </div>






					{{-- <div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div id="_pond_invest" class="" style="height:165px;"></div>
						</div>	
					</div> --}}

						
						
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
				<div style="height: 130px" class="panel card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body row pa-0">
							<div class="sm-data-box">
								<div class="container-fluid">
									<div class="row">
										<div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
											<span class="weight-500 uppercase-font block">Ponds</span>
											<span class="block counter txt-dark"><span class="counter-anim">{{ count($pond) }}</span></span>
										</div>
										<div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
											<span class="weight-500 uppercase-font block">Blocks</span>
											<span class="block counter txt-dark"><span class="counter-anim">{{ count($block) }}</span></span>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="height: 130px" class="panel card-view">
					<div class="panel-wrapper collapse in">
						<div class="panel-body row pa-0">
							<div class="sm-data-box">
								<div class="container-fluid">
									<div class="row">
										<div class="col-xs-12 text-center pl-0 pr-0 data-wrap-left">
											<span class="weight-500 uppercase-font block">Cultivation Fishes</span>
											<span class="block counter txt-dark"><span class="counter-anim">{{ count($typesOfFish) }}</span></span>
										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			{{-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-default card-view panel-refresh">
						<div class="refresh-container">
							<div class="la-anim-1"></div>
						</div>
						<div class="panel-heading">
							<div class="pull-left">
								<h6 class="panel-title txt-dark">Ponds Invest</h6>
							</div>
							<div class="pull-right">
								<a href="#" class="pull-left inline-block refresh">
									<i class="zmdi zmdi-replay"></i>
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-wrapper collapse in">
							<div class="panel-body">
								<div id="pondsInvest" class="" style="height:294px;"></div>
								<div class="label-chatrs mt-15">
									<div class="inline-block mr-15">
										<span class="clabels inline-block bg-blue mr-5" style="background-color: #0FC5BB !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 1</span>
									</div>
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #92F2EF !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 2</span>
									</div>
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #1B76BC !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 3</span>
									</div>		
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2B3991 !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 4</span>
									</div>		
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2CB474 !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 5</span>
									</div>									
								</div>
							</div>	
						</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <div class="panel panel-default card-view panel-refresh">
						<div class="refresh-container">
							<div class="la-anim-1"></div>
						</div>
						<div class="panel-heading">
							<div class="pull-left">
								<h6 class="panel-title txt-dark">Ponds Revenue</h6>
							</div>
							<div class="pull-right">
								<a href="#" class="pull-left inline-block refresh">
									<i class="zmdi zmdi-replay"></i>
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-wrapper collapse in">
							<div class="panel-body">
								<div id="pondsRevenue" class="" style="height:294px;"></div>
								<div class="label-chatrs mt-15">
									<div class="inline-block mr-15">
										<span class="clabels inline-block bg-blue mr-5" style="background-color: #0FC5BB !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 1</span>
									</div>
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #92F2EF !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 2</span>
									</div>
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #1B76BC !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 3</span>
									</div>		
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2B3991 !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 4</span>
									</div>		
									<div class="inline-block  mr-15">
										<span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2CB474 !important"></span>
										<span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 5</span>
									</div>									
								</div>
							</div>	
						</div>
				</div>
			</div> --}}

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 _hidden">
				<div class="panel panel-default card-view panel-refresh">

					<div class="refresh-container">
						<div class="la-anim-1"></div>
					</div>
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Pond Invest Day Wise</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block refresh">
								<i class="zmdi zmdi-replay"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="box box-primary">
			            <div class="box-body no-padding">
			              <div style="padding:20px" id="dashboardCalendar"></div>
			            </div>
			        </div>
		         </div>
	        </div>
	        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 _hidden">
				<div class="panel panel-default card-view panel-refresh">

					<div class="refresh-container">
						<div class="la-anim-1"></div>
					</div>
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Current Investment - {{ $investTotalAmount->totalPrice }}</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block refresh">
								<i class="zmdi zmdi-replay"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div  class="panel-body">
						@foreach($investList as $value)
							<span class="font-12 head-font txt-dark">{{ $value->name }} <span class="pull-right">{{ $value->inventoriesTotalPrice }}</span></span>
							<div class="progress mt-10 mb-30">
								<div class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%" role="progressbar"> <span class="sr-only">85% Complete (success)</span> </div>
							</div>
						@endforeach
					</div>
		         </div>
	        </div>
		</div>
		<div class="row">
			{{-- Pond Cost --}}

			<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view panel-refresh">
					<div class="refresh-container">
						<div class="la-anim-1"></div>
					</div>
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Pond Invest</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block refresh">
								<i class="zmdi zmdi-replay"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div id="pond_invest" class="" style="height:330px;"></div>
						</div>	
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view panel-refresh">
					<div class="refresh-container">
						<div class="la-anim-1"></div>
					</div>
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Pond Revenue</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block refresh">
								<i class="zmdi zmdi-replay"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body">
							<div id="e_chart_2" class="" style="height:330px;"></div>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<style type="text/css">
			#exTab3 .nav-pills > li > a {
			  border-radius: 4px 4px 0 0 ;
			}

			#exTab3 .tab-content {
			  color : white;
			  background-color: _rgba(15, 197, 187, .7);
			  border: 2px solid rgba(15, 197, 187, 1);
			  padding : 5px 15px;
			  box-shadow: 2px 2px 2px rgba(15, 197, 187, 1);
			  border-radius: 0px 0px 5px 5px;
			}
		</style>

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view panel-refresh">
					<div class="refresh-container">
						<div class="la-anim-1"></div>
					</div>
					<div class="panel-heading">
						<div class="pull-left">
							<h6 class="panel-title txt-dark">Pond Details</h6>
						</div>
						<div class="pull-right">
							<a href="#" class="pull-left inline-block refresh">
								<i class="zmdi zmdi-replay"></i>
							</a>
							<a href="#" class="pull-left inline-block refresh">
								<i class="zmdi zmdi-replay"></i>
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="box box-primary">
			            <div  style="padding:20px 0;" class="box-body no-padding">
			            	<div id="exTab3" class="">
								<ul  class="nav nav-pills">
									<li class="active">
										<a  href="#1b" data-toggle="tab">Overview</a>
									</li>
									<li>
										<a href="#2b" data-toggle="tab">Staff</a>
									</li>
									<li>
										<a href="#3b" data-toggle="tab">Pona Stock</a>
									</li>
									<li>
										<a href="#4b" data-toggle="tab">Pona Sampling</a>
									</li>
									<li>
										<a href="#5b" data-toggle="tab">Monitoring Parameter</a>
									</li>
								</ul>
								<div class="tab-content clearfix">
									<div class="tab-pane active" id="1b">
										<div class="row" style="color:#878787;" >
											<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="border-right: 1px solid #02fbfb;">
												<div class="_panel _panel-default">
													<div class="panel-wrapper collapse in">
														<div class="panel-body">
															<p class="">Block Name : <span class="">{{ count($block) }}</span></p>
															<p class="">Pond ID : <span class="">{{ count($pond) }}</span></p>
															<p class="">Pond Name : <span class="">{{ count($pond) }}</span></p>
															<p class="">Pond Area : <span class="">{{ count($pond) }}</span></p>
															<p class="">Fisheries Officer Name : <span class="">{{ count($pond) }}</span></p>
															<p class="">Pond Superviser Name : <span class="">{{ count($pond) }}</span></p>
															<p class="">Cultivation Fishes : <span class="">{{ count($typesOfFish) }}</span></p>
															<p class="">Cultivation Type : <span class="">{{ count($pond) }}</span></p>
															<p class="">Cultivation Period : <span class="">{{ count($pond) }}</span></p>
														</div>	
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
												<div class="_panel _panel-default">
													<div class="panel-wrapper collapse in">
														<div class="panel-body">
															<p class="">Last Pona Stock : <span class="">{{ count($block) }}</span></p>
															<p class="">Last Pona Sampling : <span class="">{{ count($pond) }}</span></p>
															<p class="">last feeding time : <span class="">{{ count($pond) }}</span></p>
															<p class="">Last Monitoring Parameter : <span class="">{{ count($typesOfFish) }}</span></p>
														</div>	
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="2b">
										<div class="table-wrap">
											<div class="table-responsive">
												<table class="table table-hover display  pb-30 datatable" >
													<thead>
														<tr>
															<th>Sl</th>
															<th>Block</th>
															<th>Pond Name</th>
															<th>Pond Number</th>
															<th>Staff</th>
															<th>Created At</th>
														</tr>
													</thead>
													
													<tbody>
															<tr>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
													</tbody>
												
													<tfoot>
														<tr>
															<th>Sl</th>
															<th>Block</th>
															<th>Pond Name</th>
															<th>Pond Number</th>
															<th>Staff</th>
															<th>Created At</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="3b">
										<div class="table-wrap">
											<div class="table-responsive">
												<table class="table table-hover display  pb-30 datatable" >
													<thead>
														<tr>
															<th>Sl</th>
															<th>Block</th>
															<th>Pond Name</th>
															<th>Pond Number</th>
															<th>Fish</th>
															<th>Quantity</th>
															<th>Cost</th>
															<th>Nursing During</th>
															<th>Stock Date</th>
															<th>Created At</th>
														</tr>
													</thead>
													
													<tbody>
															<tr>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
													</tbody>
												
													<tfoot>
														<tr>
															<th>Sl</th>
															<th>Block</th>
															<th>Pond Name</th>
															<th>Pond Number</th>
															<th>Fish</th>
															<th>Quantity</th>
															<th>Cost</th>
															<th>Nursing During</th>
															<th>Stock Date</th>
															<th>Created At</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="4b">
										<div class="table-wrap">
											<div class="table-responsive">
												<table class="table table-hover display  pb-30 datatable" >
													<thead>
														<tr>
															<th>Sl</th>
															<th>Block</th>
															<th>Pond Name</th>
															<th>Pond Number</th>
															<th>Fish</th>
															<th>Number Of Sample</th>
															<th>Average Body Weight</th>
															<th>Current Biomas</th>
															<th>Survival Rate</th>
															<th>Total Weight</th>
															<th>Pl Price</th>
															<th>Created At</th>
														</tr>
													</thead>
													
													<tbody>
															<tr>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
													</tbody>
												
													<tfoot>
														<tr>
															<th>Sl</th>
															<th>Block</th>
															<th>Pond Name</th>
															<th>Pond Number</th>
															<th>Fish</th>
															<th>Number Of Sample</th>
															<th>Average Body Weight</th>
															<th>Current Biomas</th>
															<th>Survival Rate</th>
															<th>Total Weight</th>
															<th>Pl Price</th>
															<th>Created At</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="5b">
										<div class="table-wrap">
											<div class="table-responsive">
												<table class="table table-hover display  pb-30 datatable" >
													<thead>
														<tr>
															<th>Sl</th>
															<th>Block</th>
															<th>Pond Name</th>
															<th>Pond Number</th>
															<th>Staff</th>
															<th>Created At</th>
														</tr>
													</thead>
													
													<tbody>
															<tr>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
																<td></td>
															</tr>
													</tbody>
												
													<tfoot>
														<tr>
															<th>Sl</th>
															<th>Block</th>
															<th>Pond Name</th>
															<th>Pond Number</th>
															<th>Staff</th>
															<th>Created At</th>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
			            </div>
			        </div>
				</div>
			</div>
		</div>
		<!-- /Row -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default card-view panel-refresh">
					<iframe src="https://www.google.com/maps/d/embed?mid=1QElAMwhfDO0frK8O0yzfr7GkHr4_LPlc" width="100%" height="650"></iframe>
				</div>
			</div>
		</div>
		<!-- /Row -->
	</div>
	<script src="{{ URL::asset('themeAssets/bower_components/fullcalendar/dist/adminlte.min.js') }}"></script>
	<script src="{{ URL::asset('themeAssets/bower_components/fullcalendar/dist/moment.js') }}"></script>
	<script src="{{ URL::asset('themeAssets/bower_components/fullcalendar/dist/fullcalendar.min.js') }}"></script>
	<script src="{{ URL::asset('themeAssets/bower_components/chartist/dist/Chart.min.js') }}"></script>
	{{-- <script src="{{ URL::asset('themeAssets/js/calendar.js') }}"></script> --}}
	<script type="text/javascript">
		$(document).ready(function(){
			$('.datatable').DataTable({
				"bFilter": true,
				"bLengthChange": true,
				"bPaginate": true,
				"bInfo": true,
				dom: 'Bfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf', 'print'
				]
			});


			if( $('#pond_invest').length > 0 ){
				var eChart_2 = echarts.init(document.getElementById('pond_invest'));
				var option1 = {
					tooltip : {
						backgroundColor: 'rgba(33,33,33,1)',
						borderRadius:0,
						padding:10,
						axisPointer: {
							type: 'cross',
							label: {
								backgroundColor: 'rgba(33,33,33,1)'
							}
						},
						textStyle: {
							color: '#fff',
							fontStyle: 'normal',
							fontWeight: 'normal',
							fontFamily: "'Open Sans', sans-serif",
							fontSize: 12
						}	
					},
					// color: ['#0FC5BB', '#92F2EF', '#D0F6F5'],
					color: ['#0FC5BB', '#0FC5BB', '#5AC4CC'],
					series : [
						{
							name: 'task',
							type: 'pie',
							radius : '55%',
							center: ['50%', '50%'],
							roseType : 'radius',
							tooltip : {
								trigger: 'item',
								formatter: "{a} <br/>{b} : {c} ({d}%)",
								backgroundColor: 'rgba(33,33,33,1)',
								borderRadius:0,
								padding:10,
								textStyle: {
									color: '#fff',
									fontStyle: 'normal',
									fontWeight: 'normal',
									fontFamily: "'Open Sans', sans-serif",
									fontSize: 12
								}	
							},
							data:[
								@foreach($investPondWise as $value)
									{value:{{ $value->pondTotalInvest }}, name:'{{ $value->name }}'},
								@endforeach
							],
							itemStyle: {
								emphasis: {
									shadowBlur: 10,
									shadowOffsetX: 0,
									shadowColor: 'rgba(0, 0, 0, 0.5)'
								}
							}
						}
					]
				};
				eChart_2.setOption(option1);
				eChart_2.resize();
			}
		})
		$(function () {

		    /* initialize the external events
		     -----------------------------------------------------------------*/
		    function init_events(ele) {
		      ele.each(function () {

		        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
		        // it doesn't need to have a start or end
		        var eventObject = {
		          title: $.trim($(this).text()) // use the element's text as the event title
		        }

		        // store the Event Object in the DOM element so we can get to it later
		        $(this).data('eventObject', eventObject)

		        // make the event draggable using jQuery UI
		        $(this).draggable({
		          zIndex        : 1070,
		          revert        : true, // will cause the event to go back to its
		          revertDuration: 0  //  original position after the drag
		        })

		      })
		    }

		    init_events($('#external-events div.external-event'))

		    /* initialize the calendar
		     -----------------------------------------------------------------*/
		    //Date for the calendar events (dummy data)
		    var date = new Date()
		    var d    = date.getDate(),
		        m    = date.getMonth(),
		        y    = date.getFullYear()
		    $('#dashboardCalendar').fullCalendar({
		      header    : {
		        left  : 'prev,next today',
		        center: 'title',
		        right : 'month,agendaWeek,agendaDay'
		      },
		      buttonText: {
		        today: 'today',
		        month: 'month',
		        week : 'week',
		        day  : 'day'
		      },
		      //Random default events
		      events    : [
		      	@foreach($calenderData as $value)
			        {
			          title          : '{{ $value->totalPrice }} /=',
			          start          : '{{ $value->created_at->toDateString() }}',
			          backgroundColor: '#f39c12', //yellow
			          borderColor    : '#f39c12', //yellow
			          url            : 'http://google.com/',
			        },
		        @endforeach
		      ],
		      editable  : true,
		      droppable : true, // this allows things to be dropped onto the calendar !!!
		      drop      : function (date, allDay) { // this function is called when something is dropped

		        // retrieve the dropped element's stored Event Object
		        var originalEventObject = $(this).data('eventObject')

		        // we need to copy it, so that multiple events don't have a reference to the same object
		        var copiedEventObject = $.extend({}, originalEventObject)

		        // assign it the date that was reported
		        copiedEventObject.start           = date
		        copiedEventObject.allDay          = allDay
		        copiedEventObject.backgroundColor = $(this).css('background-color')
		        copiedEventObject.borderColor     = $(this).css('border-color')

		        // render the event on the calendar
		        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
		        $('#dashboardCalendar').fullCalendar('renderEvent', copiedEventObject, true)

		        // is the "remove after drop" checkbox checked?
		        if ($('#drop-remove').is(':checked')) {
		          // if so, remove the element from the "Draggable Events" list
		          $(this).remove()
		        }

		      }
		    })

		    /* ADDING EVENTS */
		    var currColor = '#3c8dbc' //Red by default
		    //Color chooser button
		    var colorChooser = $('#color-chooser-btn')
		    $('#color-chooser > li > a').click(function (e) {
		      e.preventDefault()
		      //Save color
		      currColor = $(this).css('color')
		      //Add color effect to button
		      $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
		    })
		    $('#add-new-event').click(function (e) {
		      e.preventDefault()
		      //Get value and make sure it is not null
		      var val = $('#new-event').val()
		      if (val.length == 0) {
		        return
		      }

		      //Create events
		      var event = $('<div />')
		      event.css({
		        'background-color': currColor,
		        'border-color'    : currColor,
		        'color'           : '#fff'
		      }).addClass('external-event')
		      event.html(val)
		      $('#external-events').prepend(event)

		      //Add draggable funtionality
		      init_events(event)

		      //Remove event from text input
		      $('#new-event').val('')
		    })
		  })
	</script>
	
@endsection