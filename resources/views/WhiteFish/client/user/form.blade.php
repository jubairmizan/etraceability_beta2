@extends('layouts.WhiteFish.client.master')
@section('main-content')
	<div class="row" style="margin-top: 30px;">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">User Entry</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									{{ Form::open(['method' => 'POST','route' => array('WhiteFish.client.user.store'),'role'=>'form','data-toggle'=>'validator']) }}
										{{ csrf_field() }}
										<div class="form-group">
											<label for="type_id" class="control-label mb-10">User Type</label>
											{!! Form::select('type_id', $userType,null, ['placeholder' => 'Choose Type','class' => 'form-control','required'=>'required','id' => 'type_id']) !!}
										</div>
										<div class="form-group">
											<label for="user_id" class="control-label mb-10">User ID</label>
											{{ Form::text('user_id',null,['class'=>'form-control','required','placeholder'=>'User ID','id'=>'user_id']) }}
										</div>
										<div class="form-group">
											<label for="password" class="control-label mb-10">Password</label>
											{{ Form::text('password',null,['class'=>'form-control','required','placeholder'=>'*********','id'=>'password']) }}
										</div>
										<div class="form-group">
											<label for="name" class="control-label mb-10">Full Name</label>
											{{ Form::text('name',null,['class'=>'form-control','required','placeholder'=>'User Name','id'=>'name']) }}
										</div>
										<div class="form-group">
											<label for="email" class="control-label mb-10">Email</label>
											{{ Form::text('email',null,['class'=>'form-control','required','placeholder'=>'User Email','id'=>'email']) }}
										</div>
										<div class="form-group">
											<label for="cell_no" class="control-label mb-10">Mobile No.</label>
											{{ Form::text('cell_no',null,['class'=>'form-control','required','placeholder'=>'Mobile No.','id'=>'cell_no']) }}
										</div>
										<div class="form-group">
											<label for="gender_id" class="control-label mb-10">Gender</label>
											<select class="form-control" name="gender_id">
												<option value="1">Male</option>
												<option value="0">Female</option>
											</select>
										</div>
										<div class="form-group">
											<label for="dob" class="control-label mb-10">Date of Birth</label>
											{{ Form::text('dob',null,['class'=>'form-control datepicker','required','placeholder'=>'Date of Birth','id'=>'dob']) }}
										</div>
										<div class="form-group">
											<label for="salary" class="control-label mb-10">Salary</label>
											{{ Form::text('salary',null,['class'=>'form-control','required','placeholder'=>'Salary','id'=>'salary']) }}
										</div>
										<div class="form-group mb-0">
											<button type="submit" class="btn btn-primary btn-anim"><i class="icon-rocket"></i><span class="btn-text">submit</span></button>
										</div>
									{{ Form::close() }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection