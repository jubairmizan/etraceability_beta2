@extends('layouts.WhiteFish.client.master')
@section('main-content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default card-view">
			<div class="panel-wrapper collapse in">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-5 col-xs-12">
							<a class="btn btn-primary" href="{{ route('WhiteFish.client.inventories.create') }}" style="background-color: #03A9F4;margin-bottom: 20px;">Add New</a>
						</div>
						<div class="col-md-7 col-xs-12">
							<h3>Inventory </h3>
						</div>
					</div>
					<hr>
					<div class="table-wrap">
						<div class="table-responsive">
							<table id="example" class="table table-hover display  pb-30" >
								<thead>
									<tr>
										<th>Sl</th>
										<th>Name</th>
										<th>Type</th>
										<th>Brand</th>
										<th>Code</th>
										<th>Status</th>
										<th>Created At</th>
									</tr>
								</thead>
								
								<tbody>
									<?php $sl = 1; ?>
									@foreach($getData as $value)
										<tr>
											<td>{{ $sl++ }}</td>
											<td>{{ $value->name }}</td>
											<td>{{ $value->rel_inventory_type->name }}</td>
											<td>{{ $value->rel_inventory_brand->name }}</td>
											<td>{{ $value->code }}</td>
											<td>
												@if($value->status==1)
													Active
												@else
													Inactive
												@endif
											</td>
											<td>{{ $value->created_at }}</td>
										</tr>
									@endforeach
								</tbody>
							
								<tfoot>
									<tr>
										<th>Sl</th>
										<th>Name</th>
										<th>Type</th>
										<th>Brand</th>
										<th>Code</th>
										<th>Status</th>
										<th>Created At</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
@endsection