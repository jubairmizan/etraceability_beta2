@extends('layouts.WhiteFish.client.master')
@section('main-content')
	<div class="row" style="margin-top: 30px;">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default card-view">
				<div class="panel-heading">
					<div class="pull-left">
						<h6 class="panel-title txt-dark">Inventory Entry</h6>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						
						<div class="row">
							<div class="col-md-12">
								<div class="form-wrap">
									{{ Form::open(['method' => 'POST','route' => array('WhiteFish.client.inventories.store'),'role'=>'form','data-toggle'=>'validator']) }}
										{{ csrf_field() }}
										<div class="form-group">
											<label for="name" class="control-label mb-10">Name</label>
											{{ Form::text('name',null,['class'=>'form-control','required','placeholder'=>'Name','id'=>'name']) }}
										</div>
										<div class="form-group">
											<label for="type_id" class="control-label mb-10">Type</label>
											{!! Form::select('type_id', $type,null, ['placeholder' => 'Choose Type','class' => 'form-control','required'=>'required','id' => 'type_id']) !!}
										</div>
										<div class="form-group">
											<label for="brand_id" class="control-label mb-10">Brand</label>
											{!! Form::select('brand_id', $brand,null, ['placeholder' => 'Choose Brand','class' => 'form-control','required'=>'required','id' => 'brand_id']) !!}
										</div>
										<div class="form-group">
											<label for="code" class="control-label mb-10">Code</label>
											{{ Form::text('code',null,['class'=>'form-control','placeholder'=>'Code','id'=>'code']) }}
										</div>
										<div class="form-group">
											<label for="status" class="control-label mb-10">Status</label>
											<select class="form-control" name="status">
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
										</div>
										<div class="form-group mb-0">
											<button type="submit" class="btn btn-primary btn-anim"><i class="icon-rocket"></i><span class="btn-text">submit</span></button>
										</div>
									{{ Form::close() }}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection