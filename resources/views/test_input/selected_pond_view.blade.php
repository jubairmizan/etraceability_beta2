<div class="col-md-9">
    {{ Form::select('pond_id',$ponds,null,['class'=>'form-control ponds','required','placeholder'=>'Select Pond','id'=>'pond_id']) }}
    <span class="help-block with-errors">{{$errors->first('pond_id')}}</span>
</div>