<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from hencework.com/theme/goofy/full-width-dark/index4.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 May 2018 17:16:28 GMT -->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>{{ $title }}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Data table CSS -->
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Toast CSS -->
    <link href="{{ asset('css/jquery.toast.min.css') }}" rel="stylesheet" type="text/css">

    <!-- bootstrap-select CSS -->
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Calendar CSS -->
    <link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap Datetimepicker CSS -->
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <!-- Data table JavaScript -->
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

    <style type="text/css">
        /* .side-nav li span,.side-nav li i,{color: white !important;} */
    </style>
</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-3-active pimary-color-blue">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="{{ route('admin.dashboard') }}">
                        <img class="brand-img" src="{{ asset('image/nav_logo.png') }}" alt="eTraceability"/>
                        <span class="brand-text">eTraceability</span>
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
        </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Main Content -->
    <div class="page-wrapper">

        @if($errors->any())
            @if($errors->any())
                <ul class="alert alert-danger fade in animated slideInRight alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"><i  class="fa fa-times" aria-hidden="true"></i></a>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
        @endif
        @if(Session::has('message'))
            <ul class="alert alert-success fade in animated slideInRight alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"><i  class="fa fa-times" aria-hidden="true"></i></a>
                <li>{{ Session::get("message") }}</li>
            </ul>
        @endif
        {{--set some message after action--}}
        @if (Session::has('message'))
            <script type="text/javascript">
                $(window).on("load",function(){
                    window.setTimeout(function(){
                        $.toast({
                            heading: '{{ Session::get("message") }}',
                            text: '',
                            position: 'bottom-left',
                            loaderBg:'#7BAB44',
                            icon: '',
                            hideAfter: 15000,
                            stack: 6
                        });
                    }, 1000);
                });
                /*****Load function* end*****/
            </script>
        @endif
        <div class="container-fluid">
            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Semi-Intensive Test Input</h5>
                </div>

                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        {{--<li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>--}}
                        {{--<li><a href="#"><span>Users</span></a></li>--}}
                        {{--<li class="active"><span>@if($create==1) Create @else Edit @endif</span></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->

            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default card-view">
                                            <div class="panel-wrapper collapse in">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-wrap">
                                                                {{ Form::open(['files'=>true, 'class'=>'form-horizontal','id'=>'insert_form']) }}
                                                                <div class="form-body">
                                                                    <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Data Entry for Sampling</h6>
                                                                    <hr class="light-grey-hr"/>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('user_id')?' has-error':''}}">
                                                                                {{ Form::label('date','Date',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    <div class='input-group date datetimepicker1'>
                                                                                        {{  Form::text('date',null,['class'=>'form-control','required']) }}
                                                                                        <span class="input-group-addon">
																							<span class="fa fa-calendar"></span>
																						</span>
                                                                                    </div>
                                                                                    <span class="help-block with-errors">{{$errors->first('date')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('farm_id')?' has-error':''}}">
                                                                                {{ Form::label('farm_id','Farm',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::select('farm_id',$farms,null,['class'=>'form-control','required','id'=>'farm_id']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('farm_id')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        {{--<div class="col-md-4">--}}
                                                                            {{--<div class="form-group{{$errors->has('pond_id')?' has-error':''}}">--}}
                                                                                {{--{{ Form::label('pond_id','Pond',['class'=>'control-label col-md-3']) }}--}}
                                                                                {{--<div id="selected_ponds">--}}
                                                                                    {{--<div class="col-md-9">--}}
                                                                                        {{--{{ Form::select('pond_id',$ponds,null,['class'=>'form-control ponds','required','placeholder'=>'Select Pond','id'=>'pond_id']) }}--}}
                                                                                        {{--<span class="help-block with-errors">{{$errors->first('pond_id')}}</span>--}}
                                                                                    {{--</div>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                        <!--/span-->
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <span id="feed_error"></span>
                                                                            <table class="table" id="feed_table">
                                                                                <tr>
                                                                                    <th>Sampling</th>
                                                                                    <th><button type="button" name="add" class="btn btn-success btn-sm sampling_add"><span class="glyphicon glyphicon-plus"></span></button></th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{{ Form::select('pond_id[]',$ponds,null,['class'=>'form-control col-md-3 pond_id','placeholder'=>'Select Pond']) }}</td>
                                                                                    <td>{{ Form::text('number_of_sample[]',null,['class'=>'form-control col-md-3 number_of_sample','placeholder'=>'No. of Sample']) }}</td>
                                                                                    <td>{{ Form::text('total_weight[]',null,['class'=>'form-control col-md-3 total_weight','placeholder'=>'Total Weight']) }}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    {{--<div class="row">--}}
                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group{{$errors->has('transparency')?' has-error':''}}">--}}
                                                                                {{--{{ Form::label('number_of_sample','No. of Sample',['class'=>'control-label col-md-3']) }}--}}
                                                                                {{--<div class="col-md-9">--}}
                                                                                    {{--{{ Form::text('number_of_sample',null,['class'=>'form-control','required','placeholder'=>'Enter No. of Sample','id'=>'number_of_sample']) }}--}}
                                                                                    {{--<span class="help-block with-errors">{{$errors->first('number_of_sample')}}</span>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group{{$errors->has('total_weight')?' has-error':''}}">--}}
                                                                                {{--{{ Form::label('total_weight','Total Weight',['class'=>'control-label col-md-3']) }}--}}
                                                                                {{--<div class="col-md-9">--}}
                                                                                    {{--{{ Form::text('total_weight',null,['class'=>'form-control','required','placeholder'=>'Enter Total Weight','id'=>'total_weight']) }}--}}
                                                                                    {{--<span class="help-block with-errors">{{$errors->first('total_weight')}}</span>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    {{--</div>--}}
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <!--/span-->
                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group{{$errors->has('current_biomass')?' has-error':''}}">--}}
                                                                                {{--{{ Form::label('current_biomass','Current Biomass',['class'=>'control-label col-md-3']) }}--}}
                                                                                {{--<div class="col-md-9">--}}
                                                                                    {{--{{ Form::text('current_biomass',null,['class'=>'form-control','placeholder'=>'Enter Current Biomass','id'=>'current_biomass']) }}--}}
                                                                                    {{--<span class="help-block with-errors">{{$errors->first('current_biomass')}}</span>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                        <!--/span-->
                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group{{$errors->has('average_body_weight')?' has-error':''}}">--}}
                                                                                {{--{{ Form::label('average_body_weight','Avg Body Weight',['class'=>'control-label col-md-3']) }}--}}
                                                                                {{--<div class="col-md-9">--}}
                                                                                    {{--{{ Form::text('average_body_weight',null,['class'=>'form-control','required','placeholder'=>'Enter Avg Body Weight','id'=>'average_body_weight']) }}--}}
                                                                                    {{--<span class="help-block with-errors">{{$errors->first('average_body_weight')}}</span>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                        <!--/span-->
                                                                    </div>
                                                                    <div class="row">
                                                                        <!--/span-->
                                                                        {{--<div class="col-md-6">--}}
                                                                            {{--<div class="form-group{{$errors->has('survival_rate')?' has-error':''}}">--}}
                                                                                {{--{{ Form::label('survival_rate','Survival Rate (%)',['class'=>'control-label col-md-3']) }}--}}
                                                                                {{--<div class="col-md-9">--}}
                                                                                    {{--{{ Form::text('survival_rate',null,['class'=>'form-control','placeholder'=>'Enter Survival Rate','id'=>'survival_rate']) }}--}}
                                                                                    {{--<span class="help-block with-errors">{{$errors->first('survival_rate')}}</span>--}}
                                                                                {{--</div>--}}
                                                                            {{--</div>--}}
                                                                        {{--</div>--}}
                                                                    </div>
                                                                    <!-- /Row -->
                                                                </div>
                                                                <div class="form-actions mt-10">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn btn-success  mr-10">Submit</button>
                                                                                    <button type="button" class="btn btn-default">Cancel</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6"> </div>
                                                                    </div>
                                                                </div>
                                                                {{ Form::close() }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>

        <!-- Footer -->
        <footer class="footer container-fluid pl-30 pr-30">
            <div class="row">
                <div class="col-sm-12">
                    <p><?php date('Y'); ?> &copy; 2018, eTraceability.</p>
                </div>
            </div>
        </footer>
        <!-- /Footer -->

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- Moment JavaScript -->
<script type="text/javascript" src="{{ asset('js/moment-with-locales.min.js') }}"></script>
<!-- Bootstrap Datetimepicker JavaScript -->
<script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<!-- Slimscroll JavaScript -->
<script src="{{ asset('js/jquery.slimscroll.js') }}"></script>

<!-- Calender JavaScripts -->
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar-data.js') }}"></script>

<!-- Switchery JavaScript -->
<script src="{{ asset('js/switchery.min.js') }}"></script>

<!-- Bootstrap Select JavaScript -->
<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>

<!-- Init JavaScript -->
<script src="{{ asset('js/init.js') }}"></script>

{{-- Validator --}}
<script type="text/javascript" src="{{ asset('js/validator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
<script>
    $(document).ready(function(){
        $('#farm_id').change(function(){
            $.ajax({
                method:'get',
                url:'{{route('get_pond_by_farm_id')}}',
                data:{'farm_id':$(this).val()}
            }).done(function(data){
                $('.ponds').remove();
                $('#selected_ponds').html(data);
            });
        });
        $(document).on('click', '.sampling_add', function(){
            var html = '';

            html += '<tr>';
            html += '<td>{{ Form::select('pond_id[]',$ponds,null,['class'=>'form-control col-md-3 pond_id','placeholder'=>'Select Pond']) }}</td>';
            html += '<td>{{ Form::text('number_of_sample[]',null,['class'=>'form-control col-md-3 number_of_sample','placeholder'=>'No. of Sample']) }}</td>';
            html += '<td>{{ Form::text('total_weight[]',null,['class'=>'form-control col-md-3 total_weight','placeholder'=>'Total Weight']) }}</td>';
            html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm sampling_remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
            $('#feed_table').append(html);
        });
        $(document).on('click', '.sampling_remove', function(){
            $(this).closest('tr').remove();
        });
        <!--$('#insert_form').on('submit',function(e){-->
        <!--e.preventDefault();-->
        <!--var feed_error = '';-->
        <!--var medicine_error = '';-->
        <!--$('.feed_code').each(function(){-->
        <!--var count = 1;-->
        <!--if($(this).val() == '')-->
        <!--{-->
//                    <!--feed_error += "<p>Select Feed at "+count+" Row</p>";-->
        <!--return false;-->
        <!--}-->
        <!--count = count + 1;-->
        <!--});-->
        <!--$('.feed_quantity').each(function(){-->
        <!--var count = 1;-->
        <!--if($(this).val() == '')-->
        <!--{-->
//                    <!--feed_error += "<p>Enter Feed Quantity at "+count+" Row</p>";-->
        <!--return false;-->
        <!--}-->
        <!--count = count + 1;-->
        <!--});-->
        <!--$('.medicine_code').each(function(){-->
        <!--var count = 1;-->
        <!--if($(this).val() == '')-->
        <!--{-->
//                    <!--medicine_error += "<p>Select Medicine at "+count+" Row</p>";-->
        <!--return false;-->
        <!--}-->
        <!--count = count + 1;-->
        <!--});-->
        <!--$('.medicine_quantity').each(function(){-->
        <!--var count = 1;-->
        <!--if($(this).val() == '')-->
        <!--{-->
//                    <!--medicine_error += "<p>Enter Medicine Quantity at "+count+" Row</p>";-->
        <!--return false;-->
        <!--}-->
        <!--count = count + 1;-->
        <!--});-->
        <!--var form_data = $(this).serialize();-->
        <!--if(feed_error == '' && medicine_error=='')-->
        <!--{-->
        <!--$.ajax({-->
        {{--                    <!--url:"{{route('test_input.store')}}",-->--}}
        <!--method:"post",-->
        <!--data:form_data,-->
        <!--success:function(data)-->
        <!--{-->
        <!--if(data == 'ok')-->
        <!--{-->
        <!--$('#item_table').find("tr:gt(0)").remove();-->
//                            <!--$('#error').html('<div class="alert alert-success">Item Details Saved</div>');-->
        <!--}-->
        <!--}-->
        <!--});-->
        <!--}-->
        <!--else if(feed_error!='')-->
        <!--{-->
//                <!--$('#feed_error').html('<div class="alert alert-danger">'+feed_error+'</div>');-->
        <!--}else if(medicine_error!=''){-->
//                <!--$('#medicine_error').html('<div class="alert alert-danger">'+medicine_error+'</div>');-->
        <!--}-->
        <!--});-->
    });
</script>
</body>
</html>
