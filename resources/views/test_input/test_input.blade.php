<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from hencework.com/theme/goofy/full-width-dark/index4.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 24 May 2018 17:16:28 GMT -->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>{{ $title }}</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Data table CSS -->
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Toast CSS -->
    <link href="{{ asset('css/jquery.toast.min.css') }}" rel="stylesheet" type="text/css">

    <!-- bootstrap-select CSS -->
    <link href="{{ asset('css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Calendar CSS -->
    <link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Bootstrap Datetimepicker CSS -->
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="{{ asset('css/theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>

    <!-- Data table JavaScript -->
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

    <style type="text/css">
        /* .side-nav li span,.side-nav li i,{color: white !important;} */
    </style>
</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-3-active pimary-color-blue">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="{{ route('admin.dashboard') }}">
                        <img class="brand-img" src="{{ asset('image/nav_logo.png') }}" alt="eTraceability"/>
                        <span class="brand-text">eTraceability</span>
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
        </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Main Content -->
    <div class="page-wrapper">

        @if($errors->any())
            @if($errors->any())
                <ul class="alert alert-danger fade in animated slideInRight alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"><i  class="fa fa-times" aria-hidden="true"></i></a>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
        @endif
        @if(Session::has('message'))
            <ul class="alert alert-success fade in animated slideInRight alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close"><i  class="fa fa-times" aria-hidden="true"></i></a>
                <li>{{ Session::get("message") }}</li>
            </ul>
        @endif
        {{--set some message after action--}}
        @if (Session::has('message'))
            <script type="text/javascript">
                $(window).on("load",function(){
                    window.setTimeout(function(){
                        $.toast({
                            heading: '{{ Session::get("message") }}',
                            text: '',
                            position: 'bottom-left',
                            loaderBg:'#7BAB44',
                            icon: '',
                            hideAfter: 15000,
                            stack: 6
                        });
                    }, 1000);
                });
                /*****Load function* end*****/
            </script>
        @endif
        <div class="container-fluid">
            <!-- Title -->
            <div class="row heading-bg">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h5 class="txt-dark">Semi-Intensive Test Input</h5>
                </div>

                <!-- Breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        {{--<li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>--}}
                        {{--<li><a href="#"><span>Users</span></a></li>--}}
                        {{--<li class="active"><span>@if($create==1) Create @else Edit @endif</span></li>--}}
                    </ol>
                </div>
                <!-- /Breadcrumb -->

            </div>
            <!-- /Title -->

            <!-- Row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default card-view">
                                            <div class="panel-wrapper collapse in">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-wrap">
                                                                {{ Form::open(['files'=>true, 'class'=>'form-horizontal','id'=>'insert_form']) }}
                                                                <div class="form-body">
                                                                    <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Data Entry for During Enclosure Time</h6>
                                                                    <hr class="light-grey-hr"/>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <!--/span-->
                                                                        <div class="col-md-4">
                                                                            <div class="form-group{{$errors->has('user_id')?' has-error':''}}">
                                                                                {{ Form::label('date','Date',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    <div class='input-group date datetimepicker1'>
                                                                                        {{  Form::text('date',null,['class'=>'form-control','required']) }}
                                                                                        <span class="input-group-addon">
																							<span class="fa fa-calendar"></span>
																						</span>
                                                                                    </div>
                                                                                    <span class="help-block with-errors">{{$errors->first('date')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group{{$errors->has('farm_id')?' has-error':''}}">
                                                                                {{ Form::label('farm_id','Farm',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::select('farm_id',$farms,null,['class'=>'form-control','required','id'=>'farm_id']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('farm_id')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-4">
                                                                            <div class="form-group{{$errors->has('pond_id')?' has-error':''}}">
                                                                                {{ Form::label('pond_id','Pond',['class'=>'control-label col-md-3']) }}
                                                                                <div id="selected_ponds">
                                                                                    <div class="col-md-9">
                                                                                        {{ Form::select('pond_id',$ponds,null,['class'=>'form-control ponds','required','placeholder'=>'Select Pond','id'=>'pond_id']) }}
                                                                                        <span class="help-block with-errors">{{$errors->first('pond_id')}}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <span id="feed_error"></span>
                                                                            <table class="table" id="feed_table">
                                                                                <tr>
                                                                                    <th>Feed</th>
                                                                                    <th><button type="button" name="add" class="btn btn-success btn-sm feed_add"><span class="glyphicon glyphicon-plus"></span></button></th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{{ Form::select('feed_code[]',$feeds,null,['class'=>'form-control col-md-3 feed_code']) }}</td>
                                                                                    <td>{{ Form::text('feed_quantity[]',null,['class'=>'form-control col-md-3 feed_quantity','placeholder'=>'Feed Quantity']) }}</td>
                                                                                    <td>{{ Form::select('feed_timing[]',$feed_timing,null,['class'=>'form-control col-md-3 feed_timing']) }}</td>
                                                                                    <td>{{ Form::text('remaining_feed_quantity[]',0,['class'=>'form-control col-md-3 remaining_feed_quantity','placeholder'=>'Remaining Quantity']) }}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <span id="medicine_error"></span>
                                                                            <table class="table" id="medicine_table">
                                                                                <tr>
                                                                                    <th>Medicine</th>
                                                                                    <th><button type="button" name="add" class="btn btn-success btn-sm medicine_add"><span class="glyphicon glyphicon-plus"></span></button></th>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>{{ Form::select('medicine_code[]',$medicines,null,['class'=>'form-control col-md-3 medicine_code']) }}</td>
                                                                                    <td>{{ Form::text('medicine_quantity[]',null,['class'=>'form-control col-md-3 medicine_quantity','placeholder'=>'Medicine Quantity']) }}</td>
                                                                                    <td>{{ Form::select('unit[]',$unit,null,['class'=>'form-control col-md-3 unit']) }}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" style="min-height: 65px;">
                                                                        <div class="col-md-6">
                                                                            {{ Form::label('dissolved_oxygen_am','Dissolved Oxygen (AM)',['class'=>'control-label col-md-4']) }}
                                                                            <div class="col-md-8">
                                                                            {{ Form::text('dissolved_oxygen_am',null,['class'=>'form-control','id'=>'dissolved_oxygen_am']) }}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            {{ Form::label('dissolved_oxygen_pm','Dissolved Oxygen (PM)',['class'=>'control-label col-md-4']) }}
                                                                            <div class="col-md-8">
                                                                            {{ Form::text('dissolved_oxygen_pm',null,['class'=>'form-control','id'=>'do2']) }}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" style="min-height: 65px;">
                                                                        <div class="col-md-6">
                                                                            {{ Form::label('ph_am','pH (AM)',['class'=>'control-label col-md-4']) }}
                                                                            <div class="col-md-8">
                                                                            {{ Form::text('ph_am',null,['class'=>'form-control','id'=>'ph_am']) }}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            {{ Form::label('ph_pm','pH (PM)',['class'=>'control-label col-md-4']) }}
                                                                            <div class="col-md-8">
                                                                            {{ Form::text('ph_pm',null,['class'=>'form-control','id'=>'ph_pm']) }}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <div class="row" style="min-height: 65px;">
                                                                        <div class="col-md-6">
                                                                            {{ Form::label('temperature_am','Temperature (AM)',['class'=>'control-label col-md-3']) }}
                                                                            <div class="col-md-8">
                                                                            {{ Form::text('temperature_am',null,['class'=>'form-control','id'=>'temperature_am']) }}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            {{ Form::label('temperature_pm','Temperature (PM)',['class'=>'control-label col-md-3']) }}
                                                                            <div class="col-md-8">
                                                                            {{ Form::text('temperature_pm',null,['class'=>'form-control','id'=>'temperature_pm']) }}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('transparency')?' has-error':''}}">
                                                                                {{ Form::label('transparency','Transparency',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('transparency',null,['class'=>'form-control','placeholder'=>'Enter Transparency','id'=>'transparency']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('transparency')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('alkanitity_hco3')?' has-error':''}}">
                                                                                {{ Form::label('alkanitity_hco3','HC03',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('alkanitity_hco3',null,['class'=>'form-control','placeholder'=>'Enter HC03','id'=>'alkanitity_hco3']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('alkanitity_hco3')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('carbonate_co3')?' has-error':''}}">
                                                                                {{ Form::label('carbonate_co3','C03',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('carbonate_co3',null,['class'=>'form-control','placeholder'=>'Enter C03','id'=>'carbonate_co3']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('carbonate_co3')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('calcium_ca')?' has-error':''}}">
                                                                                {{ Form::label('calcium_ca','Ca',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('calcium_ca',null,['class'=>'form-control','placeholder'=>'Enter Ca','id'=>'calcium_ca']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('calcium_ca')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('chlorine_cl2')?' has-error':''}}">
                                                                                {{ Form::label('chlorine_cl2','Cl2',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('chlorine_cl2',null,['class'=>'form-control','placeholder'=>'Enter Cl2','id'=>'chlorine_cl2']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('chlorine_cl2')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('magnesium_mg')?' has-error':''}}">
                                                                                {{ Form::label('magnesium_mg','Mg',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('magnesium_mg',null,['class'=>'form-control','placeholder'=>'Enter Mg','id'=>'magnesium_mg']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('magnesium_mg')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('green')?' has-error':''}}">
                                                                                {{ Form::label('green','Green',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('green',null,['class'=>'form-control','placeholder'=>'Enter Green','id'=>'green']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('green')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('yellow')?' has-error':''}}">
                                                                                {{ Form::label('yellow','Yellow',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('yellow',null,['class'=>'form-control','placeholder'=>'Enter Yellow','id'=>'yellow']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('yellow')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('vibrio_count')?' has-error':''}}">
                                                                                {{ Form::label('vibrio_count','Vibrio Count',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('vibrio_count',null,['class'=>'form-control','placeholder'=>'Enter Vibrio Count','id'=>'vibrio_count']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('vibrio_count')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('potassium_k')?' has-error':''}}">
                                                                                {{ Form::label('potassium_k','Potassium',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('potassium_k',null,['class'=>'form-control','placeholder'=>'Enter Potassium','id'=>'potassium_k']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('potassium_k')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('ammonia_nh3')?' has-error':''}}">
                                                                                {{ Form::label('ammonia_nh3','NH3',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('ammonia_nh3',null,['class'=>'form-control','placeholder'=>'Enter NH3','id'=>'ammonia_nh3']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('ammonia_nh3')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('nitrate_no3')?' has-error':''}}">
                                                                                {{ Form::label('nitrate_no3','NO3',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('nitrate_no3',null,['class'=>'form-control','placeholder'=>'Enter NO3','id'=>'nitrate_no3']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('nitrate_no3')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <div class="row">
                                                                        <!--/span-->
                                                                        <!--/span-->
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('ammonium_nh4')?' has-error':''}}">
                                                                                {{ Form::label('ammonium_nh4','NH4',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('ammonium_nh4',null,['class'=>'form-control','placeholder'=>'Enter NH4','id'=>'ammonium_nh4']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('ammonium_nh4')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('plankton_type')?' has-error':''}}">
                                                                                {{ Form::label('plankton_type','Plankton Type',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('plankton_type',null,['class'=>'form-control','placeholder'=>'Enter Plankton Type','id'=>'plankton_type']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('plankton_type')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--/span-->
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group{{$errors->has('salinity')?' has-error':''}}">
                                                                                {{ Form::label('salinity','Salinity',['class'=>'control-label col-md-3']) }}
                                                                                <div class="col-md-9">
                                                                                    {{ Form::text('salinity',null,['class'=>'form-control','placeholder'=>'Enter Salinity','id'=>'salinity']) }}
                                                                                    <span class="help-block with-errors">{{$errors->first('salinity')}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- /Row -->
                                                                    <!-- /Row -->
                                                                </div>
                                                                <div class="form-actions mt-10">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn btn-success  mr-10">Submit</button>
                                                                                    <button type="button" class="btn btn-default">Cancel</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6"> </div>
                                                                    </div>
                                                                </div>
                                                                {{ Form::close() }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Row -->
        </div>

        <!-- Footer -->
        <footer class="footer container-fluid pl-30 pr-30">
            <div class="row">
                <div class="col-sm-12">
                    <p><?php date('Y'); ?> &copy; 2018, eTraceability.</p>
                </div>
            </div>
        </footer>
        <!-- /Footer -->

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- Moment JavaScript -->
<script type="text/javascript" src="{{ asset('js/moment-with-locales.min.js') }}"></script>
<!-- Bootstrap Datetimepicker JavaScript -->
<script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<!-- Slimscroll JavaScript -->
<script src="{{ asset('js/jquery.slimscroll.js') }}"></script>

<!-- Calender JavaScripts -->
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar-data.js') }}"></script>

<!-- Switchery JavaScript -->
<script src="{{ asset('js/switchery.min.js') }}"></script>

<!-- Bootstrap Select JavaScript -->
<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>

<!-- Init JavaScript -->
<script src="{{ asset('js/init.js') }}"></script>

{{-- Validator --}}
<script type="text/javascript" src="{{ asset('js/validator.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
<script>
    $(document).ready(function(){
        var feed_timing_count=1;
        $('#farm_id').change(function(){
            $.ajax({
                method:'get',
                url:'{{route('get_pond_by_farm_id')}}',
                data:{'farm_id':$(this).val()}
            }).done(function(data){
                $('.ponds').remove();
                $('#selected_ponds').html(data);
            });
        });
        $(document).on('click', '.feed_add', function(){
            var html = '';
            feed_timing_count++;

            html += '<tr>';
            html += '<td>{{ Form::select('feed_code[]',$feeds,null,['class'=>'form-control col-md-3']) }}</td>';
            html += '<td>{{ Form::text('feed_quantity[]',null,['class'=>'form-control col-md-3','placeholder'=>'Feed Quantity']) }}</td>';
            if(feed_timing_count==2){
                html += '<td>{{ Form::select('feed_timing[]',$feed_timing,2,['class'=>'form-control col-md-3 feed_timing']) }}</td>';
            }else if(feed_timing_count==3){
                html += '<td>{{ Form::select('feed_timing[]',$feed_timing,3,['class'=>'form-control col-md-3 feed_timing']) }}</td>';
            }else if(feed_timing_count==4){
                html += '<td>{{ Form::select('feed_timing[]',$feed_timing,4,['class'=>'form-control col-md-3 feed_timing']) }}</td>';
            }else{
                html += '<td>{{ Form::select('feed_timing[]',$feed_timing,null,['class'=>'form-control col-md-3 feed_timing']) }}</td>';
            }
            html += '<td>{{ Form::text('remaining_feed_quantity[]',0,['class'=>'form-control col-md-3 remaining_feed_quantity','placeholder'=>'Remaining Quantity']) }}</td>';
            html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm feed_remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
            $('#feed_table').append(html);
        });
        $(document).on('click', '.feed_remove', function(){
            $(this).closest('tr').remove();
            feed_timing_count--;
            if(feed_timing_count<0){
                feed_timing_count==0;
            }
        });
        $(document).on('click', '.medicine_add', function(){
            var html = '';
            html += '<tr>';
            html += '<td>{{ Form::select('medicine_code[]',$medicines,null,['class'=>'form-control col-md-3']) }}</td>';
            html += '<td>{{ Form::text('medicine_quantity[]',null,['class'=>'form-control col-md-3','placeholder'=>'Medicine Quantity']) }}</td>';
            html += '<td>{{ Form::select('unit[]',$unit,null,['class'=>'form-control col-md-3 unit']) }}</td>';
            html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm medicine_remove"><span class="glyphicon glyphicon-minus"></span></button></td></tr>';
            $('#medicine_table').append(html);
        });
        $(document).on('click', '.medicine_remove', function(){
            $(this).closest('tr').remove();
        });
        <!--$('#insert_form').on('submit',function(e){-->
        <!--e.preventDefault();-->
        <!--var feed_error = '';-->
        <!--var medicine_error = '';-->
        <!--$('.feed_code').each(function(){-->
        <!--var count = 1;-->
        <!--if($(this).val() == '')-->
        <!--{-->
//                    <!--feed_error += "<p>Select Feed at "+count+" Row</p>";-->
        <!--return false;-->
        <!--}-->
        <!--count = count + 1;-->
        <!--});-->
        <!--$('.feed_quantity').each(function(){-->
        <!--var count = 1;-->
        <!--if($(this).val() == '')-->
        <!--{-->
//                    <!--feed_error += "<p>Enter Feed Quantity at "+count+" Row</p>";-->
        <!--return false;-->
        <!--}-->
        <!--count = count + 1;-->
        <!--});-->
        <!--$('.medicine_code').each(function(){-->
        <!--var count = 1;-->
        <!--if($(this).val() == '')-->
        <!--{-->
//                    <!--medicine_error += "<p>Select Medicine at "+count+" Row</p>";-->
        <!--return false;-->
        <!--}-->
        <!--count = count + 1;-->
        <!--});-->
        <!--$('.medicine_quantity').each(function(){-->
        <!--var count = 1;-->
        <!--if($(this).val() == '')-->
        <!--{-->
//                    <!--medicine_error += "<p>Enter Medicine Quantity at "+count+" Row</p>";-->
        <!--return false;-->
        <!--}-->
        <!--count = count + 1;-->
        <!--});-->
        <!--var form_data = $(this).serialize();-->
        <!--if(feed_error == '' && medicine_error=='')-->
        <!--{-->
        <!--$.ajax({-->
        {{--                    <!--url:"{{route('test_input.store')}}",-->--}}
        <!--method:"post",-->
        <!--data:form_data,-->
        <!--success:function(data)-->
        <!--{-->
        <!--if(data == 'ok')-->
        <!--{-->
        <!--$('#item_table').find("tr:gt(0)").remove();-->
//                            <!--$('#error').html('<div class="alert alert-success">Item Details Saved</div>');-->
        <!--}-->
        <!--}-->
        <!--});-->
        <!--}-->
        <!--else if(feed_error!='')-->
        <!--{-->
//                <!--$('#feed_error').html('<div class="alert alert-danger">'+feed_error+'</div>');-->
        <!--}else if(medicine_error!=''){-->
//                <!--$('#medicine_error').html('<div class="alert alert-danger">'+medicine_error+'</div>');-->
        <!--}-->
        <!--});-->
    });
</script>
</body>
</html>
