@extends('layouts.shrimp_culture.client.master')
@section('main-content')
    <div class="container-fluid pt-25">
        <!-- Row -->
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body sm-data-box-1">
                            <span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total Invest</span>
                            <div class="cus-sat-stat weight-500 txt-primary text-center mt-5">
                                <span class="counter-anim">94000</span>
                            </div>
                            <div class="progress-anim mt-20">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary
									wow animated progress-animated" role="progressbar" aria-valuenow="93.12" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <ul class="flex-stat mt-5">
                                <li>
                                    <span class="block">Feeds</span>
                                    <span class="block weight-500 txt-dark font-15">23200</span>
                                </li>
                                <li>
                                    <span class="block">Probiotics</span>
                                    <span class="block weight-500 txt-dark font-15">30000</span>
                                </li>
                                <li>
                                    <span class="block">Others</span>
                                    <span class="block weight-500 txt-dark font-15">40800</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="col-md-3">--}}
            {{--<div class="panel panel-default card-view">--}}
            {{--<div class="panel-wrapper collapse in">--}}
            {{--<div class="panel-body sm-data-box-1">--}}
            {{--<span class="uppercase-font weight-500 font-14 block text-center txt-dark">Total Revenue</span>--}}
            {{--<div class="cus-sat-stat weight-500 txt-primary text-center mt-5">--}}
            {{--<span class="counter-anim">120000</span>--}}
            {{--</div>--}}
            {{--<div class="progress-anim mt-20">--}}
            {{--<div class="progress">--}}
            {{--<div class="progress-bar progress-bar-primary--}}
            {{--wow animated progress-animated" role="progressbar" aria-valuenow="93.12" aria-valuemin="0" aria-valuemax="100"></div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<ul class="flex-stat mt-5">--}}
            {{--<li>--}}
            {{--<span class="block">Ruhi</span>--}}
            {{--<span class="block weight-500 txt-dark font-15">40000</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<span class="block">Pangash</span>--}}
            {{--<span class="block weight-500 txt-dark font-15">30000</span>--}}
            {{--</li>--}}
            {{--<li>--}}
            {{--<span class="block">Silver Corpe</span>--}}
            {{--<span class="block weight-500 txt-dark font-15">50000</span>--}}
            {{--</li>--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <div class="panel panel-default card-view">
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Last 7 Days Total Invest</h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div  class="panel-body">
                            <span class="font-12 head-font txt-dark">Feed <span class="pull-right">20000</span></span>
                            <div class="progress mt-10 mb-30">
                                <div class="progress-bar progress-bar-info" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%" role="progressbar"> <span class="sr-only">85% Complete (success)</span> </div>
                            </div>
                            <span class="font-12 head-font txt-dark">Probiotics<span class="pull-right">10000</span></span>
                            <div class="progress mt-10 mb-30">
                                <div class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 80%" role="progressbar"> <span class="sr-only">85% Complete (success)</span> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-4 col-sm-5 col-xs-12">
                <div class="panel card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body row pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-4 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="weight-500 uppercase-font block">Hatchery</span>
                                            <span class="block counter txt-dark"><span class="counter-anim">1</span></span>
                                        </div>
                                        <div class="col-xs-4 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="weight-500 uppercase-font block">Semi-Intensive Ponds</span>
                                            <span class="block counter txt-dark"><span class="counter-anim">18</span></span>
                                        </div>
                                        <div class="col-xs-4 text-center pl-0 pr-0 data-wrap-right">
                                            <span class="weight-500 uppercase-font block">Extensive Ponds</span>
                                            <span class="block counter txt-dark"><span class="counter-anim">19</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body row pa-0">
                            <div class="sm-data-box">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                            <span class="weight-500 uppercase-font block">Feeds</span>
                                            <span class="block counter txt-dark"><span class="counter-anim">20</span></span>
                                        </div>
                                        <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                            <span class="weight-500 uppercase-font block">Probiotics</span>
                                            <span class="block counter txt-dark"><span class="counter-anim">33</span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            {{-- Pond Cost --}}

            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default card-view panel-refresh">
                    <div class="refresh-container">
                        <div class="la-anim-1"></div>
                    </div>
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Pond Invest</h6>
                        </div>
                        <div class="pull-right">
                            <a href="#" class="pull-left inline-block refresh">
                                <i class="zmdi zmdi-replay"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div id="pond_invest" class="" style="height:330px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default card-view panel-refresh">
                    <div class="refresh-container">
                        <div class="la-anim-1"></div>
                    </div>
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Pond Revenue</h6>
                        </div>
                        <div class="pull-right">
                            <a href="#" class="pull-left inline-block refresh">
                                <i class="zmdi zmdi-replay"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div id="e_chart_2" class="" style="height:330px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view panel-refresh">
                    <div class="refresh-container">
                        <div class="la-anim-1"></div>
                    </div>
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Ponds Invest</h6>
                        </div>
                        <div class="pull-right">
                            <a href="#" class="pull-left inline-block refresh">
                                <i class="zmdi zmdi-replay"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div id="pondsInvest" class="" style="height:294px;"></div>
                            <div class="label-chatrs mt-15">
                                <div class="inline-block mr-15">
                                    <span class="clabels inline-block bg-blue mr-5" style="background-color: #0FC5BB !important"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 1</span>
                                </div>
                                <div class="inline-block  mr-15">
                                    <span class="clabels inline-block bg-light-blue mr-5" style="background-color: #92F2EF !important"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 2</span>
                                </div>
                                <div class="inline-block  mr-15">
                                    <span class="clabels inline-block bg-light-blue mr-5" style="background-color: #1B76BC !important"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 3</span>
                                </div>
                                <div class="inline-block  mr-15">
                                    <span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2B3991 !important"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 4</span>
                                </div>
                                <div class="inline-block  mr-15">
                                    <span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2CB474 !important"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 5</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="panel panel-default card-view panel-refresh">
                    <div class="refresh-container">
                        <div class="la-anim-1"></div>
                    </div>
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Ponds Revenue</h6>
                        </div>
                        <div class="pull-right">
                            <a href="#" class="pull-left inline-block refresh">
                                <i class="zmdi zmdi-replay"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div id="pondsRevenue" class="" style="height:294px;"></div>
                            <div class="label-chatrs mt-15">
                                <div class="inline-block mr-15">
                                    <span class="clabels inline-block bg-blue mr-5" style="background-color: #0FC5BB !important"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 1</span>
                                </div>
                                <div class="inline-block  mr-15">
                                    <span class="clabels inline-block bg-light-blue mr-5" style="background-color: #92F2EF !important"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 2</span>
                                </div>
                                <div class="inline-block  mr-15">
                                    <span class="clabels inline-block bg-light-blue mr-5" style="background-color: #1B76BC !important"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 3</span>
                                </div>
                                <div class="inline-block  mr-15">
                                    <span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2B3991 !important"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 4</span>
                                </div>
                                <div class="inline-block  mr-15">
                                    <span class="clabels inline-block bg-light-blue mr-5" style="background-color: #2CB474 !important"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Pond 5</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <iframe src="https://www.google.com/maps/d/embed?mid=1QElAMwhfDO0frK8O0yzfr7GkHr4_LPlc" width="1250" height="650"></iframe>
        </div>
        <!-- /Row -->
    </div>
@endsection