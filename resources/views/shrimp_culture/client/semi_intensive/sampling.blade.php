@extends('layouts.shrimp_culture.client.master')
@section('main_content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            {{--<div class="col-md-12 col-xs-12 page_header">--}}
                                {{--<h3>{{$title}}</h3>--}}
                            {{--</div>--}}
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h4 class="txt-dark">{{$title}}</h4>
                            </div>
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                <ol class="breadcrumb">
                                    <li><a href="{{ route('shrimp_culture.client.dashboard') }}">Dashboard</a></li>
                                    <li><a href="#">Semi-Intensive Farms</a></li>
                                    <li><a href="{{route('shrimp_culture.client.semi_intensive.sampling')}}"><span>Sampling</span></a></li>
                                </ol>
                            </div>
                            <div class="col-md-12" style="margin-top: 12px">
                                <div class="row">
                                    {{ Form::open(['method'=>'get']) }}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('farm_id','Select Farm',['class'=>'control-label','style'=>'margin-bottom:5px']) }}
                                            {{ Form::select('farm_id',$farms,$farm_id,['class'=>'form-control','required','placeholder'=>'Select Farm','id'=>'farm_id']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('pond_id','Select Pond',['class'=>'control-label','style'=>'margin-bottom:5px']) }}
                                            {{ Form::select('pond_id',$ponds,$pond_id,['class'=>'form-control','required','placeholder'=>'Select Pond','id'=>'pond_id']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            {{ Form::label('submit_button','Select Pond',['class'=>'control-label hidden_text']) }}
                                            {{ Form::submit('See the Information',['class'=>'form-control btn btn-primary','id'=>'see_pond_info']) }}
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="example" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Total Weight (G)</th>
                                        <th>Number of Sample</th>
                                        <th>Average Body Weight (G)</th>
                                        <th>Current Biomass (KG)</th>
                                        <th>Survival Rate (%)</th>
                                        <th>Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $sl = 1; ?>
                                    @if(count($animal_sampling)>0)
                                        @foreach($animal_sampling as $value)
                                            <tr>
                                                <td>{{ $sl }}</td>
                                                <td>{{ $value->total_weight }}</td>
                                                <td>{{ $value->number_of_sample }}</td>
                                                <td>{{ $value->average_body_weight }}</td>
                                                <td>{{ $value->current_biomass }}</td>
                                                <td>{{ $value->survival_rate }}</td>
                                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$value->created_at)->format('F d, Y') }}</td>
                                            </tr>
                                            <?php $sl++; ?>
                                        @endforeach
                                    @else
                                        <tr class="warning"><td colspan="7" align="center">No information found</td></tr>
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>SL</th>
                                        <th>Total Weight (G)</th>
                                        <th>Number of Sample</th>
                                        <th>Average Body Weight (G)</th>
                                        <th>Current Biomass (KG)</th>
                                        <th>Survival Rate (%)</th>
                                        <th>Date</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection