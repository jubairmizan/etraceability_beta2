<div class="previous_pond_info">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th colspan="4">Pond's Basic Information</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th>Pond Name</th>
            <td>Dimension</td>
            <td>Survival Rate</td>
            <td>Stocked PL</td>
        </tr>
        <tr>
            <th>{{isset($pond_info->name)?$pond_info->name:''}} (<a target="_blank" href="https://www.google.com/maps/d/u/0/viewer?ll=20.975850070853237%2C92.25990836779442&z=17&mid=1M17MO6eCTuYJj1ESK8iRiWoeryAzwfiI">See in Google Maps</a>)</th>
            <td>{{isset($pond_info->area)?$pond_info->area.' Feet':''}}</td>
            <td>{{isset($pond_info->last_sampling_data)?$pond_info->last_sampling_data->survival_rate:'0.00'}}%</td>
            <td>{{isset($pond_info->last_sampling_data)?$pond_info->last_sampling_data->stocked_pl_quantity:'0'}}</td>
        </tr>
        <tr>
            <th>Average Body Weight</th>
            <td>Current Biomass</td>
            <td>Staffs</td>
            <td colspan="2">Aerators</td>
        </tr>
        <tr>
            <th>{{isset($pond_info->last_sampling_data)?$pond_info->last_sampling_data->average_body_weight:'0.00'}} G</th>
            <td>{{isset($pond_info->last_sampling_data)?$pond_info->last_sampling_data->current_biomass:'0.00'}} KG</td>
            <td>{{isset($pond_info->last_staffs_data)?$pond_info->last_staffs_data->no_of_staff:'0'}}</td>
            <td>No information found</td>
        </tr>
        </tbody>
    </table>
    <div id="dashboardChart" class="" style="height:294px;margin-top: 25px;margin-bottom: 25px"></div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th colspan="8">Today's Inputs and Parameters</th>
        </tr>
        </thead>
        @if(count($pond_management_today)<=0)
            <tbody>
            <tr><td colspan="8">No information found</td></tr>
            </tbody>
        @else
            <thead>
            <tr>
                <th colspan="4">Feeds</th>
                <th colspan="4">Probiotics, Minerals and Others</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th colspan="2">Feeds' Details</th>
                <th colspan="2">Total Feeds</th>
                <th colspan="4">Probiotics, Minerals and Others' Details</th>
            </tr>
            <tr>
                <td colspan="2">
                    @php $feed_count=0; @endphp
                    @foreach($pond_management_today->inventory_used_in_water_details as $inventory)
                        @if($inventory->inventory['type_id']==1)
                            {{$inventory->inventory['name']}}: {{$inventory->quantity}} KG<br>
                            @php $feed_count+=$inventory->quantity; @endphp
                        @endif
                    @endforeach
                </td>
                <td colspan="2">{{$feed_count}} KG</td>
                <td colspan="4">
                    @foreach($pond_management_today->inventory_used_in_water_details as $inventory)
                        @if($inventory->inventory['type_id']!=1)
                            {{$inventory->inventory['name']}}: {{$inventory->quantity}} KG<br>
                        @endif
                    @endforeach
                </td>
            </tr>
            </tbody>
            <thead>
            </thead>
            <thead>
            <tr>
                <th colspan="2">pH</th>
                <th colspan="4">Dissolved Oxygen</th>
                <th colspan="2">Temperature</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th>AM</th>
                <th>PM</th>
                <th colspan="2">AM</th>
                <th colspan="2">PM</th>
                <th>AM</th>
                <th>PM</th>
            </tr>
            <tr>
                @if(count($pond_management_today->ph_details)>0)
                @foreach($pond_management_today->ph_details as $ph)
                    @if($ph->timing=='am')
                        <td>{{$ph->quantity}}</td>
                    @else
                        <td>{{$ph->quantity}}</td>
                    @endif
                @endforeach
                @endif
                @if(count($pond_management_today->dissolved_oxygen_details)>0)
                @foreach($pond_management_today->dissolved_oxygen_details as $dissolved_oxygen)
                    @if($dissolved_oxygen->timing=='am')
                        <td colspan="2">{{$dissolved_oxygen->quantity}}</td>
                    @else
                        <td colspan="2">{{$dissolved_oxygen->quantity}}</td>
                    @endif
                @endforeach
                @endif
                @if(count($pond_management_today->temperature_details)>0)
                @foreach($pond_management_today->temperature_details as $temperature)
                    @if($temperature->timing=='am')
                        <td>{{$temperature->quantity}}</td>
                    @else
                        <td>{{$temperature->quantity}}</td>
                    @endif
                @endforeach
                @endif
            </tr>
            <tr>
                <th>Alkanity</th>
                <th>Calcium</th>
                <th>Chlorine</th>
                <th>Magnesium</th>
                <th>Potassium</th>
                <th>Ammonia</th>
                <th>Nitrate</th>
                <th>Ammonium</th>
            </tr>
            <tr>
                @foreach($pond_management_today->water_parameter_details as $water_parameter)
                    <td>{{$water_parameter->alkalinity_hco3?$water_parameter->alkalinity_hco3:'--'}}</td>
                    <td>{{$water_parameter->calcium_ca?$water_parameter->calcium_ca:'--'}}</td>
                    <td>{{$water_parameter->chlorine_cl2?$water_parameter->chlorine_cl2:'--'}}</td>
                    <td>{{$water_parameter->magnesium_mg?$water_parameter->magnesium_mg:'--'}}</td>
                    <td>{{$water_parameter->potassium_k?$water_parameter->potassium_k:'--'}}</td>
                    <td>{{$water_parameter->ammonia_nh3?$water_parameter->ammonia_nh3:'--'}}</td>
                    <td>{{$water_parameter->nitrate_no3?$water_parameter->nitrate_no3:'--'}}</td>
                    <td>{{$water_parameter->ammonium_nh4?$water_parameter->ammonium_nh4:'--'}}</td>
                @endforeach
            </tr>
            <tr>
                <th>Salinity</th>
                <th colspan="2">Transparency</th>
                <th>Plankton Type</th>
                <th>Green</th>
                <th>Yellow</th>
                <th colspan="2">Vibrio Count</th>
            </tr>
            <tr>
                @foreach($pond_management_today->water_parameter_details as $water_parameter)
                    <td>{{$water_parameter->salinity?$water_parameter->salinity:'--'}}</td>
                    <td colspan="2">{{$water_parameter->transparency?$water_parameter->transparency:'--'}}</td>
                    <td>{{$water_parameter->plankton_type?$water_parameter->plankton_type:'--'}}</td>
                    <td>{{$water_parameter->green?$water_parameter->green:'--'}}</td>
                    <td>{{$water_parameter->yellow?$water_parameter->yellow:'--'}}</td>
                    <td colspan="2">{{$water_parameter->vibrio_count?$water_parameter->vibrio_count:'--'}}</td>
                @endforeach
            </tr>
            </tbody>
        @endif
    </table>
</div>
<script>
    if( $('#dashboardChart').length > 0 ){
        var eChart_3 = echarts.init(document.getElementById('dashboardChart'));
        var colors = ['#0FC5BB', '#92F2EF','#337AFF'];
        {{--var DateData=[];--}}
        {{--var FeedData=[];--}}
        {{--var BiomassData=[];--}}
        {{--var FCRData=[];--}}
        {{--@foreach($graph_date_data as $date_data)--}}
            {{--DateData.push('{{$date_data}}');--}}
        {{--@endforeach--}}
        {{--@foreach($graph_feed_data as $feed_data)--}}
            {{--FeedData.push('{{$feed_data}}');--}}
        {{--@endforeach--}}
        {{--@foreach($graph_biomass_data as $biomass_data)--}}
            {{--BiomassData.push('{{$biomass_data}}');--}}
        {{--@endforeach--}}
         {{--@foreach($graph_fcr_data as $fcr_data)--}}
            {{--FCRData.push('{{$fcr_data}}');--}}
         {{--@endforeach--}}
        var option = {
            title : {
                text: 'Feed Consumption and Biomass Calculation with FCR',
                subtext: '{{$pond_info->farm->name}}: Pond {{$pond_info->name}}',
                x: 'center',
                align: 'right'
            },
            color: colors,
            grid: {
                bottom: 80
            },
            toolbox: {
                feature: {
//                    dataZoom: {
//                        yAxisIndex: 'none'
//                    },
                    restore: {},
                    saveAsImage: {}
                }
            },
            tooltip : {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    animation: false,
                    label: {
                        backgroundColor: '#505765'
                    }
                }
            },
            legend: {
                data:['BM','DF','FCR'],
                x: 'left'
            },
            dataZoom: [
                {
                    show: true,
                    realtime: true,
                    start: 65,
                    end: 85
                },
                {
                    type: 'inside',
                    realtime: true,
                    start: 65,
                    end: 85
                }
            ],
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    axisLine: {onZero: false},
                    data : []
                }
            ],
            yAxis: [
                {
                    name: 'KG',
                    type: 'value',
//                    max: 500
                },
                {
                    name: 'Range 2.0',
//                    nameLocation: 'start',
                    max: 2,
                    type: 'value'
//                        inverse: true
                }
            ],
            series: [
                {
                    name:'BM',
                    type:'line',
                    animation: false,
                    areaStyle: {
                    },
                    lineStyle: {
                        width: 1
                    },
                        markArea: {
                            silent: true,
//                            data: [[{
//                                xAxis: '2009/9/12\n7:00'
//                            }, {
//                                xAxis: '2009/9/22\n7:00'
//                            }]]
                        },
                    data: []
                },
                {
                    name:'DF',
                    type:'line',
//                        yAxisIndex:1,
                    animation: false,
                    areaStyle: {
                    },
                    lineStyle: {
                        width: 1
                    },
                        markArea: {
                            silent: true,
//                            data: [[{
//                                xAxis: '2009/9/10\n7:00'
//                            }, {
//                                xAxis: '2009/9/20\n7:00'
//                            }]]
                        },
                    data: []
                },
                {
                    name:'FCR',
                    type:'line',
                    yAxisIndex:1,
                    animation: false,
                    areaStyle: {
                    },
                    lineStyle: {
                        width: 1
                    },
                        markArea: {
                            silent: true,
//                            data: [[{
//                                xAxis: '2009/9/10\n7:00'
//                            }, {
//                                xAxis: '2009/9/20\n7:00'
//                            }]]
                        },
                    data: []
                }
            ]
        };


        eChart_3.setOption(option);
        eChart_3.resize();
    }
</script>