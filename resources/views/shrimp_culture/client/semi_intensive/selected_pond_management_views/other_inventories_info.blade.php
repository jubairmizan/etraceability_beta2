<table id="other_inventories_datatable" class="table table-hover display pb-30 previous_other_inventories_info">
    <thead>
    <tr>
        <th>Inventory</th>
        <th>Quantity</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    @if(count($other_inventories_info)>0)
        @foreach($other_inventories_info as $other_inventories)
            <tr>
                <td>{{$other_inventories->name}}</td>
                <td>{{$other_inventories->quantity}} {{$other_inventories->unit}}</td>
                <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$other_inventories->created_at)->format('F d, Y | g:i A')}}</td>
            </tr>
        @endforeach
    @else
        <tr class="warning"><td colspan="3">No information found</td></tr>
    @endif
    </tbody>
    <tfoot>
    <tr>
        <th>Inventory</th>
        <th>Quantity</th>
        <th>Date</th>
    </tr>
    </tfoot>
</table>
<script>
    $('#other_inventories_datatable').DataTable();
</script>