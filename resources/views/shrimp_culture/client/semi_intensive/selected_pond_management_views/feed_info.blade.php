<table id="feed_datatable" class="table table-hover display pb-30 previous_feed_info">
    <thead>
    <tr>
        <th>Feed Name</th>
        <th>Timing</th>
        <th>Quantity</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    @if(count($feed_info)>0)
        @foreach($feed_info as $feed)
            <tr>
                <td>{{$feed->name}}</td>
                <td>{{$feed->timing}}</td>
                <td>{{$feed->quantity}} {{$feed->unit}}</td>
                <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$feed->created_at)->format('F d, Y | g:i A')}}</td>
            </tr>
        @endforeach
    @else
        <tr class="warning"><td colspan="4">No information found</td></tr>
    @endif
    </tbody>
    <tfoot>
    <tr>
        <th>Feed Name</th>
        <th>Timing</th>
        <th>Quantity</th>
        <th>Date</th>
    </tr>
    </tfoot>
</table>
<script>
    $('#feed_datatable').DataTable();
</script>