@if(count($monitoring_parameter_info)>0)
    <table id="monitoring_parameter_datatable" class="table table-hover display pb-30 previous_monitoring_parameter_info">
        @if(!is_null($parameter_selection))
            @if($parameter_selection==0)
                <thead>
                <tr>
                    <th>pH Quantity</th>
                    <th>Timing</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @if(count($monitoring_parameter_info)>0)
                    @foreach($monitoring_parameter_info as $monitoring_parameter)
                        <tr>
                            <td>{{$monitoring_parameter->quantity}}</td>
                            <td>{{$monitoring_parameter->timing}}</td>
                            <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$monitoring_parameter->created_at)->format('F d, Y | g:i A')}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr class="warning"><td colspan="3">No information found</td></tr>
                @endif
                </tbody>
                <tfoot>
                <tr>
                    <th>pH Quantity</th>
                    <th>Timing</th>
                    <th>Date</th>
                </tr>
                </tfoot>
            @elseif($parameter_selection==1)
                <thead>
                <tr>
                    <th>Dissolved Oxygen Quantity</th>
                    <th>Timing</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @if(count($monitoring_parameter_info)>0)
                    @foreach($monitoring_parameter_info as $monitoring_parameter)
                        <tr>
                            <td>{{$monitoring_parameter->quantity}}</td>
                            <td>{{$monitoring_parameter->timing}}</td>
                            <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$monitoring_parameter->created_at)->format('F d, Y | g:i A')}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr class="warning"><td colspan="3">No information found</td></tr>
                @endif
                </tbody>
                <tfoot>
                <tr>
                    <th>Dissolved Oxygen Quantity</th>
                    <th>Timing</th>
                    <th>Date</th>
                </tr>
                </tfoot>
            @elseif($parameter_selection==2)
                <thead>
                <tr>
                    <th>Temperature Quantity</th>
                    <th>Timing</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @if(count($monitoring_parameter_info)>0)
                    @foreach($monitoring_parameter_info as $monitoring_parameter)
                        <tr>
                            <td>{{$monitoring_parameter->quantity}}</td>
                            <td>{{$monitoring_parameter->timing}}</td>
                            <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$monitoring_parameter->created_at)->format('F d, Y | g:i A')}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr class="warning"><td colspan="3">No information found</td></tr>
                @endif
                </tbody>
                <tfoot>
                <tr>
                    <th>Temperature Quantity</th>
                    <th>Timing</th>
                    <th>Date</th>
                </tr>
                </tfoot>
            @elseif($parameter_selection==3)
                <thead>
                <tr>
                    <th>Alkalinity (HCO3)</th>
                    <th>Carbonate (CO3)</th>
                    <th>Calcium (Ca)</th>
                    <th>Chlorine (Cl2)</th>
                    <th>Magnesium (Mg)</th>
                    <th>Potassium (K)</th>
                    <th>Ammonia (NH3)</th>
                    <th>Nitrate (NO3)</th>
                    <th>Ammonium (NH4)</th>
                    <th>Salinity</th>
                    <th>Transparency</th>
                    <th>Plankton Type</th>
                    <th>Green</th>
                    <th>Yellow</th>
                    <th>Vibrio Count</th>
                    <th>Date</th>
                </tr>
                </thead>
                <tbody>
                @if(count($monitoring_parameter_info)>0)
                    @foreach($monitoring_parameter_info as $monitoring_parameter)
                        <tr>
                            <td>{{$monitoring_parameter->alkalinity_hco3}}</td>
                            <td>{{$monitoring_parameter->carbonate_co3}}</td>
                            <td>{{$monitoring_parameter->calcium_ca}}</td>
                            <td>{{$monitoring_parameter->chlorine_cl2}}</td>
                            <td>{{$monitoring_parameter->magnesium_mg}}</td>
                            <td>{{$monitoring_parameter->potassium_k}}</td>
                            <td>{{$monitoring_parameter->ammonia_nh3}}</td>
                            <td>{{$monitoring_parameter->nitrate_no3}}</td>
                            <td>{{$monitoring_parameter->ammonium_nh4}}</td>
                            <td>{{$monitoring_parameter->salinity}}</td>
                            <td>{{$monitoring_parameter->transparency}}</td>
                            <td>{{$monitoring_parameter->plankton_type}}</td>
                            <td>{{$monitoring_parameter->green}}</td>
                            <td>{{$monitoring_parameter->yellow}}</td>
                            <td>{{$monitoring_parameter->vibrio_count}}</td>
                            <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$monitoring_parameter->created_at)->format('F d, Y | g:i A')}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr class="warning"><td colspan="16">No information found</td></tr>
                @endif
                </tbody>
                <tfoot>
                <tr>
                    <th>Alkalinity (HCO3)</th>
                    <th>Carbonate (CO3)</th>
                    <th>Calcium (Ca)</th>
                    <th>Chlorine (Cl2)</th>
                    <th>Magnesium (Mg)</th>
                    <th>Potassium (K)</th>
                    <th>Ammonia (NH3)</th>
                    <th>Nitrate (NO3)</th>
                    <th>Ammonium (NH4)</th>
                    <th>Salinity</th>
                    <th>Transparency</th>
                    <th>Plankton Type</th>
                    <th>Green</th>
                    <th>Yellow</th>
                    <th>Vibrio Count</th>
                    <th>Date</th>
                </tr>
                </tfoot>
            @endif
        @endif
    </table>
    <script>
        $('#monitoring_parameter_datatable').DataTable({

        });
    </script>
@endif