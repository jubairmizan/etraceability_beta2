<script>
    $(document).ready(function () {
        $('#see_pond_info').click(function (e) {
            e.preventDefault();
            $('#loading_data').show();
            $.ajax({
                type:'get',
                url:'{{route('shrimp_culture.client.semi_intensive.get_pond_info')}}',
                data:{farm_id:$('#farm_id').val(),pond_id:$('#pond_id').val()}
            }).done(function (data) {
                console.log(data);
                $('#pond_overview_tab').click();
                $('#loading_data').hide();
                $('.pond_no_info').remove();
                $('.previous_pond_info').remove();
                $('.dataTables_wrapper').remove();
                $('#pond_basic_information').append(data);
            }).fail(function (data) {
                
            });
        });
        $('#feed_tab').click(function (e) {
            e.preventDefault();
            $('#loading_data').show();
            $.ajax({
                type:'get',
                url:'{{route('shrimp_culture.client.semi_intensive.get_feed_info')}}',
                data:{farm_id:$('#farm_id').val(),pond_id:$('#pond_id').val()}
            }).done(function (data) {
                $('#loading_data').hide();
//                $('.pond_no_info').remove();
                $('.previous_feed_info').remove();
                $('.dataTables_wrapper').remove();
                $('#feed_info').append(data);
            }).fail(function (data) {

            });
        });
        $('#probiotic_mineral_other_tab').click(function (e) {
            e.preventDefault();
            $('#loading_data').show();
            $.ajax({
                type:'get',
                url:'{{route('shrimp_culture.client.semi_intensive.get_probiotic_mineral_other_info')}}',
                data:{farm_id:$('#farm_id').val(),pond_id:$('#pond_id').val()}
            }).done(function (data) {
                $('#loading_data').hide();
                $('.previous_other_inventories_info').remove();
                $('.dataTables_wrapper').remove();
                $('#other_inventories_info').append(data);
            }).fail(function (data) {

            });
        });
        $('#monitoring_parameter_tab').click(function (e) {
            e.preventDefault();
            var farm_id=$('#farm_id').val();
            var pond_id=$('#pond_id').val();
            var parameter_selection=$('#parameter_selection').val();
            monitoringParameterShow(farm_id, pond_id, parameter_selection);
        });
        $('#parameter_selection').change(function (e) {
            e.preventDefault();
            var farm_id=$('#farm_id').val();
            var pond_id=$('#pond_id').val();
            var parameter_selection=$('#parameter_selection').val();
            monitoringParameterShow(farm_id, pond_id, parameter_selection);
        });
        function monitoringParameterShow(farm_id, pond_id, parameter_selection) {
            $('#loading_data').show();
            $.ajax({
                type:'get',
                url:'{{route('shrimp_culture.client.semi_intensive.get_monitoring_parameter_info')}}',
                data:{farm_id:farm_id,pond_id:pond_id,parameter_selection:parameter_selection}
            }).done(function (data) {
                $('#loading_data').hide();
                $('.monitoring_parameter_no_info').remove();
                $('.previous_monitoring_parameter_info').remove();
                $('.dataTables_wrapper').remove();
                $('#monitoring_parameter_info').append(data);
            }).fail(function (data) {

            });
        }
        {{--$('#see_harvest_info').click(function (e) {--}}
            {{--e.preventDefault();--}}
            {{--$('#loading_data').show();--}}
            {{--$.ajax({--}}
                {{--type:'get',--}}
                {{--url:'{{route('shrimp_culture.client.semi_intensive.get_pond_info')}}',--}}
                {{--data:{farm_id:$('#farm_id').val(),pond_id:$('#pond_id').val()}--}}
            {{--}).done(function (data) {--}}
                {{--$('#pond_overview_tab').click();--}}
                {{--$('#loading_data').hide();--}}
                {{--$('.pond_no_info').remove();--}}
                {{--$('.previous_pond_info').remove();--}}
                {{--$('.dataTables_wrapper').remove();--}}
                {{--$('#pond_basic_information').append(data);--}}
            {{--}).fail(function (data) {--}}

            {{--});--}}
        {{--});--}}
    });
</script>