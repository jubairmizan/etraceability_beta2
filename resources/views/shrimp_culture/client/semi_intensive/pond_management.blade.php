@extends('layouts.shrimp_culture.client.master')
@section('main_content')
    <div id='loading_data'>
        <img src="{{asset('image/ajax-loader.gif')}}" id="loading_image"/>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                                <h4 class="txt-dark">{{$title}}</h4>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-md-8 col-xs-12">
                                <ol class="breadcrumb">
                                    <li><a href="{{ route('shrimp_culture.client.dashboard') }}">Dashboard</a></li>
                                    <li><a href="#">Semi-Intensive Farms</a></li>
                                    <li><a href="{{route('shrimp_culture.client.semi_intensive.pond_management')}}"><span>Pond Management</span></a></li>
                                </ol>
                            </div>
                            <div class="col-md-12" style="margin-top: 12px">
                                <div class="row">
{{--                                    {{ Form::open(['method'=>'get']) }}--}}
                                    <div class="col-md-4">
                                    <div class="form-group">
                                    {{ Form::label('farm_id','Select Farm',['class'=>'control-label','style'=>'margin-bottom:5px']) }}
                                    {{ Form::select('farm_id',$farms,$farm_id,['class'=>'form-control','required','placeholder'=>'Select Farm','id'=>'farm_id']) }}
                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                    {{ Form::label('pond_id','Select Pond',['class'=>'control-label','style'=>'margin-bottom:5px']) }}
                                    {{ Form::select('pond_id',$ponds,$pond_id,['class'=>'form-control','required','placeholder'=>'Select Pond','id'=>'pond_id']) }}
                                    </div>
                                    </div>
                                    <div class="col-md-4">
                                    <div class="form-group">
                                    {{ Form::label('submit_button','Select Pond',['class'=>'control-label hidden_text']) }}
                                    {{ Form::submit('See the Information',['class'=>'form-control btn btn-primary','id'=>'see_pond_info']) }}
                                    </div>
                                    </div>
{{--                                    {{ Form::close() }}--}}
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div  class="pills-struct mt-40">
                            <ul role="tablist" class="nav nav-pills" id="myTabs_9">
                                <li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="pond_overview_tab" href="#pond_overview_content">Pond Overview</a></li>
                                <li role="presentation" class=""><a  data-toggle="tab" id="feed_tab" role="tab" href="#feed_content" aria-expanded="false">Feeds</a></li>
                                <li role="presentation" class=""><a  data-toggle="tab" id="probiotic_mineral_other_tab" role="tab" href="#probiotic_mineral_other_content" aria-expanded="false">Probiotics, Minerals and Others</a></li>
                                <li role="presentation" class=""><a  data-toggle="tab" id="monitoring_parameter_tab" role="tab" href="#monitoring_parameter_content" aria-expanded="false">Monitoring Parameters</a></li>
                                <li role="presentation" class=""><a  data-toggle="tab" id="projection_tab" role="tab" href="#projection_content" aria-expanded="false">Projections</a></li>
                            </ul>
                            <div class="tab-content" id="myTabContent_9">
                                <div id="pond_overview_content" class="tab-pane fade active in" role="tabpanel">
                                    <div class="panel-wrapper collapse in">
                                        <div  class="panel-body">
                                            <div class="table-responsive mt-40">
                                                <div class="pond_no_info">No information found</div>
                                                <div id="pond_basic_information"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="feed_content" class="tab-pane fade" role="tabpanel">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <div id="feed_info"></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="probiotic_mineral_other_content" class="tab-pane fade" role="tabpanel">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <div id="other_inventories_info"></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="monitoring_parameter_content" class="tab-pane fade" role="tabpanel">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            {{ Form::select('parameter_selection',['pH','Dissolved Oxygen','Temperature','Other Parameters'],null,['class'=>'form-control','required','placeholder'=>'Select Parameter(s)','id'=>'parameter_selection']) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="monitoring_parameter_no_info">No information found</div>
                                            <div id="monitoring_parameter_info"></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="projection_content" class="tab-pane fade" role="tabpanel">
                                    <p>No information found</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function(){--}}
            {{--$('#datatableWithFooterCount').DataTable( {--}}
                {{--"footerCallback": function ( row, data, start, end, display ) {--}}
                    {{--var api = this.api(), data;--}}

                    {{--// Remove the formatting to get integer data for summation--}}
                    {{--var intVal = function ( i ) {--}}
                        {{--return typeof i === 'string' ?--}}
                            {{--i.replace(/[\$,]/g, '')*1 :--}}
                            {{--typeof i === 'number' ?--}}
                                {{--i : 0;--}}
                    {{--};--}}

                    {{--// Total over all pages--}}
                    {{--total = api--}}
                        {{--.column( 7 )--}}
                        {{--.data()--}}
                        {{--.reduce( function (a, b) {--}}
                            {{--return intVal(a) + intVal(b);--}}
                        {{--}, 0 );--}}

                    {{--// Total over this page--}}
                    {{--pageTotal = api--}}
                        {{--.column( 7, { page: 'current'} )--}}
                        {{--.data()--}}
                        {{--.reduce( function (a, b) {--}}
                            {{--return intVal(a) + intVal(b);--}}
                        {{--}, 0 );--}}

                    {{--// Update footer--}}
                    {{--$( api.column( 7 ).footer() ).html(--}}
                        {{--// pageTotal +' ('+ total+')'--}}
                        {{--total--}}
                    {{--);--}}
                {{--}--}}
            {{--} );--}}
        {{--})--}}
    {{--</script>--}}
@endsection
@section('js')
    @include('shrimp_culture.client.semi_intensive._script')
@endsection

{{--<style>--}}
    {{--.nav-pills > li {--}}
        {{--margin-right: 0;--}}
    {{--}--}}
{{--</style>--}}
{{--<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">--}}
{{--<div class="container-fluid">--}}
    {{--<!-- Title -->--}}
    {{--<div class="row heading-bg">--}}
        {{--<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">--}}
            {{--<h5 class="txt-dark">{{$title}}</h5>--}}
        {{--</div>--}}
        {{--<!-- Breadcrumb -->--}}
    {{--<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">--}}
    {{--<ol class="breadcrumb">--}}
    {{--<li><a href="{{ route('client.dashboard') }}">Dashboard</a></li>--}}
    {{--<li><a href="#"><span>During Enclosure</span></a></li>--}}
    {{--</ol>--}}
    {{--</div>--}}
    {{--<!-- /Breadcrumb -->--}}
    {{--</div>--}}
    {{--<!-- /Title -->--}}

    {{--<!-- Row -->--}}
    {{--<div class="row">--}}
        {{--<div class="col-lg-12 col-sm-12">--}}
            {{--<div class="panel panel-default card-view">--}}
                {{--<div class="panel-heading">--}}
                    {{--<div class="pull-left">--}}
                        {{--<h6 class="panel-title txt-dark">Pond Details</h6>--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                {{--</div>--}}
                {{--<div class="panel-wrapper collapse in">--}}
                    {{--<div class="panel-body">--}}
                        {{--<div class="row">--}}
                            {{--{{ Form::open(['method'=>'get']) }}--}}
                            {{--<div class="col-md-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('farm_id','Select Farm',['class'=>'control-label','style'=>'margin-bottom:5px']) }}--}}
                                    {{--{{ Form::select('farm_id',$farms,$farm_id,['class'=>'form-control','required','placeholder'=>'Select Farm','id'=>'farm_id']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('pond_id','Select Pond',['class'=>'control-label','style'=>'margin-bottom:5px']) }}--}}
                                    {{--{{ Form::select('pond_id',$ponds,$pond_id,['class'=>'form-control','required','placeholder'=>'Select Pond','id'=>'pond_id']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-md-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--{{ Form::label('pond_id','Select Pond',['class'=>'control-label','style'=>'margin-bottom:5px;color:#212121']) }}--}}
                                    {{--{{ Form::submit('See the Information',['class'=>'form-control btn btn-primary']) }}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--{{ Form::close() }}--}}
                        {{--</div>--}}
                        {{--<div  class="pills-struct mt-40">--}}
                            {{--<ul role="tablist" class="nav nav-pills" id="myTabs_9">--}}
                                {{--<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="pond_overview_tab" href="#pond_overview_content">Pond Overview</a></li>--}}
                                {{--<li role="presentation" class=""><a  data-toggle="tab" id="feed_tab" role="tab" href="#feed_content" aria-expanded="false">Feeds</a></li>--}}
                                {{--<li role="presentation" class=""><a  data-toggle="tab" id="probiotics_tab" role="tab" href="#probiotics_minerals_others_content" aria-expanded="false">Probiotics, Minerals and Others</a></li>--}}
                                {{--<li role="presentation" class=""><a  data-toggle="tab" id="ph_tab" role="tab" href="#monitoring_parameters" aria-expanded="false">Monitoring Parameters</a></li>--}}
                                {{--<li role="presentation" class=""><a  data-toggle="tab" id="projection_tab" role="tab" href="#projection_content" aria-expanded="false">Projections</a></li>--}}
                            {{--</ul>--}}
                            {{--<div class="tab-content" id="myTabContent_9">--}}
                                {{--<div id="pond_overview_content" class="tab-pane fade active in" role="tabpanel">--}}
                                    {{--<div class="panel-wrapper collapse in">--}}
                                        {{--<div  class="panel-body">--}}
                                            {{--<div class="table-responsive mt-40">--}}
                                                {{--<table class="table table-bordered table-striped">--}}
                                                    {{--<thead>--}}
                                                    {{--<tr style="margin: 0 auto">--}}
                                                        {{--<th colspan="5">Pond's Basic Information</th>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<th>Pond Name</th>--}}
                                                        {{--<td colspan="4">{{isset($pond_info->name)?$pond_info->name:''}}</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<th>Google Map</th>--}}
                                                        {{--<td colspan="4"><a target="_blank" href="https://www.google.com/maps/d/u/0/viewer?ll=22.58255862904249%2C89.55279535&z=11&mid=1aa3XCs0zdV6HrZIRuiLHliNCVDIqaew4">Click to see the Pond at Google Maps</a></td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<th>Pond ID</th>--}}
                                                        {{--<td colspan="4">{{isset($pond_info->pond_id)?$pond_info->pond_id:''}}</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                    {{--<th>Farm</th>--}}
                                                    {{--<td colspan="4">{{isset($pond_info->farm)?$pond_info->farm->name:''}}</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<th>Farm Manager</th>--}}
                                                        {{--<td colspan="4">{{isset($pond_info->farm_manager)?$pond_info->farm_manager->name:''}}</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                    {{--<th>Farm Owner</th>--}}
                                                    {{--<td colspan="4">{{isset($pond_info->client)?$pond_info->client->name:''}}</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<th>Dimension</th>--}}
                                                        {{--<td colspan="4">{{isset($pond_info->area)?$pond_info->area.' Feet':''}}</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                    {{--<th>Type of Soil</th>--}}
                                                    {{--<td colspan="4">{{isset($pond_info->type_of_soil)?$pond_info->type_of_soil:''}}</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<th>Survival Rate(%)</th>--}}
                                                        {{--<td colspan="4">80</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<th>Average Body Weight</th>--}}
                                                        {{--<td colspan="4">80 kg</td>--}}
                                                    {{--</tr>--}}
                                                    {{--<tr>--}}
                                                        {{--<th>Current Biomass</th>--}}
                                                        {{--<td colspan="4">4 kg</td>--}}
                                                    {{--</tr>--}}
                                                    {{--</thead>--}}
                                                {{--</table>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div id="feed_content" class="tab-pane fade" role="tabpanel">--}}
                                    {{--<div class="table-wrap">--}}
                                        {{--<div class="table-responsive">--}}
                                            {{--<table id="feed_datatable" class="table table-hover display pb-30" >--}}
                                                {{--<thead>--}}
                                                {{--<tr>--}}
                                                    {{--<th>Feed Name</th>--}}
                                                    {{--<th>Timing</th>--}}
                                                    {{--<th>Quantity</th>--}}
                                                    {{--<th>Date</th>--}}
                                                {{--</tr>--}}
                                                {{--</thead>--}}
                                                {{--<tbody>--}}
                                                {{--@if(count($during_enclosure)>0)--}}
                                                {{--@foreach($during_enclosure as $de)--}}
                                                {{--@foreach($de->feed as $feed)--}}
                                                {{--<tr>--}}
                                                {{--<td>{{$feed->feed->name}}</td>--}}
                                                {{--<td>{{$feed->timing}}</td>--}}
                                                {{--<td>{{$feed->quantity}}</td>--}}
                                                {{--<td>{{$feed->created_at}}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endforeach--}}
                                                {{--@endforeach--}}
                                                {{--@endif--}}
                                                {{--</tbody>--}}
                                                {{--<tfoot>--}}
                                                {{--<tr>--}}
                                                    {{--<th>Feed Name</th>--}}
                                                    {{--<th>Timing</th>--}}
                                                    {{--<th>Quantity</th>--}}
                                                    {{--<th>Date</th>--}}
                                                {{--</tr>--}}
                                                {{--</tfoot>--}}
                                            {{--</table>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div id="probiotics_minerals_others_content" class="tab-pane fade " role="tabpanel">--}}
                                    {{--<div class="table-wrap">--}}
                                        {{--<div class="table-responsive">--}}
                                            {{--<table id="probiotic_datatable" class="table table-hover display pb-30" >--}}
                                                {{--<thead>--}}
                                                {{--<tr>--}}
                                                    {{--<th>Probiotic Name</th>--}}
                                                    {{--<th>Timing</th>--}}
                                                    {{--<th>Quantity</th>--}}
                                                    {{--<th>Date</th>--}}
                                                {{--</tr>--}}
                                                {{--</thead>--}}
                                                {{--<tbody>--}}
                                                {{--@if(count($during_enclosure)>0)--}}
                                                {{--@foreach($during_enclosure as $de)--}}
                                                {{--@foreach($de->remark as $remark)--}}
                                                {{--<tr>--}}
                                                {{--<td>{{$remark->remark->name}}</td>--}}
                                                {{--<td>{{$remark->timing}}</td>--}}
                                                {{--<td>{{$remark->quantity}}</td>--}}
                                                {{--<td>{{$remark->created_at}}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endforeach--}}
                                                {{--@endforeach--}}
                                                {{--@endif--}}
                                                {{--</tbody>--}}
                                                {{--<tfoot>--}}
                                                {{--<tr>--}}
                                                    {{--<th>Probiotic Name</th>--}}
                                                    {{--<th>Timing</th>--}}
                                                    {{--<th>Quantity</th>--}}
                                                    {{--<th>Date</th>--}}
                                                {{--</tr>--}}
                                                {{--</tfoot>--}}
                                            {{--</table>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div id="monitoring_parameters" class="tab-pane fade" role="tabpanel">--}}
                                    {{--<div class="table-wrap">--}}
                                        {{--<div class="table-responsive">--}}
                                            {{--<table id="ph_datatable" class="table table-hover display pb-30" >--}}
                                                {{--<thead>--}}
                                                {{--<tr>--}}
                                                    {{--<th>Timing</th>--}}
                                                    {{--<th>Quantity</th>--}}
                                                    {{--<th>Date</th>--}}
                                                {{--</tr>--}}
                                                {{--</thead>--}}

                                                {{--<tbody>--}}
                                                {{--@if(count($during_enclosure)>0)--}}
                                                {{--@foreach($during_enclosure as $de)--}}
                                                {{--@foreach($de->ph as $ph)--}}
                                                {{--<tr>--}}
                                                {{--<td>{{$ph->timing}}</td>--}}
                                                {{--<td>{{$ph->quantity}}</td>--}}
                                                {{--<td>{{$ph->created_at}}</td>--}}
                                                {{--</tr>--}}
                                                {{--@endforeach--}}
                                                {{--@endforeach--}}
                                                {{--@endif--}}
                                                {{--</tbody>--}}
                                                {{--<tfoot>--}}
                                                {{--<tr>--}}
                                                    {{--<th>Timing</th>--}}
                                                    {{--<th>Quantity</th>--}}
                                                    {{--<th>Date</th>--}}
                                                {{--</tr>--}}
                                                {{--</tfoot>--}}
                                            {{--</table>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div id="projection_content" class="tab-pane fade" role="tabpanel">--}}
                                    {{--<p>No information found</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- /Row -->--}}
{{--</div>--}}