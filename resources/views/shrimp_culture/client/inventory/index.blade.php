@extends('layouts.shrimp_culture.client.master')
@section('main_content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                                <h4 class="txt-dark">{{$title}}</h4>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-md-8 col-xs-12">
                                <ol class="breadcrumb">
                                    <li><a href="{{ route('shrimp_culture.client.dashboard') }}">Dashboard</a></li>
                                    <li><a href="{{route('shrimp_culture.client.inventory')}}"><span>Inventories</span></a></li>
                                </ol>
                            </div>
                        </div>
                        <hr>
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="example" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Type</th>
                                        <th>Inventory</th>
                                        <th>Quantity</th>
                                        <th>Remaining Quantity</th>
                                        <th>Price</th>
                                        <th>Total Price</th>
                                        <th>Stocked Date</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php $sl = 1; ?>
                                    @foreach($inventories as $value)
                                        <tr>
                                            <td>{{ $sl++ }}</td>
                                            <td>{{ $value->rel_inventory->rel_inventory_type->name }}</td>
                                            <td>{{ $value->rel_inventory->name }}</td>
                                            <td>{{ $value->quantity }}</td>
                                            <td>{{ $value->remaining_quantity }}</td>
                                            <td>{{ $value->price }}</td>
                                            <td>{{ $value->total_price }}</td>
                                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$value->created_at)->format('F d, Y') }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Type</th>
                                        <th>Inventory</th>
                                        <th>Quantity</th>
                                        <th>Remaining Quantity</th>
                                        <th>Price</th>
                                        <th>Total Price</th>
                                        <th>Created At</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection