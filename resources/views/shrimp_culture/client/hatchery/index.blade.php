@extends('layouts.shrimp_culture.client.master')
@section('main_content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                                <h4 class="txt-dark">{{$title}}</h4>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-md-8 col-xs-12">
                                <ol class="breadcrumb">
                                    <li><a href="{{ route('shrimp_culture.client.dashboard') }}">Dashboard</a></li>
                                    <li><a href="{{route('shrimp_culture.client.hatchery')}}"><span>Hatcheries</span></a></li>
                                </ol>
                            </div>
                        </div>
                        <hr>
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="example" class="table table-hover display  pb-30" >
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Hatchery Name</th>
                                        <th>Map</th>
                                        <th>License</th>
                                        <th>District</th>
                                        <th>Upazila</th>
                                        <th>Union</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php $sl = 1; ?>
                                    @if(count($hatcheries)>0)
                                        @foreach($hatcheries as $value)
                                            <tr>
                                                <td>{{ $sl }}</td>
                                                <td>{{ $value->name }}</td>
                                                <td>
                                                    <a target="_blank" href="https://www.google.com/maps/d/u/0/viewer?ll=21.127566458104123%2C92.15467749999993&z=11&mid=1M17MO6eCTuYJj1ESK8iRiWoeryAzwfiI">See the Google Map</a>
                                                </td>
                                                <td>{{ $value->license_number }}</td>
                                                <td>{{ $value->district->name }}</td>
                                                <td>{{ $value->upazila->name }}</td>
                                                <td>{{ $value->union->name }}</td>
                                                <td>
                                                    @if($value->status==1)
                                                        Active
                                                    @else
                                                        In-active
                                                    @endif
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        @endforeach
                                    @else
                                        <tr class="warning"><td colspan="9" align="center">No information found</td></tr>
                                    @endif
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>SL</th>
                                        <th>Hatchery Name</th>
                                        <th>Map</th>
                                        <th>License</th>
                                        <th>District</th>
                                        <th>Upazila</th>
                                        <th>Union</th>
                                        <th>Status</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection