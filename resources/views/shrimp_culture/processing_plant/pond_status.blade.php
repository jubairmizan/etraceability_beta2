@extends('layouts.shrimp_culture.processing_plant.master')
@section('main_content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h4 class="txt-dark">{{$title}}</h4>
                            </div>
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                <ol class="breadcrumb">
                                    <li><a href="{{ route('shrimp_culture.processing_plant.dashboard') }}">Dashboard</a></li>
                                    <li><a href="{{route('shrimp_culture.processing_plant.suppliers',[$type_of_farming])}}"><span>Suppliers</span></a></li>
                                    @if(strcasecmp($type_of_farming,'extensive')==0)
                                        <li><a href="{{route('shrimp_culture.processing_plant.clusters',[$type_of_farming,$client->id])}}"><span>Clusters</span></a></li>
                                    @else
                                        <li><a href="{{route('shrimp_culture.processing_plant.farms',[$type_of_farming,$client->id])}}"><span>Farms</span></a></li>
                                    @endif
                                    <li><a href="{{route('shrimp_culture.processing_plant.ponds',[$type_of_farming,$client->id,$cluster_or_farm_id])}}"><span>Ponds</span></a></li>
                                    <li><span>Pond's status</span></li>
                                </ol>
                            </div>
                        </div>
                        <hr>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-responsive mt-40">
                                    <table class="table table-bordered table-striped">
                                        @if(strcasecmp($type_of_farming,'extensive')==0)
                                            <thead>
                                            <tr>
                                                <th>Cluster Pond Name</th>
                                                <td colspan="4">Name: {{$pond->name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Cluster</th>
                                                <td colspan="4">{{$pond->cluster->name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Owner</th>
                                                <td colspan="4">{{$client->name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Total Capacity</th>
                                                <td colspan="4">--</td>
                                            </tr>
                                            <tr>
                                                <th>Target Harvest Weight</th>
                                                <td colspan="4">--</td>
                                            </tr>
                                            <tr>
                                                <th>Current Average Body  Weight</th>
                                                <td colspan="4">
                                                    @if($pond->id==2)
                                                        20.00 g
                                                    @elseif($pond->id==3)
                                                        12.29 g
                                                    @elseif($pond->id==4)
                                                        14.29 g
                                                    @elseif($pond->id==5)
                                                        20.00 g
                                                    @elseif($pond->id==6)
                                                        20.00 g
                                                    @elseif($pond->id==7)
                                                        31.08 g
                                                    @elseif($pond->id==8)
                                                        33.39 g
                                                    @elseif($pond->id==9)
                                                        32.08 g
                                                    @elseif($pond->id==10)
                                                        38.50 g
                                                    @elseif($pond->id==11)
                                                        32.61 g
                                                    @elseif($pond->id==12)
                                                        34.00 g
                                                    @elseif($pond->id==13)
                                                        12.13 g
                                                    @elseif($pond->id==14)
                                                        12.29 g
                                                    @elseif($pond->id==15)
                                                        12.39 g
                                                    @elseif($pond->id==16)
                                                        18.32 g
                                                    @elseif($pond->id==17)
                                                        12.11 g
                                                    @else
                                                        --
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Total (Current) Biomass</th>
                                                <td colspan="4">
                                                    @if($pond->id==2)
                                                        1,548 kg
                                                    @elseif($pond->id==3)
                                                        --
                                                    @elseif($pond->id==4)
                                                        --
                                                    @elseif($pond->id==5)
                                                        --
                                                    @elseif($pond->id==6)
                                                        1,836 kg
                                                    @elseif($pond->id==7)
                                                        2,028 kg
                                                    @elseif($pond->id==8)
                                                        1,791 kg
                                                    @elseif($pond->id==9)
                                                        1,419 kg
                                                    @elseif($pond->id==10)
                                                        2,327 kg
                                                    @elseif($pond->id==11)
                                                        1,654 kg
                                                    @elseif($pond->id==12)
                                                        1,408 kg
                                                    @elseif($pond->id==13)
                                                        466 kg
                                                    @elseif($pond->id==14)
                                                        472 kg
                                                    @elseif($pond->id==15)
                                                        515 kg
                                                    @elseif($pond->id==16)
                                                        645 kg
                                                    @elseif($pond->id==17)
                                                        465 kg
                                                    @else
                                                        --
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Days of Culture</th>
                                                <td colspan="4">--</td>
                                            </tr>
                                            <tr>
                                                <th>Expected Price</th>
                                                <td colspan="4">--</td>
                                            </tr>
                                            </thead>
                                        @else
                                            <thead>
                                            <tr>
                                                <th>Pond Name and Map Link</th>
                                                <td colspan="4">Name: {{$pond->name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Farm</th>
                                                <td colspan="4">{{$pond->farm->name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Farm Owner</th>
                                                <td colspan="4">{{$client->name}}</td>
                                            </tr>
                                            <tr>
                                                <th>Total Capacity</th>
                                                <td colspan="4">--</td>
                                            </tr>
                                            <tr>
                                                <th>Target Harvest Weight</th>
                                                <td colspan="4">Coming Soon</td>
                                            </tr>
                                            <tr>
                                                <th>Current Average Body  Weight</th>
                                                <td colspan="4">
                                                    @if($pond->id==2)
                                                        20.00 g
                                                    @elseif($pond->id==3)
                                                        12.29 g
                                                    @elseif($pond->id==4)
                                                        14.29 g
                                                    @elseif($pond->id==5)
                                                        20.00 g
                                                    @elseif($pond->id==6)
                                                        20.00 g
                                                    @elseif($pond->id==7)
                                                        31.08 g
                                                    @elseif($pond->id==8)
                                                        33.39 g
                                                    @elseif($pond->id==9)
                                                        32.08 g
                                                    @elseif($pond->id==10)
                                                        38.50 g
                                                    @elseif($pond->id==11)
                                                        32.61 g
                                                    @elseif($pond->id==12)
                                                        34.00 g
                                                    @elseif($pond->id==13)
                                                        12.13 g
                                                    @elseif($pond->id==14)
                                                        12.29 g
                                                    @elseif($pond->id==15)
                                                        12.39 g
                                                    @elseif($pond->id==16)
                                                        18.32 g
                                                    @elseif($pond->id==17)
                                                        12.11 g
                                                    @else
                                                        --
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Total (Current) Biomass</th>
                                                <td colspan="4">
                                                    @if($pond->id==2)
                                                        1,548 kg
                                                    @elseif($pond->id==3)
                                                    @elseif($pond->id==4)
                                                    @elseif($pond->id==5)
                                                    @elseif($pond->id==6)
                                                        1,836 kg
                                                    @elseif($pond->id==7)
                                                        2,028 kg
                                                    @elseif($pond->id==8)
                                                        1,791 kg
                                                    @elseif($pond->id==9)
                                                        1,419 kg
                                                    @elseif($pond->id==10)
                                                        2,327 kg
                                                    @elseif($pond->id==11)
                                                        1,654 kg
                                                    @elseif($pond->id==12)
                                                        1,408 kg
                                                    @elseif($pond->id==13)
                                                        466 kg
                                                    @elseif($pond->id==14)
                                                        472 kg
                                                    @elseif($pond->id==15)
                                                        515 kg
                                                    @elseif($pond->id==16)
                                                        645 kg
                                                    @elseif($pond->id==17)
                                                        465 kg
                                                    @else
                                                        --
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Days of Culture</th>
                                                <td colspan="4">--</td>
                                            </tr>
                                            <tr>
                                                <th>Expected Price</th>
                                                <td colspan="4">--</td>
                                            </tr>
                                            </thead>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div  class="panel-body">
                                <div class="table-responsive mt-40">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th colspan="5">Pond's Input</th>
                                        </tr>
                                        <tr>
                                            <th>Feeds</th>
                                            <th>Probiotics, Minerals and Others</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                @foreach($feed_details as $feed)
                                                    <p>{{$feed->name}}</p>
                                                @endforeach
                                            </td>
                                            <td>
                                                @foreach($remark_details as $remark)
                                                    <p>{{$remark->name}}</p>
                                                @endforeach
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="col-sm-4 col-sm-offset-4 col-xs-12 mt-15 text-center">
                                        {{--<button class="btn btn-primary btn-rounded btn-block">Make an RFQ</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection