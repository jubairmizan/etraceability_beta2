@extends('layouts.shrimp_culture.processing_plant.master')
@section('main_content')
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default card-view">
				<div class="panel-wrapper collapse in">
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<h4 class="txt-dark">{{$title}}</h4>
							</div>
							<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
								<ol class="breadcrumb">
									<li><a href="{{ route('shrimp_culture.processing_plant.dashboard') }}">Dashboard</a></li>
									<li><a href="{{route('shrimp_culture.processing_plant.suppliers',[$type_of_farming])}}"><span>Suppliers</span></a></li>
									@if(strcasecmp($type_of_farming,'extensive')==0)
										<li><span>Clusters</span></li>
									@else
										<li><span>Farms</span></li>
									@endif
								</ol>
							</div>
						</div>
						<hr>
						<div class="table-wrap">
							<div class="table-responsive">
								<table id="example" class="table table-hover display pb-30" >
									<thead>
									<tr>
										<th>SL</th>
										@if(strcasecmp($type_of_farming,'extensive')==0)
											<th>Clusters' Name</th>
											<th>Map</th>
											<th>Cluster Ponds</th>
										@else
											<th>Farms' Name</th>
											<th>Map</th>
											<th>Semi-Intensive Ponds</th>
										@endif
										<th>Total Capacity</th>
										<th>RFQ</th>
										<th>Ratings</th>
									</tr>
									</thead>

									<tbody>
                                    <?php $sl = 1; ?>
									@foreach($cluster_or_farm as $value)
										<tr>
											<td>{{ $sl }}</td>
											<td>{{ $value->name }}</td>
											@if(strcasecmp($type_of_farming,'extensive')==0)
												<td><a href="https://www.google.com/maps/d/u/0/viewer?ll=22.692393220851287%2C89.41166279999993&z=17&mid=18uC9F-5A8cfqIhA8Ql1EL5lCRitm--td">See the Google Maps</a></td>
											@else
												@if($value->id==2)
													<td><a href="https://www.google.com/maps/d/u/0/viewer?ll=22.582558629042502,89.55279535&z=11&mid=1aa3XCs0zdV6HrZIRuiLHliNCVDIqaew4">See the Google Maps</a></td>
												@else
													<td><a href="https://www.google.com/maps/d/u/0/viewer?ll=21.12756645810412,92.15467749999993&z=11&mid=1M17MO6eCTuYJj1ESK8iRiWoeryAzwfiI">See the Google Maps</a></td>
												@endif
											@endif
											<td><a href="{{ route('shrimp_culture.processing_plant.ponds',[$type_of_farming,$client_id,$value->id]) }}">Click to see the Ponds</a></td>
											<td>--</td>
											<td>--</td>
											<td>(5.0) <span class="fa fa-star checked"></span>
												<span class="fa fa-star checked"></span>
												<span class="fa fa-star checked"></span>
												<span class="fa fa-star checked"></span>
												<span class="fa fa-star checked"></span>
											</td>
										</tr>
                                        <?php $sl++; ?>
									@endforeach
									</tbody>
									<tfoot>
									<tr>
										<th>SL</th>
										@if(strcasecmp($type_of_farming,'extensive')==0)
											<th>Clusters' Name</th>
											<th>Map</th>
											<th>Cluster Ponds</th>
										@else
											<th>Farms' Name</th>
											<th>Map</th>
											<th>Semi-Intensive Ponds</th>
										@endif
										<th>Total Capacity</th>
										<th>RFQ</th>
										<th>Ratings</th>
									</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection