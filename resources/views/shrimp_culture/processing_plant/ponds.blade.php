@extends('layouts.shrimp_culture.processing_plant.master')
@section('main_content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h4 class="txt-dark">{{$title}}</h4>
                            </div>
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                <ol class="breadcrumb">
                                    <li><a href="{{ route('shrimp_culture.processing_plant.dashboard') }}">Dashboard</a></li>
                                    <li><a href="{{route('shrimp_culture.processing_plant.suppliers',[$type_of_farming])}}"><span>Suppliers</span></a></li>
                                    @if(strcasecmp($type_of_farming,'extensive')==0)
                                        <li><a href="{{route('shrimp_culture.processing_plant.clusters',[$type_of_farming,$client_id])}}"><span>Clusters</span></a></li>
                                    @else
                                        <li><a href="{{route('shrimp_culture.processing_plant.farms',[$type_of_farming,$client_id])}}"><span>Farms</span></a></li>
                                    @endif
                                    <li><span>Ponds</span></li>
                                </ol>
                            </div>
                        </div>
                        <hr>
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="example" class="table table-hover display pb-30" >
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Name</th>
                                        <th>Total Capacity</th>
                                        <th>Average Body  Weight</th>
                                        <th>Expected Price</th>
                                        <th>Status</th>
                                        <th>RFQ</th>
                                        <th>Rating</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php $sl = 1; ?>
                                    @foreach($pondsList as $value)
                                        <tr>
                                            <td>{{ $sl }}</td>
                                            <td>{{ $value->name }}</td>
                                            <td>--</td>
                                            <td>
                                                @if($value->id==2)
                                                    20.00 g
                                                @elseif($value->id==3)
                                                    12.29 g
                                                @elseif($value->id==4)
                                                    14.29 g
                                                @elseif($value->id==5)
                                                    20.00 g
                                                @elseif($value->id==6)
                                                    20.00 g
                                                @elseif($value->id==7)
                                                    31.08 g
                                                @elseif($value->id==8)
                                                    33.39 g
                                                @elseif($value->id==9)
                                                    32.08 g
                                                @elseif($value->id==10)
                                                    38.50 g
                                                @elseif($value->id==11)
                                                    32.61 g
                                                @elseif($value->id==12)
                                                    34.00 g
                                                @elseif($value->id==13)
                                                    12.13 g
                                                @elseif($value->id==14)
                                                    12.29 g
                                                @elseif($value->id==15)
                                                    12.39 g
                                                @elseif($value->id==16)
                                                    18.32 g
                                                @elseif($value->id==17)
                                                    12.11 g
                                                @else
                                                    --
                                                @endif
                                            </td>
                                            <td>--</td>
                                            <td><a href="{{route('shrimp_culture.processing_plant.pond_status',[$type_of_farming,$client_id,$value->id])}}">Click to see the Pond's Status</a></td>

                                            {{--@if($value->id >= 2 && $value->id <= 18)--}}
                                            {{--<td><a href="{{route('processing_plant.ponds.history',[$type_of_farming, $value->id,$client_id])}}">Click to see the Pond's Status</a></td>--}}
                                            {{--@else--}}
                                            {{--<td><a href="#">Click to see the Pond's Status</a></td>--}}
                                            {{--@endif--}}
                                            <td>--</td>
                                            <td>(5.0) <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                            </td>
                                        </tr>
                                        <?php $sl++; ?>
                                    @endforeach
                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <th>SL</th>
                                        <th>Name</th>
                                        <th>Total Capacity</th>
                                        <th>Average Body  Weight</th>
                                        <th>Expected Price</th>
                                        <th>Status</th>
                                        <th>RFQ</th>
                                        <th>Rating</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection