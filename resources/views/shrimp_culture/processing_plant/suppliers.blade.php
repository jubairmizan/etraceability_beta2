@extends('layouts.shrimp_culture.processing_plant.master')
@section('main_content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <h4 class="txt-dark">{{$title}}</h4>
                            </div>
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                <ol class="breadcrumb">
                                    <li><a href="{{ route('shrimp_culture.processing_plant.dashboard') }}">Dashboard</a></li>
                                    <li><span>Suppliers</span></li>
                                </ol>
                            </div>
                        </div>
                        <hr>
                        <div class="table-wrap">
                            <div class="table-responsive">
                                <table id="example" class="table table-hover display  pb-30" >
                                    @if(strcasecmp($type_of_farming,'extensive')==0)
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Suppliers' Name</th>
                                            <th>Clusters</th>
                                            <th>Total Capacity</th>
                                            <th>RFQ</th>
                                            <th>Rating</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php $sl = 1; ?>
                                        @foreach($suppliers as $value)
                                            <tr>
                                                <td>{{ $sl }}</td>
                                                <td>{{ $value->name }}</td>
                                                 <td><a href="{{ route('shrimp_culture.processing_plant.clusters',[$type_of_farming,$value->id]) }}">Click to see the Cluster</a></td>
                                                <td>--</td>
                                                <td>--</td>
                                                <td>(5.0) <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        @endforeach
                                        </tbody>

                                        <tfoot>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Farms</th>
                                            <th>Total Capacity</th>
                                            <th>RFQ</th>
                                            <th>Rating</th>
                                        </tr>
                                        </tfoot>
                                    @else
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Suppliers' Name</th>
                                            <th>Farms</th>
                                            <th>Total Capacity</th>
                                            <th>RFQ</th>
                                            <th>Rating</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php $sl = 1; ?>
                                        @foreach($suppliers as $value)
                                            <tr>
                                                <td>{{ $sl }}</td>
                                                <td>{{ $value->name }}</td>
                                                <td><a href="{{ route('shrimp_culture.processing_plant.farms',[$type_of_farming,$value->id]) }}">Click to see the Farms</a></td>
                                                <td>--</td>
                                                <td>--</td>
                                                <td>(5.0) <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                </td>
                                            </tr>
                                            <?php $sl++; ?>
                                        @endforeach
                                        </tbody>

                                        <tfoot>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Farms</th>
                                            <th>Total Capacity</th>
                                            <th>RFQ</th>
                                            <th>Rating</th>
                                        </tr>
                                        </tfoot>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection