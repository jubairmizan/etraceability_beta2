<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemarkDetails extends Model
{
    protected $table='remark_details';
    public function remark(){
        return $this->belongsTo('App\Medicine','remark_id','id');
    }
}
