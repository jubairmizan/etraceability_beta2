<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HarvestDetails extends Model
{
    protected $table='harvest_details';
}
