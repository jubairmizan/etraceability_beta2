<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upazila extends Model
{
    protected $table='upazilas';
    protected $fillable=['name','code','status'];
    public function scopeActive($query){
        $query->where('status',1);
    }
}
