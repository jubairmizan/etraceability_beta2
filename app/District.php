<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table='districts';
    protected $fillable=['name','code','status'];
    public function scopeActive($query){
        $query->where('status',1);
    }
}
