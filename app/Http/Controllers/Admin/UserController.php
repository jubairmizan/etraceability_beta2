<?php

namespace App\Http\Controllers\Admin;

use App\District;
use App\Division;
use App\FarmingType;
use App\Union;
use App\Upazila;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $data['title'] = 'User(s) List';
        $data['users'] = User::all();
        return view('admin.users.index',$data);
    }
    public function create()
    {
        $data['title'] = 'Users Create';
        $data['create'] = 1;
        $data['user_types']=UserType::pluck('name','id');
        $data['farming_type']=FarmingType::pluck('name','id');
        $data['genders']=Config::get('etraceability.genders');
        $data['divisions']=Division::active()->pluck('name','id');
        $data['districts']=District::active()->pluck('name','id');
        $data['upazilas']=Upazila::active()->pluck('name','id');
        $data['unions']=Union::active()->pluck('name','id');
        return view('admin.users.form',$data);
    }
    public function store(Request $request)
    {
//        return $request->all();
        $rule=[
            'type_id'=>'required|numeric',
            'farming_type_id'=>'nullable|numeric',
            'name'=>'required|string',
            'gender'=>'required|string',
            'email'=>'required|string|email|unique:users',
            'cell_no'=>'required|unique:users',
            'nid'=>'nullable|unique:users',
            'status'=>'required|numeric',
            'password'=>'required|string|confirmed',
            'photo'=>'image|mimes:jpeg,jpg,png|max:500',
            'division_id'=>'required|numeric',
            'district_id'=>'required|numeric',
            'upazila_id'=>'required|numeric',
            'union_id'=>'required|numeric',
        ];
        $validator=Validator::make($request->all(),$rule);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $user=new User();
        $user->type_id=$request->type_id;
        if(!is_null($request->farming_type_id)){
            $user->farming_type_id=$request->farming_type_id;
        }
        $user->name=$request->name;
        if($request->type_id==1){
            $user->user_id='AD'.time();
        }elseif ($request->type_id==2){
            $user->user_id='CT'.time();
        }elseif ($request->type_id==3){
            $user->user_id='BR'.time();
        }elseif ($request->type_id==4){
            $user->user_id='FR'.time();
        }elseif ($request->type_id==5) {
            $user->user_id='HM'.time();
        }elseif ($request->type_id==6){
            $user->user_id='FM'.time();
        }elseif ($request->type_id==7){
            $user->user_id='CM'.time();
        }elseif ($request->type_id==8){
            $user->user_id='CC'.time();
        }elseif ($request->type_id==9){
            $user->user_id='PC'.time();
        }
        $user->password=bcrypt($request->password);
        $user->email=$request->email;
        $user->cell_no=$request->cell_no;
        $user->nid=$request->nid;
        $user->gender=$request->gender;
        $user->dob=$request->dob;
        $user->division_id=$request->division_id;
        $user->district_id=$request->district_id;
        $user->upazila_id=$request->upazila_id;
        $user->union_id=$request->union_id;
        $user->mouja=$request->mouja;
        $user->post_office=$request->post_office;
        $user->post_code=$request->post_code;
        $user->ward_no=$request->ward_no;
        $user->status=$request->status;
        if($request->hasFile('photo')){
            $user->photo='uploads/'.Storage::disk('local')->put('users',$request->file('photo'));
        }
        $user->save();
        Session::flash('message','User Created Successfully!');
        return redirect()->route('admin.user.index');
    }
}
