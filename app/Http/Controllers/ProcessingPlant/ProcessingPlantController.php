<?php

namespace App\Http\Controllers\ProcessingPlant;

use App\ShrimpCultureModels\Cluster;
use App\ShrimpCultureModels\ClusterPond;
use App\ShrimpCultureModels\ClusterPondManagement;
use App\ShrimpCultureModels\SiFarm;
use App\ShrimpCultureModels\SiPond;
use App\ShrimpCultureModels\SiPondManagement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Controller;

class ProcessingPlantController extends Controller
{
    public function index(){
        $data['title'] = "Processing Plant Dashboard";
        $data['userData'] = User::where('id',Auth::user()->id)->first();
        return view('processing_plant.dashboard',$data);
    }
    public function profile(){
    	$data['title'] = "Profile";
    	$data['userData'] = User::where('id',Auth::user()->id)->get();
    	return view('processing_plant.profile',$data);
    }
    public function supplierList($type_of_farming){
    	$data['title'] = "Suppliers";
    	if(strcasecmp($type_of_farming,'extensive')==0){
            $data['suppliers'] = User::client()
                ->join('rel_cluster_client','rel_cluster_client.client_id','=','users.id')
                ->join('clusters','clusters.id','=','rel_cluster_client.cluster_id')
                ->select('users.*')
                ->get();
        }else{
            $data['suppliers'] = User::client()
                ->join('rel_si_farm_client','rel_si_farm_client.client_id','=','users.id')
                ->join('si_farms','si_farms.id','=','rel_si_farm_client.si_farm_id')
                ->select('users.*')
                ->get();
        }
        $data['type_of_farming']=$type_of_farming;
    	return view('processing_plant.suppliers',$data);
    }
    public function farms($type_of_farming,$client_id){
    	if($type_of_farming==1){
            $data['title'] = "Cluster List";
            $data['cluster_or_farm'] = Cluster::join('rel_cluster_client','rel_cluster_client.cluster_id','=','clusters.id')
                ->where('rel_cluster_client.client_id','=',$client_id)->select('clusters.*')->get();
        }else{
            $data['title'] = "Farms List";
            $data['cluster_or_farm'] = SiFarm::join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
                ->where('rel_si_farm_client.client_id','=',$client_id)->select('si_farms.*')->get();
        }
        $data['type_of_farming']=$type_of_farming;
        $data['client_id']=$client_id;
    	return view('processing_plant.cluster_or_farm_list',$data);
    }
    public function clusters($type_of_farming,$client_id){
        $data['title'] = "Clusters";
        $data['type_of_farming']=$type_of_farming;
        $data['cluster_or_farm']=Cluster::join('rel_cluster_client','rel_cluster_client.cluster_id','=','clusters.id')
            ->where('rel_cluster_client.client_id','=',$client_id)->select('clusters.*')->get();
        $data['client_id']=$client_id;
        return view('processing_plant.cluster_or_farm_list',$data);
    }
    public function ponds($type_of_farming,$cluster_or_farm_id, $client_id){
        if($type_of_farming==1){
            $data['title'] = "Cluster Ponds List";
            $data['pondsList'] = ClusterPond::where('cluster_id',$cluster_or_farm_id)->get();
        }else{
            $data['title'] = "Semi-Intensive Ponds List";
            $data['pondsList'] = SiPond::where('si_farm_id',$cluster_or_farm_id)->get();
        }
        $data['type_of_farming']=$type_of_farming;
        $data['cluster_or_farm_id']=$cluster_or_farm_id;
        $data['client_id']=$client_id;
    	return view('processing_plant.pondsList',$data);
    }
    public function pondHistory($type_of_farming,$pondId,$client_id){
//        if($pondId==1 || $pondId==18){
        $data['title'] = "Pond's Status";
        if($type_of_farming==1){
            $data['pond']=ClusterPondManagement::where('pond_id',$pondId)->first();

            $data['feed_details']=ClusterPondManagement::
            join('inventory_used_in_water_details','inventory_used_in_water_details.ct_pond_management_id','=','cluster_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                ->where('cluster_pond_management.pond_id',$pondId)
                ->where('inventories.type_id',1)
                ->select('inventories.name')
                ->distinct('inventory_used_in_water_details.inventory_id')->get();

            $data['water_parameter_details']=ClusterPondManagement::
            join('water_parameter_details','water_parameter_details.ct_pond_management_id','=','cluster_pond_management.id')
                ->where('pond_id',$pondId)->select('water_parameter_details.*')
                ->distinct()
                ->get();

            $data['remark_details']=ClusterPondManagement::
            join('inventory_used_in_water_details','inventory_used_in_water_details.ct_pond_management_id','=','cluster_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                ->where('cluster_pond_management.pond_id',$pondId)
                ->whereBetween('inventories.type_id',[2,6])
                ->select('inventories.name')
                ->distinct('inventory_used_in_water_details.inventory_id')->get();
        }else{
            $data['pond']=SiPondManagement::where('pond_id',$pondId)->first();
            $data['feed_details']= SiPondManagement::
            join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                ->where('si_pond_management.pond_id',$pondId)
                ->where('inventories.type_id',1)
                ->select('inventories.name')
                ->distinct('inventory_used_in_water_details.inventory_id')->get();

            $data['water_parameter_details']=SiPondManagement::
            join('water_parameter_details','water_parameter_details.si_pond_management_id','=','si_pond_management.id')
                ->where('pond_id',$pondId)->select('water_parameter_details.*')
                ->distinct()
                ->get();

            $data['remark_details']=SiPondManagement::
            join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                ->where('si_pond_management.pond_id','=',$pondId)
                ->whereBetween('inventories.type_id',[2,6])
                ->select('inventories.name')
                ->distinct('inventory_used_in_water_details.inventory_id')->get();
        }
        $data['type_of_farming']=$type_of_farming;
        $data['client']=User::findOrFail($client_id);
        return view('processing_plant.pond_history',$data);
//        }
    }
}
