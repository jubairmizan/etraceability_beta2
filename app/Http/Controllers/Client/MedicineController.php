<?php

namespace App\Http\Controllers\Client;

use App\Medicine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MedicineController extends Controller
{
    public function index()
    {
        $data['title'] = 'Medicine(s) List';
        $data['medicines'] = Medicine::where('client_id',auth()->user()->id)->get();
        return view('client.medicines.index',$data);
    }
}
