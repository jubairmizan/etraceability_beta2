<?php

namespace App\Http\Controllers\Client;

use App\SiPond;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SemiIntensivePondController extends Controller
{
    public function index()
    {
        $data['title'] = 'Pond(s) List';
        $client_id=auth()->user()->id;
        $data['ponds']=SiPond::join('si_farms','si_farms.id','=','si_ponds.si_farm_id')
            ->join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
                ->where('rel_si_farm_client.client_id','=',$client_id)->select('si_ponds.*')->get();
        return view('client.si_ponds.index',$data);
    }
    public function create(){}
    public function store(){}
}
