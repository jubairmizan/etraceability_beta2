<?php

namespace App\Http\Controllers\Client;

use App\SiDuringEnclosure;
use App\SiEnterToEnclosure;
use App\SiFarm;
use App\SiPond;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SemiIntensivePondManagementController extends Controller
{
    public function enter_to_enclosure(){
        $data['title'] = 'Enter to Enclosure(s) List';
        $data['enter_to_enclosure'] = SiEnterToEnclosure::
            join('si_farms','si_farms.id','=','si_enter_to_enclosure.farm_id')
            ->join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)
            ->select('si_enter_to_enclosure.*')->get();
        return view('client.si_pond_management.enter_to_enclosure',$data);
    }
    public function during_enclosure(Request $request){
        $data['title'] = 'During Enclosure';
        $data['farm_id']='';
        $data['pond_id']='';
        $data['pond_info']='';
        $data['during_enclosure']=[];
        if ($request->has('farm_id') && $request->farm_id != null) {
            if ($request->has('pond_id') && $request->pond_id != null) {
                $data['pond_info']=SiPond::find($request->pond_id);
                $during_enclosure=SiDuringEnclosure::where('pond_id',$request->pond_id)->where('farm_id',$request->farm_id);
                $data['during_enclosure']=$during_enclosure->get();
                $data['farm_id']=$request->farm_id;
                $data['pond_id']=$request->pond_id;
            }
        }
        $data['farms']=SiFarm::join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_farms.name as name','si_farms.id as id');

        $data['ponds']=SiPond::join('si_farms','si_farms.id','=','si_ponds.si_farm_id')
            ->join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_ponds.name as name','si_ponds.id as id');
        return view('client.si_pond_management.during_enclosure',$data);
    }
    public function during_enclosure_test(Request $request){
        $data['title'] = 'During Enclosure';
        $data['farm_id']='';
        $data['pond_id']='';
        $data['pond_info']='';
        $data['during_enclosure']=[];
        $data['feed_details']=[];
        $data['remarks_details']=[];
        if ($request->has('farm_id') && $request->farm_id != null) {
            if ($request->has('pond_id') && $request->pond_id != null) {
                $data['pond_info']=SiPond::find($request->pond_id);
                $during_enclosure=SiPondManagement::where('pond_id',$request->pond_id)->where('farm_id',$request->farm_id);
                $data['during_enclosure']=$during_enclosure->orderBy('created_at','asc')->get();
                $data['feed_details']=SiPondManagement::
                join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                    ->where('inventories.type_id','=',1)
                    ->where('si_pond_management.pond_id','=',$request->pond_id)
                    ->where('si_pond_management.farm_id','=',$request->farm_id)
                ->select('inventories.name','inventory_used_in_water_details.quantity','inventory_used_in_water_details.timing','inventory_used_in_water_details.remaining_quantity','inventory_used_in_water_details.created_at')
                    ->orderBy('inventory_used_in_water_details.created_at','asc')
                    ->get();
                $data['remarks_details']=SiPondManagement::
                join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
                    ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                    ->whereBetween('inventories.type_id',[2,6])
                    ->where('si_pond_management.pond_id','=',$request->pond_id)
                    ->where('si_pond_management.farm_id','=',$request->farm_id)
                    ->select('inventories.name','inventory_used_in_water_details.quantity','inventory_used_in_water_details.remaining_quantity','inventory_used_in_water_details.created_at')
                    ->orderBy('inventory_used_in_water_details.created_at','asc')
                    ->get();
                $data['farm_id']=$request->farm_id;
                $data['pond_id']=$request->pond_id;
            }
        }
        $data['farms']=SiFarm::join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_farms.name as name','si_farms.id as id');

        $data['ponds']=SiPond::join('si_farms','si_farms.id','=','si_ponds.si_farm_id')
            ->join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_ponds.name as name','si_ponds.id as id');
        return view('client.si_pond_management.during_enclosure',$data);
    }
}
