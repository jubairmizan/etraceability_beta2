<?php

namespace App\Http\Controllers\Client;

use App\Cluster;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClusterController extends Controller
{
    public function index()
    {
        $data['title'] = 'Cluster(s) List';
        $client_id=auth()->user()->id;
        $data['clusters']=Cluster::join('rel_cluster_client','rel_cluster_client.cluster_id','=','clusters.id')
            ->where('rel_cluster_client.client_id','=',$client_id)->select('clusters.*')->get();
        return view('client.clusters.index',$data);
    }
}
