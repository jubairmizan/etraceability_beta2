<?php

namespace App\Http\Controllers\Client;

use App\ClusterPond;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClusterPondController extends Controller
{
    public function index()
    {
        $data['title'] = 'Cluster Pond(s) List';
        $client_id=auth()->user()->id;
        $data['cluster_ponds']=ClusterPond::join('clusters','clusters.id','=','cluster_ponds.cluster_id')
            ->join('rel_cluster_client','rel_cluster_client.cluster_id','=','clusters.id')
            ->where('rel_cluster_client.client_id','=',$client_id)->select('cluster_ponds.*')->get();
        return view('client.cluster_ponds.index',$data);
    }
}
