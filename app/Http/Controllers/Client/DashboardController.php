<?php

namespace App\Http\Controllers\Client;

use App\Hatchery;
use App\ShrimpCultureModels\Cluster;
use App\ShrimpCultureModels\ClusterPond;
use App\ShrimpCultureModels\SiFarm;
use App\ShrimpCultureModels\SiPond;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        $data['title'] = "Shrimp Culture Client Dashboard";
        $client_id=auth()->user()->id;
        $data['hatcheries']=Hatchery::join('rel_hatchery_client','rel_hatchery_client.hatchery_id','=','hatcheries.id')
            ->where('rel_hatchery_client.client_id','=',$client_id)->count();
        $data['clusters']=Cluster::join('rel_cluster_client','rel_cluster_client.cluster_id','=','clusters.id')
            ->where('rel_cluster_client.client_id','=',$client_id)->count();
        $data['cluster_ponds']=ClusterPond::join('clusters','clusters.id','=','cluster_ponds.cluster_id')
            ->join('rel_cluster_client','rel_cluster_client.cluster_id','=','clusters.id')
            ->where('rel_cluster_client.client_id','=',$client_id)->count();
        $data['farms']=SiFarm::join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',$client_id)->count();
        $data['si_ponds']=SiPond::join('si_farms','si_farms.id','=','si_ponds.si_farm_id')
            ->join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',$client_id)->count();
        return view('shrimp_culture.client.dashboard',$data);
    }
}
