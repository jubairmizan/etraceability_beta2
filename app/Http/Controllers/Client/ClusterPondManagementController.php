<?php

namespace App\Http\Controllers\Client;

use App\ClusterEnterToEnclosure;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClusterPondManagementController extends Controller
{
    public function enter_to_enclosure(){
        $data['title'] = 'Enter to Enclosure(s) List';
        $data['enter_to_enclosure'] = ClusterEnterToEnclosure::
            join('clusters','clusters.id','=','cluster_enter_to_enclosure.cluster_id')
            ->join('rel_cluster_client','rel_cluster_client.cluster_id','=','clusters.id')
                ->where('rel_cluster_client.client_id','=',auth()->user()->id)
            ->select('cluster_enter_to_enclosure.*')->get();
        return view('client.cluster_pond_management.enter_to_enclosure',$data);
    }
    public function during_enclosure(){
//        $data['title'] = 'During Enclosure';
//        $data['cluster_id']='';
//        $data['pond_id']='';
//        $data['pond_info']='';
//        $data['during_enclosure']=[];
//        if ($request->has('farm_id') && $request->farm_id != null) {
//            if ($request->has('pond_id') && $request->pond_id != null) {
//                $data['pond_info']=ClusterPond::find($request->pond_id);
//                $during_enclosure=SiDuringEnclosure::where('pond_id',$request->pond_id)->where('farm_id',$request->farm_id);
//                $data['during_enclosure']=$during_enclosure->get();
//                $data['farm_id']=$request->farm_id;
//                $data['pond_id']=$request->pond_id;
//            }
//        }
//        $data['farms']=SiFarm::join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
//            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_farms.name as name','si_farms.id as id');
//
//        $data['ponds']=SiPond::join('si_farms','si_farms.id','=','si_ponds.si_farm_id')
//            ->join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
//            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_ponds.name as name','si_ponds.id as id');
        return view('client.si_pond_management.during_enclosure');
    }
}
