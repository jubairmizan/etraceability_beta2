<?php

namespace App\Http\Controllers\Client;

use App\District;
use App\Division;
use App\FarmingType;
use App\SiFarm;
use App\Union;
use App\Upazila;
use App\User;
use App\UserType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FarmController extends Controller
{
    public function index()
    {
        $data['title'] = 'Farms(s) List';
        $client_id=auth()->user()->id;
        $data['farms']=SiFarm::join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',$client_id)->select('si_farms.*')->get();
        return view('client.farms.index',$data);
    }
    public function create()
    {

    }
    public function store(Request $request)
    {
//
    }
}
