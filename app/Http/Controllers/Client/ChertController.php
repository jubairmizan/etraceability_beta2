<?php

namespace App\Http\Controllers\Client;

use App\Chert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChertController extends Controller
{
    public function index()
    {
        $data['title'] = 'Chert List';
        $data['chert'] = Chert::where('client_id',auth()->user()->id)->get();
        return view('client.chert.index',$data);
    }
}
