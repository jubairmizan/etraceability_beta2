<?php

namespace App\Http\Controllers\Client;

use App\Hatchery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HatcheryController extends Controller
{
    public function index()
    {
        $data['title'] = 'Hatchery(s) List';
        $client_id=auth()->user()->id;
        $data['hatcheries']=Hatchery::join('rel_hatchery_client','rel_hatchery_client.hatchery_id','=','hatcheries.id')
            ->where('rel_hatchery_client.client_id','=',$client_id)->select('hatcheries.*')->get();
        return view('client.hatcheries.index',$data);
    }
}
