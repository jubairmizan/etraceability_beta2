<?php

namespace App\Http\Controllers\Client;

use App\Feed;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedController extends Controller
{
    public function index()
    {
        $data['title'] = 'Feed(s) List';
        $data['feeds'] = Feed::where('client_id',auth()->user()->id)->get();
        return view('client.feeds.index',$data);
    }
}
