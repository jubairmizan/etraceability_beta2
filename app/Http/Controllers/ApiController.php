<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ShrimpCulture\ShrimpApiController;
use App\Http\Controllers\WhiteFish\AllApiController;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function pondDetails(Request $request){
        //Created by Shreya
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    $shrimp_api=new ShrimpApiController();
                    $result=$shrimp_api->pondDetails($user);
                }else{
                    //Created by Mizan
                    $whiteFish=new AllApiController();
                    $result=$whiteFish->pondDetails($user);
                }
                if($result){
                    return response()->json([
                        'pond_details'=>$result,
                        'status' => 'Result found',
                        'code'=>200
                    ]);
                }else{
                    return response()->json([
                        'status' => 'No result found',
                        'code'=>204
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function staffUpdate(Request $request){
        $result=[];
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    if($request->has('farm_id') && !is_null($request->farm_id)
                    && $request->has('pond_id') && !is_null($request->pond_id)
                    && $request->has('no_of_staff') && !is_null($request->no_of_staff)){
                        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
                        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
                        $farm_cluster_id=$request->farm_id;
                        $pond_id=$request->pond_id;
                        $no_of_staff=$request->no_of_staff;
                        $shrimp_api=new ShrimpApiController();
                        $result=$shrimp_api->staffUpdate($user,$farm_cluster_id,$pond_id,$no_of_staff);
                    }
                }else{
                    if($request->has('farm_id') && !is_null($request->farm_id)
                    && $request->has('pond_id') && !is_null($request->pond_id)
                    && $request->has('user_id') && !is_null($request->user_id)
                    && $request->has('block_id') && !is_null($request->block_id)
                    && $request->has('no_of_staff') && !is_null($request->no_of_staff)){
                        $user_id=$request->user_id;
                        $farm_id=$request->farm_id;
                        $block_id=$request->block_id;
                        $pond_id=$request->pond_id;
                        $no_of_staff=$request->no_of_staff;
                        $whiteFish = new AllApiController();
                        $result = $whiteFish->staffUpdate($user_id,$farm_id,$block_id,$pond_id,$no_of_staff);
                    }
                }
                if($result){
                    return response()->json([
                        'status' => 'Staff(s) added successfully',
                        'code'=>200
                    ]);
                }else{
                    return response()->json([
                        'status' => 'Staff(s) not added',
                        'code'=>400
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function animalDetails(Request $request){
        $result=[];
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    $shrimp_api=new ShrimpApiController();
                    $result=$shrimp_api->animalDetails($user);
                }else{
                    $whiteFish=new AllApiController();
                    $result=$whiteFish->animalDetails($user);
                }
                if($result){
                    return $result;
                }else{
                    return response()->json([
                        'status' => 'No result found',
                        'code'=>204
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function pLStock(Request $request){
        $result=[];
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    if($request->has('farm_id') && !is_null($request->farm_id)
                        && $request->has('pond_id') && !is_null($request->pond_id)
                        && $request->has('pl_amount') && !is_null($request->pl_amount)
                        && $request->has('pl_age') && !is_null($request->pl_age)
                        && $request->has('pl_price') && !is_null($request->pl_price)
                        && $request->has('hatchery_id') && !is_null($request->hatchery_id)
                    ){
                        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
                        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
                        $farm_cluster_id=$request->farm_id;
                        $pond_id=$request->pond_id;
                        $pl_quantity=$request->pl_amount;
                        $pl_age=$request->pl_age;
                        $pl_price=$request->pl_price;
                        $hatchery_id=$request->hatchery_id;
                        $shrimp_api=new ShrimpApiController();
                        $result=$shrimp_api->pLStock($user,$farm_cluster_id,$pond_id,$pl_quantity,$pl_age,$pl_price,$hatchery_id);
                    }
                }else{
                    if($request->has('user_id') && !is_null($request->user_id)
                        && $request->has('farm_id') && !is_null($request->farm_id)
                        && $request->has('block_id') && !is_null($request->block_id)
                        && $request->has('pond_id') && !is_null($request->pond_id)
                        && $request->has('pl_amount') && !is_null($request->pl_amount)
                        && $request->has('pl_age') && !is_null($request->pl_age)
                        && $request->has('pl_price') && !is_null($request->pl_price)
                        && $request->has('fish_id') && !is_null($request->fish_id)
                    ){
                        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
                        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
                        $user=$request->user_id;
                        $farm_id=$request->farm_id;
                        $block_id=$request->block_id;
                        $pond_id=$request->pond_id;
                        $pl_amount=$request->pl_amount;
                        $pl_age=$request->pl_age;
                        $pl_price=$request->pl_price;
                        $fish_id=$request->fish_id;
                        $whiteFish=new AllApiController();
                        $result=$whiteFish->pLStock($user,$farm_id,$block_id,$pond_id,$pl_amount,$pl_age,$pl_price,$fish_id);
                    }
                }
                if($result){
                    return response()->json([
                        'status' => 'PL(s) stocked successfully',
                        'code'=>200
                    ]);
                }else{
                    return response()->json([
                        'status' => 'PL(s) not stocked',
                        'code'=>400
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function pLSampling(Request $request){
        $result=[];
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    if($request->has('farm_id') && !is_null($request->farm_id)
                        && $request->has('pond_id') && !is_null($request->pond_id)
                        && $request->has('survival_rate') && !is_null($request->survival_rate)
                        && $request->has('biomass') && !is_null($request->biomass)
                        && $request->has('num_of_sample') && !is_null($request->num_of_sample)
                        && $request->has('total_weight') && !is_null($request->total_weight)
                        && $request->has('avg_body_weight') && !is_null($request->avg_body_weight)
                    ){
                        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
                        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
                        $farm_cluster_id=$request->farm_id;
                        $pond_id=$request->pond_id;
                        $survival_rate=$request->survival_rate;
                        $current_biomass=$request->biomass;
                        $number_of_sample=$request->num_of_sample;
                        $total_weight=$request->total_weight;
                        $average_body_weight=$request->avg_body_weight;
                        $shrimp_api=new ShrimpApiController();
                        $result=$shrimp_api->pLSampling($user,$farm_cluster_id,$pond_id,$number_of_sample,$average_body_weight,$current_biomass,$survival_rate,$total_weight);
                    }
                }else{
                    if($request->has('user_id') && !is_null($request->user_id)
                        && $request->has('farm_id') && !is_null($request->farm_id)
                        && $request->has('block_id') && !is_null($request->block_id)
                        && $request->has('pond_id') && !is_null($request->pond_id)
                        && $request->has('fish_id') && !is_null($request->fish_id)
                        && $request->has('survival_rate') && !is_null($request->survival_rate)
                        && $request->has('biomass') && !is_null($request->biomass)
                        && $request->has('num_of_sample') && !is_null($request->num_of_sample)
                        && $request->has('total_weight') && !is_null($request->total_weight)
                        && $request->has('avg_body_weight') && !is_null($request->avg_body_weight)
                    ){
                        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
                        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
                        $user=$request->user_id;
                        $farm_id=$request->farm_id;
                        $block_id=$request->block_id;
                        $pond_id=$request->pond_id;
                        $fish_id=$request->fish_id;
                        $survival_rate=$request->survival_rate;
                        $biomass=$request->biomass;
                        $num_of_sample=$request->num_of_sample;
                        $total_weight=$request->total_weight;
                        $avg_body_weight=$request->avg_body_weight;
                        $whiteFish=new AllApiController();
                        $result=$whiteFish->pLSampling($user,$farm_id,$block_id,$pond_id,$fish_id,$survival_rate,$biomass,$num_of_sample,$total_weight,$avg_body_weight);
                    }
                }
                if($result){
                    return response()->json([
                        'status' => 'PL(s) sampled successfully',
                        'code'=>200
                    ]);
                }else{
                    return response()->json([
                        'status' => 'PL(s) not sampled',
                        'code'=>400
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function pondList(Request $request){
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    $shrimp_api=new ShrimpApiController();
                    $result=$shrimp_api->pondList($user);
                }else{
                    $whiteFishAPi = new AllApiController();
                    $result=$whiteFishAPi->pondList($user);
                }
                if($result){
                    return response()->json([
                        'pond_list'=>$result,
                        'status' => 'Result found',
                        'code'=>200
                    ]);
                }else{
                    return response()->json([
                        'status' => 'No result found',
                        'code'=>204
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function harvestDetails(Request $request){
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    $shrimp_api=new ShrimpApiController();
                    $result=$shrimp_api->harvestDetails($user);
                }else{
                    $whiteFishAPi = new AllApiController();
                    $result=$whiteFishAPi->pondWiseFishList($user);
                }
                if($result){
                    return response()->json([
                        'ponds'=>$result,
                        'status' => 'Result found',
                        'code'=>200
                    ]);
                }else{
                    return response()->json([
                        'status' => 'No result found',
                        'code'=>204
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function harvest(Request $request){
        $result=[];
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    if($request->has('farm_id') && !is_null($request->farm_id)
                        && $request->has('pond_id') && !is_null($request->pond_id) &&
                        $request->has('full_harvest')&&
                        $request->has('harvest') && count($request->harvest)>0 && $request->has('comment')
                    ){
                        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
                        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
                        $farm_cluster_id=$request->farm_id;
                        $pond_id=$request->pond_id;
                        $full_harvest=$request->full_harvest;
                        $comment=$request->comment;
                        $harvest=$request->harvest;
                        $shrimp_api=new ShrimpApiController();
                        $result=$shrimp_api->harvest($user,$farm_cluster_id,$pond_id,$full_harvest,$comment,$harvest);
                    }
                }else{
                    if($request->has('farm_id') && !is_null($request->farm_id)
                        && $request->has('block_id') && !is_null($request->block_id)
                        && $request->has('pond_id') && !is_null($request->pond_id) &&
                        $request->has('full_harvest') &&
                        $request->has('harvest') && count($request->harvest)>0
                    ){
                        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
                        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
                        $farm_id=$request->farm_id;
                        $block_id=$request->block_id;
                        $pond_id=$request->pond_id;
                        $full_harvest=$request->full_harvest;
                        $comment=$request->comment;
                        $harvest=$request->harvest;
                        $whiteFish = new AllApiController();
                        $result= $whiteFish->harvest($user,$farm_id,$block_id,$pond_id,$full_harvest,$comment,$harvest);
                    }
                }
                if($result){
                    return response()->json([
                        'status' => 'Harvesting done successfully',
                        'code'=>200
                    ]);
                }else{
                    return response()->json([
                        'status' => 'Harvesting not done',
                        'code'=>400
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function addMonitoringParameters(Request $request){
        $result=[];
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    if($request->has('farm_id') && !is_null($request->farm_id)
                        && $request->has('pond_id') && !is_null($request->pond_id)
                    ){
                        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
                        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
                        $farm_cluster_id=$request->farm_id;
                        $pond_id=$request->pond_id;
                        $ph=isset($request->pH)?$request->pH:null;
                        $dissolved_oxygen=isset($request->dO)?$request->dO:null;
                        $temperature=isset($request->temperature)?$request->temperature:null;
                        $hco3=isset($request->hCO3)?$request->hCO3:null;
                        $co3=isset($request->cO3)?$request->cO3:null;
                        $ca=isset($request->cA)?$request->cA:null;
                        $cl2=isset($request->cL2)?$request->cL2:null;
                        $mg=isset($request->mG)?$request->mG:null;
                        $k=isset($request->k)?$request->k:null;
                        $nh3=isset($request->nH3)?$request->nH3:null;
                        $no3=isset($request->nO3)?$request->nO3:null;
                        $nh4=isset($request->nH4)?$request->nH4:null;
                        $salinity=isset($request->salinity)?$request->salinity:null;
                        $transparency=isset($request->transparency)?$request->transparency:null;
                        $plankton_type=isset($request->planktonType)?$request->planktonType:null;
                        $green=isset($request->green)?$request->green:null;
                        $yellow=isset($request->yellow)?$request->yellow:null;
                        $vibrio_count=isset($request->vibrioCount)?$request->vibrioCount:null;
                        $shrimp_api=new ShrimpApiController();
                        $result=$shrimp_api->addMonitoringParameters($user,$farm_cluster_id,$pond_id,$ph,$dissolved_oxygen,$temperature,$hco3,
                            $co3,$ca,$cl2,$mg,$k,$nh3,$no3,$nh4,$salinity,$transparency,$plankton_type,$green,$yellow,$vibrio_count);
                    }
                }else{
                    if($request->has('farm_id') && !is_null($request->farm_id)
                        && $request->has('block_id') && !is_null($request->block_id)
                        && $request->has('pond_id') && !is_null($request->pond_id)
                    ){
                        $block_id=$request->block_id;
                        $farm_id=$request->farm_id;
                        $pond_id=$request->pond_id;
                        $ph=isset($request->pH)?$request->pH:null;
                        $dissolved_oxygen=isset($request->dO)?$request->dO:null;
                        $temperature=isset($request->temperature)?$request->temperature:null;
                        $hco3=isset($request->hCO3)?$request->hCO3:null;
                        $co3=isset($request->cO3)?$request->cO3:null;
                        $ca=isset($request->cA)?$request->cA:null;
                        $cl2=isset($request->cL2)?$request->cL2:null;
                        $mg=isset($request->mG)?$request->mG:null;
                        $k=isset($request->k)?$request->k:null;
                        $nh3=isset($request->nH3)?$request->nH3:null;
                        $no3=isset($request->nO3)?$request->nO3:null;
                        $nh4=isset($request->nH4)?$request->nH4:null;
                        $salinity=isset($request->salinity)?$request->salinity:null;
                        $transparency=isset($request->transparency)?$request->transparency:null;
                        $plankton_type=isset($request->planktonType)?$request->planktonType:null;
                        $green=isset($request->green)?$request->green:null;
                        $yellow=isset($request->yellow)?$request->yellow:null;
                        $vibrio_count=isset($request->vibrioCount)?$request->vibrioCount:null;

                        $whiteFishAPi = new AllApiController();
                        $result=$whiteFishAPi->addMonitoringParameters($user,$block_id,$farm_id,$pond_id,$ph,$dissolved_oxygen,$temperature,$hco3,
                            $co3,$ca,$cl2,$mg,$k,$nh3,$no3,$nh4,$salinity,$transparency,$plankton_type,$green,$yellow,$vibrio_count);

                    }
                }
                if($result){
                    return response()->json([
                        'status' => 'Monitoring Parameters(s) added successfully',
                        'code'=>200
                    ]);
                }else{
                    return response()->json([
                        'status' => 'Monitoring Parameters(s) not added',
                        'code'=>400
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function commercialInputDetails(Request $request){
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    $shrimp_api=new ShrimpApiController();
                    $result=$shrimp_api->commercialInputDetails($user);
                }else{
                    $commercialInputDetails = new AllApiController();
                    $result = $commercialInputDetails->getFeed($user);
                }
                if($result){
                    return $result;
                }else{
                    return response()->json([
                        'status' => 'No Data found',
                        'code'=>204
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function addCommercialInput(Request $request){
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    if($request->has('farm_id') && !is_null($request->farm_id)
                        && $request->has('pond_id') && !is_null($request->pond_id)
                        && $request->has('timing') && !is_null($request->timing)
                        && $request->has('remaining') && !is_null($request->remaining)
                        && $request->has('commercial_inputs') && count($request->commercial_inputs)>0
                    ){
                        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
                        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
                        $farm_cluster_id=$request->farm_id;
                        $pond_id=$request->pond_id;
                        $timing=$request->timing;
                        $remaining=$request->remaining;
                        $commercial_inputs=$request->commercial_inputs;
                        $shrimp_api=new ShrimpApiController();
                        $result=$shrimp_api->addCommercialInput($user,$farm_cluster_id,$pond_id,$timing,$remaining,$commercial_inputs);
                    }
                }else{
                    if($request->has('farm_id') && !is_null($request->farm_id)
                        && $request->has('block_id') && !is_null($request->block_id)
                        && $request->has('pond_id') && !is_null($request->pond_id)
                        && $request->has('commercial_inputs') && !is_null($request->commercial_inputs)
                    ){
                        $farm_id = $request->farm_id;
                        $block_id = $request->block_id;
                        $pond_id = $request->pond_id;
                        $pond_name = $request->pond_name;
                        $timing = $request->timing;
                        $remaining = $request->remaining;
                        $commercial_inputs = $request->commercial_inputs;

                        $whiteFish = new AllApiController();
                        $result = $whiteFish->addFeed($user,$farm_id,$block_id,$pond_id,$pond_name,$timing,$remaining,$commercial_inputs);
                    }
                }
                if($result){
                    return response()->json([
                        'status' => 'Commercial Input added successfully',
                        'code'=>200
                    ]);
                }else{
                    return response()->json([
                        'status' => 'Commercial Input not added',
                        'code'=>400
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function updateInventory(Request $request){
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    if($request->has('items') && count($request->items)>0){
                        $items=$request->items;
                        $shrimp_api=new ShrimpApiController();
                        $result=$shrimp_api->updateInventory($user,$items);
                    }
                }else{
                    if(count($request->items)>0){
                        $items = $request->items;
                        $whiteFish = new AllApiController();
                        $result = $whiteFish->updateStockInventory($user,$items);

                        // return $result;
                    }
                }
                if($result){
                    return response()->json([
                        'status' => 'Inventory Input added successfully',
                        'code'=>200
                    ]);
                }else{
                    return response()->json([
                        'status' => 'Inventory Input not added',
                        'code'=>400
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function inventoryList(Request $request){
        if($request->has('user_id') && !is_null($request->user_id)){
            $user=User::where('user_id',$request->user_id)->first();
            if(count($user)>0){
                if($user->cat_id==1){
                    $shrimp_api=new ShrimpApiController();
                    $result=$shrimp_api->inventoryList($user);
                }else{
                    $inventoryList = new AllApiController();
                    $result = $inventoryList->stockInventoryList($user);
                }
                if($result){
                    return $result;
                }else{
                    return response()->json([
                        'status' => 'No Data found',
                        'code'=>204
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>404
        ]);
    }
    public function enterToEnclosure(Request $request){
//        $token=$request->token;
        if((isset($request->user_id) && !is_null($request->user_id)) && (isset($request->pond_id) && !is_null($request->pond_id))
            && (isset($request->farm_cluster_id) && !is_null($request->farm_cluster_id)) && (isset($request->hatchery_id) && !is_null($request->hatchery_id))){
            if($request->pond_id!=(-1) && $request->farm_cluster_id!=(-1) && $request->hatchery_id!=(-1)){
                $user_id=$request->user_id;
                $pond_id=$request->pond_id;
                $farm_cluster_id=$request->farm_cluster_id;
                $hatchery_id=$request->hatchery_id;
                $user=User::active()->where('user_id',$user_id)->first();
                if(count($user)>0){
                    if($user->type_id==6){
                        $pond_info=SiPond::where('si_farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)
                            ->where('pond_id','like','%'.$pond_id.'%')
                            ->first();
                        $previous_enter_to_enclosure=SiEnterToEnclosure::where('hatchery_id',$hatchery_id)
                            ->where('farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)
                            ->where('pond_id',$pond_info->id)
                            ->first();
                        if(count($previous_enter_to_enclosure)>0){
                            $enter_to_enclosure_id=$previous_enter_to_enclosure->id;
                        }else{
                            $enter_to_enclosure=new SiEnterToEnclosure();
                            $enter_to_enclosure->hatchery_id=$hatchery_id;
                            $enter_to_enclosure->farm_id=$farm_cluster_id;
                            $enter_to_enclosure->farm_manager_id=$user->id;
                            $enter_to_enclosure->pond_id=$pond_info->id;
                            $enter_to_enclosure->pl_quantity=isset($request->pl_amount)?$request->pl_amount:0;
                            $enter_to_enclosure->price=isset($request->pl_price)?$request->pl_price:0;
                            $enter_to_enclosure->save();
                            $enter_to_enclosure_id=$enter_to_enclosure->id;
                        }
                        $avg_bw=isset($request->avg_bw)?$request->avg_bw:0;
                        $current_biomass=isset($request->current_biomass)?$request->current_biomass:0;
                        $number_of_sample=isset($request->number_of_sample)?$request->number_of_sample:0;
                        $survival_rate=isset($request->survival_rate)?$request->survival_rate:0;
                        $total_weight=isset($request->total_weight)?$request->total_weight:0;

                        $animal_sampling=new SiAnimalSampling();
                        $animal_sampling->farm_id=$farm_cluster_id;
                        $animal_sampling->farm_manager_id=$user->id;
                        $animal_sampling->hatchery_id=$hatchery_id;
                        $animal_sampling->pond_id=$pond_info->id;
                        $animal_sampling->number_of_sample=$number_of_sample;
                        $animal_sampling->average_body_weight=$avg_bw;
                        $animal_sampling->current_biomass=$current_biomass;
                        $animal_sampling->survival_rate=$survival_rate;
                        $animal_sampling->total_weight=$total_weight;
//                        $animal_sampling->pl_price=$total_weight;
                        if(count($previous_enter_to_enclosure)>0){
                            $animal_sampling->stocked_pl_amount=$previous_enter_to_enclosure->pl_quantity;
                            $animal_sampling->stocked_pl_price=$previous_enter_to_enclosure->price;
                        }else{
                            $animal_sampling->stocked_pl_amount=$enter_to_enclosure_id->pl_quantity;
                            $animal_sampling->stocked_pl_price=$enter_to_enclosure_id->price;
                        }
                        $animal_sampling->si_enter_to_enclosure_id=$enter_to_enclosure_id;
                        $animal_sampling->save();

                        return response()->json([
                            'message'=>'Data stored successfully!',
                            'result'=>1
                        ]);
                    }elseif ($user->type_id==7){
                        //Cluster Manager
                        $pond_info=ClusterPond::where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)
                            ->where('pond_id','like','%'.$pond_id.'%')
                            ->first();
                        $previous_enter_to_enclosure=ClusterEnterToEnclosure::where('hatchery_id',$hatchery_id)
                            ->where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)
                            ->where('pond_id',$pond_id)
                            ->first();
                        if(count($previous_enter_to_enclosure)>0){
                            $enter_to_enclosure_id=$previous_enter_to_enclosure->id;
                        }else{
                            $enter_to_enclosure=new ClusterEnterToEnclosure();
                            $enter_to_enclosure->hatchery_id=$hatchery_id;
                            $enter_to_enclosure->cluster_id=$farm_cluster_id;
                            $enter_to_enclosure->cluster_manager_id=$user->id;
                            $enter_to_enclosure->pond_id=$pond_info->id;
                            $enter_to_enclosure->pl_quantity=isset($request->pl_amount)?$request->pl_amount:0;
                            $enter_to_enclosure->price=isset($$request->pl_price)?$request->pl_price:0;
                            $enter_to_enclosure->save();
                            $enter_to_enclosure_id=$enter_to_enclosure->id;
                        }
                        $avg_bw=isset($request->avg_bw)?$request->avg_bw:0;
                        $current_biomass=isset($request->current_biomass)?$request->current_biomass:0;
                        $number_of_sample=isset($request->number_of_sample)?$request->number_of_sample:0;
                        $survival_rate=isset($request->survival_rate)?$request->survival_rate:0;
                        $total_weight=isset($request->total_weight)?$request->total_weight:0;

                        $animal_sampling=new ClusterAnimalSampling();
                        $animal_sampling->cluster_id=$farm_cluster_id;
                        $animal_sampling->cluster_manager_id=$user->id;
                        $animal_sampling->hatchery_id=$hatchery_id;
                        $animal_sampling->pond_id=$pond_info->id;
                        $animal_sampling->number_of_sample=$number_of_sample;
                        $animal_sampling->average_body_weight=$avg_bw;
                        $animal_sampling->current_biomass=$current_biomass;
                        $animal_sampling->survival_rate=$survival_rate;
                        $animal_sampling->total_weight=$total_weight;
                        if(count($previous_enter_to_enclosure)<0){
                            $animal_sampling->stocked_pl_amount=$request->pl_amount;
                            $animal_sampling->stocked_pl_price=$request->pl_price;
                        }
                        $animal_sampling->ct_enter_to_enclosure_id=$enter_to_enclosure_id;
                        $animal_sampling->save();
                        return response()->json([
                            'message'=>'Data stored successfully!',
                            'result'=>2
                        ]);
                    }
                }
            }
        }
        return response()->json([
            'message'=>'Data not saved!',
            'result'=>0
        ]);
    }
    public function duringEnclosure(Request $request){
        if((isset($request->pondId) && !is_null($request->pondId)) && (isset($request->farmClusterId) && !is_null($request->farmClusterId))
            && (isset($request->userId) && !is_null($request->userId))){
            if($request->pondId!=(-1) && $request->farmClusterId!=(-1)){
                $pond_id=$request->pondId;
                $farm_cluster_id=$request->farmClusterId;
                $user_id=$request->userId;
                $si_during_enclosure_id=0;
                $ct_during_enclosure_id=0;
                $user=User::active()->where('user_id',$user_id)->first();
                if(count($user)>0){
                    if($user->type_id==6){
                        $pond_info=SiPond::where('si_farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)
                            ->where('name','like','%'.$pond_id.'%')
                            ->first();
                        $today_during_enclosure=SiDuringEnclosure::where('pond_id',$pond_info->id)->where('farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)->whereDate('created_at','=',Carbon::today())->first();
                        if(count($today_during_enclosure)>0){
                            $si_during_enclosure_id=$today_during_enclosure->id;
                        }else{
                            $during_enclosure=new SiDuringEnclosure();
                            $during_enclosure->pond_id=$pond_info->id;
                            $during_enclosure->farm_id=$farm_cluster_id;
                            $during_enclosure->farm_manager_id=$user->id;
                            $during_enclosure->save();
                            $si_during_enclosure_id=$during_enclosure->id;
                        }
                    }elseif ($user->type_id==7){
                        $pond_info=ClusterPond::where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)
                            ->where('name','like','%'.$pond_id.'%')
                            ->first();
                        $today_during_enclosure=ClusterDuringEnclosure::where('pond_id',$pond_info->id)->where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)->whereDate('created_at','=',Carbon::today())->first();
                        if(count($today_during_enclosure)>0){
                            $ct_during_enclosure_id=$today_during_enclosure->id;
                        }else{
                            $during_enclosure=new ClusterDuringEnclosure();
                            $during_enclosure->pond_id=$pond_info->id;
                            $during_enclosure->cluster_id=$farm_cluster_id;
                            $during_enclosure->cluster_manager_id=$user->id;
                            $during_enclosure->save();
                            $ct_during_enclosure_id=$during_enclosure->id;
                        }
                    }
                    if($si_during_enclosure_id>0 || $ct_during_enclosure_id>0){
                        $selectedFeedTime=isset($request->selectedFeedTime)?$request->selectedFeedTime:NULL;
                        $selectedFeeds=isset($request->selectedFeeds)?$request->selectedFeeds:NULL;
                        if (!is_null($selectedFeedTime) || count($selectedFeeds)>0){
                            foreach ($request->selectedFeeds as $feed){
                                if($feed['feedCode']!=(-1)){
                                    $feed_details=new FeedDetails();
                                    $feed_details->si_during_enclosure_id=$si_during_enclosure_id;
                                    $feed_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                                    $feed_details->feed_id=$feed['feedCode'];
                                    $feed_details->quantity=$feed['feedQty'];
                                    $feed_details->timing=$request->selectedFeedTime;
                                    $feed_details->save();
                                }
                            }
                        }
                        $selectedRemarks=isset($request->selectedRemarks)?$request->selectedRemarks:NULL;
                        if(count($selectedRemarks)>0){
                            foreach ($request->selectedRemarks as $remark){
                                if($remark['remarkCode']!=(-1)){
                                    $remark_details=new RemarkDetails();
                                    $remark_details->si_during_enclosure_id=$si_during_enclosure_id;
                                    $remark_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                                    $remark_details->remark_id=$remark['remarkCode'];
                                    $remark_details->quantity=$remark['remarkQty'];
                                    $remark_details->save();
                                }
                            }
                        }
                        if((isset($request->hCO3) && !is_null($request->hCO3))
                            || (isset($request->cO3) && !is_null($request->cO3)) || (isset($request->nH3) && !is_null($request->nH3))
                            || (isset($request->nO3) && !is_null($request->nO3)) || (isset($request->salinity) && !is_null($request->salinity)) || (isset($request->transparency) && !is_null($request->transparency))
                            || (isset($request->planktonType) && !is_null($request->planktonType)) || (isset($request->green) && !is_null($request->green)) || (isset($request->yellow) && !is_null($request->yellow))
                            || (isset($request->vibrioCount) && !is_null($request->vibrioCount))){
                            $water_parameter_details=new WaterParameterDetails();
                            $water_parameter_details->si_during_enclosure_id=$si_during_enclosure_id;
                            $water_parameter_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                            $water_parameter_details->hco3=$request->hCO3;
                            $water_parameter_details->co3=$request->cO3;
                            $water_parameter_details->nh3=$request->nH3;
                            $water_parameter_details->n03=$request->nO3;
                            $water_parameter_details->salinity=$request->salinity;
                            $water_parameter_details->transparency=$request->transparency;
                            $water_parameter_details->plankton_type=$request->planktonType;
                            $water_parameter_details->green=$request->green;
                            $water_parameter_details->yellow=$request->yellow;
                            $water_parameter_details->vibrio_count=$request->vibrioCount;
                            $water_parameter_details->save();
                        }
                        //Dissolved Oxygen data insert
                        if (isset($request->dO) && !is_null($request->dO)){
                            if(!is_null($request->dO['doQty'])){
                                $dissolved_oxygen_details=new DissolvedOxygenDetails();
                                $dissolved_oxygen_details->si_during_enclosure_id=$si_during_enclosure_id;
                                $dissolved_oxygen_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                                $dissolved_oxygen_details->quantity=$request->dO['doQty'];
                                if($request->dO['timeCode']==0){
                                    $dissolved_oxygen_details->timing='am';
                                }else{
                                    $dissolved_oxygen_details->timing='pm';
                                }
                                $dissolved_oxygen_details->save();
                            }
                        }
                        //PH data insert
                        if (isset($request->pH) && !is_null($request->pH)){
                            if(!is_null($request->pH['pH'])){
                                $ph_details=new PHDetails();
                                $ph_details->si_during_enclosure_id=$si_during_enclosure_id;
                                $ph_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                                $ph_details->quantity=$request->pH['pH'];
                                if($request->pH['timeCode']==0){
                                    $ph_details->timing='am';
                                }else{
                                    $ph_details->timing='pm';
                                }
                                $ph_details->save();
                            }
                        }
                        //Temperature data insert
                        if (isset($request->temperature) && !is_null($request->temperature)){
                            if(!is_null($request->temperature['temperature'])){
                                $temperature_details=new TemperatureDetails();
                                $temperature_details->si_during_enclosure_id=$si_during_enclosure_id;
                                $temperature_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                                $temperature_details->quantity=$request->temperature['temperature'];
                                if($request->temperature['timeCode']==0){
                                    $temperature_details->timing='am';
                                }else{
                                    $temperature_details->timing='pm';
                                }
                                $temperature_details->save();
                            }
                        }
                        return response()->json([
                            'message'=>'Data stored successfully!',
                            'result'=>1
                        ]);
                    }
                }
            }
        }
        return response()->json([
            'message' => 'Data not saved!',
            'result'=>0
        ]);
    }
    public function harvest_previous(Request $request){
        if((isset($request->user_id) && !is_null($request->user_id)) && (isset($request->pond_id) && !is_null($request->pond_id))
            && (isset($request->firm_cluster_id) && !is_null($request->firm_cluster_id))){
            if($request->pond_id!=(-1) && $request->firm_cluster_id!=(-1)){
                $user_id=$request->user_id;
                $pond_id=$request->pond_id;
                $farm_cluster_id=$request->firm_cluster_id;
                $user=User::active()->where('user_id',$user_id)->first();
                if(count($user)>0) {
                    if ($user->type_id == 6) {
                        $pond_info=SiPond::where('si_farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)
                            ->where('name','like','%'.$pond_id.'%')
                            ->first();
                        $previous_enter_to_enclosure=SiEnterToEnclosure::where('farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)
                            ->where('pond_id',$pond_info->id)
                            ->first();
                        $previous_enter_to_enclosure->status='harvest';
                        $previous_enter_to_enclosure->save();

                        $harvest=new SiHarvest();
                        $harvest->si_during_enclosure_id=$previous_enter_to_enclosure->id;
                        $harvest->farm_manager_id=$user->id;
                        $harvest->farm_id=$farm_cluster_id;
                        $harvest->pond_id=$pond_info->id;
                        $harvest->save();

                        return response()->json([
                            'message'=>'Animal harvest done successfully!',
                            'result'=>1
                        ]);
                    }elseif ($user->type_id==7){
                        $pond_info=ClusterPond::where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)
                            ->where('name','like','%'.$pond_id.'%')
                            ->first();
                        $previous_enter_to_enclosure=ClusterEnterToEnclosure::where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)
                            ->where('pond_id',$pond_info->id)
                            ->first();
                        $previous_enter_to_enclosure->status='harvest';
                        $previous_enter_to_enclosure->save();

                        $harvest=new ClusterHarvest();
                        $harvest->ct_during_enclosure_id=$previous_enter_to_enclosure->id;
                        $harvest->cluster_manager_id=$user->id;
                        $harvest->cluster_id=$farm_cluster_id;
                        $harvest->pond_id=$pond_info->id;
                        $harvest->save();

                        return response()->json([
                            'message'=>'Animal harvest done successfully!',
                            'result'=>2
                        ]);
                    }
                }
            }
        }
        return response()->json([
            'message' => 'Animal harvest not done!',
            'result'=>0
        ]);
    }
}
