<?php

namespace App\Http\Controllers\Test;

use App\DissolvedOxygenDetails;
use App\FarmingType;
use App\HarvestDetails;
use App\Inventory;
use App\InventoryUsedInWaterDetails;
use App\PHDetails;
use App\ShrimpCultureModels\SiAnimalHarvest;
use App\ShrimpCultureModels\SiAnimalSampling;
use App\ShrimpCultureModels\SiAnimalSamplingNew;
use App\ShrimpCultureModels\SiFarm;
use App\ShrimpCultureModels\SiPlStock;
use App\ShrimpCultureModels\SiPond;
use App\ShrimpCultureModels\SiPondManagement;
use App\TemperatureDetails;
use App\User;
use App\WaterParameterDetails;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class TestController extends Controller
{
    public function indexInput(){
        $data['title']='Test Input';
//        $data['feeds']=Inventory::where('type_id',1)->where('brand_id',3)->pluck('name','id');
        $data['feeds']=Inventory::where('type_id',1)->pluck('name','id');
        $data['medicines']=Inventory::whereBetween('type_id',[2,6])->pluck('name','id');
//        $data['farms']=SiFarm::withoutZero()->where('id',2)->pluck('name','id');
        $data['farms']=SiFarm::withoutZero()->where('id',1)->pluck('name','id');
//        $data['ponds']=SiPond::where('si_farm_id',2)->pluck('name','id');
        $data['ponds']=SiPond::where('si_farm_id',1)->pluck('name','id');
        $data['farming_type']=FarmingType::pluck('name','id');
        $data['feed_timing']=[1=>1,2=>2,3=>3,4=>4,5=>5];
        $data['unit']=['KG'=>'KG','G'=>'G','L'=>'L'];
        return view('test_input.test_input',$data);
    }
    public function getPondByFarmId(Request $request){
        if(!is_null($request->farm_id)){
//            $data['ponds']=SiPond::where('si_farm_id',2)->pluck('name','id');
            $data['ponds']=SiPond::where('si_farm_id',1)->pluck('name','id');
            return view('test_input.selected_pond_view',$data);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeInput(Request $request){
//        return $request->all();
        $request->validate([
            'farm_id' => 'required',
            'pond_id' => 'required',
            'date' => 'required',
//            'feed_code'=>'nullable|numeric',
//            'feed_quantity'=>'nullable|numeric',
//            'remaining_feed_quantity'=>'numeric',
//            'feed_timing'=>'nullable|numeric',
//            'medicine_code'=>'nullable|numeric',
//            'medicine_quantity'=>'nullable|numeric',
//            'dissolved_oxygen'=>'nullable|numeric',
//            'dissolved_oxygen_timing'=>'nullable|numeric',
//            'ph'=>'nullable|numeric',
//            'ph_timing'=>'nullable|numeric',
//            'temperature'=>'nullable|numeric',
//            'temperature_timing'=>'nullable|numeric',
//            'salinity'=>'nullable|numeric',
//            'transparency'=>'nullable|numeric',
//            'alkanitity_hco3'=>'nullable|numeric',
//            'carbonate_co3'=>'nullable|numeric',
//            'calcium_ca'=>'nullable|numeric',
//            'chlorine_cl2'=>'nullable|numeric',
//            'magnesium_mg'=>'nullable|numeric',
//            'green'=>'nullable|numeric',
//            'yellow'=>'nullable|numeric',
//            'vibrio_count'=>'nullable|numeric',
//            'potassium_k'=>'nullable|numeric',
//            'ammonia_nh3'=>'nullable|numeric',
//            'nitrate_no3'=>'nullable|numeric',
//            'ammonium_nh4'=>'nullable|numeric',
//            'plankton_type'=>'nullable',
        ]);
        $feed_code=$request->feed_code;
        $feed_quantity=$request->feed_quantity;
        $remaining_feed_quantity=$request->remaining_feed_quantity;
        $feed_timing=$request->feed_timing;
        $medicine_code=$request->medicine_code;
        $medicine_quantity=$request->medicine_quantity;
        $unit=$request->unit;
        $dissolved_oxygen_am=$request->dissolved_oxygen_am;
        $dissolved_oxygen_pm=$request->dissolved_oxygen_pm;
        $ph_am=$request->ph_am;
        $ph_pm=$request->ph_pm;
        $temperature_am=$request->temperature_am;
        $temperature_pm=$request->temperature_pm;
        $salinity=$request->salinity;
        $transparency=$request->transparency;
        $hco3=$request->alkanitity_hco3;
        $co3=$request->carbonate_co3;
        $ca=$request->calcium_ca;
        $cl2=$request->chlorine_cl2;
        $mg=$request->magnesium_mg;
        $green=$request->green;
        $yellow=$request->yellow;
        $vibrio_count=$request->vibrio_count;
        $k=$request->potassium_k;
        $nh3=$request->ammonia_nh3;
        $no3=$request->nitrate_no3;
        $nh4=$request->ammonium_nh4;
        $plankton_type=$request->plankton_type;

        $user_id = 7;
        $farm_id = $request->farm_id;
        $pond_id = $request->pond_id;
        $date=new Carbon($request->date);
        $date=$date->format('Y-m-d H:i:s');
//        return $farm_id;
        $before_si_pond_management=SiPondManagement::where('pond_id',$pond_id)->where('farm_id',$farm_id)
            ->where('farm_manager_id',$user_id)->whereDate('created_at','=',$date)->first();
//        return $before_si_pond_management;
        if(count($before_si_pond_management)>0){
            $si_pond_management_id=$before_si_pond_management->id;
        }else{
            $si_pond_management=new SiPondManagement();
            $si_pond_management->pond_id=$pond_id;
            $si_pond_management->farm_id=$farm_id;
            $si_pond_management->farm_manager_id=$user_id;
            $si_pond_management->created_at=$date;
            $si_pond_management->save();
            $si_pond_management_id=$si_pond_management->id;
        }

        if($si_pond_management_id>0){
            //Adding Inventory Details
            if (count($feed_code)>0 && count($feed_quantity)>0 && count($feed_timing)>0 && count($remaining_feed_quantity)>0){
                $i=0;
                foreach ($feed_quantity as $feed){
                    if(!is_null($feed)){
                        $feed_details=new InventoryUsedInWaterDetails();
                        $feed_details->si_pond_management_id=$si_pond_management_id;
                        $feed_details->ct_pond_management_id=0;
                        $feed_details->wf_during_enclosure_id=0;
                        $feed_details->inventory_id=$feed_code[$i];
                        $feed_details->quantity=$feed;
                        $feed_details->remaining_quantity=$remaining_feed_quantity[$i];
                        $feed_details->timing=$feed_timing[$i];
                        $feed_details->created_at=$date;
                        $feed_details->save();
                    }
                    $i++;
                }
            }
            if (count($medicine_code)>0 && count($medicine_quantity)>0){
                $i=0;
                foreach ($medicine_quantity as $medicine){
                    if(!is_null($medicine)){
                        $medicine_details=new InventoryUsedInWaterDetails();
                        $medicine_details->si_pond_management_id=$si_pond_management_id;
                        $medicine_details->ct_pond_management_id=0;
                        $medicine_details->wf_during_enclosure_id=0;
                        $medicine_details->inventory_id=$medicine_code[$i];
                        $medicine_details->quantity=$medicine;
                        $medicine_details->unit=$unit[$i];
                        $medicine_details->created_at=$date;
                        $medicine_details->save();
                    }
                    $i++;
                }
            }

            //Adding Water Parameters
            if(!is_null($hco3) || !is_null($co3) || !is_null($ca) || !is_null($cl2) || !is_null($mg) || !is_null($k) || !is_null($nh3)
                || !is_null($no3) || !is_null($nh4) || !is_null($salinity) || !is_null($transparency) || !is_null($green)
                || !is_null($yellow) || !is_null($vibrio_count)){
                $water_parameter_details=new WaterParameterDetails();
                $water_parameter_details->si_pond_management_id=$si_pond_management_id;
                $water_parameter_details->ct_pond_management_id=0;
                $water_parameter_details->wf_during_enclosure_id=0;
                $water_parameter_details->alkalinity_hco3=$hco3;
                $water_parameter_details->carbonate_co3=$co3;
                $water_parameter_details->calcium_ca=$ca;
                $water_parameter_details->chlorine_cl2=$cl2;
                $water_parameter_details->magnesium_mg=$mg;
                $water_parameter_details->potassium_k=$k;
                $water_parameter_details->ammonia_nh3=$nh3;
                $water_parameter_details->nitrate_no3=$no3;
                $water_parameter_details->ammonium_nh4=$nh4;
                $water_parameter_details->salinity=$salinity;
                $water_parameter_details->transparency=$transparency;
                $water_parameter_details->plankton_type=$plankton_type;
                $water_parameter_details->green=$green;
                $water_parameter_details->yellow=$yellow;
                $water_parameter_details->vibrio_count=$vibrio_count;
                $water_parameter_details->created_at=$date;
                $water_parameter_details->save();
            }

            //Adding Dissolved Oxygen
            if(!is_null($dissolved_oxygen_am)){
                $this->dissolvedOxygenEntry($si_pond_management_id, $dissolved_oxygen_am, 'am', $date);
            }
            if(!is_null($dissolved_oxygen_pm)){
                $this->dissolvedOxygenEntry($si_pond_management_id, $dissolved_oxygen_pm, 'pm', $date);
            }

            //Adding PH
            if(!is_null($ph_am)){
                $this->phEntry($si_pond_management_id, $ph_am, 'am', $date);
            }
            if(!is_null($ph_pm)){
                $this->phEntry($si_pond_management_id, $ph_pm, 'pm', $date);
            }

            //Adding Temperature
            if(!is_null($temperature_am)){
                $this->temperatureEntry($si_pond_management_id, $temperature_am, 'am', $date);
            }
            if(!is_null($temperature_pm)){
                $this->temperatureEntry($si_pond_management_id, $temperature_pm, 'pm', $date);
            }
        }
        Session::flash('message','Data Entered Successfully!');
        return redirect()->route('test_input.index');
    }
    public function dissolvedOxygenEntry($si_pond_management_id, $dissolved_oxygen, $dissolved_oxygen_timing, $date)
    {
        $dissolved_oxygen_details=new DissolvedOxygenDetails();
        $dissolved_oxygen_details->si_pond_management_id=$si_pond_management_id;
        $dissolved_oxygen_details->ct_pond_management_id=0;
        $dissolved_oxygen_details->wf_during_enclosure_id=0;
        $dissolved_oxygen_details->quantity=$dissolved_oxygen;
        $dissolved_oxygen_details->timing=$dissolved_oxygen_timing;
        $dissolved_oxygen_details->created_at=$date;
        $dissolved_oxygen_details->save();
    }
    public function phEntry($si_pond_management_id, $ph, $ph_timing, $date)
    {
        $ph_details=new PHDetails();
        $ph_details->si_pond_management_id=$si_pond_management_id;
        $ph_details->ct_pond_management_id=0;
        $ph_details->wf_during_enclosure_id=0;
        $ph_details->quantity=$ph;
        $ph_details->timing=$ph_timing;
        $ph_details->created_at=$date;
        $ph_details->save();
    }
    public function temperatureEntry($si_pond_management_id, $temperature, $temperature_timing, $date)
    {
        $temperature_details=new TemperatureDetails();
        $temperature_details->si_pond_management_id=$si_pond_management_id;
        $temperature_details->ct_pond_management_id=0;
        $temperature_details->wf_during_enclosure_id=0;
        $temperature_details->quantity=$temperature;
        $temperature_details->timing=$temperature_timing;
        $temperature_details->created_at=$date;
        $temperature_details->save();
    }
    public function storeFeedInput(Request $request){
        return $request->validate([
            'user_id'=>'required',
            'ph'=>'required',
            'ph_timing'=>'required',
            'temperature'=>'required',
            'temperature_timing'=>'required',
            'salinity'=>'required',
            'transparency'=>'required',
            'hco3'=>'required',
            'co3'=>'required',
            'nh3'=>'required',
            'no3'=>'required',
            'green'=>'required',
            'yellow'=>'required',
            'vibrio_count'=>'required',
            'chlorin'=>'required',
            'magnesium'=>'required',
            'calcium'=>'required',
        ]);
        $user=User::where('user_id','like','%'.$request->user_id.'%')->first();
        if(count($user)>0){
            if($user->type_id==6){

            }
        }
    }
    public function storePHInput(){}
    public function indexSampling(){
        $data['title']='Test Sampling';
        $data['farms']=SiFarm::where('id',2)->pluck('name','id');
        $data['ponds']=SiPond::where('si_farm_id',2)->pluck('name','id');
        return view('test_input.test_sampling',$data);
    }
    public function storeSampling(Request $request){
//        return $request->all();
        $request->validate([
            'farm_id' => 'required',
            'date' => 'required',
        ]);
        $user_id = 7;
        $farm_id = $request->farm_id;
//        $pond_ids = $request->pond_id?$request->pond_id:null;
        $date=new Carbon($request->date);
        $date=$date->format('Y-m-d H:i:s');
        $count=0;
        if(!is_null($farm_id) && ($request->has('pond_id')) && count($request->pond_id)>0){
            foreach ($request->pond_id as $pond_id){
                $total_weight=$request->total_weight[$count]?$request->total_weight[$count]:null;
                $number_of_sample=$request->number_of_sample[$count]?$request->number_of_sample[$count]:null;
                if(!is_null($pond_id) && !is_null($total_weight) && !is_null($number_of_sample)){
                    $stocking_data=SiPlStock::where('farm_id',$farm_id)
                        ->where('farm_manager_id',$user_id)
                        ->where('pond_id',$pond_id)
                        ->first();

                    if(count($stocking_data)>0){
                        $average_body_weight=floatval(number_format((float) $total_weight/$number_of_sample,2,'.',''));
                        $biomass=floatval(number_format((float) $number_of_sample*$average_body_weight/1000,2,'.',''));

                        $animal_sampling=new SiAnimalSamplingNew();
                        $animal_sampling->farm_id=$farm_id;
                        $animal_sampling->farm_manager_id=$user_id;
                        $animal_sampling->hatchery_id=$stocking_data->hatchery_id;
                        $animal_sampling->pond_id=$pond_id;
                        $animal_sampling->number_of_sample=$number_of_sample;

                        $animal_sampling->average_body_weight=$average_body_weight;
                        $animal_sampling->current_biomass=$biomass;
//        $animal_sampling->survival_rate=$request->survival_rate;
                        $animal_sampling->total_weight=$total_weight;
                        $animal_sampling->stocked_pl_quantity=$stocking_data->pl_quantity;
                        $animal_sampling->stocked_pl_age=$stocking_data->pl_age;
                        $animal_sampling->stocked_pl_price=$stocking_data->pl_price;
                        $animal_sampling->si_pl_stock_id=$stocking_data->id;
                        $animal_sampling->created_at=$date;
                        $animal_sampling->save();
                        $count++;
                    }
                }
            }
        }


        Session::flash('message','Data Entered Successfully!');
        return redirect()->route('test_sampling.index');
    }
    public function indexHarvest(){
        $data['title']='Test Harvesting';
        $data['farms']=SiFarm::withoutZero()->pluck('name','id');
        $data['ponds']=SiPond::pluck('name','id');
        $data['unit']=['KG'=>'KG','G'=>'G','Pieces'=>'Pieces','Bundle'=>'Bundle'];
        $data['crop_number']=[1=>'1st Crop',2=>'2nd Crop',3=>'3rd Crop',4=>'4th Crop'];
        return view('test_input.test_harvesting',$data);
    }
    public function storeHarvest(Request $request){
        $request->validate([
            'farm_id' => 'required',
            'pond_id' => 'required',
            'date' => 'required',
        ]);
        $user_id = 7;
        $farm_id = $request->farm_id;
        $pond_id = $request->pond_id;
        $date=new Carbon($request->date);
        $date=$date->format('Y-m-d H:i:s');

        $harvest=new SiAnimalHarvest();
        $harvest->farm_id=$farm_id;
        $harvest->farm_manager_id=$user_id;
        $harvest->pond_id=$pond_id;
        $harvest->days_of_culture=$request->days_of_culture;
        $harvest->crop_number=$request->crop_number;
        $harvest->total_feed=$request->total_feed;
        $harvest->total_biomass=$request->biomass;
        $harvest->total_quantity=$request->weight;
        $harvest->total_price=$request->price;
        $harvest->fcr=$request->fcr;
        $harvest->created_at=$date;
        $harvest->save();

        $harvest_details=new HarvestDetails();
        $harvest_details->si_harvest_id=$harvest->id;
        $harvest_details->ct_harvest_id=0;
        $harvest_details->wf_harvest_id=0;
        $harvest_details->quantity=$request->weight;
        $harvest_details->unit=$request->unit;
//        $harvest_details->biomass=$request->biomass;
        $harvest_details->price=$request->price;
        $harvest_details->created_at=$date;
        $harvest_details->save();

        $harvest_count=SiAnimalHarvest::withoutZero()->where('farm_id',$farm_id)->where('farm_manager_id',$user_id)->where('pond_id',$pond_id)->count('id');

        $si_pl_stock=SiPlStock::where('farm_id',$farm_id)->where('farm_manager_id',$user_id)->where('pond_id',$pond_id)->where('status','enclosure')
            ->where('harvest_tracking_id',0)->where('harvest_counter',0)->get();

        foreach ($si_pl_stock as $pl_stock){
            $pl_stock->status='harvested';
            $pl_stock->harvest_tracking_id=$harvest->id;
            $pl_stock->harvest_counter=$harvest_count;
            $pl_stock->save();
        }

        $si_animal_sampling=SiAnimalSampling::where('farm_id',$farm_id)->where('farm_manager_id',$user_id)->where('pond_id',$pond_id)
            ->where('harvest_tracking_id',0)->where('harvest_counter',0)->get();

        foreach ($si_animal_sampling as $animal_sampling){
            $animal_sampling->harvest_tracking_id=$harvest->id;
            $animal_sampling->harvest_counter=$harvest_count;
            $animal_sampling->save();
        }

        $si_pond_management=SiPondManagement::where('farm_id',$farm_id)->where('farm_manager_id',$user_id)->where('pond_id',$pond_id)
            ->where('harvest_tracking_id',0)->where('harvest_counter',0)->get();

        foreach ($si_pond_management as $pond_management){
            $pond_management->harvest_tracking_id=$harvest->id;
            $pond_management->harvest_counter=$harvest_count;
            $pond_management->save();
        }

        Session::flash('message','Data Entered Successfully!');
        return redirect()->route('test_harvesting.index');
    }
}
