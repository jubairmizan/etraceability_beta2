<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        $credentials['status'] = 1;
        return $credentials;
    }
    protected function authenticated(Request $request,$user)
    {
        if($user->cat_id==1){
            if ($user->type_id==1) {
                return redirect()->route('admin.dashboard');
            }
            elseif ($user->type_id==2){
                return redirect()->route('shrimp_culture.client.dashboard');
            }
            if ($user->type_id==9) {
                return redirect()->route('shrimp_culture.processing_plant.dashboard');
            }
        }
        elseif($user->cat_id==2){
            if ($user->type_id==2) {
                return redirect()->route('WhiteFish.client.dashboard');
            }
        }
//        elseif ($user->type_id==2 && $user->farming_type_id==2){
//            return redirect()->route('semi_intensive_client.dashboard');
//        }
//        if ($user->type_id==9) {
//            return redirect()->route('processing_plant.dashboard');
//        }
//        elseif ($user->type_id==3){
//            return redirect()->route('buyer.home');
//        }elseif ($user->type_id==3){
//            return redirect()->route('farmer.home');
//        }elseif ($user->type_id==4){
//            return redirect()->route('hatchery_manager.home');
//        }elseif ($user->type_id==5){
//            return redirect()->route('farm_manager.home');
//        }elseif ($user->type_id==6){
//            return redirect()->route('collection_center_manager.home');
//        }else{
//            return redirect()->route('processing_plant_manager.home');
//        }
    }
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors=[$this->username()=>trans('auth.failed')];
        $user=User::where($this->username(),$request->{$this->username()})->first();
        if($user && \Hash::check($request->password,$user->password) && $user->status!=1){
            $errors = [$this->username() => 'This account is not active.'];
        }
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }
    public function username()
    {
        return 'user_id';
    }
}
