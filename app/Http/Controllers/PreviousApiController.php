<?php

namespace App\Http\Controllers;

use App\DissolvedOxygenDetails;
use App\InventoryUsedInWaterDetails;
use App\PHDetails;
use App\ShrimpCultureModels\ClusterAnimalHarvest;
use App\ShrimpCultureModels\ClusterAnimalSampling;
use App\ShrimpCultureModels\ClusterPlStock;
use App\ShrimpCultureModels\ClusterPond;
use App\ShrimpCultureModels\ClusterPondManagement;
use App\ShrimpCultureModels\SiAnimalHarvest;
use App\ShrimpCultureModels\SiAnimalSampling;
use App\ShrimpCultureModels\SiPlStock;
use App\ShrimpCultureModels\SiPond;
use App\ShrimpCultureModels\SiPondManagement;
use App\TemperatureDetails;
use App\User;
use App\WaterParameterDetails;
use Illuminate\Http\Request;

class PreviousApiController extends Controller
{
    public function enterToEnclosure(Request $request){
//        $token=$request->token;
        if((isset($request->user_id) && !is_null($request->user_id)) && (isset($request->pond_id) && !is_null($request->pond_id))
            && (isset($request->farm_cluster_id) && !is_null($request->farm_cluster_id)) && (isset($request->hatchery_id) && !is_null($request->hatchery_id))){
            if($request->pond_id!=(-1) && $request->farm_cluster_id!=(-1) && $request->hatchery_id!=(-1)){
                $user_id=$request->user_id;
                $pond_id=$request->pond_id;
                $farm_cluster_id=$request->farm_cluster_id;
                $hatchery_id=$request->hatchery_id;
                $user=User::active()->where('user_id',$user_id)->first();
                if(count($user)>0){
                    if($user->type_id==6){
                        $pond_info=SiPond::where('si_farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)
                            ->where('pond_id','like','%'.$pond_id.'%')
                            ->first();
                        $previous_enter_to_enclosure=SiPlStock::where('hatchery_id',$hatchery_id)
                            ->where('farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)
                            ->where('pond_id',$pond_info->id)
                            ->first();
                        if(count($previous_enter_to_enclosure)>0){
                            $enter_to_enclosure_id=$previous_enter_to_enclosure->id;
                        }else{
                            $enter_to_enclosure=new SiPlStock();
                            $enter_to_enclosure->hatchery_id=$hatchery_id;
                            $enter_to_enclosure->farm_id=$farm_cluster_id;
                            $enter_to_enclosure->farm_manager_id=$user->id;
                            $enter_to_enclosure->pond_id=$pond_info->id;
                            $enter_to_enclosure->pl_quantity=isset($request->pl_amount)?$request->pl_amount:0;
                            $enter_to_enclosure->price=isset($request->pl_price)?$request->pl_price:0;
                            $enter_to_enclosure->save();
                            $enter_to_enclosure_id=$enter_to_enclosure->id;
                        }
                        $avg_bw=isset($request->avg_bw)?$request->avg_bw:0;
                        $current_biomass=isset($request->current_biomass)?$request->current_biomass:0;
                        $number_of_sample=isset($request->number_of_sample)?$request->number_of_sample:0;
                        $survival_rate=isset($request->survival_rate)?$request->survival_rate:0;
                        $total_weight=isset($request->total_weight)?$request->total_weight:0;

                        $animal_sampling=new SiAnimalSampling();
                        $animal_sampling->farm_id=$farm_cluster_id;
                        $animal_sampling->farm_manager_id=$user->id;
                        $animal_sampling->hatchery_id=$hatchery_id;
                        $animal_sampling->pond_id=$pond_info->id;
                        $animal_sampling->number_of_sample=$number_of_sample;
                        $animal_sampling->average_body_weight=$avg_bw;
                        $animal_sampling->current_biomass=$current_biomass;
                        $animal_sampling->survival_rate=$survival_rate;
                        $animal_sampling->total_weight=$total_weight;
//                        $animal_sampling->pl_price=$total_weight;
                        if(count($previous_enter_to_enclosure)>0){
                            $animal_sampling->stocked_pl_quantity=$previous_enter_to_enclosure->pl_quantity;
                            $animal_sampling->stocked_pl_price=$previous_enter_to_enclosure->price;
                        }else{
                            $animal_sampling->stocked_pl_quantity=$enter_to_enclosure_id->pl_quantity;
                            $animal_sampling->stocked_pl_price=$enter_to_enclosure_id->price;
                        }
                        $animal_sampling->si_pl_stock_id=$enter_to_enclosure_id;
                        $animal_sampling->save();

                        return response()->json([
                            'message'=>'Data stored successfully!',
                            'result'=>1
                        ]);
                    }elseif ($user->type_id==7){
                        //Cluster Manager
                        $pond_info=ClusterPond::where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)
                            ->where('pond_id','like','%'.$pond_id.'%')
                            ->first();
                        $previous_enter_to_enclosure=ClusterPlStock::where('hatchery_id',$hatchery_id)
                            ->where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)
                            ->where('pond_id',$pond_id)
                            ->first();
                        if(count($previous_enter_to_enclosure)>0){
                            $enter_to_enclosure_id=$previous_enter_to_enclosure->id;
                        }else{
                            $enter_to_enclosure=new ClusterPlStock();
                            $enter_to_enclosure->hatchery_id=$hatchery_id;
                            $enter_to_enclosure->cluster_id=$farm_cluster_id;
                            $enter_to_enclosure->cluster_manager_id=$user->id;
                            $enter_to_enclosure->pond_id=$pond_info->id;
                            $enter_to_enclosure->pl_quantity=isset($request->pl_amount)?$request->pl_amount:0;
                            $enter_to_enclosure->price=isset($$request->pl_price)?$request->pl_price:0;
                            $enter_to_enclosure->save();
                            $enter_to_enclosure_id=$enter_to_enclosure->id;
                        }
                        $avg_bw=isset($request->avg_bw)?$request->avg_bw:0;
                        $current_biomass=isset($request->current_biomass)?$request->current_biomass:0;
                        $number_of_sample=isset($request->number_of_sample)?$request->number_of_sample:0;
                        $survival_rate=isset($request->survival_rate)?$request->survival_rate:0;
                        $total_weight=isset($request->total_weight)?$request->total_weight:0;

                        $animal_sampling=new ClusterAnimalSampling();
                        $animal_sampling->cluster_id=$farm_cluster_id;
                        $animal_sampling->cluster_manager_id=$user->id;
                        $animal_sampling->hatchery_id=$hatchery_id;
                        $animal_sampling->pond_id=$pond_info->id;
                        $animal_sampling->number_of_sample=$number_of_sample;
                        $animal_sampling->average_body_weight=$avg_bw;
                        $animal_sampling->current_biomass=$current_biomass;
                        $animal_sampling->survival_rate=$survival_rate;
                        $animal_sampling->total_weight=$total_weight;

                        if(count($previous_enter_to_enclosure)>0){
                            $animal_sampling->stocked_pl_quantity=$previous_enter_to_enclosure->pl_quantity;
                            $animal_sampling->stocked_pl_price=$previous_enter_to_enclosure->price;
                        }else{
                            $animal_sampling->stocked_pl_quantity=$enter_to_enclosure_id->pl_quantity;
                            $animal_sampling->stocked_pl_price=$enter_to_enclosure_id->price;
                        }

                        $animal_sampling->cluster_pl_stock_id=$enter_to_enclosure_id;
                        $animal_sampling->save();
                        return response()->json([
                            'message'=>'Data stored successfully!',
                            'result'=>2
                        ]);
                    }
                }
            }
        }
        return response()->json([
            'message'=>'Data not saved!',
            'result'=>0
        ]);
    }
    public function duringEnclosure(Request $request){
        if((isset($request->pondId) && !is_null($request->pondId)) && (isset($request->farmClusterId) && !is_null($request->farmClusterId))
            && (isset($request->userId) && !is_null($request->userId))){
            if($request->pondId!=(-1) && $request->farmClusterId!=(-1)){
                $pond_id=$request->pondId;
                $farm_cluster_id=$request->farmClusterId;
                $user_id=$request->userId;
                $si_during_enclosure_id=0;
                $ct_during_enclosure_id=0;
                $user=User::active()->where('user_id',$user_id)->first();
                if(count($user)>0){
                    if($user->type_id==6){
                        $pond_info=SiPond::where('si_farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)
                            ->where('name','like','%'.$pond_id.'%')
                            ->first();
                        $today_during_enclosure=SiPondManagement::where('pond_id',$pond_info->id)->where('farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)->whereDate('created_at','=',Carbon::today())->first();
                        if(count($today_during_enclosure)>0){
                            $si_during_enclosure_id=$today_during_enclosure->id;
                        }else{
                            $during_enclosure=new SiPondManagement();
                            $during_enclosure->pond_id=$pond_info->id;
                            $during_enclosure->farm_id=$farm_cluster_id;
                            $during_enclosure->farm_manager_id=$user->id;
                            $during_enclosure->save();
                            $si_during_enclosure_id=$during_enclosure->id;
                        }
                    }elseif ($user->type_id==7){
                        $pond_info=ClusterPond::where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)
                            ->where('name','like','%'.$pond_id.'%')
                            ->first();
                        $today_during_enclosure=ClusterPondManagement::where('pond_id',$pond_info->id)->where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)->whereDate('created_at','=',Carbon::today())->first();
                        if(count($today_during_enclosure)>0){
                            $ct_during_enclosure_id=$today_during_enclosure->id;
                        }else{
                            $during_enclosure=new ClusterPondManagement();
                            $during_enclosure->pond_id=$pond_info->id;
                            $during_enclosure->cluster_id=$farm_cluster_id;
                            $during_enclosure->cluster_manager_id=$user->id;
                            $during_enclosure->save();
                            $ct_during_enclosure_id=$during_enclosure->id;
                        }
                    }
                    if($si_during_enclosure_id>0 || $ct_during_enclosure_id>0){
                        $selectedFeedTime=isset($request->selectedFeedTime)?$request->selectedFeedTime:NULL;
                        $selectedFeeds=isset($request->selectedFeeds)?$request->selectedFeeds:NULL;
                        if (!is_null($selectedFeedTime) || count($selectedFeeds)>0){
                            foreach ($request->selectedFeeds as $feed){
                                if($feed['feedCode']!=(-1)){
                                    $feed_details=new InventoryUsedInWaterDetails();
                                    $feed_details->si_during_enclosure_id=$si_during_enclosure_id;
                                    $feed_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                                    $feed_details->wf_during_enclosure_id=0;
                                    $feed_details->inventory_id=$feed['feedCode'];
                                    $feed_details->quantity=$feed['feedQty'];
                                    $feed_details->timing=$request->selectedFeedTime;
                                    $feed_details->save();
                                }
                            }
                        }
                        $selectedRemarks=isset($request->selectedRemarks)?$request->selectedRemarks:NULL;
                        if(count($selectedRemarks)>0){
                            foreach ($request->selectedRemarks as $remark){
                                if($remark['remarkCode']!=(-1)){
                                    $remark_details=new InventoryUsedInWaterDetails();
                                    $remark_details->si_during_enclosure_id=$si_during_enclosure_id;
                                    $remark_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                                    $remark_details->wf_during_enclosure_id=0;
                                    $remark_details->inventory_id=$remark['remarkCode'];
                                    $remark_details->quantity=$remark['remarkQty'];
                                    $remark_details->save();
                                }
                            }
                        }
                        if((isset($request->hCO3) && !is_null($request->hCO3))
                            || (isset($request->cO3) && !is_null($request->cO3)) || (isset($request->nH3) && !is_null($request->nH3))
                            || (isset($request->nO3) && !is_null($request->nO3)) || (isset($request->salinity) && !is_null($request->salinity)) || (isset($request->transparency) && !is_null($request->transparency))
                            || (isset($request->planktonType) && !is_null($request->planktonType)) || (isset($request->green) && !is_null($request->green)) || (isset($request->yellow) && !is_null($request->yellow))
                            || (isset($request->vibrioCount) && !is_null($request->vibrioCount))){
                            $water_parameter_details=new WaterParameterDetails();
                            $water_parameter_details->si_during_enclosure_id=$si_during_enclosure_id;
                            $water_parameter_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                            $water_parameter_details->wf_during_enclosure_id=0;
                            $water_parameter_details->alkalinity_hco3=$request->hCO3;
                            $water_parameter_details->carbonate_co3=$request->cO3;
                            $water_parameter_details->ammonia_nh3=$request->nH3;
                            $water_parameter_details->nitrate_no3=$request->nO3;
                            $water_parameter_details->salinity=$request->salinity;
                            $water_parameter_details->transparency=$request->transparency;
                            $water_parameter_details->plankton_type=$request->planktonType;
                            $water_parameter_details->green=$request->green;
                            $water_parameter_details->yellow=$request->yellow;
                            $water_parameter_details->vibrio_count=$request->vibrioCount;
                            $water_parameter_details->save();
                        }
                        //Dissolved Oxygen data insert
                        if (isset($request->dO) && !is_null($request->dO)){
                            if(!is_null($request->dO['doQty'])){
                                $dissolved_oxygen_details=new DissolvedOxygenDetails();
                                $dissolved_oxygen_details->si_during_enclosure_id=$si_during_enclosure_id;
                                $dissolved_oxygen_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                                $dissolved_oxygen_details->wf_during_enclosure_id=0;
                                $dissolved_oxygen_details->quantity=$request->dO['doQty'];
                                if($request->dO['timeCode']==0){
                                    $dissolved_oxygen_details->timing='am';
                                }else{
                                    $dissolved_oxygen_details->timing='pm';
                                }
                                $dissolved_oxygen_details->save();
                            }
                        }
                        //PH data insert
                        if (isset($request->pH) && !is_null($request->pH)){
                            if(!is_null($request->pH['pH'])){
                                $ph_details=new PHDetails();
                                $ph_details->si_during_enclosure_id=$si_during_enclosure_id;
                                $ph_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                                $ph_details->wf_during_enclosure_id=0;
                                $ph_details->quantity=$request->pH['pH'];
                                if($request->pH['timeCode']==0){
                                    $ph_details->timing='am';
                                }else{
                                    $ph_details->timing='pm';
                                }
                                $ph_details->save();
                            }
                        }
                        //Temperature data insert
                        if (isset($request->temperature) && !is_null($request->temperature)){
                            if(!is_null($request->temperature['temperature'])){
                                $temperature_details=new TemperatureDetails();
                                $temperature_details->si_during_enclosure_id=$si_during_enclosure_id;
                                $temperature_details->ct_during_enclosure_id=$ct_during_enclosure_id;
                                $temperature_details->wf_during_enclosure_id=0;
                                $temperature_details->quantity=$request->temperature['temperature'];
                                if($request->temperature['timeCode']==0){
                                    $temperature_details->timing='am';
                                }else{
                                    $temperature_details->timing='pm';
                                }
                                $temperature_details->save();
                            }
                        }
                        return response()->json([
                            'message'=>'Data stored successfully!',
                            'result'=>1
                        ]);
                    }
                }
            }
        }
        return response()->json([
            'message' => 'Data not saved!',
            'result'=>0
        ]);
    }
    public function harvest(Request $request){
        if((isset($request->user_id) && !is_null($request->user_id)) && (isset($request->pond_id) && !is_null($request->pond_id))
            && (isset($request->firm_cluster_id) && !is_null($request->firm_cluster_id))){
            if($request->pond_id!=(-1) && $request->firm_cluster_id!=(-1)){
                $user_id=$request->user_id;
                $pond_id=$request->pond_id;
                $farm_cluster_id=$request->firm_cluster_id;
                $user=User::active()->where('user_id',$user_id)->first();
                if(count($user)>0) {
                    if ($user->type_id == 6) {
                        $pond_info=SiPond::where('si_farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)
                            ->where('name','like','%'.$pond_id.'%')
                            ->first();
                        $previous_enter_to_enclosure=SiPlStock::where('farm_id',$farm_cluster_id)
                            ->where('farm_manager_id',$user->id)
                            ->where('pond_id',$pond_info->id)
                            ->first();
                        $previous_enter_to_enclosure->status='harvest';
                        $previous_enter_to_enclosure->save();

                        $harvest=new SiAnimalHarvest();
                        $harvest->si_during_enclosure_id=$previous_enter_to_enclosure->id;
                        $harvest->farm_manager_id=$user->id;
                        $harvest->farm_id=$farm_cluster_id;
                        $harvest->pond_id=$pond_info->id;
                        $harvest->save();

                        return response()->json([
                            'message'=>'Animal harvest done successfully!',
                            'result'=>1
                        ]);
                    }elseif ($user->type_id==7){
                        $pond_info=ClusterPond::where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)
                            ->where('name','like','%'.$pond_id.'%')
                            ->first();
                        $previous_enter_to_enclosure=ClusterPlStock::where('cluster_id',$farm_cluster_id)
                            ->where('cluster_manager_id',$user->id)
                            ->where('pond_id',$pond_info->id)
                            ->first();
                        $previous_enter_to_enclosure->status='harvest';
                        $previous_enter_to_enclosure->save();

                        $harvest=new ClusterAnimalHarvest();
                        $harvest->ct_during_enclosure_id=$previous_enter_to_enclosure->id;
                        $harvest->cluster_manager_id=$user->id;
                        $harvest->cluster_id=$farm_cluster_id;
                        $harvest->pond_id=$pond_info->id;
                        $harvest->save();

                        return response()->json([
                            'message'=>'Animal harvest done successfully!',
                            'result'=>2
                        ]);
                    }
                }
            }
        }
        return response()->json([
            'message' => 'Animal harvest not done!',
            'result'=>0
        ]);
    }
}
