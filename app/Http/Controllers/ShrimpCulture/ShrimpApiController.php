<?php
//Created by Shreya
namespace App\Http\Controllers\ShrimpCulture;
use App\DissolvedOxygenDetails;
use App\HarvestDetails;
use App\Hatchery;
use App\Inventory;
use App\InventoryBrand;
use App\InventoryManagement;
use App\InventoryType;
use App\InventoryUsedInWaterDetails;
use App\PHDetails;
use App\ShrimpCultureModels\Cluster;
use App\ShrimpCultureModels\ClusterAnimalHarvest;
use App\ShrimpCultureModels\ClusterAnimalSampling;
use App\ShrimpCultureModels\ClusterPlStock;
use App\ShrimpCultureModels\ClusterPond;
use App\ShrimpCultureModels\ClusterPondManagement;
use App\ShrimpCultureModels\SiAnimalHarvest;
use App\ShrimpCultureModels\SiAnimalSampling;
use App\ShrimpCultureModels\SiFarm;
use App\ShrimpCultureModels\SiPlStock;
use App\ShrimpCultureModels\SiPond;
use App\Http\Controllers\Controller;
use App\ShrimpCultureModels\SiPondManagement;
use App\Staff;
use App\TemperatureDetails;
use App\WaterParameterDetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ShrimpApiController extends Controller
{
    public function login($user,$token){
        return $token;
    }
    public function pondDetails($user){
        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
        $user_id=$user->id;
        if($user->type_id==6){
            //Semi-Intensive
            $ponds= SiPond::join('rel_si_pond_manager','rel_si_pond_manager.si_pond_id','=','si_ponds.id')
                ->leftJoin(DB::raw("(".(DB::table('staffs')->join(DB::raw("(".DB::table('staffs')->select('pond_id',DB::raw('MAX(created_at) as d'))->groupBy('pond_id')->toSql().") ss"),function($join){
                            $join->on('ss.pond_id','=','staffs.pond_id');
                            $join->on('ss.d','=','staffs.created_at');
                        })->select('staffs.*')->toSql().") s")),'s.pond_id','=','si_ponds.id')
                ->select('si_ponds.si_farm_id as farm_id','si_ponds.pond_id','si_ponds.name as pond_name','si_ponds.area as pond_dimension',DB::raw('IFNULL(s.no_of_staff, 0) as no_of_staff'))
                ->where('rel_si_pond_manager.manager_id','=',$user_id)
                ->get();
        }else{
            //Cluster Perspective
            $ponds= ClusterPond::join('rel_cluster_pond_manager','rel_cluster_pond_manager.cluster_pond_id','=','cluster_ponds.id')
                ->leftJoin(DB::raw("(".(DB::table('staffs')->join(DB::raw("(".DB::table('staffs')->select('pond_id',DB::raw('MAX(created_at) as d'))->groupBy('pond_id')->toSql().") ss"),function($join){
                            $join->on('ss.pond_id','=','staffs.pond_id');
                            $join->on('ss.d','=','staffs.created_at');
                        })->select('staffs.*')->toSql().") s")),'s.pond_id','=','cluster_ponds.id')
                ->select('cluster_ponds.cluster_id as farm_id','cluster_ponds.pond_id','cluster_ponds.name as pond_name','cluster_ponds.area as pond_dimension',DB::raw('IFNULL(s.no_of_staff, 0) as no_of_staff'))
                ->where('rel_cluster_pond_manager.manager_id',$user_id)->get();
        }
        return $ponds;
    }
    public function staffUpdate($user,$farm_cluster_id,$pond_id,$no_of_staff){
        $user_type=$user->type_id;
        $user_id=$user->id;
        $pond_row_id=$this->getPondId($user_id,$user_type,$farm_cluster_id,$pond_id);
        if(!is_null($pond_row_id)){
            $staff=new Staff();
            $staff->category_id=$user->cat_id;
            $staff->manager_id=$user->id;
            if($user_type==6){
                //Semi-Intensive
                $staff->farm_id=$farm_cluster_id;
            }else{
                //Cluster Perspective
                $staff->cluster_id=$farm_cluster_id;
            }
            $staff->pond_id=$pond_row_id;
            $staff->no_of_staff=$no_of_staff;
            $staff->save();
            return true;
        }
        return false;
    }
    public function animalDetails($user){
        $user_id=$user->id;
        $hatchery=Hatchery::active()->select('id','name')->get();
        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
        if($user->type_id==6){
            $animal_details= SiPond::join('rel_si_pond_manager','rel_si_pond_manager.si_pond_id','=','si_ponds.id')
                ->leftJoin(DB::raw("(".(DB::table('si_animal_sampling')
                            ->join(DB::raw("(".DB::table('si_animal_sampling')
                                    ->select('pond_id',DB::raw('MAX(created_at) as d'))
                                    ->groupBy('pond_id')->toSql().") ss"),function($join){
                            $join->on('ss.pond_id','=','si_animal_sampling.pond_id');
                            $join->on('ss.d','=','si_animal_sampling.created_at');
                        })->select('si_animal_sampling.*')->toSql().") s")),'s.pond_id','=','si_ponds.id')
                ->select('si_ponds.si_farm_id as farm_id',
                    'si_ponds.pond_id',
                    'si_ponds.name as pond_name',
                    DB::raw('IFNULL(s.hatchery_id, 0) as hatchery_id'),
                    DB::raw('IFNULL(s.stocked_pl_quantity, 0) as pl_amount'),
                    DB::raw('IFNULL(s.stocked_pl_age, 0) as pl_age'),
                    DB::raw('IFNULL(s.stocked_pl_price, 0) as pl_price'),
                    DB::raw('IFNULL(s.survival_rate, 0) as survival_rate'),
                    DB::raw('IFNULL(s.current_biomass, 0) as biomass'),
                    DB::raw('IFNULL(s.number_of_sample, 0) as num_of_sample'),
                    DB::raw('IFNULL(s.total_weight, 0) as total_weight'),
                    DB::raw('IFNULL(s.average_body_weight, 0) as avg_body_weight'))
                    ->where('rel_si_pond_manager.manager_id','=',$user_id)
                ->get();
        }else{
            $animal_details= ClusterPond::join('rel_cluster_pond_manager','rel_cluster_pond_manager.cluster_pond_id','=','cluster_ponds.id')
                ->leftJoin(DB::raw("(".(DB::table('cluster_animal_sampling')->join(DB::raw("(".DB::table('cluster_animal_sampling')->select('pond_id',DB::raw('MAX(created_at) as d'))->groupBy('pond_id')->toSql().") ss"),function($join){
                            $join->on('ss.pond_id','=','cluster_animal_sampling.pond_id');
                            $join->on('ss.d','=','cluster_animal_sampling.created_at');
                        })->select('cluster_animal_sampling.*')->toSql().") s")),'s.pond_id','=','cluster_ponds.id')
                ->select('cluster_ponds.cluster_id as farm_id',
                    'cluster_ponds.pond_id',
                    'cluster_ponds.name as pond_name',
                    DB::raw('IFNULL(s.hatchery_id, 0) as hatchery_id'),
                    DB::raw('IFNULL(s.stocked_pl_quantity, 0) as pl_amount'),
                    DB::raw('IFNULL(s.stocked_pl_age, 0) as pl_age'),
                    DB::raw('IFNULL(s.stocked_pl_price, 0) as pl_price'),
                    DB::raw('IFNULL(s.survival_rate, 0) as survival_rate'),
                    DB::raw('IFNULL(s.current_biomass, 0) as biomass'),
                    DB::raw('IFNULL(s.number_of_sample, 0) as num_of_sample'),
                    DB::raw('IFNULL(s.total_weight, 0) as total_weight'),
                    DB::raw('IFNULL(s.average_body_weight, 0) as avg_body_weight'))
                ->where('rel_cluster_pond_manager.manager_id',$user_id)
                ->get();
        }
        return response()->json([
            'hatchery'=>$hatchery,
            'fish'=>[],
            'animal_details'=>$animal_details,
            'status' => 'Data found',
            'code'=>200
        ]);
    }
    public function pLStock($user,$farm_cluster_id,$pond_id,$pl_quantity,$pl_age,$pl_price,$hatchery_id){
        $user_type=$user->type_id;
        $user_id=$user->id;
        $pond_row_id=$this->getPondId($user_id,$user_type,$farm_cluster_id,$pond_id);
        if(!is_null($pond_row_id)){
            if($user_type==6){
                //Stocking Data
                $pl_stock=new SiPlStock();
                $pl_stock->farm_id=$farm_cluster_id;
                $pl_stock->farm_manager_id=$user_id;

                //Initial Sampling Data as Stocking
                $animal_sampling=new SiAnimalSampling();
                $animal_sampling->farm_id=$farm_cluster_id;
                $animal_sampling->farm_manager_id=$user_id;
            }else {
                //Stocking Data
                $pl_stock=new ClusterPlStock();
                $pl_stock->cluster_id=$farm_cluster_id;
                $pl_stock->cluster_manager_id=$user_id;

                //Initial Sampling Data as Stocking
                $animal_sampling=new ClusterAnimalSampling();
                $animal_sampling->cluster_id=$farm_cluster_id;
                $animal_sampling->cluster_manager_id=$user_id;
            }
            //Stocking
            $pl_stock->hatchery_id=$hatchery_id;
            $pl_stock->pond_id=$pond_row_id;
            $pl_stock->pl_quantity=$pl_quantity;
            $pl_stock->pl_age=$pl_age;
            $pl_stock->pl_price=$pl_price;
            $pl_stock->save();

            //Sampling
            $animal_sampling->hatchery_id=$hatchery_id;
            $animal_sampling->pond_id=$pond_row_id;
            $animal_sampling->stocked_pl_quantity=$pl_quantity;
            $animal_sampling->stocked_pl_age=$pl_age;
            $animal_sampling->stocked_pl_price=$pl_price;
            $animal_sampling->si_pl_stock_id=$pl_stock->id;
            $animal_sampling->save();
            return true;
        }
        return false;
    }
    public function pLSampling($user,$farm_cluster_id,$pond_id,$number_of_sample,$average_body_weight,$current_biomass,$survival_rate,$total_weight){
        $now=Carbon::now();
        $user_type=$user->type_id;
        $user_id=$user->id;
        $pond_row_id=$this->getPondId($user_id,$user_type,$farm_cluster_id,$pond_id);
        if(!is_null($pond_row_id)){
            if($user_type==6){
                $stocking_data=SiPlStock::where('farm_id',$farm_cluster_id)
                    ->where('farm_manager_id',$user_id)
                    ->where('pond_id',$pond_row_id)
                    ->where('harvest_tracking_id',0)
                    ->where('harvest_counter',0)
                    ->first();
                $previous_sampling=SiAnimalSampling::where('farm_id',$farm_cluster_id)
                    ->where('farm_manager_id',$user_id)
                    ->where('pond_id',$pond_row_id)
                    ->where('harvest_tracking_id',0)
                    ->where('harvest_counter',0);

                $animal_sampling=new SiAnimalSampling();
                $animal_sampling->farm_id=$farm_cluster_id;
                $animal_sampling->farm_manager_id=$user_id;
            }else {
                $stocking_data=ClusterPlStock::where('cluster_id',$farm_cluster_id)
                    ->where('cluster_manager_id',$user->id)
                    ->where('pond_id',$pond_id)
                    ->where('harvest_tracking_id',0)
                    ->where('harvest_counter',0)
                    ->first();
                $previous_sampling=ClusterAnimalSampling::where('cluster_id',$farm_cluster_id)
                    ->where('cluster_manager_id',$user->id)
                    ->where('pond_id',$pond_id)
                    ->where('harvest_tracking_id',0)
                    ->where('harvest_counter',0);

                $animal_sampling=new ClusterAnimalSampling();
                $animal_sampling->cluster_id=$farm_cluster_id;
                $animal_sampling->cluster_manager_id=$user_id;
            }
            /*
             * Biomass Calculation Starts
             */

            $days_of_culture=$this->daysOfCulture($user_id,$user_type,$farm_cluster_id,$pond_id,$now);
            if($days_of_culture<=30 && $days_of_culture>0){
                $weight_avg_current=(0.137)*pow(1.119,$days_of_culture);
                $biomass_today=$weight_avg_current*($survival_rate/100)*$stocking_data->pl_quantity;
            }else{
                $sampling_count=$previous_sampling->count();
                $previous_sampling=$previous_sampling->first();
                if($sampling_count==1){
                    $daily_growth_rate=0.15;
                }elseif($sampling_count>=2){
                    $daily_growth_rate=($average_body_weight-$previous_sampling->average_body_weight)/($days_of_culture-$previous_sampling->days_of_culture);
                }
                $biomass_today=$previous_sampling->current_biomass+($daily_growth_rate*($survival_rate/100)*$stocking_data->pl_quantity);
            }
            /*
            * Biomass Calculation Ends
            */
            $animal_sampling->hatchery_id=$stocking_data->hatchery_id;
            $animal_sampling->pond_id=$pond_row_id;
            $animal_sampling->total_weight=$total_weight;
            $animal_sampling->number_of_sample=$number_of_sample;
            $animal_sampling->average_body_weight=$average_body_weight;
            $animal_sampling->current_biomass=$biomass_today;
            $animal_sampling->survival_rate=$survival_rate;
            $animal_sampling->days_of_culture=$days_of_culture;
            $animal_sampling->stocked_pl_quantity=$stocking_data->pl_quantity;
            $animal_sampling->stocked_pl_age=$stocking_data->pl_age;
            $animal_sampling->stocked_pl_price=$stocking_data->pl_price;
            $animal_sampling->si_pl_stock_id=$stocking_data->id;
            $animal_sampling->save();
            return true;
        }
        return false;
    }
    public function pondList($user){
        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
        $user_id=$user->id;
        if($user->type_id==6){
            //Semi-Intensive
            $ponds= SiPond::join('rel_si_pond_manager','rel_si_pond_manager.si_pond_id','=','si_ponds.id')
                ->select('si_ponds.si_farm_id as farm_id','si_ponds.pond_id','si_ponds.name as pond_name')
                ->where('rel_si_pond_manager.manager_id','=',$user_id)
                ->get();
        }else{
            //Cluster Perspective
            $ponds= ClusterPond::join('rel_cluster_pond_manager','rel_cluster_pond_manager.cluster_pond_id','=','cluster_ponds.id')
                ->select('cluster_ponds.cluster_id as farm_id','cluster_ponds.pond_id','cluster_ponds.name as pond_name')
                ->where('rel_cluster_pond_manager.manager_id',$user_id)->get();
        }
        return $ponds;
    }
    public function addMonitoringParameters($user,$farm_cluster_id,$pond_id,$ph,$dissolved_oxygen,$temperature,$hco3,
                                            $co3,$ca,$cl2,$mg,$k,$nh3,$no3,$nh4,$salinity,$transparency,$plankton_type,$green,$yellow,$vibrio_count){
        $user_type=$user->type_id;
        $user_id=$user->id;
        $pond_row_id=$this->getPondId($user_id,$user_type,$farm_cluster_id,$pond_id);
        if(!is_null($pond_row_id)){
            if($user_type==6){
                $si_pond_management_id=$this->getSiPondManagementId($user_id,$farm_cluster_id,$pond_row_id);
                $ct_pond_management_id=0;
            }else {
                $si_pond_management_id=0;
                $ct_pond_management_id=$this->getCtPondManagementId($user_id,$farm_cluster_id,$pond_row_id);
            }
            if($si_pond_management_id>0 || $ct_pond_management_id>0){
                //Adding Water Parameters
                if(!is_null($hco3) || !is_null($co3) || !is_null($ca) || !is_null($cl2) || !is_null($mg) || !is_null($k) || !is_null($nh3)
                    || !is_null($no3) || !is_null($nh4) || !is_null($salinity) || !is_null($transparency) || !is_null($plankton_type) || !is_null($green)
                    || !is_null($yellow) || !is_null($vibrio_count)){
                    $water_parameter_details=new WaterParameterDetails();
                    $water_parameter_details->si_pond_management_id=$si_pond_management_id;
                    $water_parameter_details->ct_pond_management_id=$ct_pond_management_id;
                    $water_parameter_details->wf_during_enclosure_id=0;
                    $water_parameter_details->alkalinity_hco3=$hco3;
                    $water_parameter_details->carbonate_co3=$co3;
                    $water_parameter_details->calcium_ca=$ca;
                    $water_parameter_details->chlorine_cl2=$cl2;
                    $water_parameter_details->magnesium_mg=$mg;
                    $water_parameter_details->potassium_k=$k;
                    $water_parameter_details->ammonia_nh3=$nh3;
                    $water_parameter_details->nitrate_no3=$no3;
                    $water_parameter_details->ammonium_nh4=$nh4;
                    $water_parameter_details->salinity=$salinity;
                    $water_parameter_details->transparency=$transparency;
                    $water_parameter_details->plankton_type=$plankton_type;
                    $water_parameter_details->green=$green;
                    $water_parameter_details->yellow=$yellow;
                    $water_parameter_details->vibrio_count=$vibrio_count;
                    $water_parameter_details->save();
                }
                //Adding Dissolved Oxygen
                if (!is_null($dissolved_oxygen)){
                    if(!is_null($dissolved_oxygen['do_qty'])){
                        $dissolved_oxygen_details=new DissolvedOxygenDetails();
                        $dissolved_oxygen_details->si_pond_management_id=$si_pond_management_id;
                        $dissolved_oxygen_details->ct_pond_management_id=$ct_pond_management_id;
                        $dissolved_oxygen_details->wf_during_enclosure_id=0;
                        $dissolved_oxygen_details->quantity=$dissolved_oxygen['do_qty'];
                        if($dissolved_oxygen['time_code']==0){
                            $dissolved_oxygen_details->timing='am';
                        }else{
                            $dissolved_oxygen_details->timing='pm';
                        }
                        $dissolved_oxygen_details->save();
                    }
                }
                //Adding PH
                if (!is_null($ph)){
                    if(!is_null($ph['ph_qty'])){
                        $ph_details=new PHDetails();
                        $ph_details->si_pond_management_id=$si_pond_management_id;
                        $ph_details->ct_pond_management_id=$ct_pond_management_id;
                        $ph_details->wf_during_enclosure_id=0;
                        $ph_details->quantity=$ph['ph_qty'];
                        if($ph['time_code']==0){
                            $ph_details->timing='am';
                        }else{
                            $ph_details->timing='pm';
                        }
                        $ph_details->save();
                    }
                }
                //Adding Temperature
                if (!is_null($temperature)){
                    if(!is_null($temperature['temperature_qty'])){
                        $temperature_details=new TemperatureDetails();
                        $temperature_details->si_pond_management_id=$si_pond_management_id;
                        $temperature_details->ct_pond_management_id=$ct_pond_management_id;
                        $temperature_details->wf_during_enclosure_id=0;
                        $temperature_details->quantity=$temperature['temperature_qty'];
                        if($temperature['time_code']==0){
                            $temperature_details->timing='am';
                        }else{
                            $temperature_details->timing='pm';
                        }
                        $temperature_details->save();
                    }
                }
                return true;
            }
        }
        return false;
    }
    public function commercialInputDetails($user){
        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
        $user_id=$user->id;
        $user_type=$user->type_id;
        if($user->type_id==6){
            //Semi-Intensive
            $farm_cluster_id=$this->getFarmClusterId($user_id,$user_type);

            $feeds=InventoryManagement::join('inventories','inventories.id','=','inventory_management.inventory_id')
                ->where('inventory_management.remaining_quantity','!=',0)
                ->where('inventory_management.si_farm_id','=',$farm_cluster_id)
                ->where('inventories.type_id','=',1)
                ->select('inventories.id as id','inventories.name as name','inventory_management.remaining_quantity as quantity','inventory_management.id as inventory_mgt_id')->get();
            $minerals=InventoryManagement::join('inventories','inventories.id','=','inventory_management.inventory_id')
                ->where('inventory_management.remaining_quantity','!=',0)
                ->where('inventory_management.si_farm_id','=',$farm_cluster_id)
                ->whereBetween('inventories.type_id',[2,6])
                ->select('inventories.id as id','inventories.name as name','inventory_management.remaining_quantity as quantity','inventory_management.id as inventory_mgt_id')->get();
        }else{
            //Cluster Perspective
            $farm_cluster_id=$this->getFarmClusterId($user_id,$user_type);

            $feeds=InventoryManagement::join('inventories','inventories.id','=','inventory_management.inventory_id')
                ->where('inventory_management.remaining_quantity','!=',0)
                ->where('inventory_management.cluster_id','=',$farm_cluster_id)
                ->where('inventories.type_id','=',1)
                ->select('inventories.id as id','inventories.name as name','inventory_management.remaining_quantity as quantity','inventory_management.id as inventory_mgt_id')->get();
            $minerals=InventoryManagement::join('inventories','inventories.id','=','inventory_management.inventory_id')
                ->where('inventory_management.remaining_quantity','!=',0)
                ->where('inventory_management.cluster_id','=',$farm_cluster_id)
                ->whereBetween('inventories.type_id',[2,6])
                ->select('inventories.id as id','inventories.name as name','inventory_management.remaining_quantity as quantity','inventory_management.id as inventory_mgt_id')->get();
        }
        return response()->json([
            'feed'=>$feeds,
            'minerals'=>$minerals,
            'status' => 'Data found',
            'code'=>200
        ]);
    }
    public function addCommercialInput($user,$farm_cluster_id,$pond_id,$timing,$remaining,$commercial_inputs){
        $user_type=$user->type_id;
        $user_id=$user->id;
        $pond_row_id=$this->getPondId($user_id,$user_type,$farm_cluster_id,$pond_id);
        if(!is_null($pond_row_id)){
            if($user_type==6){
                $si_pond_management_id=$this->getSiPondManagementId($user_id,$farm_cluster_id,$pond_row_id);
                $ct_pond_management_id=0;
            }else {
                $si_pond_management_id=0;
                $ct_pond_management_id=$this->getCtPondManagementId($user_id,$farm_cluster_id,$pond_row_id);
            }
            if($si_pond_management_id>0 || $ct_pond_management_id>0){
                foreach ($commercial_inputs as $c_input){
                    if(!is_null($c_input['inventory_id']) && is_numeric($c_input['inventory_id'])
                        && !is_null($c_input['qty']) && !is_null($c_input['inventory_mgt_id']) && is_numeric($c_input['inventory_mgt_id'])){
                        $inventory_id=$c_input['inventory_id'];
                        $inventory_mgt_id=$c_input['inventory_mgt_id'];
                        $inventory_management=InventoryManagement::findOrFail($inventory_mgt_id);
                        $quantity=$c_input['qty'];
                        $inventory_type=$this->getInventoryType($inventory_id);
                        $inventory_used_in_water=new InventoryUsedInWaterDetails();
                        $inventory_used_in_water->si_pond_management_id=$si_pond_management_id;
                        $inventory_used_in_water->ct_pond_management_id=$ct_pond_management_id;
                        $inventory_used_in_water->wf_during_enclosure_id=0;
                        $inventory_used_in_water->inventory_id=$inventory_id;
                        $inventory_used_in_water->quantity=$quantity;
                        if($inventory_type==1){
                            $inventory_used_in_water->remaining_quantity=$remaining['qty'];
                            $inventory_used_in_water->timing=$timing;
                        }
                        $inventory_used_in_water->price=$inventory_management->price;
                        $inventory_used_in_water->total_price=$inventory_used_in_water->quantity*$inventory_used_in_water->price;
                        $inventory_used_in_water->save();
                    }
                }
                return true;
            }
        }
        return false;
    }
    public function updateInventory($user,$items){
        $user_type=$user->type_id;
        $user_id=$user->id;
        $farm_cluster_id=$this->getFarmClusterId($user_id,$user_type);
        if(!is_null($farm_cluster_id)){
            foreach($items as $item){
                if(!is_null($item['item_id'])){
                    $previous_inventory_management=InventoryManagement::findOrFail($item['inventory_mgt_id']);
                    if(count($previous_inventory_management)>0){
                        if($previous_inventory_management->remaining_quantity >= $item['item_quantity']){
                            $add_quantity=0;
                        }else{
                            $add_quantity=$item['item_quantity']-$previous_inventory_management->remaining_quantity;
                        }
                        $previous_inventory_management->quantity=$previous_inventory_management->quantity+$add_quantity;
                        $previous_inventory_management->remaining_quantity=$item['item_quantity'];
                        $previous_inventory_management->total_price=$previous_inventory_management->quantity*$previous_inventory_management->price;
                        $previous_inventory_management->item_avg_consumption=$item['item_avg_consumption'];

                        $previous_inventory_management->save();
                    }
                }else{
                    $previous_inventory=Inventory::where('name',$item['item_name'])->first();
                    if(count($previous_inventory)>0){
                        $inventory_id=$previous_inventory->id;
                    }else{
                        $inventory=new Inventory();
                        $inventory->name=$item['item_name'];
                        $inventory->type_id=$item['item_type_id'];
                        $inventory->brand_id=$item['item_brand_id'];
                        $inventory->save();
                        $inventory_id=$inventory->id;
                    }
                    $inventory_management=new InventoryManagement();
                    $inventory_management->inventory_id=$inventory_id;
                    $inventory_management->client_id=0;
                    $inventory_management->org_id=0;
                    if($user_type==6){
                        $inventory_management->si_farm_id=$farm_cluster_id;
                        $inventory_management->cluster_id=0;
                    }else{
                        $inventory_management->si_farm_id=0;
                        $inventory_management->cluster_id=$farm_cluster_id;
                    }
                    $inventory_management->quantity=$item['item_quantity'];
                    $inventory_management->remaining_quantity=$item['item_quantity'];
                    $inventory_management->price=$item['item_price'];
                    $inventory_management->total_price=$inventory_management->quantity*$inventory_management->price;
                    $inventory_management->item_avg_consumption=$item['item_avg_consumption'];
//                    return $farm_cluster_id;
                    $inventory_management->save();
                }
            }
            return true;
        }
        return false;
    }
    public function inventoryList($user){
        $user_type=$user->type_id;
        $user_id=$user->id;
        $farm_cluster_id=$this->getFarmClusterId($user_id,$user_type);
        $inventory_management=InventoryManagement::join('inventories','inventories.id','=','inventory_management.inventory_id')
            ->select('inventories.id as item_id','inventories.name as item_name','inventory_management.id as inventory_mgt_id',
                'inventory_management.price as item_price','inventory_management.remaining_quantity as item_quantity',
                'inventory_management.item_avg_consumption as item_avg_consumption')
            ->where('inventory_management.si_farm_id','=',$farm_cluster_id)
            ->orWhere('inventory_management.cluster_id','=',$farm_cluster_id)
            ->get();
        $inventory_type=InventoryType::select('id','name')->get();
        $inventory_brand=InventoryBrand::withoutZero()->select('id','name')->get();
        return response()->json([
            'items'=>$inventory_management,
            'type'=>$inventory_type,
            'brand'=>$inventory_brand,
            'status' => 'Data found',
            'code'=>200
        ]);
    }
    public function harvestDetails($user){
        //Here farm_id can be either farm id or cluster id as for Shrimp Culture,
        // as there are two different farming types.E.g., Semi-Intensive, Cluster-Perspective
        $user_id=$user->id;
        if($user->type_id==6){
            //Semi-Intensive
            $ponds= SiPond::join('rel_si_pond_manager','rel_si_pond_manager.si_pond_id','=','si_ponds.id')
                ->select('si_ponds.si_farm_id as farm_id','si_ponds.pond_id','si_ponds.name as pond_name')
                ->where('rel_si_pond_manager.manager_id','=',$user_id)
                ->get();
        }else{
            //Cluster Perspective
            $ponds= ClusterPond::join('rel_cluster_pond_manager','rel_cluster_pond_manager.cluster_pond_id','=','cluster_ponds.id')
                ->select('cluster_ponds.cluster_id as farm_id','cluster_ponds.pond_id','cluster_ponds.name as pond_name')
                ->where('rel_cluster_pond_manager.manager_id',$user_id)->get();
        }
        return $ponds;
    }
    public function harvest($user,$farm_cluster_id,$pond_id,$full_harvest,$comment,$harvest){
        $user_type=$user->type_id;
        $user_id=$user->id;
        $now=Carbon::now();
        $pond_row_id=$this->getPondId($user_id,$user_type,$farm_cluster_id,$pond_id);
        if(!is_null($pond_row_id)){
            $total_quantity=0;
            $total_price=0;
            if($user_type==6){
                //Semi-Intensive
                $harvest_entry=new SiAnimalHarvest();
                $harvest_entry->farm_id=$farm_cluster_id;
                $harvest_entry->farm_manager_id=$user_id;
            }else{
                //Cluster Perspective
                $harvest_entry=new ClusterAnimalHarvest();
                $harvest_entry->cluster_id=$farm_cluster_id;
                $harvest_entry->cluster_manager_id=$user_id;
            }
            $harvest_entry->pond_id=$pond_row_id;
            if(!$full_harvest){
                $harvest_entry->harvest_type='Partial';
            }
            $days_of_culture=$this->daysOfCulture($user_id,$user_type,$farm_cluster_id,$pond_row_id,$now);
            $harvest_entry->days_of_culture=$days_of_culture;
            $total_feed=$this->getTotalFeed($user_id,$user_type,$farm_cluster_id,$pond_row_id);
            $harvest_entry->total_feed=$total_feed;
            $total_biomass=$this->getBiomass($user_id,$user_type,$farm_cluster_id,$pond_row_id);
            $harvest_entry->total_biomass=$total_biomass;
            if($total_biomass==0){
                $harvest_entry->fcr=0;
            }else{
                $harvest_entry->fcr=$total_feed/$total_biomass;
            }
            $harvest_entry->comments=$comment;
            $harvest_entry->save();

            if($user_type==6){
                $si_animal_harvest_id=$harvest_entry->id;
                $ct_animal_harvest_id=0;
            }else {
                $si_animal_harvest_id=0;
                $ct_animal_harvest_id=$harvest_entry->id;
            }
            foreach ($harvest as $harvest_data){
                if(!is_null($harvest_data['grade_type']) && !is_null($harvest_data['total_qty']) && !is_null($harvest_data['unit_price'])
                    && !is_null($harvest_data['unit']) ){
                    $harvest_details=new HarvestDetails();
                    $harvest_details->si_harvest_id=$si_animal_harvest_id;
                    $harvest_details->ct_harvest_id=$ct_animal_harvest_id;
                    $harvest_details->wf_harvest_id=0;
                    $harvest_details->grade=$harvest_data['grade_type'];
                    $harvest_details->quantity=$harvest_data['total_qty'];
                    $harvest_details->unit=$harvest_data['unit'];
//                    $harvest_details->biomass='';
                    $harvest_details->unit_price=$harvest_data['unit_price'];
                    $harvest_details->total_price=$harvest_details->quantity*$harvest_details->unit_price;
                    $harvest_details->save();
                    $total_quantity+=$harvest_details->quantity;
                    $total_price+=$harvest_details->total_price;
                }
            }

            $harvest_entry->total_quantity=$total_quantity;
            $harvest_entry->total_price=$total_price;
            $harvest_entry->save();
            $harvest_tracking=$this->trackHarvest($user_id,$user_type,$farm_cluster_id,$pond_row_id,$harvest_entry->id);
            if($harvest_tracking){
                return true;
            }
        }
        return false;
    }
    public function getPondId($user_id,$user_type,$farm_cluster_id,$pond_id){
        if($user_type==6){
            //Semi-Intensive
            $pond=SiPond::join('rel_si_pond_manager','rel_si_pond_manager.si_pond_id','=','si_ponds.id')
                ->where('si_ponds.pond_id','LIKE','%'.$pond_id.'%')
                ->where('si_ponds.si_farm_id',$farm_cluster_id)
                ->where('rel_si_pond_manager.manager_id',$user_id)
                ->select('si_ponds.id as id')
                ->first();
        }else{
            //Cluster Perspective
            $pond=ClusterPond::join('rel_cluster_pond_manager','rel_cluster_pond_manager.cluster_pond_id','=','cluster_ponds.id')
                ->where('cluster_ponds.pond_id','LIKE','%'.$pond_id.'%')
                ->where('cluster_ponds.cluster_id',$farm_cluster_id)
                ->where('rel_cluster_pond_manager.manager_id',$user_id)
                ->select('cluster_ponds.id as id')
                ->first();
        }
        if(count($pond)>0){
            return $pond->id;
        }
        return null;
    }
    public function getSiPondManagementId($user_id,$farm_cluster_id,$pond_id){
        $today_si_pond_management=SiPondManagement::where('pond_id',$pond_id)->where('farm_id',$farm_cluster_id)
            ->where('farm_manager_id',$user_id)->whereDate('created_at','=',Carbon::today())->first();
        if(count($today_si_pond_management)>0){
            $si_pond_management_id=$today_si_pond_management->id;
        }else{
            $si_pond_management=new SiPondManagement();
            $si_pond_management->pond_id=$pond_id;
            $si_pond_management->farm_id=$farm_cluster_id;
            $si_pond_management->farm_manager_id=$user_id;
            $si_pond_management->save();
            $si_pond_management_id=$si_pond_management->id;
        }
        return $si_pond_management_id;
    }
    public function getCtPondManagementId($user_id,$farm_cluster_id,$pond_id){
        $today_ct_pond_management=ClusterPondManagement::where('pond_id',$pond_id)->where('cluster_id',$farm_cluster_id)
            ->where('cluster_manager_id',$user_id)->whereDate('created_at','=',Carbon::today())->first();
        if(count($today_ct_pond_management)>0){
            $ct_pond_management_id=$today_ct_pond_management->id;
        }else{
            $ct_pond_management=new ClusterPondManagement();
            $ct_pond_management->pond_id=$pond_id;
            $ct_pond_management->cluster_id=$farm_cluster_id;
            $ct_pond_management->cluster_manager_id=$user_id;
            $ct_pond_management->save();
            $ct_pond_management_id=$ct_pond_management->id;
        }
        return $ct_pond_management_id;
    }
    public function validateFarmManager($user,$farm_cluster_id,$pond_id){
        $pond=SiPond::join('rel_si_pond_manager','rel_si_pond_manager.si_pond_id','=','si_ponds.id')
        ->where('si_ponds.pond_id','LIKE','%'.$pond_id.'%')
            ->where('si_ponds.si_farm_id',$farm_cluster_id)
            ->where('rel_si_pond_manager.manager_id',$user)
            ->select('si_ponds.name')
            ->first();
        if(count($pond)>0){
            return $pond->id;
        }
        return false;
    }
    public function validateClusterManager($user,$farm_cluster_id,$pond_id){
        $pond=ClusterPond::join('rel_cluster_pond_manager','rel_cluster_pond_manager.cluster_pond_id','=','cluster_ponds.id')
            ->where('cluster_ponds.pond_id','LIKE','%'.$pond_id.'%')
            ->where('cluster_ponds.cluster_id',$farm_cluster_id)
            ->where('rel_cluster_pond_manager.manager_id',$user)
            ->select('cluster_ponds.name')
            ->first();
        if(count($pond)>0){
            return $pond->id;
        }
        return false;
    }
    public function getFarmClusterId($user_id,$user_type){
        if($user_type==6){
            //Semi-Intensive
            $farm_cluster_id=SiFarm::join('si_ponds','si_ponds.si_farm_id','=','si_farms.id')
                ->join('rel_si_pond_manager','rel_si_pond_manager.si_pond_id','=','si_ponds.id')
                ->where('rel_si_pond_manager.manager_id',$user_id)
                ->select('si_farms.id as id')
                ->first();
        }else{
            //Cluster Perspective
            $farm_cluster_id=Cluster::join('cluster_ponds','cluster_ponds.cluster_id','=','clusters.id')
                ->join('rel_cluster_pond_manager','rel_cluster_pond_manager.cluster_pond_id','=','cluster_ponds.id')
                ->where('rel_cluster_pond_manager.manager_id',$user_id)
                ->select('clusters.id as id')
                ->first();
        }
        if(count($farm_cluster_id)>0){
            return $farm_cluster_id->id;
        }
        return null;
    }
    public function getInventoryType($inventory_id){
        $inventory=Inventory::findOrFail($inventory_id);
        return $inventory->type_id;
    }
    public function daysOfCulture($user_id,$user_type,$farm_cluster_id,$pond_id,$now){
        $days_of_culture=0;
        if($user_type==6){
            //Semi-Intensive
            $si_pl_stock=SiPlStock::where('farm_id',$farm_cluster_id)->where('farm_manager_id',$user_id)->where('pond_id',$pond_id)->where('status','enclosure')
                ->where('harvest_tracking_id',0)->where('harvest_counter',0)->first();
            if(count($si_pl_stock)>0){
                $stocking_date=new Carbon($si_pl_stock->created_at);
                $stocking_date=$stocking_date->toDateString();
                $days_of_culture=$now->diffInDays($stocking_date, true);
            }
        }else{
            //Cluster Perspective
            $cluster_pl_stock=ClusterPlStock::where('cluster_id',$farm_cluster_id)->where('cluster_manager_id',$user_id)->where('pond_id',$pond_id)->where('status','enclosure')
                ->where('harvest_tracking_id',0)->where('harvest_counter',0)->first();
            if(count($cluster_pl_stock)>0){
                $stocking_date=new Carbon($cluster_pl_stock->created_at);
                $stocking_date=$stocking_date->toDateString();
                $days_of_culture=$now->diffInDays($stocking_date, true);
            }

        }
        return $days_of_culture;
    }
    public function getTotalFeed($user_id,$user_type,$farm_cluster_id,$pond_id){
        if($user_type==6){
            $feed=SiPondManagement::join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                ->where('si_pond_management.pond_id','=',$pond_id)
                ->where('si_pond_management.farm_id','=',$farm_cluster_id)
                ->where('si_pond_management.farm_manager_id','=',$user_id)
                ->where('si_pond_management.harvest_tracking_id','=',0)
                ->where('si_pond_management.harvest_counter','=',0)
                ->where('inventories.type_id','=',1)
                ->sum('inventory_used_in_water_details.quantity');
        }else{
            $feed=ClusterPondManagement::join('inventory_used_in_water_details','inventory_used_in_water_details.ct_pond_management_id','=','cluster_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                ->where('cluster_pond_management.pond_id','=',$pond_id)
                ->where('cluster_pond_management.cluster_id','=',$farm_cluster_id)
                ->where('cluster_pond_management.cluster_manager_id','=',$user_id)
                ->where('cluster_pond_management.harvest_tracking_id','=',0)
                ->where('cluster_pond_management.harvest_counter','=',0)
                ->where('inventories.type_id','=',1)
                ->sum('inventory_used_in_water_details.quantity');
        }
        return $feed;
    }
    public function getBiomass($user_id,$user_type,$farm_cluster_id,$pond_id){
        if($user_type==6){
            $biomass=SiAnimalSampling::where('pond_id',$pond_id)
                ->where('farm_id',$farm_cluster_id)
                ->where('farm_manager_id',$user_id)
                ->where('harvest_tracking_id',0)
                ->where('harvest_counter',0)
                ->select('current_biomass')
                ->orderBy('id','desc')
                ->first();
        }else{
            $biomass=ClusterAnimalSampling::where('pond_id',$pond_id)
                ->where('cluster_id',$farm_cluster_id)
                ->where('cluster_manager_id',$user_id)
                ->where('harvest_tracking_id',0)
                ->where('harvest_counter',0)
                ->select('current_biomass')
                ->orderBy('id','desc')
                ->first();
        }
        if(count($biomass)>0){
            return $biomass->current_biomass;
        }
        return 0;
    }

    public function trackHarvest($user_id,$user_type,$farm_cluster_id,$pond_id,$harvest_entry_id){
        DB::beginTransaction();
        try{
            if($user_type==6){
                $harvest_count=SiAnimalHarvest::withoutZero()->where('farm_id',$farm_cluster_id)->where('farm_manager_id',$user_id)->where('pond_id',$pond_id)->count('id');

                $si_pl_stock=SiPlStock::where('farm_id',$farm_cluster_id)->where('farm_manager_id',$user_id)->where('pond_id',$pond_id)->where('status','enclosure')
                    ->where('harvest_tracking_id',0)->where('harvest_counter',0)->get();

                if(count($si_pl_stock)>0){
                    foreach ($si_pl_stock as $pl_stock){
                        $pl_stock->status='harvested';
                        $pl_stock->harvest_tracking_id=$harvest_entry_id;
                        $pl_stock->harvest_counter=$harvest_count;
                        $pl_stock->save();
                    }
                }

                $si_animal_sampling=SiAnimalSampling::where('farm_id',$farm_cluster_id)->where('farm_manager_id',$user_id)->where('pond_id',$pond_id)
                    ->where('harvest_tracking_id',0)->where('harvest_counter',0)->get();

                if(count($si_animal_sampling)>0){
                    foreach ($si_animal_sampling as $animal_sampling){
                        $animal_sampling->harvest_tracking_id=$harvest_entry_id;
                        $animal_sampling->harvest_counter=$harvest_count;
                        $animal_sampling->save();
                    }
                }

                $si_pond_management=SiPondManagement::where('farm_id',$farm_cluster_id)->where('farm_manager_id',$user_id)->where('pond_id',$pond_id)
                    ->where('harvest_tracking_id',0)->where('harvest_counter',0)->get();

                if(count($si_pond_management)>0){
                    foreach ($si_pond_management as $pond_management){
                        $pond_management->harvest_tracking_id=$harvest_entry_id;
                        $pond_management->harvest_counter=$harvest_count;
                        $pond_management->save();
                    }
                }
            }else{
                $harvest_count=ClusterAnimalHarvest::withoutZero()->where('cluster_id',$farm_cluster_id)->where('cluster_manager_id',$user_id)->where('pond_id',$pond_id)->count('id');

                $ct_pl_stock=ClusterPlStock::where('cluster_id',$farm_cluster_id)->where('cluster_manager_id',$user_id)->where('pond_id',$pond_id)->where('status','enclosure')
                    ->where('harvest_tracking_id',0)->where('harvest_counter',0)->get();
                if(count($ct_pl_stock)>0){
                    foreach ($ct_pl_stock as $pl_stock){
                        $pl_stock->status='harvested';
                        $pl_stock->harvest_tracking_id=$harvest_entry_id;
                        $pl_stock->harvest_counter=$harvest_count;
                        $pl_stock->save();
                    }
                }

                $ct_animal_sampling=ClusterAnimalSampling::where('cluster_id',$farm_cluster_id)->where('cluster_manager_id',$user_id)->where('pond_id',$pond_id)
                    ->where('harvest_tracking_id',0)->where('harvest_counter',0)->get();
                if(count($ct_animal_sampling)>0){
                    foreach ($ct_animal_sampling as $animal_sampling){
                        $animal_sampling->harvest_tracking_id=$harvest_entry_id;
                        $animal_sampling->harvest_counter=$harvest_count;
                        $animal_sampling->save();
                    }
                }

                $ct_pond_management=ClusterPondManagement::where('cluster_id',$farm_cluster_id)->where('cluster_manager_id',$user_id)->where('pond_id',$pond_id)
                    ->where('harvest_tracking_id',0)->where('harvest_counter',0)->get();
                if(count($ct_pond_management)>0){
                    foreach ($ct_pond_management as $pond_management){
                        $pond_management->harvest_tracking_id=$harvest_entry_id;
                        $pond_management->harvest_counter=$harvest_count;
                        $pond_management->save();
                    }
                }
            }
            DB::commit();
            return true;
        }catch (Exception $e){
            DB::rollBack();
            return false;
        }
    }
}
