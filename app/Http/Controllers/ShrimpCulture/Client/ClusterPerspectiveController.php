<?php

namespace App\Http\Controllers\ShrimpCulture\Client;

use App\ShrimpCultureModels\Cluster;
use App\ShrimpCultureModels\ClusterPond;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClusterPerspectiveController extends Controller
{
    public function pondManagement(){
        $data['title'] = 'Extensive Pond Management';
        $data['farms']=Cluster::join('rel_cluster_client','rel_cluster_client.cluster_id','=','clusters.id')
            ->where('rel_cluster_client.client_id','=',auth()->user()->id)->pluck('clusters.name as name','clusters.id as id');
        $data['ponds']=ClusterPond::join('clusters','clusters.id','=','cluster_ponds.cluster_id')
            ->join('rel_cluster_client','rel_cluster_client.cluster_id','=','clusters.id')
            ->where('rel_cluster_client.client_id','=',auth()->user()->id)->pluck('cluster_ponds.name as name','cluster_ponds.id as id');
        return view('shrimp_culture.client.cluster_perspective.pond_management',$data);
    }
    public function getPondInfo(Request $request){
        $data['pond_info']=[];
        $data['graph_date_data']=[];
        $data['graph_biomass_data']=[];
        if($request->has('pond_id') && !is_null($request->pond_id)){
            $data['pond_info']=SiPond::active()->with(['last_staffs_data'=>function($query){
                $query->orderBy('id','desc');
            }])->with(['last_sampling_data'=>function($query){
                $query->enclosured()->orderBy('id','desc');
            }])->findOrFail($request->pond_id);
            $graph_date_data=SiAnimalSampling::where('pond_id',$request->pond_id)
                ->where('harvest_tracking_id',0)
                ->where('harvest_counter',0)
                ->select('si_animal_sampling.current_biomass','si_animal_sampling.created_at')
                ->get();
            foreach ($graph_date_data as $graph_data){
                $date=new Carbon($graph_data->created_at);
                array_push($data['graph_date_data'],$date->format('Y-m-d'));
                array_push($data['graph_biomass_data'],$graph_data->current_biomass);
            }
            $feed_data=SiPondManagement::join('')
                ->where('pond_id',$request->pond_id)
                ->where('harvest_tracking_id',0)
                ->where('harvest_counter',0)
                ->get();
        }
//        return $data;
        return view('shrimp_culture.client.semi_intensive.selected_ajax_views.pond_basic_info',$data);
    }
    public function getFeedInfo(Request $request){
        $data['feed_info']=[];
        if($request->has('farm_id') && !is_null($request->farm_id) && $request->has('pond_id') && !is_null($request->pond_id)){
            $data['feed_info']=SiPondManagement::join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                ->where('inventories.type_id','=',1)
                ->where('si_pond_management.farm_id','=',$request->farm_id)
                ->where('si_pond_management.pond_id','=',$request->pond_id)
                ->where('si_pond_management.id','!=',0)
                ->where('si_pond_management.harvest_tracking_id','=',0)
                ->where('si_pond_management.harvest_counter','=',0)
                ->select('inventories.name','inventory_used_in_water_details.quantity','inventory_used_in_water_details.remaining_quantity',
                    'inventory_used_in_water_details.timing','inventory_used_in_water_details.created_at')
                ->get();
        }
        return view('shrimp_culture.client.semi_intensive.selected_ajax_views.feed_info',$data);
    }
    public function getProbioticMineralOtherInfo(Request $request){
        $data['other_inventories_info']=[];
        if($request->has('farm_id') && !is_null($request->farm_id) && $request->has('pond_id') && !is_null($request->pond_id)){
            $data['other_inventories_info']=SiPondManagement::join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                ->whereBetween('inventories.type_id',[2,6])
                ->where('si_pond_management.farm_id','=',$request->farm_id)
                ->where('si_pond_management.pond_id','=',$request->pond_id)
                ->where('si_pond_management.id','!=',0)
                ->where('si_pond_management.harvest_tracking_id','=',0)
                ->where('si_pond_management.harvest_counter','=',0)
                ->select('inventories.name','inventory_used_in_water_details.quantity','inventory_used_in_water_details.remaining_quantity',
                    'inventory_used_in_water_details.timing','inventory_used_in_water_details.created_at')
                ->get();
        }
        return view('shrimp_culture.client.semi_intensive.selected_ajax_views.other_inventories_info',$data);
    }
    public function getMonitoringParameterInfo(Request $request){
        $data['monitoring_parameter_info']=[];
        $data['parameter_selection']='';
        if($request->has('farm_id') && !is_null($request->farm_id) && $request->has('pond_id') && !is_null($request->pond_id) && $request->has('parameter_selection') && !is_null($request->parameter_selection)){
            $data['parameter_selection']=$request->parameter_selection;
            if($request->parameter_selection==0){
                $data['monitoring_parameter_info']=SiPondManagement::join('ph_details','ph_details.si_pond_management_id','=','si_pond_management.id')
                    ->where('si_pond_management.farm_id','=',$request->farm_id)
                    ->where('si_pond_management.pond_id','=',$request->pond_id)
                    ->where('si_pond_management.id','!=',0)
                    ->where('si_pond_management.harvest_tracking_id','=',0)
                    ->where('si_pond_management.harvest_counter','=',0)
                    ->select('ph_details.quantity','ph_details.timing','ph_details.created_at')
                    ->get();
            }elseif ($request->parameter_selection==1){
                $data['monitoring_parameter_info']=SiPondManagement::join('dissolved_oxygen_details','dissolved_oxygen_details.si_pond_management_id','=','si_pond_management.id')
                    ->where('si_pond_management.farm_id','=',$request->farm_id)
                    ->where('si_pond_management.pond_id','=',$request->pond_id)
                    ->where('si_pond_management.id','!=',0)
                    ->where('si_pond_management.harvest_tracking_id','=',0)
                    ->where('si_pond_management.harvest_counter','=',0)
                    ->select('dissolved_oxygen_details.quantity','dissolved_oxygen_details.timing','dissolved_oxygen_details.created_at')
                    ->get();
            }elseif ($request->parameter_selection==2){
                $data['monitoring_parameter_info']=SiPondManagement::join('temperature_details','temperature_details.si_pond_management_id','=','si_pond_management.id')
                    ->where('si_pond_management.farm_id','=',$request->farm_id)
                    ->where('si_pond_management.pond_id','=',$request->pond_id)
                    ->where('si_pond_management.id','!=',0)
                    ->where('si_pond_management.harvest_tracking_id','=',0)
                    ->where('si_pond_management.harvest_counter','=',0)
                    ->select('temperature_details.quantity','temperature_details.timing','temperature_details.created_at')
                    ->get();
            }else{
                $data['monitoring_parameter_info']=SiPondManagement::join('water_parameter_details','water_parameter_details.si_pond_management_id','=','si_pond_management.id')
                    ->where('si_pond_management.farm_id','=',$request->farm_id)
                    ->where('si_pond_management.pond_id','=',$request->pond_id)
                    ->where('si_pond_management.id','!=',0)
                    ->where('si_pond_management.harvest_tracking_id','=',0)
                    ->where('si_pond_management.harvest_counter','=',0)
                    ->select('water_parameter_details.*')
                    ->get();
            }
        }
        return view('shrimp_culture.client.semi_intensive.selected_ajax_views.monitoring_parameter_info',$data);
    }
    public function getTotalFeed($farm_id,$pond_id){
        $feed=SiPondManagement::join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
            ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
            ->where('si_pond_management.pond_id','=',$pond_id)
            ->where('si_pond_management.farm_id','=',$farm_id)
            ->where('si_pond_management.harvest_tracking_id','=',0)
            ->where('si_pond_management.harvest_counter','=',0)
            ->where('inventories.type_id','=',1)
            ->sum('inventory_used_in_water_details.quantity');
        return $feed;
    }
    public function harvest(){

    }
}
