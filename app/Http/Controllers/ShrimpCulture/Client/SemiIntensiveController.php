<?php

namespace App\Http\Controllers\ShrimpCulture\Client;

use App\ShrimpCultureModels\SiAnimalHarvest;
use App\ShrimpCultureModels\SiAnimalSampling;
use App\ShrimpCultureModels\SiFarm;
use App\ShrimpCultureModels\SiPond;
use App\ShrimpCultureModels\SiPondManagement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SemiIntensiveController extends Controller
{
    public function pondManagement(){
        $data['title'] = 'Semi-Intensive Pond Management';
        $data['farm_id']='';
        $data['pond_id']='';
        $data['pond_info']='';
        $data['pond_management']=[];
        $data['farms']=SiFarm::join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_farms.name as name','si_farms.id as id');

        $data['ponds']=SiPond::join('si_farms','si_farms.id','=','si_ponds.si_farm_id')
            ->join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_ponds.name as name','si_ponds.id as id');
        return view('shrimp_culture.client.semi_intensive.pond_management',$data);
    }
    public function getPondInfo(Request $request){
        $data['pond_info']=[];
        $data['graph_date_data']=[];
        $data['graph_feed_data']=[];
        $data['graph_biomass_data']=[];
        $data['graph_fcr_data']=[];
        if($request->has('farm_id') && !is_null($request->farm_id) && $request->has('pond_id') && !is_null($request->pond_id)){
            $farm_id=$request->farm_id;
            $data['pond_info']=SiPond::active()->with(['last_staffs_data'=>function($query){
                $query->where('cluster_id',0)->orderBy('id','desc');
            }])->with(['last_sampling_data'=>function($query){
                $query->enclosured()->orderBy('id','desc');
            }])->findOrFail($request->pond_id);
            $pond_management_today=SiPondManagement::with([])->
            whereDate('created_at','=',Carbon::today()->toDateString())->select('id');
            $data['pond_management_today']=$feed=SiPondManagement::with(['inventory_used_in_water_details'=>function($query){
                $query->with(['inventory'=>function($query){
                    $query->select('id','type_id','name');
                }])->select('si_pond_management_id','inventory_id','quantity');
            },'ph_details'=>function($query){
                $query->select('si_pond_management_id','quantity','timing');
            },'dissolved_oxygen_details'=>function($query){
                $query->select('si_pond_management_id','quantity','timing');
            },'temperature_details'=>function($query){
                $query->select('si_pond_management_id','quantity','timing');
            },'water_parameter_details'])
                ->whereDate('created_at','=',Carbon::today()->toDateString())->select('id')->first();
//            $data['feed_today']=$this->getTodaysFeed($pond_management_today);
//            $data['other_inventory_today']=$this->getTodaysOtherInventory($pond_management_today);
//            $data['ph_today']=$this->getTodaysPH($pond_management_today);
//            $data['dissolved_oxygen_today']=$this->getTodaysDissolvedOxygen($pond_management_today);
//            $data['temperature_today']=$this->getTodaysTemperature($pond_management_today);
//            $data['water_parameter_today']=$this->getTodaysWaterParameter($pond_management_today);
            $graph_date_data=SiAnimalSampling::where('pond_id',$request->pond_id)
                ->where('harvest_tracking_id',0)
                ->where('harvest_counter',0)
                ->select('si_animal_sampling.current_biomass','si_animal_sampling.created_at')
                ->get();
            $graph_feed_data=SiPondManagement::join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
                ->where('pond_id',$request->pond_id)
                ->where('harvest_tracking_id',0)
                ->where('harvest_counter',0)
                ->select(DB::raw('SUM(inventory_used_in_water_details.quantity) as total_quantity'),DB::raw('DATE(inventory_used_in_water_details.created_at) as created_at'))
                ->groupBy(DB::raw('date(inventory_used_in_water_details.created_at)'))->get();
//            $period=CarbonPeriod::create();
            $from_date = $graph_feed_data[0]->created_at;
            $to_date = ($graph_feed_data[count($graph_feed_data)-1]->created_at);
            $dates = $this->generateDateRange($from_date, $to_date);

            $countGraph=0;
//            foreach ($dates as $dateValue){
//                if(){}
//            }
//            return $dates;
//            $countGraph=0;
//            foreach ($graph_feed_data as $graph_data){
//                $date=new Carbon($graph_data->created_at);
//                $biomassdate=new Carbon($graph_date_data[$countGraph]->created_at);
//                $biomassdate=$biomassdate->format('Y-m-d');
////                array_push($data['graph_date_data'],$date->format('Y-m-d'));
////                array_push($data['graph_feed_data'],$graph_data->total_quantity);
//                $previous_biomass=$graph_date_data[$countGraph]->current_biomass;
//                if($date==$biomassdate){
////                    array_push($data['graph_biomass_data'],$graph_date_data[$countGraph]->current_biomass);
//                }else{
//                    if($countGraph>1){
////                        array_push($data['graph_biomass_data'],$graph_date_data[$countGraph-1]->current_biomass);
//                    }else{
////                        array_push($data['graph_biomass_data'],$graph_date_data[$countGraph]->current_biomass);
//                    }
//                }
////                array_push($data['graph_fcr_data'],number_format($graph_data->total_quantity/$graph_date_data[$countGraph]->current_biomass,2));
//                $countGraph++;
//            }
//            $feed_data=SiPondManagement::join('')
//                ->where('pond_id',$request->pond_id)
//                ->where('harvest_tracking_id',0)
//                ->where('harvest_counter',0)
//                ->get();
        }
//        return $data;
        return view('shrimp_culture.client.semi_intensive.selected_pond_management_views.pond_basic_info',$data);
    }
    public function getFeedInfo(Request $request){
        $data['feed_info']=[];
        if($request->has('farm_id') && !is_null($request->farm_id) && $request->has('pond_id') && !is_null($request->pond_id)){
            $data['feed_info']=SiPondManagement::join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                ->where('inventories.type_id','=',1)
                ->where('si_pond_management.farm_id','=',$request->farm_id)
                ->where('si_pond_management.pond_id','=',$request->pond_id)
                ->where('si_pond_management.id','!=',0)
                ->where('si_pond_management.harvest_tracking_id','=',0)
                ->where('si_pond_management.harvest_counter','=',0)
                ->select('inventories.name','inventory_used_in_water_details.quantity','inventory_used_in_water_details.remaining_quantity',
                    'inventory_used_in_water_details.timing','inventory_used_in_water_details.created_at')
                ->get();
        }
        return view('shrimp_culture.client.semi_intensive.selected_pond_management_views.feed_info',$data);
    }
    public function getProbioticMineralOtherInfo(Request $request){
        $data['other_inventories_info']=[];
        if($request->has('farm_id') && !is_null($request->farm_id) && $request->has('pond_id') && !is_null($request->pond_id)){
            $data['other_inventories_info']=SiPondManagement::join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
                ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
                ->whereBetween('inventories.type_id',[2,6])
                ->where('si_pond_management.farm_id','=',$request->farm_id)
                ->where('si_pond_management.pond_id','=',$request->pond_id)
                ->where('si_pond_management.id','!=',0)
                ->where('si_pond_management.harvest_tracking_id','=',0)
                ->where('si_pond_management.harvest_counter','=',0)
                ->select('inventories.name','inventory_used_in_water_details.quantity','inventory_used_in_water_details.remaining_quantity',
                    'inventory_used_in_water_details.timing','inventory_used_in_water_details.created_at')
                ->get();
        }
        return view('shrimp_culture.client.semi_intensive.selected_pond_management_views.other_inventories_info',$data);
    }
    public function getMonitoringParameterInfo(Request $request){
        $data['monitoring_parameter_info']=[];
        $data['parameter_selection']='';
        if($request->has('farm_id') && !is_null($request->farm_id) && $request->has('pond_id') && !is_null($request->pond_id) && $request->has('parameter_selection') && !is_null($request->parameter_selection)){
            $data['parameter_selection']=$request->parameter_selection;
            if($request->parameter_selection==0){
                $data['monitoring_parameter_info']=SiPondManagement::join('ph_details','ph_details.si_pond_management_id','=','si_pond_management.id')
                    ->where('si_pond_management.farm_id','=',$request->farm_id)
                    ->where('si_pond_management.pond_id','=',$request->pond_id)
                    ->where('si_pond_management.id','!=',0)
                    ->where('si_pond_management.harvest_tracking_id','=',0)
                    ->where('si_pond_management.harvest_counter','=',0)
                    ->select('ph_details.quantity','ph_details.timing','ph_details.created_at')
                    ->get();
            }elseif ($request->parameter_selection==1){
                $data['monitoring_parameter_info']=SiPondManagement::join('dissolved_oxygen_details','dissolved_oxygen_details.si_pond_management_id','=','si_pond_management.id')
                    ->where('si_pond_management.farm_id','=',$request->farm_id)
                    ->where('si_pond_management.pond_id','=',$request->pond_id)
                    ->where('si_pond_management.id','!=',0)
                    ->where('si_pond_management.harvest_tracking_id','=',0)
                    ->where('si_pond_management.harvest_counter','=',0)
                    ->select('dissolved_oxygen_details.quantity','dissolved_oxygen_details.timing','dissolved_oxygen_details.created_at')
                    ->get();
            }elseif ($request->parameter_selection==2){
                $data['monitoring_parameter_info']=SiPondManagement::join('temperature_details','temperature_details.si_pond_management_id','=','si_pond_management.id')
                    ->where('si_pond_management.farm_id','=',$request->farm_id)
                    ->where('si_pond_management.pond_id','=',$request->pond_id)
                    ->where('si_pond_management.id','!=',0)
                    ->where('si_pond_management.harvest_tracking_id','=',0)
                    ->where('si_pond_management.harvest_counter','=',0)
                    ->select('temperature_details.quantity','temperature_details.timing','temperature_details.created_at')
                    ->get();
            }else{
                $data['monitoring_parameter_info']=SiPondManagement::join('water_parameter_details','water_parameter_details.si_pond_management_id','=','si_pond_management.id')
                    ->where('si_pond_management.farm_id','=',$request->farm_id)
                    ->where('si_pond_management.pond_id','=',$request->pond_id)
                    ->where('si_pond_management.id','!=',0)
                    ->where('si_pond_management.harvest_tracking_id','=',0)
                    ->where('si_pond_management.harvest_counter','=',0)
                    ->select('water_parameter_details.*')
                    ->get();
            }
        }
        return view('shrimp_culture.client.semi_intensive.selected_pond_management_views.monitoring_parameter_info',$data);
    }
    public function getTotalFeed($farm_id,$pond_id){
        $feed=SiPondManagement::join('inventory_used_in_water_details','inventory_used_in_water_details.si_pond_management_id','=','si_pond_management.id')
            ->join('inventories','inventories.id','=','inventory_used_in_water_details.inventory_id')
            ->where('si_pond_management.pond_id','=',$pond_id)
            ->where('si_pond_management.farm_id','=',$farm_id)
            ->where('si_pond_management.harvest_tracking_id','=',0)
            ->where('si_pond_management.harvest_counter','=',0)
            ->where('inventories.type_id','=',1)
            ->sum('inventory_used_in_water_details.quantity');
    return $feed;
    }
    public function getTodaysFeed($pond_management_today){
        $feed=$pond_management_today->with(['inventory_used_in_water_details'=>function($query){
            $query->with(['inventory'=>function($query){
                $query->where('type_id',1)->select('id','name');
            }])->select('si_pond_management_id','inventory_id','quantity');
        }])->first();
        return $feed;
    }
    public function getTodaysOtherInventory($pond_management_today){
        $other_inventory=$pond_management_today->with(['inventory_used_in_water_details'=>function($query){
            $query->with(['inventory'=>function($query){
                $query->whereBetween('type_id',[2,6])->select('id','name');
            }])->select('si_pond_management_id','inventory_id','quantity');
        }])->get();
        return $other_inventory;
    }
    public function getTodaysPH($pond_management_today){
        $ph=$pond_management_today->with(['ph_details'=>function($query){
            $query->select('si_pond_management_id','quantity','timing');
        }])->first();
        return $ph;
    }
    public function getTodaysDissolvedOxygen($pond_management_today){
        $dissolved_oxygen=$pond_management_today->with(['dissolved_oxygen_details'=>function($query){
            $query->select('si_pond_management_id','quantity','timing');
        }])->first();
        return $dissolved_oxygen;
    }
    public function getTodaysTemperature($pond_management_today){
        $temperature=$pond_management_today->with(['temperature_details'=>function($query){
            $query->select('si_pond_management_id','quantity','timing');
        }])->first();
        return $temperature;
    }
    public function getTodaysWaterParameter($pond_management_today){
        $water_parameter=$pond_management_today->with(['water_parameter_details'])->first();
        return $water_parameter;

        $pond_management_today=$feed=SiPondManagement::with(['inventory_used_in_water_details'=>function($query){
            $query->with(['inventory'=>function($query){
                $query->where('type_id',1)->select('id','name');
            }])->select('si_pond_management_id','inventory_id','quantity');
        },'ph_details'=>function($query){
            $query->select('si_pond_management_id','quantity','timing');
        },'dissolved_oxygen_details'=>function($query){
            $query->select('si_pond_management_id','quantity','timing');
        },'temperature_details'=>function($query){
            $query->select('si_pond_management_id','quantity','timing');
        },'water_parameter_details'])->first();
    }
    public function sampling(Request $request){
        $data['title'] = 'Semi-Intensive Sampling';
        $data['farm_id']='';
        $data['pond_id']='';
        $data['animal_sampling']=[];
        if($request->has('farm_id') && !is_null($request->farm_id) && $request->has('pond_id') && !is_null($request->pond_id)){
            $data['farm_id']=$request->has('farm_id');
            $data['pond_id']=$request->has('pond_id');
            $data['animal_sampling']=SiAnimalSampling::where('farm_id',$request->farm_id)
            ->where('pond_id',$request->pond_id)
            ->where('harvest_tracking_id',0)
            ->where('harvest_counter',0)->get();
        }
        $data['farms']=SiFarm::join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_farms.name as name','si_farms.id as id');
        $data['ponds']=SiPond::join('si_farms','si_farms.id','=','si_ponds.si_farm_id')
            ->join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_ponds.name as name','si_ponds.id as id');
        return view('shrimp_culture.client.semi_intensive.sampling',$data);

    }
    public function harvest(Request $request){
        $data['title'] = 'Semi-Intensive Harvest';
        $data['farm_id']='';
        $data['pond_id']='';
        $animal_harvest=SiAnimalHarvest::withoutZero();
        if($request->has('farm_id') && !is_null($request->farm_id) && $request->has('pond_id') && !is_null($request->pond_id)){
            $data['farm_id']=$request->has('farm_id');
            $data['pond_id']=$request->has('pond_id');
            $animal_harvest=$animal_harvest::where('farm_id',$request->farm_id)
            ->where('pond_id',$request->pond_id);
        }
        $data['farms']=SiFarm::join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_farms.name as name','si_farms.id as id');
        $data['ponds']=SiPond::join('si_farms','si_farms.id','=','si_ponds.si_farm_id')
            ->join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->pluck('si_ponds.name as name','si_ponds.id as id');
        $data['animal_harvest']=$animal_harvest->get();
        return view('shrimp_culture.client.semi_intensive.sampling',$data);
    }
    private function generateDateRange(Carbon $start_date, Carbon $end_date){
        $dates = [];
        for($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }
        return $dates;
    }
}
