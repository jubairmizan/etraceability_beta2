<?php

namespace App\Http\Controllers\ShrimpCulture\Client;

use App\Hatchery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HatcheryController extends Controller
{
    public function index(){
        $data['title']='Hatcheries';
        $client_id=auth()->user()->id;
        $data['hatcheries']=Hatchery::join('rel_hatchery_client','rel_hatchery_client.hatchery_id','=','hatcheries.id')
            ->where('rel_hatchery_client.client_id','=',$client_id)->select('hatcheries.*')->get();
        return view('shrimp_culture.client.hatchery.index',$data);
    }
}
