<?php

namespace App\Http\Controllers\ShrimpCulture\Client;

use App\InventoryManagement;
use App\ShrimpCultureModels\SiFarm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InventoryController extends Controller
{
    public function index(){
        $data['title']='Inventories';
        $farm=SiFarm::join('rel_si_farm_client','rel_si_farm_client.si_farm_id','=','si_farms.id')
            ->where('rel_si_farm_client.client_id','=',auth()->user()->id)->first();
        $data['inventories']=InventoryManagement::where('si_farm_id',$farm->id)->get();
        return view('shrimp_culture.client.inventory.index',$data);
    }
}
