<?php

namespace App\Http\Controllers\WhiteFish;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WhiteFish\Pond;
use App\WhiteFish\WfStaff;
use App\WhiteFish\WfDuringEnclosure;
use App\Hatchery;
use App\WhiteFish\PonaStock;
use App\WhiteFish\TypesOfFish;
use App\WhiteFish\PonaSampling;
use App\WhiteFish\Farm;
use App\WhiteFish\WfHarvest;
use App\WhiteFish\WfNotification;
use App\TemperatureDetails;
use App\WaterParameterDetails;
use App\DissolvedOxygenDetails;
use App\PHDetails;
use App\User;
use App\HarvestDetails;
use App\Inventory;
use App\InventoryBrand;
use App\InventoryManagement;
use App\InventoryType;
use App\InventoryUsedInWaterDetails;
use App\RelInventoryDetails;
use Auth;
use DB;

class AllApiController extends Controller
{
    public function pondDetails($user){
    	$ponds = Pond::select('farm_id','block_id','pond_id','name as pond_name','area as pond_dimension')->addSelect(DB::raw('(SELECT no_of_staff FROM wf_staffs WHERE wf_ponds.id = wf_staffs.pond_id ORDER BY created_at DESC LIMIT 1) no_of_staff'))->get();

    	return $ponds;
    }
    public function staffUpdate($user_id,$farm_id,$block_id,$pond_id,$no_of_staff){
        $user=User::where('user_id',$user_id)->first();
        $pond = Pond::where(['org_id'=>$user->org_id,'farm_id'=>$farm_id,'block_id'=>$block_id,'id'=>$pond_id])->count();
        
        if($pond>0){
            $staff=new WfStaff();
            $staff->org_id=$user->org_id;
            $staff->farm_id=$farm_id;
            $staff->block_id=$block_id;
            $staff->pond_id=$pond_id;
            $staff->no_of_staff=$no_of_staff;
            $staff->save();
            return true;
        }
        return false;
    }
    public function pLStock($user_id,$farm_id,$block_id,$pond_id,$pl_amount,$pl_age,$pl_price,$fish_id){
        $user=User::where('user_id',$user_id)->first();
        $pond = Pond::where(['org_id'=>$user->org_id,'farm_id'=>$farm_id,'block_id'=>$block_id,'id'=>$pond_id])->count();
        if($pond>0 && count($user)>0){
            $ponaStock=new PonaStock();
            $ponaStock->org_id=$user->org_id;
            $ponaStock->farm_id=$farm_id;
            $ponaStock->block_id=$block_id;
            $ponaStock->pond_id=$pond_id;
            $ponaStock->stock_date=date('Y-m-d');
            $ponaStock->fish_id=$fish_id;
            $ponaStock->quantity=$pl_amount;
            $ponaStock->cost=$pl_price;
            $ponaStock->establish_nursery=1;
            $ponaStock->nursing_during=$pl_age;
            $ponaStock->save();
            return true;
        }
        return false;
    }

    public function animalDetails($user){
        $hatchery=Hatchery::active()->select('id','name')->get();
        $fish = TypesOfFish::where('org_id',$user->org_id)->select('id','name')->get();
        // $plDetails = DB::table('wf_pona_stocks')->join('wf_ponds','wf_pona_stocks.pond_id','=','wf_ponds.id')->leftJoin('wf_pona_sampling','')->select('wf_pona_stocks.farm_id','wf_pona_stocks.block_id','wf_pona_stocks.pond_id','wf_ponds.pond_name','wf_pona_stocks.quantity as pl_amount','wf_pona_stocks.nursing_during as pl_age','wf_pona_stocks.cost as pl_price')->get();

        $plDetails = Pond::
                        select(
                                'farm_id','block_id','pond_id','name as pond_name',
                                DB::raw('(SELECT quantity FROM wf_pona_stocks WHERE wf_ponds.id = wf_pona_stocks.pond_id ORDER BY created_at DESC LIMIT 1) as pl_amount'),
                                DB::raw('(SELECT IFNULL(nursing_during,0) FROM wf_pona_stocks WHERE wf_ponds.id = wf_pona_stocks.pond_id ORDER BY created_at DESC LIMIT 1) as pl_age'),
                                DB::raw('(SELECT IFNULL(cost,0) FROM wf_pona_stocks WHERE wf_ponds.id = wf_pona_stocks.pond_id ORDER BY created_at DESC LIMIT 1) as pl_price'),
                                DB::raw('(SELECT IFNULL(survival_rate,0) FROM wf_pona_sampling WHERE wf_ponds.id = wf_pona_sampling.pond_id ORDER BY created_at DESC LIMIT 1) as survival_rate'),
                                DB::raw('(SELECT IFNULL(current_biomass,0) FROM wf_pona_sampling WHERE wf_ponds.id = wf_pona_sampling.pond_id ORDER BY created_at DESC LIMIT 1) as biomass'),
                                DB::raw('(SELECT IFNULL(number_of_sample,0) FROM wf_pona_sampling WHERE wf_ponds.id = wf_pona_sampling.pond_id ORDER BY created_at DESC LIMIT 1) as num_of_sample'),
                                DB::raw('(SELECT IFNULL(total_weight,0) FROM wf_pona_sampling WHERE wf_ponds.id = wf_pona_sampling.pond_id ORDER BY created_at DESC LIMIT 1) as total_weight'),
                                DB::raw('(SELECT IFNULL(average_body_weight,0) FROM wf_pona_sampling WHERE wf_ponds.id = wf_pona_sampling.pond_id ORDER BY created_at DESC LIMIT 1) as avg_body_weight'))
                        ->get();

        $newArray = array();
        $sl = 0;
        foreach($plDetails as $value){
            $newArray[$sl]['farm_id'] = $value->farm_id;
            $newArray[$sl]['block_id'] = $value->block_id;
            $newArray[$sl]['pond_id'] = $value->pond_id;
            $newArray[$sl]['pond_name'] = $value->pond_name;
            $newArray[$sl]['pl_amount'] = (!is_null($value->pl_amount)?$value->pl_amount:0);
            $newArray[$sl]['pl_age'] = (!is_null($value->pl_age)?$value->pl_age:0);
            $newArray[$sl]['pl_price'] = (!is_null($value->pl_price)?$value->pl_price:0);
            $newArray[$sl]['survival_rate'] = (!is_null($value->survival_rate)?$value->survival_rate:0);
            $newArray[$sl]['biomass'] = (!is_null($value->biomass)?$value->biomass:0);
            $newArray[$sl]['num_of_sample'] = (!is_null($value->num_of_sample)?$value->num_of_sample:0);
            $newArray[$sl]['total_weight'] = (!is_null($value->total_weight)?$value->total_weight:0);
            $newArray[$sl]['avg_body_weight'] = (!is_null($value->avg_body_weight)?$value->avg_body_weight:0);
            $sl++; 
        }

        return response()->json([
            'hatchery'=>$hatchery,
            'fish'=>$fish,
            'animal_details'=>$newArray,
            'status' => 'Data found',
            'code'=>200
        ]);
    }

    public function pLSampling($user,$farm_id,$block_id,$pond_id,$fish_id,$survival_rate,$biomass,$num_of_sample,$total_weight,$avg_body_weight){
        $user=User::where('user_id',$user)->first();
        $pond = Pond::where(['org_id'=>$user->org_id,'farm_id'=>$farm_id,'block_id'=>$block_id,'id'=>$pond_id])->count();
        if($pond>0 && count($user)>0){
            $pLSampling=new PonaSampling();
            $pLSampling->org_id=$user->org_id;
            $pLSampling->farm_id=$farm_id;
            $pLSampling->block_id=$block_id;
            $pLSampling->pond_id=$pond_id;
            $pLSampling->farm_manager_id=$user->id;
            $pLSampling->fish_id=$fish_id;
            $pLSampling->number_of_sample=$num_of_sample;
            $pLSampling->average_body_weight=$avg_body_weight;
            $pLSampling->current_biomass=$biomass;
            $pLSampling->survival_rate=$survival_rate;
            $pLSampling->total_weight=$total_weight;
            $pLSampling->save();
            return true;
        }
        return false;
    }

    public function pondList($user){
        $ponds = Pond::select('farm_id','block_id','id as pond_id','name as pond_name')->where('org_id',$user->org_id)->get();
        return $ponds;
    }

    public function addMonitoringParameters($user,$block_id,$farm_id,$pond_id,$ph,$dissolved_oxygen,$temperature,$hco3,
                                            $co3,$ca,$cl2,$mg,$k,$nh3,$no3,$nh4,$salinity,$transparency,$plankton_type,$green,$yellow,$vibrio_count){
        
        $user_id=$user->id;
        $pond= Pond::findOrFail($pond_id);
        if(!is_null($pond)){

            $WfDuringEnclosure = new WfDuringEnclosure();
            $WfDuringEnclosure->org_id = $user->org_id;
            $WfDuringEnclosure->farm_id = $farm_id;
            $WfDuringEnclosure->block_id = $block_id;
            $WfDuringEnclosure->pond_id = $pond_id;
            $WfDuringEnclosure->inventory_management_id = 0;
            $WfDuringEnclosure->save();

            if(!is_null($WfDuringEnclosure->id) && (!is_null($hco3) || !is_null($co3) || !is_null($ca) || !is_null($cl2) || !is_null($mg) || !is_null($k) || !is_null($nh3)
                    || !is_null($no3) || !is_null($nh4) || !is_null($salinity) || !is_null($transparency) || !is_null($plankton_type) || !is_null($green)
                    || !is_null($yellow) || !is_null($vibrio_count))){
                $water_parameter_details=new WaterParameterDetails();
                $water_parameter_details->si_pond_management_id=0;
                $water_parameter_details->ct_pond_management_id=0;
                $water_parameter_details->wf_during_enclosure_id=$WfDuringEnclosure->id;
                $water_parameter_details->alkalinity_hco3=$hco3;
                $water_parameter_details->carbonate_co3=$co3;
                $water_parameter_details->calcium_ca=$ca;
                $water_parameter_details->chlorine_cl2=$cl2;
                $water_parameter_details->magnesium_mg=$mg;
                $water_parameter_details->potassium_k=$k;
                $water_parameter_details->ammonia_nh3=$nh3;
                $water_parameter_details->nitrate_no3=$no3;
                $water_parameter_details->ammonium_nh4=$nh4;
                $water_parameter_details->salinity=$salinity;
                $water_parameter_details->transparency=$transparency;
                $water_parameter_details->plankton_type=$plankton_type;
                $water_parameter_details->green=$green;
                $water_parameter_details->yellow=$yellow;
                $water_parameter_details->vibrio_count=$vibrio_count;
                $water_parameter_details->save();
            }
            //Adding Dissolved Oxygen
            if (!is_null($WfDuringEnclosure->id) && !is_null($dissolved_oxygen)){
                if(!is_null($dissolved_oxygen['do_qty'])){
                    $dissolved_oxygen_details=new DissolvedOxygenDetails();
                    $dissolved_oxygen_details->si_pond_management_id=0;
                    $dissolved_oxygen_details->ct_pond_management_id=0;
                    $dissolved_oxygen_details->wf_during_enclosure_id=$WfDuringEnclosure->id;
                    $dissolved_oxygen_details->quantity=$dissolved_oxygen['do_qty'];
                    if($dissolved_oxygen['time_code']==0){
                        $dissolved_oxygen_details->timing='am';
                    }else{
                        $dissolved_oxygen_details->timing='pm';
                    }
                    $dissolved_oxygen_details->save();
                }
            }
            //Adding PH
            if (!is_null($WfDuringEnclosure->id) && !is_null($ph)){
                if(!is_null($ph['ph_qty'])){
                    $ph_details=new PHDetails();
                    $ph_details->si_pond_management_id=0;
                    $ph_details->ct_pond_management_id=0;
                    $ph_details->wf_during_enclosure_id=$WfDuringEnclosure->id;
                    $ph_details->quantity=$ph['ph_qty'];
                    if($ph['time_code']==0){
                        $ph_details->timing='am';
                    }else{
                        $ph_details->timing='pm';
                    }
                    $ph_details->save();
                }
            }
            //Adding Temperature
            if (!is_null($WfDuringEnclosure->id) && !is_null($temperature)){
                if(!is_null($temperature['temperature_qty'])){
                    $temperature_details=new TemperatureDetails();
                    $temperature_details->si_pond_management_id=0;
                    $temperature_details->ct_pond_management_id=0;
                    $temperature_details->wf_during_enclosure_id=$WfDuringEnclosure->id;
                    $temperature_details->quantity=$temperature['temperature_qty'];
                    if($temperature['time_code']==0){
                        $temperature_details->timing='am';
                    }else{
                        $temperature_details->timing='pm';
                    }
                    $temperature_details->save();
                }
            }
            return true;
        }
        return false;
    }

    public function getFeed($user){
        // $feed = InventoryManagement::join('inventories','inventories.id','inventory_management.inventory_id')->where('inventory_management.org_id',$user->org_id)->where('inventory_management.remaining_quantity','>',0)->select('inventories.id','inventories.name','inventory_management.id as inventory_mgt_id')->where('inventories.type_id',1)->where('inventories.status',1)->groupBy('inventory_management.inventory_id')->get();
        $feed = InventoryManagement::join('inventories','inventories.id','inventory_management.inventory_id')->where('inventory_management.org_id',$user->org_id)->where('inventory_management.remaining_quantity','>',0)->select('inventories.id','inventories.name','inventory_management.id as inventory_mgt_id','inventory_management.remaining_quantity as quantity')->where('inventories.type_id',1)->where('inventories.status',1)->groupBy('inventory_management.inventory_id')->get();
        $minerals = InventoryManagement::join('inventories','inventories.id','inventory_management.inventory_id')->where('inventory_management.org_id',$user->org_id)->where('inventory_management.remaining_quantity','>',0)->select('inventories.id','inventories.name','inventory_management.id as inventory_mgt_id','inventory_management.remaining_quantity as quantity')->where('inventories.type_id',3)->where('inventories.status',1)->groupBy('inventory_management.inventory_id')->get();
        return response()->json([
            'feed'=>$feed,
            'minerals'=>$minerals,
            'status' => 'Data found',
            'code'=>200
        ]);
    }
    public function addFeed($user,$farm_id,$block_id,$pond_id,$pond_name,$timing,$remaining,$commercial_inputs)
    {
        if(!is_null($user) && !is_null($commercial_inputs)){
            foreach($commercial_inputs as $value){
                $inventoryManagement = InventoryManagement::where('id',$value['inventory_mgt_id'])->first();

                if(!is_null($inventoryManagement)){
                    $wfDuringEnclosure = new WfDuringEnclosure();
                    $wfDuringEnclosure->org_id = $user->org_id;
                    $wfDuringEnclosure->farm_id = $farm_id;
                    $wfDuringEnclosure->block_id = $block_id;
                    $wfDuringEnclosure->pond_id = $pond_id;
                    $wfDuringEnclosure->inventory_management_id = $value['inventory_mgt_id'];
                    $wfDuringEnclosure->save();
                }else{
                    return false;
                }

                if(!is_null($inventoryManagement) && $wfDuringEnclosure->id > 0){
                    $inventoryUsedInWaterDetails = new InventoryUsedInWaterDetails();
                    $inventoryUsedInWaterDetails->wf_during_enclosure_id = $wfDuringEnclosure->id;
                    $inventoryUsedInWaterDetails->wf_during_enclosure_id = $wfDuringEnclosure->id;
                    $inventoryUsedInWaterDetails->wf_during_enclosure_id = $wfDuringEnclosure->id;
                    $inventoryUsedInWaterDetails->inventory_id = $value['inventory_id'];
                    $inventoryUsedInWaterDetails->quantity = $value['qty'];
                    // $inventoryUsedInWaterDetails->remaining_quantity = $remaining['qty'];
                    $inventoryUsedInWaterDetails->price = $inventoryManagement->price;
                    // $inventoryUsedInWaterDetails->total_price = ($inventoryManagement->price * ($inventoryUsedInWaterDetails->quantity - $inventoryUsedInWaterDetails->remaining_quantity));
                    $inventoryUsedInWaterDetails->total_price = $inventoryManagement->price * $inventoryUsedInWaterDetails->quantity;
                    $inventoryUsedInWaterDetails->timing = $timing;
                    $inventoryUsedInWaterDetails->inventory_management_id = $value['inventory_mgt_id'];

                    $inventoryUsedInWaterDetails->save();
                }else{
                    return false;
                }

                $WfNotification = new WfNotification();

                $WfNotification->org_id = $user->org_id;
                $WfNotification->block_id = $block_id; 
                $WfNotification->pond_id = $pond_id; 
                $WfNotification->notificationType = '1'; 
                $WfNotification->notification = 1; 
                $WfNotification->inventory_used_in_water_details_id = $inventoryUsedInWaterDetails->id; 
                $WfNotification->harvest_id = 0; 
                $WfNotification->created_by = $user->id; 

                $WfNotification->save();

                /*if(!is_null($inventoryManagement) && $inventoryUsedInWaterDetails->id > 0){
                    $relInventoryDetails = new RelInventoryDetails();
                    $relInventoryDetails->inventory_details_id = $inventoryUsedInWaterDetails->id;
                    $relInventoryDetails->inventory_management_id = $wfDuringEnclosure->inventory_management_id;

                    $relInventoryDetails->save();
                }else{
                    return false;
                }*/
            }

            return true;
        }else{
            return false;
        }
    }

    public function updateStockInventory($user,$itemList){
        if(!is_null($user)){
            if(count($itemList)>0){
                foreach($itemList as $value){
                    if(!is_null($value['item_id'])){
                        $data = InventoryManagement::find($value['inventory_mgt_id']);

                        if($data->remaining_quantity >= $value['item_quantity']){
                            $newAddQty = 0;
                        }else{
                            $newAddQty = $value['item_quantity'] - $data->remaining_quantity;
                        }

                        $total_price = $data->quantity * $data->price;
                        $quantity = $data->quantity + $newAddQty;
                        $remaining_quantity = $value['item_quantity'];

                        $inventoryManagementUpdate = Db::table('inventory_management')->where('id',$value['inventory_mgt_id'])->update(['total_price'=>$total_price,'quantity'=>$quantity,'remaining_quantity'=>$remaining_quantity]);
                    }else{
                        $inventories = new Inventory();
                        $inventories->name = $value['item_name'];
                        $inventories->type_id = $value['item_type_id'];
                        $inventories->brand_id = $value['item_brand_id'];
                        $inventories->status = 1;

                        $inventories->save();

                        if($inventories->id > 0){
                            $inventoryManagementEntry = new InventoryManagement();
                            $inventoryManagementEntry->inventory_id = $inventories->id;
                            $inventoryManagementEntry->client_id = $user->id;
                            $inventoryManagementEntry->org_id = $user->org_id;
                            $inventoryManagementEntry->quantity = $value['item_quantity'];
                            $inventoryManagementEntry->remaining_quantity = $value['item_quantity'];
                            $inventoryManagementEntry->price = $value['item_price'];
                            $inventoryManagementEntry->total_price = $value['item_price'] * $value['item_quantity'];

                            $inventoryManagementEntry->save();
                        }
                    }
                }

                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    public function stockInventoryList($user){
        $items = InventoryManagement::join('inventories','inventories.id','inventory_management.inventory_id')->where('inventory_management.org_id',$user->org_id)->where('inventory_management.remaining_quantity','>',0)->select('inventories.id as item_id','inventories.name as item_name','inventory_management.id as inventory_mgt_id','inventory_management.price as item_price','inventory_management.remaining_quantity as item_quantity','inventory_management.item_avg_consumption')->where('inventories.type_id',1)->where('inventories.status',1)->groupBy('inventory_management.inventory_id')->get();

        $type = InventoryType::select('id','name')->get();
        $brand = InventoryBrand::select('id','name')->get();
        
        return response()->json([
            'items'=>$items,
            'type'=>$type,
            'brand'=>$brand,
            'status' => 'Data found',
            'code'=>200
        ]);
    }
    public function pondWiseFishList($user){
        $result = Pond::with('relPondWiseFish')->where('org_id',$user->org_id)->get();
        $getData = array();
        $sl = 0;
        foreach($result as $value){
            $getData[$sl]['pond_id'] = $value->id;
            $getData[$sl]['farm_id'] = $value->farm_id;
            $getData[$sl]['block_id'] = $value->block_id;
            $getData[$sl]['pond_number'] = $value->pond_id;
            $getData[$sl]['pond_name'] = $value->name;

            if(!is_null($value->relPondWiseFish)){
                $relPondWiseFishSl = 0;
                foreach($value->relPondWiseFish as $relPondWiseFishValue){
                    $getData[$sl]['fish_type'][$relPondWiseFishSl]['id'] = $relPondWiseFishValue->id;
                    $getData[$sl]['fish_type'][$relPondWiseFishSl]['name'] = $relPondWiseFishValue->name;

                    $relPondWiseFishSl++;
                }
            }

            $sl++;
        }

        return $getData;
    }

    public function harvest($user,$farm_id,$block_id,$pond_id,$full_harvest,$comment,$harvest){
        $pond = Pond::where('id',$pond_id)->first();
        if(!is_null($user) && !is_null($pond)){
            foreach($harvest as $value){
                $wfHarvest = new WfHarvest();
                $wfHarvest->org_id = $user->org_id;
                $wfHarvest->user_id = $user->id; 
                $wfHarvest->farm_id = $farm_id; 
                $wfHarvest->block_id = $block_id; 
                $wfHarvest->pond_id = $pond_id; 
                $wfHarvest->harvest_type = $full_harvest; 


                $wfHarvest->days_of_culture = '0'; 
                $wfHarvest->crop_number = '0'; 
                $wfHarvest->total_feed = '0'; 
                $wfHarvest->total_biomass = '0'; 

                $wfHarvest->total_quantity = $value['total_qty'];  
                $wfHarvest->unit_price = $value['unit_price'];  
                $wfHarvest->total_price = $wfHarvest->total_quantity * $wfHarvest->unit_price; 
                $wfHarvest->comments = $comment; 

                $wfHarvest->fish_id = $value['fish_type']['id']; 
                $wfHarvest->grade_type = $value['grade_type']; 
                $wfHarvest->unit = $value['unit']; 

                $wfHarvest->save();

                if(!is_null($wfHarvest)){
                    $harvest_details = new HarvestDetails();
                    $harvest_details->si_harvest_id = 0;
                    $harvest_details->ct_harvest_id = 0;
                    $harvest_details->wf_harvest_id = $wfHarvest->id;
                    $harvest_details->quantity = $value['total_qty'];  
                    $harvest_details->unit = $value['unit']; 
                    $harvest_details->unit_price = $value['unit_price'];  
                    $harvest_details->total_price = $wfHarvest->total_price;  

                    $harvest_details->save();
                }

                $WfNotification = new WfNotification();

                $WfNotification->org_id = $user->org_id;
                $WfNotification->block_id = $block_id; 
                $WfNotification->pond_id = $pond_id; 
                $WfNotification->notificationType = '2'; 
                $WfNotification->notification = 1; 
                $WfNotification->inventory_used_in_water_details_id = 0; 
                $WfNotification->harvest_id = $wfHarvest->id; 
                $WfNotification->created_by = $user->id; 

                $WfNotification->save();
            }

            return true;
        }

        return false;
    }
}
