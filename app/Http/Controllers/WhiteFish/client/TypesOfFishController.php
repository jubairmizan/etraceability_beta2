<?php

namespace App\Http\Controllers\WhiteFish\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WhiteFish\TypesOfFish;
use Illuminate\Support\Facades\Session;
use Auth;

class TypesOfFishController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $org_id = Auth::user()->org_id;

        $data['title'] = "Types Of Fish";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'typesOfFish';

        $data['getData'] = TypesOfFish::where('org_id',$org_id)->get();
        return view('WhiteFish.client.typesOfFish.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Entry Types Of Fish";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'typesOfFish';

        return view('WhiteFish.client.typesOfFish.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $typesOfFish = new TypesOfFish();
        if(Auth::user()->type_id == 2){
            $typesOfFish->org_id = Auth::user()->org_id;
        }
        $typesOfFish->name = $request->name;
        $typesOfFish->save();

        Session::flash('message','Successfully Created');
        return redirect()->route('WhiteFish.client.typesOfFish');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
