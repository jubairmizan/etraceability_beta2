<?php
namespace App\Http\Controllers\WhiteFish\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Inventory;

class AjaxData extends Controller
{
    public function getInventory(Request $request){
    	$inventory = Inventory::where('type_id',$request->id)->pluck('name','id');

    	$inventoryList = '<option value="">Choose Inventory</option>';
    	foreach($inventory as $key=>$value){
    		$inventoryList .= '<option value="'.$key.'">'.$value.'</option>';
    	}

    	echo $inventoryList;
    }
}
