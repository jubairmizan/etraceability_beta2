<?php

namespace App\Http\Controllers\WhiteFish\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WhiteFish\Block;
use App\WhiteFish\Pond;
use App\WhiteFish\WfStaff;
use App\WhiteFish\PonaStock;
use App\WhiteFish\PonaSampling;
use App\WhiteFish\TypesOfFish;
use App\WhiteFish\RelPondWiseFish;
use App\WaterParameterDetails;
use App\WhiteFish\CultivationPeriod;
use App\InventoryUsedInWaterDetails;
use App\WhiteFish\WfHarvest;
use Illuminate\Support\Facades\Session;
use DB;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index(Request $request){
        $org_id = auth()->user()->org_id;
        $data['title'] = "Dashboard";
        $data['menu'] = 'dashboard';

        $data['block'] = Block::where('org_id',$org_id)->pluck('name','id');
        $data['pond'] = Pond::where('org_id',$org_id)->pluck('pond_id','id');
        $data['typesOfFish'] = TypesOfFish::where('org_id',$org_id)->pluck('name','id');
        $data['cultivationPeriod'] = CultivationPeriod::where('org_id',$org_id)->pluck('name','id');

        //Dashboard Data
        $data['investTotalAmount'] = InventoryUsedInWaterDetails::
                                        join('wf_during_enclosure','wf_during_enclosure.id','=','inventory_used_in_water_details.wf_during_enclosure_id')
                                        ->select(DB::raw('SUM(inventory_used_in_water_details.total_price) as totalPrice'))
                                        ->where(function($query) use ($org_id,$request)
                                            {
                                                $query->where('wf_during_enclosure.org_id',$org_id);
                                                if(!is_null($request)){
                                                    if($request->block_id <>'')
                                                        $query->where('wf_during_enclosure.block_id',$request->block_id);
                                                    if($request->pond_id <>'')
                                                        $query->where('wf_during_enclosure.pond_id',$request->pond_id);
                                                }
                                            })
                                        ->first();

        $data['harvestTotalAmount'] = WfHarvest::
                                        select(DB::raw('SUM(wf_harvests.total_price) as totalPrice'))
                                        ->where(function($query) use ($org_id,$request)
                                            {
                                                $query->where('org_id',$org_id);
                                                if(!is_null($request)){
                                                    if($request->block_id <>'')
                                                        $query->where('block_id',$request->block_id);
                                                    if($request->pond_id <>'')
                                                        $query->where('pond_id',$request->pond_id);
                                                }
                                            })
                                        ->first();

        $data['harvestFishWise'] = WfHarvest::
                                        join('wf_type_of_fishes','wf_type_of_fishes.id','=','wf_harvests.fish_id')
                                        ->select('wf_type_of_fishes.name',
                                                    DB::raw('SUM(wf_harvests.total_price) as totalPrice'))
                                        ->where(function($query) use ($org_id,$request)
                                            {
                                                $query->where('wf_harvests.org_id',$org_id);

                                                if(!is_null($request)){
                                                    if($request->block_id <>'')
                                                        $query->where('wf_harvests.block_id',$request->block_id);
                                                    if($request->pond_id <>'')
                                                        $query->where('wf_harvests.pond_id',$request->pond_id);
                                                }
                                            })
                                        ->orderByDesc('totalPrice')
                                        ->groupBy('wf_type_of_fishes.name')
                                        ->get();
        $data['investInventoryTypeList'] = InventoryUsedInWaterDetails::
                                        join('wf_during_enclosure','wf_during_enclosure.id','=','inventory_used_in_water_details.wf_during_enclosure_id')
                                        ->join('inventories','inventories.id','inventory_used_in_water_details.inventory_id')
                                        ->join('inventory_types','inventory_types.id','inventories.type_id')
                                        ->select('inventory_types.name',
                                                    DB::raw('SUM(inventory_used_in_water_details.total_price) as inventoriesTotalPrice'))
                                        ->where(function($query) use ($org_id,$request)
                                            {
                                                $query->where('wf_during_enclosure.org_id',$org_id);

                                                if(!is_null($request)){
                                                    if($request->block_id <>'')
                                                        $query->where('wf_during_enclosure.block_id',$request->block_id);
                                                    if($request->pond_id <>'')
                                                        $query->where('wf_during_enclosure.pond_id',$request->pond_id);
                                                }
                                            })
                                        ->orderByDesc('inventoriesTotalPrice')
                                        ->groupBy('inventories.type_id')
                                        ->get();


        $data['calenderData'] = InventoryUsedInWaterDetails::
                                        join('wf_during_enclosure','wf_during_enclosure.id','=','inventory_used_in_water_details.wf_during_enclosure_id')
                                        ->select(DB::raw('SUM(inventory_used_in_water_details.total_price) as totalPrice'),'inventory_used_in_water_details.created_at')
                                        ->where(function($query) use ($org_id,$request)
                                            {
                                                $query->where('wf_during_enclosure.org_id',$org_id);
                                                
                                                if(!is_null($request)){
                                                    if($request->block_id <>'')
                                                        $query->where('wf_during_enclosure.block_id',$request->block_id);
                                                    if($request->pond_id <>'')
                                                        $query->where('wf_during_enclosure.pond_id',$request->pond_id);
                                                }
                                            })
                                        ->orderBy('inventory_used_in_water_details.created_at','ASC')
                                        ->groupBy(DB::raw('Date(inventory_used_in_water_details.created_at)'))
                                        ->get();

        $data['harvestCalenderData'] = WfHarvest::
                                        select(DB::raw('SUM(total_price) as totalPrice'),'created_at')
                                        ->where(function($query) use ($org_id,$request)
                                            {
                                                $query->where('org_id',$org_id);
                                                
                                                if(!is_null($request)){
                                                    if($request->block_id <>'')
                                                        $query->where('block_id',$request->block_id);
                                                    if($request->pond_id <>'')
                                                        $query->where('pond_id',$request->pond_id);
                                                }
                                            })
                                        ->orderBy('created_at','ASC')
                                        ->groupBy(DB::raw('Date(created_at)'))
                                        ->get();


        $data['investPondWise'] = InventoryUsedInWaterDetails::
                                        join('wf_during_enclosure','wf_during_enclosure.id','inventory_used_in_water_details.wf_during_enclosure_id')
                                        ->join('wf_ponds','wf_ponds.id','wf_during_enclosure.pond_id')
                                        ->select('wf_ponds.name',DB::raw('SUM(inventory_used_in_water_details.total_price) as pondTotalInvest'))
                                        ->where(function($query) use ($org_id,$request)
                                            {
                                                $query->where('wf_during_enclosure.org_id',$org_id);
                                                
                                                if(!is_null($request)){
                                                    if($request->block_id <>'')
                                                        $query->where('wf_during_enclosure.block_id',$request->block_id);
                                                    if($request->pond_id <>'')
                                                        $query->where('wf_during_enclosure.pond_id',$request->pond_id);
                                                }
                                            })
                                        ->groupBy('wf_during_enclosure.pond_id')
                                        ->get();

        $data['harvestPondWise'] = WfHarvest::
                                        join('wf_ponds','wf_ponds.id','wf_harvests.pond_id')
                                        ->select('wf_ponds.name',DB::raw('SUM(wf_harvests.total_price) as pondTotalInvest'))
                                        ->where(function($query) use ($org_id,$request)
                                            {
                                                $query->where('wf_harvests.org_id',$org_id);
                                                
                                                if(!is_null($request)){
                                                    if($request->block_id <>'')
                                                        $query->where('wf_harvests.block_id',$request->block_id);
                                                    if($request->pond_id <>'')
                                                        $query->where('wf_harvests.pond_id',$request->pond_id);
                                                }
                                            })
                                        ->groupBy('wf_harvests.pond_id')
                                        ->get();

        if(!is_null($request)){
            if($request->block_id <>'')
                $data['block_id'] = $request->block_id;
            if($request->pond_id <>''){
                $data['pond_details'] = Pond::where('id',$request->pond_id)->first();
                $data['pond_staff'] = WfStaff::where(['pond_id'=>$request->pond_id,'org_id'=>$org_id])->orderBy('id','DESC')->get();
                $data['pond_stock'] = PonaStock::where(['pond_id'=>$request->pond_id,'org_id'=>$org_id])->orderBy('id','DESC')->get();
                $data['pond_pona_sampling'] = PonaSampling::where(['pond_id'=>$request->pond_id,'org_id'=>$org_id])->orderBy('id','DESC')->get();
                $data['pond_monitoring_parameter'] = WaterParameterDetails::join('wf_during_enclosure','water_parameter_details.wf_during_enclosure_id','=','wf_during_enclosure.id')->where('water_parameter_details.wf_during_enclosure_id','!=',0)->where('wf_during_enclosure.pond_id',$request->pond_id)->get();

                $data['pond_id'] = $request->pond_id;
            }
        }
         
        return view('WhiteFish.client.dashboard3',$data);
    }

    /*Dashboard Search API*/
    public function search_pond_by_block(Request $request){
        $org_id = auth()->user()->org_id;
        $pond = Pond::where(['org_id'=>$org_id,'block_id'=>$request->block_id])->get();

        $pondList = '<option value="">Choose Pond</option>';
        
        if(count($pond)>0){
            foreach($pond as $value){
                $pondList .= '<option value="'.$value->id.'">'.$value->pond_id.'</option>';
            }
        }

        return $pondList;
    }

    public function search_fish_by_pond(Request $request){
        $org_id = auth()->user()->org_id;
        $fish = RelPondWiseFish::with('fish')->where(['org_id'=>$org_id,'pond_id'=>$request->pond_id])->get();

        $fishList = '<option value="">Choose Fish</option>';

        if(count($fish)>0){
            foreach($fish as $value){
                $fishList .= '<option value="'.$value->fish->id.'">'.$value->fish->name.'</option>';
            }
        }

        return $fishList;
    }
}
