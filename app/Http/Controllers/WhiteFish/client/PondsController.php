<?php

namespace App\Http\Controllers\WhiteFish\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WhiteFish\Pond;
use App\WhiteFish\Farm;
use App\WhiteFish\PondSupervisor;
use App\WhiteFish\CultivationProcess;
use App\WhiteFish\TypesOfFish;
use App\WhiteFish\RelPondWiseFish;
use App\WhiteFish\CultivationPeriod;
use App\WhiteFish\FarmManager;
use App\WhiteFish\Block;
use App\WhiteFish\WfStaff;
use App\WhiteFish\PonaStock;
use App\WhiteFish\PonaSampling;
use App\FarmingType;
use App\WaterParameterDetails;
use Illuminate\Support\Facades\Session;
use App\User;
use Auth;

class PondsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $org_id = auth()->user()->org_id;

        $data['title'] = "Ponds";
        $data['menu'] = 'ponds';
        $data['submenu'] = 'view';

        $data['getData'] = Pond::with('block','fisheriesOfficer','pondSuperviser','farmingType','culvationProcess','cultivationPeriod','relPondWiseFish','no_off_staff')->where('org_id',$org_id)->orderBy('id','DESC')->get();
        // dd($data);

        return view('WhiteFish.client.pond.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $org_id = auth()->user()->org_id;

        $data['title'] = "Entry Ponds";
        $data['menu'] = 'ponds';
        $data['submenu'] = 'create';
        
        $data['farms'] = Farm::where('org_id',$org_id)->pluck('name','id');
        $data['block'] = Block::where('org_id',$org_id)->pluck('name','id');
        $data['farming_type'] = FarmingType::where('id',3)->pluck('name','id');
        $data['fishes'] = TypesOfFish::where('org_id',$org_id)->get();
        $data['culvation_process'] = CultivationProcess::where('org_id',$org_id)->pluck('name','id');
        $data['cultivation_period'] = CultivationPeriod::where('org_id',$org_id)->pluck('name','id');
        $data['fisheries_officer'] = User::where(['org_id'=>$org_id,'type_id'=>'6'])->pluck('name','id');
        $data['pond_superviser'] = User::where(['org_id'=>$org_id,'type_id'=>'10'])->pluck('name','id');

        return view('WhiteFish.client.pond.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pond = Pond::orderBy('id','DESC')->first();
        if(count($pond)>0){
            $pondId = $pond->pond_id + 1;
        }else{
            $pondId = '1';
        }

        $request->validate([
            'farm_id' => 'required',
            'block_id' => 'required',
            'farming_type_id' => 'required',
            'culvation_process_id' => 'required',
            'cultivation_period_id' => 'required',
            'pond_number' => ['required','unique:wf_ponds']
        ]);

        $pond = new Pond();
        $pond->org_id = Auth::user()->org_id;
        $pond->farm_id = $request->farm_id;
        $pond->pond_id = $pondId;
        $pond->block_id = $request->block_id;
        $pond->fisheries_officer_id = $request->fisheries_officer_id;
        $pond->pond_superviser_id = $request->pond_superviser_id;
        $pond->farming_type_id = $request->farming_type_id;
        $pond->culvation_process_id = $request->culvation_process_id;
        $pond->cultivation_period_id = $request->cultivation_period_id;
        $pond->name = $request->name;
        $pond->pond_number = $request->pond_number;
        $pond->area = $request->area;
        $pond->flood_free = $request->flood_free;
        $pond->establishment_date = $request->establishment_date;
        // dd($pond);
        $pond->save();

        if(count(array_filter($request->cultivation_fish))>0){
            foreach($request->cultivation_fish as $value){   
                $cultivation_fish = new RelPondWiseFish(); 
                $cultivation_fish->org_id = Auth::user()->org_id;
                $cultivation_fish->farm_id = $request->farm_id;
                $cultivation_fish->pond_id = $pond->id;
                $cultivation_fish->fish_id = $value;
                $cultivation_fish->save();
            }
        }

        Session::flash('message','Successfully Created');
        return redirect()->route('WhiteFish.client.ponds');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function staffList($pondId){
        $org_id = auth()->user()->org_id;

        $data['title'] = "Staff Work Pond";
        $data['menu'] = 'ponds';
        $data['submenu'] = 'view';

        $data['getData'] = WfStaff::with('block','pond')->where(['pond_id'=>$pondId,'org_id'=>$org_id])->orderBy('id','DESC')->get();
        return view('WhiteFish.client.pond.staffList',$data);
    }

    public function ponaStockList($pondId){
        $org_id = auth()->user()->org_id;

        $data['title'] = "Pona Stock Pond";
        $data['menu'] = 'ponds';
        $data['submenu'] = 'view';

        $data['getData'] = PonaStock::with('block','pond','fish')->where(['pond_id'=>$pondId,'org_id'=>$org_id])->orderBy('id','DESC')->get();
        return view('WhiteFish.client.pond.ponaStockList',$data);
    }

    public function waterParameterDetails($pondId){
        $org_id = auth()->user()->org_id;

        $data['title'] = "Water Parameter Details Pond Wise";
        $data['menu'] = 'ponds';
        $data['submenu'] = 'view';

        $data['getData'] = WaterParameterDetails::join('wf_during_enclosure','water_parameter_details.wf_during_enclosure_id','=','wf_during_enclosure.id')->where('water_parameter_details.wf_during_enclosure_id','!=',0)->where('wf_during_enclosure.pond_id',$pondId)->get();
        return view('WhiteFish.client.pond.waterParameterDetails',$data);
    }

    public function ponaSampling($pondId){
        $org_id = auth()->user()->org_id;

        $data['title'] = "Pona Sampling List";
        $data['menu'] = 'ponds';
        $data['submenu'] = 'view';

        $data['getData'] = PonaSampling::with('block','pond','fish')->where(['pond_id'=>$pondId,'org_id'=>$org_id])->orderBy('id','DESC')->get();
        // dd($data);
        return view('WhiteFish.client.pond.ponaSampling',$data);
    }
}
