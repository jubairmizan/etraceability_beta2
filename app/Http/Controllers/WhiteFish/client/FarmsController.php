<?php

namespace App\Http\Controllers\WhiteFish\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WhiteFish\Farm;
use Illuminate\Support\Facades\Session;
use Auth;

class FarmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $org_id = auth()->user()->org_id;

        $data['title'] = "Farms";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'farms';

        $data['getData'] = Farm::where('org_id',$org_id)->get();
        return view('WhiteFish.client.farms.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Entry Farms";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'farms';

        return view('WhiteFish.client.farms.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $farm = new Farm();
        $farm->org_id = Auth::user()->org_id;
        $farm->name = $request->name;
        $farm->area = $request->area;
        $farm->status = 1;
        $farm->save();

        Session::flash('message','Successfully Created');
        return redirect()->route('WhiteFish.client.farms');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
