<?php

namespace App\Http\Controllers\WhiteFish\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\InventoryType;
use App\InventoryBrand;
use App\InventoryManagement;
use App\WhiteFish\TypesOfFish;
use App\WhiteFish\RelPondWiseFish;
use App\WhiteFish\CultivationPeriod;
use App\InventoryUsedInWaterDetails;
use App\WhiteFish\PonaStock;
use App\WhiteFish\Pond;
use App\WhiteFish\Block;
use App\WhiteFish\WfHarvest;
use App\Inventory;
use Illuminate\Support\Facades\Session;
use Auth;
use DB;

class InventoriesController extends Controller
{
    public function inventoryTypes(){   
        $data['title'] = "Inventory Types";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'inventory_types';

        $data['getData'] = InventoryType::all();
        return view('WhiteFish.client.inventoryType.index',$data);
    }

    /*Inventory Brands*/
    public function inventoryBrand(){      
        $data['title'] = "Inventory Types";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'inventory_brands';

        $data['getData'] = InventoryBrand::all();
        return view('WhiteFish.client.inventoryBrand.index',$data);
    }
    public function inventoryBrandCreate(){    
        $data['title'] = "Inventory Types";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'inventory_brands';

        return view('WhiteFish.client.inventoryBrand.form',$data);
    }
    public function inventoryBrandStore(Request $request){
    	$request->validate([
            'name' => 'required'
        ]);

        $block = new InventoryBrand();
        $block->name = $request->name;
        $block->save();

        Session::flash('message','Successfully Created');
        return redirect()->route('WhiteFish.client.inventory_brands');
    }

    /*inventories*/
    public function inventories(){   
        $data['title'] = "Inventory Lists";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'inventories';

        $data['getData'] = Inventory::with('rel_inventory_type','rel_inventory_brand')->get();
        return view('WhiteFish.client.inventory.index',$data);
    }
    public function inventoriesCreate(){    
        $data['title'] = "Inventory Create";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'inventories';

        $data['type'] = InventoryType::pluck('name','id');
        $data['brand'] = InventoryBrand::pluck('name','id');

        return view('WhiteFish.client.inventory.form',$data);
    }
    public function inventoriesStore(Request $request){
    	$request->validate([
            'name' => 'required',
            'type_id' => 'required',
            'brand_id' => 'required',
        ]);

        $block = new Inventory();
        $block->name = $request->name;
        $block->type_id = $request->type_id;
        $block->brand_id = $request->brand_id;
        $block->code = $request->code;
        $block->status = $request->status;
        $block->save();

        Session::flash('message','Successfully Created');
        return redirect()->route('WhiteFish.client.inventories');
    }

    /*Inventory Management*/
    public function inventoryManagement(){  
        $data['title'] = "Inventory Management Lists";
        $data['menu'] = 'inventory';
        $data['submenu'] = 'inventory_management';

        // $data['getData'] = InventoryManagement::with('inventoryType')->where('org_id',auth::user()->org_id)->orderByDesc('id')->where('id',28)->get();
        $data['getData'] = InventoryManagement::join('inventories','inventories.id','inventory_management.inventory_id')->join('inventory_types','inventory_types.id','inventories.type_id')->where('inventory_management.org_id',auth::user()->org_id)->select('inventory_management.*','inventory_types.name as typeName','inventories.name as inventoryName')->orderBy('inventory_management.id','DESC')->get();
        // dd($data);
        return view('WhiteFish.client.inventoryManagement.index',$data);
    }
    public function inventoryManagementCreate(){    
        $data['title'] = "Inventory Create";
        $data['menu'] = 'inventory';
        $data['submenu'] = 'inventories';

        $data['inventoryType'] = InventoryType::pluck('name','id');
        return view('WhiteFish.client.inventoryManagement.form',$data);
    }
    public function inventoryManagementStore(Request $request){
    	$request->validate([
            'inventory_id' => 'required',
            'quantity' => 'required',
            'price' => 'required',
        ]);

        $inventoryManagement = new InventoryManagement();
        $inventoryManagement->inventory_id = $request->inventory_id;
        $inventoryManagement->org_id = auth::user()->org_id;
        $inventoryManagement->client_id = auth::user()->id;
        $inventoryManagement->si_farm_id = 0;
        $inventoryManagement->cluster_id = 0;
        $inventoryManagement->quantity = $request->quantity;
        $inventoryManagement->remaining_quantity = $request->quantity;
        $inventoryManagement->price = $request->price;
        $inventoryManagement->total_price = $request->price * $inventoryManagement->quantity;
        $inventoryManagement->item_avg_consumption = '';
        $inventoryManagement->save();

        Session::flash('message','Successfully Created');
        return redirect()->route('WhiteFish.client.inventory_management');
    }

    /*Inventories Out*/
    public function inventoryStockOut(){  
        $data['title'] = "Inventory Management Lists";
        $data['menu'] = 'inventory';
        $data['submenu'] = 'inventory_management';

        $data['getData'] = InventoryUsedInWaterDetails::where('wf_during_enclosure_id','!=',0)->get();
        return view('WhiteFish.client.inventoryStockOut.index',$data);
    }
    public function inventoryStockOutCreate(){    
        $data['title'] = "Inventory Create";
        $data['menu'] = 'inventory';
        $data['submenu'] = 'inventories';

        $data['inventory'] = Inventory::pluck('name','id');
        return view('WhiteFish.client.inventoryStockOut.form',$data);
    }
    public function inventoryStockOutStore(Request $request){
        $request->validate([
            'inventory_id' => 'required',
            'quantity' => 'required',
            'price' => 'required',
        ]);

        $inventoryManagement = new InventoryManagement();
        $inventoryManagement->inventory_id = $request->inventory_id;
        $inventoryManagement->org_id = auth::user()->org_id;
        $inventoryManagement->client_id = auth::user()->id;
        $inventoryManagement->quantity = $request->quantity;
        $inventoryManagement->price = $request->price;
        $inventoryManagement->total_price = $request->price * $inventoryManagement->quantity;
        $inventoryManagement->save();

        Session::flash('message','Successfully Created');
        return redirect()->route('WhiteFish.client.inventory_management');
    }

    /*pona*/
    public function ponaLists(){  
        $org_id = auth::user()->org_id;
        $data['title'] = "Pona Stock Lists";
        $data['menu'] = 'inventory';
        $data['submenu'] = 'pona';

        $data['getData'] = PonaStock::with('farm','block','pond','fish')->where('org_id',$org_id)->get();
        $data['pond'] = Pond::where('org_id',$org_id)->pluck('pond_id','id');
        $data['block'] = Block::where('org_id',$org_id)->pluck('name','id');

        return view('WhiteFish.client.pona.index',$data);
    }
    public function ponaSearch(Request $request){
        $org_id = auth::user()->org_id;
        $data['title'] = "Pona Stock Lists";
        $data['menu'] = 'inventory';
        $data['submenu'] = 'pona';

        $data['pond'] = Pond::where('org_id',$org_id)->pluck('pond_id','id');
        $data['block'] = Block::where('org_id',$org_id)->pluck('name','id');


        $data['getData'] = PonaStock::with('farm','block','pond','fish')
                            ->where(function($query) use ($org_id,$request){
                                                                            $query->where('org_id',$org_id);
                                                                            if($request->pond_id != null)
                                                                                $query->where('pond_id',$request->pond_id);
                                                                            if($request->block_id != null)
                                                                                $query->where('block_id',$request->block_id);
                                                                            })
                            ->orderBy('id','DESC')->get();
        if($request->pond_id != null)
            $data['pond_id'] = $request->pond_id;
        if($request->block_id != null)
            $data['block_id'] = $request->block_id;

        return view('WhiteFish.client.pona.index',$data);
    }

    public function inventoryTypesSlug($inventoryTypeId){
        $inventoryType = InventoryType::find($inventoryTypeId);
        $data['title'] = ucWords(str_replace(' ','_',$inventoryType->name));
        $data['menu'] = 'inventory';
        $data['submenu'] = str_replace(' ','_',$inventoryType->name);

        $data['getData'] = DB::table('inventory_management')
                            ->join('inventories', 'inventory_management.inventory_id', '=', 'inventories.id')
                            ->join('inventory_types', 'inventories.type_id', '=', 'inventory_types.id')
                            ->join('inventory_brands', 'inventories.brand_id', '=', 'inventory_brands.id')
                            ->select('inventory_management.*','inventories.name as inventoriesName','inventories.code as inventoriesCode','inventory_types.name as inventoryTypesName','inventory_brands.name as inventoryBrandName')
                            ->where('inventories.type_id',$inventoryTypeId)
                            ->where('inventory_management.org_id',auth::user()->org_id)
                            ->get();
        // dd($data);

        return view('WhiteFish.client.menuInventoryType.index',$data);
    }
    public function inventoryStockingData($inventoryTypeId){
        $org_id = auth::user()->org_id;
        $inventoryType = InventoryType::find($inventoryTypeId); 
        $data['title'] = ucWords(str_replace(' ','_',$inventoryType->name));
        $data['menu'] = 'inventoryStockList';

        $data['pond'] = Pond::where('org_id',$org_id)->pluck('pond_id','id');
        $data['block'] = Block::where('org_id',$org_id)->pluck('name','id');
        $data['submenu'] = str_replace(' ','_',$inventoryType->name); 
        $data['inventoryTypeId'] = $inventoryTypeId;                           
        $data['getData'] = DB::table('inventory_used_in_water_details as inventory_used_details')
                            ->join('wf_during_enclosure', 'inventory_used_details.wf_during_enclosure_id', '=', 'wf_during_enclosure.id')
                            ->join('inventory_management', 'wf_during_enclosure.inventory_management_id', '=', 'inventory_management.id')
                            ->join('inventories', 'inventory_management.inventory_id', '=', 'inventories.id')
                            ->join('inventory_types', 'inventories.type_id', '=', 'inventory_types.id')
                            ->join('inventory_brands', 'inventories.brand_id', '=', 'inventory_brands.id')
                            ->select('inventory_used_details.quantity',
                                     'inventory_used_details.timing',
                                     'inventory_used_details.timing',
                                     'inventory_used_details.created_at',
                                     'inventory_management.remaining_quantity',
                                     'inventory_management.price',
                                     'inventory_management.total_price',
                                     'inventories.name as inventoryName',
                                     'inventory_types.name as inventoryTypeName',
                                     'inventory_brands.name as inventoryBrandName'
                                    )
                            ->where('inventories.type_id',$inventoryTypeId)
                            ->where('wf_during_enclosure.org_id',auth::user()->org_id)
                            ->get();
        // dd($data);

        return view('WhiteFish.client.menuInventoryStockDetails.index',$data);
    }
    public function InventoryStoreSearch(Request $request,$inventoryTypeId){
        $org_id = auth::user()->org_id;
        $inventoryType = InventoryType::find($inventoryTypeId); 
        $data['title'] = ucWords(str_replace(' ','_',$inventoryType->name));
        $data['menu'] = 'inventoryStockList';

        $data['inventoryTypeId'] = $inventoryTypeId;
        $data['pond'] = Pond::where('org_id',$org_id)->pluck('pond_id','id');
        $data['block'] = Block::where('org_id',$org_id)->pluck('name','id');
        $data['submenu'] = str_replace(' ','_',$inventoryType->name);                            
        $data['getData'] = DB::table('inventory_used_in_water_details as inventory_used_details')
                            ->join('wf_during_enclosure', 'inventory_used_details.wf_during_enclosure_id', '=', 'wf_during_enclosure.id')
                            ->join('inventory_management', 'wf_during_enclosure.inventory_management_id', '=', 'inventory_management.id')
                            ->join('inventories', 'inventory_management.inventory_id', '=', 'inventories.id')
                            ->join('inventory_types', 'inventories.type_id', '=', 'inventory_types.id')
                            ->join('inventory_brands', 'inventories.brand_id', '=', 'inventory_brands.id')
                            ->select('inventory_used_details.quantity',
                                     'inventory_used_details.timing',
                                     'inventory_used_details.timing',
                                     'inventory_used_details.created_at',
                                     'inventory_management.remaining_quantity',
                                     'inventory_management.price',
                                     'inventory_management.total_price',
                                     'inventories.name as inventoryName',
                                     'inventory_types.name as inventoryTypeName',
                                     'inventory_brands.name as inventoryBrandName'
                                    )
                            ->where('inventories.type_id',$inventoryTypeId)
                            ->where(function($query) use ($org_id,$request){
                                                                            $query->where('wf_during_enclosure.org_id',$org_id);
                                                                            if($request->pond_id != null)
                                                                                $query->where('wf_during_enclosure.pond_id',$request->pond_id);
                                                                            if($request->block_id != null)
                                                                                $query->where('wf_during_enclosure.block_id',$request->block_id);
                                                                            })
                            ->get();
        if($request->pond_id != null)
            $data['pond_id'] = $request->pond_id;
        if($request->block_id != null)
            $data['block_id'] = $request->block_id;

        return view('WhiteFish.client.menuInventoryStockDetails.index',$data);
    }

    public function investBreakdown(Request $request){
        $org_id = auth()->user()->org_id;
        $data['title'] = 'investBreakdown';
        $data['menu'] = 'inventoryStockList';
        $data['block'] = Block::where('org_id',$org_id)->pluck('name','id');
        $data['pond'] = Pond::where('org_id',$org_id)->pluck('pond_id','id');

        $data['getData'] = InventoryUsedInWaterDetails::
                                        join('wf_during_enclosure','wf_during_enclosure.id','=','inventory_used_in_water_details.wf_during_enclosure_id')
                                        ->join('wf_pond_blocks','wf_pond_blocks.id','wf_during_enclosure.block_id')
                                        ->join('wf_ponds','wf_ponds.id','wf_during_enclosure.pond_id')
                                        ->leftJoin('inventories','inventories.id','inventory_used_in_water_details.inventory_id')
                                        ->select('inventory_used_in_water_details.*','wf_pond_blocks.name as blockName','wf_ponds.name as pondName','wf_ponds.pond_id as pondNumber','inventories.name as inventoryName')
                                        ->where(function($query) use ($org_id,$request)
                                            {
                                                $query->where('wf_during_enclosure.org_id',$org_id);
                                                
                                                if(!is_null($request)){
                                                    if($request->from<>'' && $request->to<>''){
                                                        if($request->from <>''){
                                                            $query->whereDate('inventory_used_in_water_details.created_at','>=',$request->from);
                                                        }
                                                        if($request->to <>''){
                                                            $query->whereDate('inventory_used_in_water_details.created_at','<=',$request->to);
                                                        }
                                                    }
                                                    if($request->from =='' && $request->to <>''){
                                                        if($request->to <>''){
                                                            $query->whereDate('inventory_used_in_water_details.created_at','=',$request->to);
                                                        }
                                                    }
                                                    
                                                    if($request->block_id <>'')
                                                        $query->where('wf_during_enclosure.block_id',$request->block_id);
                                                    if($request->pond_id <>'')
                                                        $query->where('wf_during_enclosure.pond_id',$request->pond_id);
                                                }
                                            })
                                        ->orderBy('inventory_used_in_water_details.created_at','ASC')
                                        ->get();
        if(!is_null($request)){
            if($request->block_id <>''){
                $data['block_id'] = $request->block_id;
            }
            if($request->pond_id <>''){
                $data['pond_id'] = $request->pond_id;
            }
            if($request->from <>''){
                $data['from'] = $request->from;
            }
            if($request->to <>''){
                $data['to'] = $request->to;
            }
        }

        return view('WhiteFish.client.investBreakdown',$data);
    }
    public function harvestBreakdown(Request $request){
        $org_id = auth()->user()->org_id;
        $data['title'] = 'Harvest Breakdown';
        $data['menu'] = 'inventoryStockList';
        $data['block'] = Block::where('org_id',$org_id)->pluck('name','id');
        $data['pond'] = Pond::where('org_id',$org_id)->pluck('pond_id','id');

        $data['getData'] = WfHarvest::
                                        join('wf_pond_blocks','wf_pond_blocks.id','wf_harvests.block_id')
                                        ->join('wf_ponds','wf_ponds.id','wf_harvests.pond_id')
                                        ->join('wf_type_of_fishes','wf_type_of_fishes.id','wf_harvests.fish_id')
                                        ->select('wf_pond_blocks.name as blockName','wf_ponds.name as pondName','wf_ponds.pond_id as pondNumber','wf_type_of_fishes.name as fishName','wf_harvests.*')
                                        ->where(function($query) use ($org_id,$request)
                                            {
                                                $query->where('wf_harvests.org_id',$org_id);
                                                
                                                if(!is_null($request)){
                                                    if($request->from<>'' && $request->to<>''){
                                                        if($request->from <>''){
                                                            $query->whereDate('wf_harvests.created_at','>=',$request->from);
                                                        }
                                                        if($request->to <>''){
                                                            $query->whereDate('wf_harvests.created_at','<=',$request->to);
                                                        }
                                                    }
                                                    if($request->from =='' && $request->to <>''){
                                                        if($request->to <>''){
                                                            $query->whereDate('wf_harvests.created_at','=',$request->to);
                                                        }
                                                    }
                                                    
                                                    if($request->block_id <>'')
                                                        $query->where('wf_harvests.block_id',$request->block_id);
                                                    if($request->pond_id <>'')
                                                        $query->where('wf_harvests.pond_id',$request->pond_id);
                                                }
                                            })
                                        ->orderBy('wf_harvests.created_at','ASC')
                                        ->get();
        if(!is_null($request)){
            if($request->block_id <>''){
                $data['block_id'] = $request->block_id;
            }
            if($request->pond_id <>''){
                $data['pond_id'] = $request->pond_id;
            }
            if($request->from <>''){
                $data['from'] = $request->from;
            }
            if($request->to <>''){
                $data['to'] = $request->to;
            }
        }

        return view('WhiteFish.client.harvestBreakdown',$data);
    }

}
