<?php

namespace App\Http\Controllers\WhiteFish\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserType;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $org_id = auth()->user()->org_id;

        $data['title'] = "User";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'user';

        $data['getData'] = User::where('org_id',$org_id)->get();
        return view('WhiteFish.client.user.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Entry User Name";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'user';
        $data['userType'] = UserType::pluck('name','id');

        return view('WhiteFish.client.user.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $user = new User();
        $user->org_id = Auth::user()->org_id;
        $user->type_id = $request->type_id;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->cell_no = $request->cell_no;
        $user->gender = $request->gender;
        $user->status = 1;
        $user->save();

        return redirect()->route('WhiteFish.client.user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
