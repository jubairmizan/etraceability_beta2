<?php

namespace App\Http\Controllers\WhiteFish\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WhiteFish\Block;
use App\WhiteFish\Farm;
use Illuminate\Support\Facades\Session;
use Auth;

class BlocksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $org_id = auth()->user()->org_id;
        
        $data['title'] = "Blocks";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'block';

        $data['getData'] = Block::with('rel_farm')->where('org_id',$org_id)->get();
        return view('WhiteFish.client.block.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['title'] = "Entry Blocks";
        $data['menu'] = 'system_setting';
        $data['submenu'] = 'block';
        $data['farms'] = Farm::where('org_id',auth()->user()->org_id)->get();

        return view('WhiteFish.client.block.form',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $block = new Block();
        $block->org_id = Auth::user()->org_id;
        $block->farm_id = $request->farm_id;
        $block->name = $request->name;
        $block->save();

        Session::flash('message','Successfully Created');
        return redirect()->route('WhiteFish.client.blocks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
