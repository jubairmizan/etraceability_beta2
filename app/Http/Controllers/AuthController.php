<?php

namespace App\Http\Controllers;

use App\AnimalSampling;
use App\Chert;
use App\Feed;
use App\Hatchery;
use App\Http\Controllers\ShrimpCulture\ShrimpApiController;
use App\Medicine;
use App\SiFarm;
use App\User;
use App\Cluster;
use App\UserToken;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $user=User::active()->where('user_id',request('user_id'))->first();
        if(count($user)>0){
            if(! $token = JWTAuth::fromUser($user)) {
                return response()->json([
                    'status' => 'User is unauthorized',
                    'code'=>401
                ]);
            }else{
                $user_token=new UserToken();
                $user_token->user_id=$user->id;
                $user_token->login_token=$token;
                $user_token->save();
                return response()->json([
                    '_token'=>$token,
                    'status' => 'Login successful',
                    'code'=>200
                ]);
            }
        }
        return response()->json([
            'status' => 'User not found',
            'code'=>400
        ]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function enterToEnclosure(Request $request)
    {
        return $request->farmId;
//        return response()->json([
//            'me'=>'ss'
//        ]);
//        return response()->json(auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token, $user)
    {
        $hatchery=Hatchery::active()->select('id','name')->get();
        $medicines=Medicine::active()->select('id','name')->get();
        $feeds=Feed::active()->select('id','name')->get();
        $chert=Chert::active()->select('id','name')->get();
        if($user->type_id==6){
            $farm=SiFarm::with(['ponds'=>function($query){
                $query->with(['animal_details'=>function($query){
                    $query->addSelect('farm_id','hatchery_id','pond_id','number_of_sample','average_body_weight as avg_bw','current_biomass','survival_rate','total_weight','stocked_pl_amount as pl_amount','stocked_pl_price as pl_price')
                        ->latest()->limit(1);
                }])
                    ->addSelect('id','pond_id','name as pond_name','si_farm_id');
            }])->select('id','name as farm_name')
                ->where('farm_manager_id',$user->id)->first();
            if(count($farm)>0){
                return response()->json([
                    'farm_manager_id'=>$user->user_id,
                    'farm_manager_name'=>$user->name,
                    'farms'=>$farm,
                    'hatcheries'=>$hatchery,
                    'medicines'=>$medicines,
                    'feeds'=>$feeds,
                    'chert'=>$chert,
                    'access_token' => $token,
                    'result'=>1
                ]);
            }else{
                return response()->json([
                    'message'=>'The user is not assigned for any farm',
                    'result'=>0
                ]);
            }
        }elseif ($user->type_id==7){
            $cluster=Cluster::with(['ponds'=>function($query){
                $query->addSelect('id','pond_id','name as pond_name','cluster_id');
            }])->select('id','name as cluster_name')
                ->where('cluster_manager_id',$user->id)->first();
            if(count($cluster)>0){
                return response()->json([
                    'cluster_manager_id'=>$user->user_id,
                    'cluster_manager_name'=>$user->name,
                    'cluster'=>$cluster,
                    'hatcheries'=>$hatchery,
                    'medicines'=>$medicines,
                    'feeds'=>$feeds,
                    'chert'=>$chert,
                    'access_token' => $token,
                    'result'=>2
                ]);
            }else {
                return response()->json([
                    'message' => 'The user is not assigned for any cluster',
                    'result' => 0
                ]);
            }
        }
    }
}
