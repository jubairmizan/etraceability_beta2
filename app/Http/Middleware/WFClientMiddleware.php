<?php

namespace App\Http\Middleware;

use Closure;
use App\InventoryType;
use App\WhiteFish\WfNotification;
use Illuminate\Support\Facades\Route;

class WFClientMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && auth()->user()->type_id==2 && auth()->user()->cat_id==2){
            /*Route Lists*/
            /*$routeCollection = Route::getRoutes();

            $routeLists = array();
            foreach ($routeCollection as $value) {
                // $routeLists[] = $value->getActionName();
                // $routeLists[] = $value->getActionMethod();
                // $routeLists[] = $value->getPrefix();
                // $routeLists[] = $value->getName();
                // $routeLists[] = $value->uri;
                // $routeLists[] = $value->uri();
                // $routeLists[] = $value->middleware();
                // $routeLists[] = $value->getAction();
                
                if(count($value->middleware())>2){
                    if($value->middleware()['2']=='wf_client' && $value->getName() != 'store'){
                        $routeLists[] = $value->getName();
                    }
                }
            }*/
            /*Route Lists*/


            $inventoryType = InventoryType::all();

            $newNotification = WfNotification::where(['notification'=>1,'org_id'=>auth()->user()->org_id])->get();
            $oldNotification = WfNotification::where(['notification'=>0,'org_id'=>auth()->user()->org_id])->get();

            view()->composer('*', function ($view) use ($inventoryType,$newNotification,$oldNotification) {
                $view->with(['inventoryType'=> $inventoryType,'newNotification'=>$newNotification,'oldNotification'=>$oldNotification]);
            });
            
            return $next($request);
        }
        return redirect()->route('login');
    }
}
