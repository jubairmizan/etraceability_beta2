<?php

namespace App\Http\Middleware;

use Closure;

class Processing_plant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && auth()->user()->type_id==9){
            return $next($request);
        }
        return redirect()->route('login');
    }
}
