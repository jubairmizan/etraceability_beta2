<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(Auth::user()->cat_id==1){
                if (Auth::user()->type_id==1) {
                    return redirect()->route('admin.dashboard');
                }
                elseif (Auth::user()->type_id==2){
                    return redirect()->route('shrimp_culture.client.dashboard');
                }elseif (Auth::user()->type_id==9){
                    return redirect()->route('shrimp_culture.processing_plant.dashboard');
                }
            }else{
                if (Auth::user()->type_id==2){
                    return redirect()->route('WhiteFish.client.dashboard');
                }
            }
//            if (Auth::user()->type_id==9){
//                return redirect()->route('processing_plant.dashboard');
//            }
        }

        return $next($request);
    }
}
