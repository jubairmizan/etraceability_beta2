<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaterParameterDetails extends Model
{
    protected $table='water_parameter_details';
}
