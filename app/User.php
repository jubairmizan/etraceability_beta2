<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable,SoftDeletes;

    protected $dates=['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_id', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeActive($query){
        $query->where('status',1);
    }
    public function type(){
        return $this->belongsTo('App\UserType','type_id','id');
    }
    public function scopeAdmin($query){
        $query->where('type_id',1);
    }
    public function scopeClient($query){
        $query->where('type_id',2);
    }
    public function scopeBuyer($query){
        $query->where('type_id',3);
    }
    public function scopeFarmer($query){
        $query->where('type_id',4);
    }
    public function scopeHatcheryManager($query){
        $query->where('type_id',5);
    }
    public function scopeFarmManager($query){
        $query->where('type_id',6);
    }
    public function scopeClusterManager($query){
        $query->where('type_id',7);
    }
    public function scopeCollectionCenterManager($query){
        $query->where('type_id',8);
    }
    public function scopeProcessingPlantClient($query){
        $query->where('type_id',9);
    }
    public function division(){
        return $this->belongsTo('App\Division','division_id','id');
    }
    public function district(){
        return $this->belongsTo('App\District','district_id','id');
    }
    public function upazila(){
        return $this->belongsTo('App\Upazila','upazila_id','id');
    }
    public function union(){
        return $this->belongsTo('App\Union','union_id','id');
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function username(){
        return 'user_id';
    }
}
