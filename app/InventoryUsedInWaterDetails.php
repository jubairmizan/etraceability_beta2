<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryUsedInWaterDetails extends Model
{
    protected $table = 'inventory_used_in_water_details';

    public function wf_during(){
    	return $this->belongsTo('App\WhiteFish\WfDuringEnclosure','wf_during_enclosure_id','id');
    }
    public function inventory(){
    	return $this->belongsTo('App\Inventory','inventory_id','id');
    }
    public function inventoryManagement(){
    	return $this->belongsTo('App\inventoryManagement','inventory_management_id','id');
    }
}
