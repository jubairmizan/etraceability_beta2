<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryManagement extends Model
{
	protected $table = 'inventory_management';
    public function rel_inventory(){
    	return $this->belongsTo('App\Inventory','inventory_id','id');
    }
}
