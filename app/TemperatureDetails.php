<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemperatureDetails extends Model
{
    protected $table='temperature_details';
}
