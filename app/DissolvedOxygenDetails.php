<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DissolvedOxygenDetails extends Model
{
    protected $table='dissolved_oxygen_details';
}
