<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelInventoryDetails extends Model
{
    protected $table = 'rel_inventory_details';
}
