<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $table='divisions';
    protected $fillable=['name','code','status'];
    public function scopeActive($query){
        $query->where('status',1);
    }
}
