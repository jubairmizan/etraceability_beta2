<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryBrand extends Model
{
    protected $table='inventory_brands';
    public function scopeWithoutZero($query){
        $query->where('id','!=',0);
    }
}
