<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class CultivationPeriod extends Model
{
    protected $table = 'wf_cultivation_periods';
}
