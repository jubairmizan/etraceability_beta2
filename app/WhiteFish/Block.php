<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;
use App\WhiteFish\Farm;

class Block extends Model
{
    protected $table = 'wf_pond_blocks';

    public function rel_farm(){
    	return $this->belongsTo(Farm::class,'farm_id','id');
    }
}
