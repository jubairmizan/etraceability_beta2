<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class PonaStock extends Model
{
    protected $table = 'wf_pona_stocks';
    
    public function farm(){
    	return $this->belongsTo('App\WhiteFish\Farm','farm_id','id');
    }
    public function block(){
    	return $this->belongsTo('App\WhiteFish\Block','block_id','id');
    }
    public function pond(){
    	return $this->belongsTo('App\WhiteFish\Pond','pond_id','id');
    }
    public function fish(){
    	return $this->belongsTo('App\WhiteFish\TypesOfFish','fish_id','id');
    }
}
