<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class PondSupervisor extends Model
{
    protected $table = 'wf_rel_pond_supervisors';
}
