<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class RelPondWiseFish extends Model
{
    protected $table = 'wf_rel_pond_wise_fish';
    
    public function fish(){
    	return $this->belongsTo('App\WhiteFish\TypesOfFish','fish_id','id');
    }
}
