<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class PonaSampling extends Model
{
    protected $table = 'wf_pona_sampling';

    public function farm(){
    	return $this->belongsTo('App\WhiteFish\Farm','farm_id','id');
    }
    public function block(){
    	return $this->belongsTo('App\WhiteFish\Block','block_id','id');
    }
    public function pond(){
    	return $this->belongsTo('App\WhiteFish\Pond','pond_id','id');
    }
    public function fish(){
    	return $this->belongsTo('App\WhiteFish\TypesOfFish','fish_id','id');
    }
}
