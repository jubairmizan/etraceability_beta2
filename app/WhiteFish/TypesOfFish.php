<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class TypesOfFish extends Model
{
    protected $table='wf_type_of_fishes';
}
