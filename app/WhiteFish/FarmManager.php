<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class FarmManager extends Model
{
    protected $table = 'wf_rel_farm_manager';
}
