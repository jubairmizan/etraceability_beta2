<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class WfDuringEnclosure extends Model
{
    protected $table = 'wf_during_enclosure';

    public function pond(){
    	return $this->belongsTo('App\WhiteFish\Pond','pond_id','id');
    }
    public function farm(){
    	return $this->belongsTo('App\WhiteFish\Farm','farm_id','id');
    }
    public function block(){
    	return $this->belongsTo('App\WhiteFish\Block','block_id','id');
    }
}
