<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class Pond extends Model
{
    protected $table = 'wf_ponds';
    public function block(){
    	return $this->belongsTo('App\WhiteFish\Block','block_id','id');
    }
    public function fisheriesOfficer(){
    	return $this->belongsTo('App\User','fisheries_officer_id','id');
    }
    public function pondSuperviser(){
    	return $this->belongsTo('App\User','pond_superviser_id','id');
    }
    public function farmingType(){
    	return $this->belongsTo('App\FarmingType','farming_type_id','id');
    }
    public function culvationProcess(){
    	return $this->belongsTo('App\WhiteFish\CultivationProcess','culvation_process_id','id');
    }
    public function cultivationPeriod(){
    	return $this->belongsTo('App\WhiteFish\CultivationPeriod','cultivation_period_id','id');
    }
    public function relPondWiseFish(){
        return $this->belongsToMany('App\WhiteFish\TypesOfFish','wf_rel_pond_wise_fish','pond_id','fish_id');
    }
    public function no_off_staff(){
        return $this->hasOne('App\WhiteFish\WfStaff','id','pond_id')->latest();
    }
    public function lastPonaStockeDetails(){
        return $this->hasOne('App\WhiteFish\PonaStock','id','pond_id')->latest();
    }
    public function lastPonaSampling(){
        return $this->hasOne('App\WhiteFish\PonaSampling','id','pond_id')->latest();
    }
}
