<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class WfNotification extends Model
{
    public function block(){
    	return $this->belongsTo('App\WhiteFish\Block','block_id','id');
    }
    public function pond(){
    	return $this->belongsTo('App\WhiteFish\Pond','pond_id','id');
    }
    public function inventoryUsedInWaterDetails(){
    	return $this->belongsTo('App\InventoryUsedInWaterDetails','inventory_used_in_water_details_id','id');
    }
    public function harvestDetails(){
    	return $this->belongsTo('App\WhiteFish\WfHarvest','harvest_id','id');
    }
}
