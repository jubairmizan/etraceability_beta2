<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class WfStaff extends Model
{
    protected $table = 'wf_staffs';

    public function block(){
    	return $this->belongsTo('App\WhiteFish\Block','block_id','id');
    }
    public function pond(){
    	return $this->belongsTo('App\WhiteFish\Pond','pond_id','id');
    }
}
