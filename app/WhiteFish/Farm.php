<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    protected $table = 'wf_farms';
}
