<?php

namespace App\WhiteFish;

use Illuminate\Database\Eloquent\Model;

class CultivationProcess extends Model
{
    protected $table= "wf_cultivation_process";
}
