<?php

namespace App\ShrimpCultureModels;

use Illuminate\Database\Eloquent\Model;

class SiAnimalSampling extends Model
{
    protected $table='si_animal_sampling';
    public function scopeEnclosured($query){
        $query->where('harvest_tracking_id',0)->where('harvest_counter',0);
    }
//    protected $hidden=['pond_id'];
//    public function farm(){
//        return $this->belongsTo('App\SiFarm','farm_id','id');
//    }
//    public function farm_manager(){
//        return $this->belongsTo('App\User','farm_manager_id','id');
//    }
//    public function ponds(){
//        return $this->belongsTo('App\SiPond','pond_id','id');
//    }
}
