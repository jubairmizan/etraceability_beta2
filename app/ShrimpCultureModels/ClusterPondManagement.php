<?php

namespace App\ShrimpCultureModels;

use Illuminate\Database\Eloquent\Model;

class ClusterPondManagement extends Model
{
    protected $table='cluster_pond_management';
    public function scopeWithoutZero($query){
        $query->where('id','!=',0);
    }
    public function scopeEnclosured($query){
        $query->where('harvest_tracking_id',0)->where('harvest_counter',0);
    }
    public function cluster(){
        return $this->belongsTo('App\ShrimpCultureModels\Cluster','cluster_id','id');
    }
//    public function cluster_manager(){
//        return $this->belongsTo('App\ShrimpModel\User','cluster_manager_id','id');
//    }
    public function ponds(){
        return $this->belongsTo('App\ShrimpCultureModels\ClusterPond','pond_id','id');
    }
    public function inventory(){
        return $this->hasMany('App\FeedDetails','ct_during_enclosure_id','id');
    }
    public function water_parameter(){
        return $this->hasMany('App\WaterParameterDetails','ct_during_enclosure_id','id');
    }
    public function temperature(){
        return $this->hasMany('App\TemperatureDetails','ct_during_enclosure_id','id');
    }
    public function ph(){
        return $this->hasMany('App\PHDetails','ct_during_enclosure_id','id');
    }
    public function dissolved_oxygen(){
        return $this->hasMany('App\DissolvedOxygenDetails','ct_during_enclosure_id','id');
    }
}
