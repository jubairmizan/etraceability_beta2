<?php

namespace App\ShrimpCultureModels;

use Illuminate\Database\Eloquent\Model;

class SiAnimalHarvest extends Model
{
    protected $table='si_animal_harvest';
    public function scopeWithoutZero($query){
        $query->where('id','!=',0);
    }
    public function pond(){
        return $this->belongsTo('App\ShrimpCultureModels\SiPond','pond_id','id');
    }
}
