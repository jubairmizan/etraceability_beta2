<?php

namespace App\ShrimpCultureModels;

use Illuminate\Database\Eloquent\Model;

class ClusterPond extends Model
{
    protected $table='cluster_ponds';
//    protected $hidden=['cluster_id'];
    public function scopeActive($query){
        $query->where('status',1);
    }
    public function last_sampling_data(){
        return $this->hasOne('App\ShrimpCultureModels\ClusterAnimalSampling','pond_id','id');
    }
    public function last_staffs_data(){
        return $this->hasOne('App\Staff','pond_id','id');
    }
    public function cluster(){
        return $this->belongsTo('App\ShrimpCultureModels\Cluster','cluster_id','id');
    }
//    public function client(){
//        return $this->belongsTo('App\User','client_id','id');
//    }
//    public function cluster_manager(){
//        return $this->belongsTo('App\User','cluster_manager_id','id');
//    }
//    public function manager(){
//        return $this->hasMany('App\ShrimpModel\RelClusterPondManager','manager_id','id');
//    }
//    public function staffs(){
//        return $this->hasOne('App\Staff','pond_id','id');
//    }
}
