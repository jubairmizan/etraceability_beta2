<?php

namespace App\ShrimpCultureModels;

use Illuminate\Database\Eloquent\Model;

class ClusterAnimalHarvest extends Model
{
    protected $table='cluster_animal_harvest';
    public function scopeWithoutZero($query){
        $query->where('id','!=',0);
    }
}
