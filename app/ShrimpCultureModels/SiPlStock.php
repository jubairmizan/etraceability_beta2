<?php

namespace App\ShrimpCultureModels;

use Illuminate\Database\Eloquent\Model;

class SiPlStock extends Model
{
    protected $table='si_pl_stock';
}
