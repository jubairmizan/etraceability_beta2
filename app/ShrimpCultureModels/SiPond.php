<?php

namespace App\ShrimpCultureModels;

use Illuminate\Database\Eloquent\Model;

class SiPond extends Model
{
    protected $table='si_ponds';
//    protected $hidden=['si_farm_id'];
    public function scopeActive($query){
        $query->where('status',1);
    }
    public function farm(){
        return $this->belongsTo('App\ShrimpCultureModels\SiFarm','si_farm_id','id');
    }
//    public function client(){
//        return $this->belongsTo('App\User','client_id','id');
//    }
//    public function manager(){
//        return $this->hasMany('App\ShrimpModel\RelSiPondManager','manager_id','id');
//    }
//    public function animal_details(){
//        return $this->hasOne('App\SiAnimalSampling','pond_id','id');
//    }
////    public function enter_to_enclosure(){
////        return $this->hasMany('App\SiEnterToEnclosure','');
////    }
    public function last_sampling_data(){
        return $this->hasOne('App\ShrimpCultureModels\SiAnimalSampling','pond_id','id');
    }
    public function last_staffs_data(){
        return $this->hasOne('App\Staff','pond_id','id');
    }
}
