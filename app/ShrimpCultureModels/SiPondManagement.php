<?php

namespace App\ShrimpCultureModels;

use Illuminate\Database\Eloquent\Model;

class SiPondManagement extends Model
{
    protected $table='si_pond_management';
    public function scopeWithoutZero($query){
        $query->where('id','!=',0);
    }
    public function farm(){
        return $this->belongsTo('App\ShrimpCultureModels\SiFarm','farm_id','id');
    }
//    public function cluster_manager(){
//        return $this->belongsTo('App\ShrimpModel\User','cluster_manager_id','id');
//    }
    public function ponds(){
        return $this->belongsTo('App\ShrimpCultureModels\SiPond','pond_id','id');
    }
    public function scopeEnclosured($query){
        $query->where('harvest_tracking_id',0)->where('harvest_counter',0);
    }

    public function inventory_used_in_water_details(){
        return $this->hasMany('App\InventoryUsedInWaterDetails','si_pond_management_id','id');
    }
    public function feed(){
        return $this->hasMany('App\FeedDetails','si_during_enclosure_id','id');
    }
    public function remark(){
        return $this->hasMany('App\RemarkDetails','si_during_enclosure_id','id');
    }
    public function water_parameter_details(){
        return $this->hasMany('App\WaterParameterDetails','si_pond_management_id','id');
    }
    public function temperature_details(){
        return $this->hasMany('App\TemperatureDetails','si_pond_management_id','id');
    }
    public function ph_details(){
        return $this->hasMany('App\PHDetails','si_pond_management_id','id');
    }
    public function dissolved_oxygen_details(){
        return $this->hasMany('App\DissolvedOxygenDetails','si_pond_management_id','id');
    }
}
