<?php

namespace App\ShrimpCultureModels;

use Illuminate\Database\Eloquent\Model;

class SiFarm extends Model
{
    protected $table='si_farms';
    public function scopeActive($query){
        $query->where('status',1);
    }
    public function scopeWithoutZero($query){
        $query->where('id','!=',0);
    }
    public function rel_si_farm_client(){
        return $this->hasMany('App\RelSiFarmClient','si_farm_id','id');
    }
    public function ponds(){
        return $this->hasMany('App\SiPond','si_farm_id','id');
    }
    public function farm_manager(){
        return $this->belongsTo('App\User','farm_manager_id','id');
    }
    public function division(){
        return $this->belongsTo('App\Division','division_id','id');
    }
    public function district(){
        return $this->belongsTo('App\District','district_id','id');
    }
    public function upazila(){
        return $this->belongsTo('App\Upazila','upazila_id','id');
    }
    public function union(){
        return $this->belongsTo('App\Union','union_id','id');
    }
}
