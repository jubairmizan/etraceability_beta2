<?php

namespace App\ShrimpCultureModels;

use Illuminate\Database\Eloquent\Model;

class Cluster extends Model
{
    protected $table='clusters';
    public function scopeActive($query){
        $query->where('status',1);
    }
    public function scopeWithoutZero($query){
        $query->where('id','!=',0);
    }
    public function rel_cluster_client(){
        return $this->hasMany('App\RelClusterClient','cluster_id','id');
    }
    public function cluster_manager(){
        return $this->belongsTo('App\User','cluster_manager_id','id');
    }
    public function ponds(){
        return $this->hasMany('App\ClusterPond','cluster_id','id');
    }
    public function division(){
        return $this->belongsTo('App\Division','division_id','id');
    }
    public function district(){
        return $this->belongsTo('App\District','district_id','id');
    }
    public function upazila(){
        return $this->belongsTo('App\Upazila','upazila_id','id');
    }
    public function union(){
        return $this->belongsTo('App\Union','union_id','id');
    }
}
