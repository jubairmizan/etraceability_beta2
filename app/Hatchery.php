<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hatchery extends Model
{
    protected $table='hatcheries';
    public function scopeActive($query){
        $query->where('status',1);
    }
    public function rel_hatchery_client(){
        return $this->hasMany('App\RelHatcheryClient','hatchery_id','id');
    }
    public function division(){
        return $this->belongsTo('App\Division','division_id','id');
    }
    public function district(){
        return $this->belongsTo('App\District','district_id','id');
    }
    public function upazila(){
        return $this->belongsTo('App\Upazila','upazila_id','id');
    }
    public function union(){
        return $this->belongsTo('App\Union','union_id','id');
    }
}
