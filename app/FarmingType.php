<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FarmingType extends Model
{
    protected $table='farming_types';
}
