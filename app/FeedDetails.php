<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedDetails extends Model
{
    protected $table='feed_details';
    public function feed(){
        return $this->belongsTo('App\Feed','feed_id','id');
    }
}
