<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table='inventories';
    public function scopeActive($query){
        $query->where('status',1);
    }
    public function rel_inventory_type(){
    	return $this->belongsTo('App\InventoryType','type_id','id');
    }
    public function rel_inventory_brand(){
    	return $this->belongsTo('App\InventoryBrand','brand_id','id');
    }
}
