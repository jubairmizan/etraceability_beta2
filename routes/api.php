<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => 'api',
//    'prefix' => 'auth'

], function () {

    //Created by Shreya
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('pond_details','ApiController@pondDetails');
    Route::post('staff_update','ApiController@staffUpdate');
    Route::post('animal_details','ApiController@animalDetails');
    Route::post('pl_stock','ApiController@pLStock');
    Route::post('pl_sampling','ApiController@pLSampling');
    Route::post('pond_list','ApiController@pondList');
    Route::post('add_monitoring_parameters','ApiController@addMonitoringParameters');
    Route::post('commercial_input_details','ApiController@commercialInputDetails');
    Route::post('add_commercial_input','ApiController@addCommercialInput');
    Route::post('update_inventory','ApiController@updateInventory');
    Route::post('inventory_list','ApiController@inventoryList');
//    Route::post('enter_to_enclosure','ApiController@enterToEnclosure');
//    Route::post('during_enclosure','ApiController@duringEnclosure');
    Route::post('harvest','ApiController@harvest');
    Route::post('harvest_details','ApiController@harvestDetails');
});
//Route::post('api/login');