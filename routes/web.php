<?php
/*Route::get('/', function () {
    return view('welcome');
});*/
//Test Input Adding Routes
use App\ShrimpCultureModels\SiPlStock;
use Carbon\Carbon;

Route::get('/test_input', 'Test\TestController@indexInput')->name('test_input.index');
Route::post('/test_input', 'Test\TestController@storeInput')->name('test_input.store');
Route::get('/get_pond_by_farm_id', 'Test\TestController@getPondByFarmId')->name('get_pond_by_farm_id');

Route::get('/test_sampling', 'Test\TestController@indexSampling')->name('test_sampling.index');
Route::post('/test_sampling', 'Test\TestController@storeSampling')->name('test_sampling.store');

Route::get('/test_harvesting', 'Test\TestController@indexHarvest')->name('test_harvesting.index');
Route::post('/test_harvesting', 'Test\TestController@storeHarvest')->name('test_harvesting.store');

Route::get('/test_feed_input', function () {
    return view('test_input.test_feed_input');
})->name('test_feed_input.index');
Route::post('/test_feed_input', 'Test/TestController@storeFeedInput')->name('test_feed_input.store');
Route::get('/test_ph_input', function () {
    return view('test_input.test_ph_input');
})->name('test_ph_input.index');
Route::post('/test_ph_input', 'Test/TestController@storePHInput')->name('test_ph_input.store');
//Test Input Adding Routes
Route::get('days',function (){
    $now=Carbon::now();
    $si_pl_stock=SiPlStock::where('farm_id',2)->where('farm_manager_id',7)->where('pond_id',20)->where('status','enclosure')
        ->where('harvest_tracking_id',0)->where('harvest_counter',0)->first();
    $stocking_date=new Carbon($si_pl_stock->created_at);
    $stocking_date=$stocking_date->toDateString();
    $days_of_culture=$now->diffInDays($stocking_date, true);
    return $days_of_culture;
});

Route::get('/', function () {
    return view('fontend.index');
});
Route::get('about', 'AboutPageController@index')->name('about');
Route::get('partners', 'PartnerPageController@index')->name('partners');
Route::get('contact', 'ContactPageController@index')->name('contact');
Route::get('fishing', 'FishingController@index')->name('fishing');
Route::get('farming', 'FarmingController@index')->name('farming');
Route::get('support', 'SupportController@index')->name('support');
Route::get('privacy', 'PrivacyPolicyController@index')->name('privacy');


Auth::routes();

//Route::get('admin/home','HomeController@dashboard')->name('admin.home');
Route::group(['prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth','admin']],function (){
    Route::get('dashboard',[
        'as'=>'admin.dashboard',
        'uses'=>'DashboardController@index',
        'title'=>'Admin Dashboard'
    ]);
    Route::get('user',[
        'as'=>'admin.user.index',
        'uses'=>'UserController@index',
        'title'=>'User List'
    ]);
    Route::get('user/create',[
        'as'=>'admin.user.create',
        'uses'=>'UserController@create',
        'title'=>'Create User'
    ]);
    Route::post('user/create',[
        'as'=>'admin.user.store',
        'uses'=>'UserController@store',
        'title'=>'Store User'
    ]);
    Route::get('user/edit',[
        'as'=>'admin.user.edit',
        'uses'=>'UserController@edit',
        'title'=>'Edit User'
    ]);
    Route::get('user/edit',[
        'as'=>'admin.user.update',
        'uses'=>'UserController@update',
        'title'=>'Update User'
    ]);
});
Route::group(['prefix'=>'shrimp_culture','namespace'=>'ShrimpCulture'],function (){
    @include('shrimp_culture.php');
});
Route::group(['prefix'=>'client','namespace'=>'Client','middleware'=>['auth','client']],function (){
    Route::get('dashboard',[
        'as'=>'client.dashboard',
        'uses'=>'DashboardController@index',
        'title'=>'Client Dashboard'
    ]);
    Route::get('hatchery',[
        'as'=>'client.hatchery.index',
        'uses'=>'HatcheryController@index',
        'title'=>'Hatchery List'
    ]);
    Route::get('cluster',[
        'as'=>'client.cluster.index',
        'uses'=>'ClusterController@index',
        'title'=>'Cluster List'
    ]);
    Route::get('cluster_pond',[
        'as'=>'client.cluster_pond.index',
        'uses'=>'ClusterPondController@index',
        'title'=>'Cluster Pond List'
    ]);
    Route::get('enter_to_enclosure',[
        'as'=>'cluster_client.enter_to_enclosure',
        'uses'=>'ClusterPondManagementController@enter_to_enclosure',
        'title'=>'Cluster Enter to Enclosure'
    ]);
    Route::get('farm',[
        'as'=>'client.farm.index',
        'uses'=>'FarmController@index',
        'title'=>'Farm List'
    ]);
    Route::get('farm/create',[
        'as'=>'client.farm.create',
        'uses'=>'FarmController@create',
        'title'=>'Create Farm'
    ]);
    Route::post('farm/create',[
        'as'=>'semi_intensive_client.farm.store',
        'uses'=>'FarmController@store',
        'title'=>'Store Farm'
    ]);
    Route::get('si_pond',[
        'as'=>'client.si_pond.index',
        'uses'=>'SemiIntensivePondController@index',
        'title'=>'Semi-Intensive Pond List'
    ]);
    Route::get('si_pond/create',[
        'as'=>'client.si_pond.create',
        'uses'=>'SemiIntensivePondController@create',
        'title'=>'Create Semi-Intensive Pond'
    ]);
    Route::post('si_pond/create',[
        'as'=>'client.si_pond.store',
        'uses'=>'SemiIntensivePondController@store',
        'title'=>'Store Semi-Intensive Pond'
    ]);
    Route::get('feed',[
        'as'=>'client.feed.index',
        'uses'=>'FeedController@index',
        'title'=>'Feed List'
    ]);
    Route::get('medicine',[
        'as'=>'client.medicine.index',
        'uses'=>'MedicineController@index',
        'title'=>'Medicine List'
    ]);
    Route::get('chert',[
        'as'=>'client.chert.index',
        'uses'=>'ChertController@index',
        'title'=>'Chert List'
    ]);
    Route::get('cluster_enter_to_enclosure',[
        'as'=>'client.cluster.enter_to_enclosure',
        'uses'=>'ClusterPondManagementController@enter_to_enclosure',
        'title'=>'Cluster Enter to Enclosure'
    ]);
    Route::get('cluster_during_enclosure',[
        'as'=>'client.cluster.during_enclosure',
        'uses'=>'ClusterPondManagementController@during_enclosure',
        'title'=>'Cluster During Enclosure'
    ]);
    Route::get('si_enter_to_enclosure',[
        'as'=>'client.si.enter_to_enclosure',
        'uses'=>'SemiIntensivePondManagementController@enter_to_enclosure',
        'title'=>'Semi-Intensive Enter to Enclosure'
    ]);
    //Ajax route
    Route::get('getPond',[
        'as'=>'client.getPond',
        'uses'=>'SemiIntensivePondManagementController@getPond',
    ]);
    //Ajax route
    Route::get('si_during_enclosure',[
        'as'=>'client.si.during_enclosure',
        'uses'=>'SemiIntensivePondManagementController@during_enclosure',
        'title'=>'Semi-Intensive During Enclosure'
    ]);
});
//Route::group(['prefix'=>'processing_plant','namespace'=>'ProcessingPlant','middleware'=>['auth','processing_plant']],function (){
//    Route::get('dashboard',[
//        'as'=>'processing_plant.dashboard',
//        'uses'=>'ProcessingPlantController@index',
//        'title'=>'Processing Plant Dashboard'
//    ]);
//    Route::get('profile',[
//        'as'=>'processing_plant.profile',
//        'uses'=>'ProcessingPlantController@profile',
//        'title'=>'Processing Plant profile'
//    ]);
//    Route::get('supplierList/{type_of_farming}',[
//        'as'=>'processing_plant.supplierList',
//        'uses'=>'ProcessingPlantController@supplierList',
//        'title'=>'Processing Plant supplierList'
//    ]);
//    Route::get('clusters/{type_of_farming}/{client_id}',[
//        'as'=>'processing_plant.clusters',
//        'uses'=>'ProcessingPlantController@clusters',
//        'title'=>'Processing Plant Clusters'
//    ]);
//    Route::get('farms/{type_of_farming}/{client_id}',[
//        'as'=>'processing_plant.farms',
//        'uses'=>'ProcessingPlantController@farms',
//        'title'=>'Processing Plant Farms'
//    ]);
//    Route::get('ponds/{type_of_farming}/{cluster_or_farm_id}/{client_id}',[
//        'as'=>'processing_plant.ponds',
//        'uses'=>'ProcessingPlantController@ponds',
//        'title'=>'Processing Plant Ponds'
//    ]);
//    Route::get('ponds_history/{type_of_farming}/{pondId}/{client_id}',[
//        'as'=>'processing_plant.ponds.history',
//        'uses'=>'ProcessingPlantController@pondHistory',
//        'title'=>'Processing Plant Pond History'
//    ]);
//});

/*White Fish*/
Route::group(['prefix'=>'WhiteFish','namespace'=>'WhiteFish\client','middleware'=>['auth','wf_client']],
function (){
      @include('WhiteFishClient.php');  
});

Route::get('logout',function (){
    auth()->logout();
    return redirect()->to('/');
})->name('logout');