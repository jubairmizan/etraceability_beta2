<?php
/**
 * Created by PhpStorm.
 * User: Shreya
 * Date: 10/7/2018
 * Time: 1:39 AM
 */
Route::group(['prefix'=>'client','namespace'=>'Client','middleware'=>['auth','shrimp_culture_client']],function (){
    Route::get('dashboard',[
        'as'=>'shrimp_culture.client.dashboard',
        'uses'=>'DashboardController@index',
        'title'=>'Shrimp Culture Client Dashboard'
    ]);
    Route::get('hatchery',[
        'as'=>'shrimp_culture.client.hatchery',
        'uses'=>'HatcheryController@index',
        'title'=>'Shrimp Culture Client Hatchery'
    ]);
    Route::group(['prefix'=>'semi_intensive'],function (){
        Route::get('pond_management',[
            'as'=>'shrimp_culture.client.semi_intensive.pond_management',
            'uses'=>'SemiIntensiveController@pondManagement',
            'title'=>'Shrimp Culture Semi-Intensive Pond Management'
        ]);
    });
    Route::group(['prefix'=>'semi_intensive'],function (){
        Route::get('sampling',[
            'as'=>'shrimp_culture.client.semi_intensive.sampling',
            'uses'=>'SemiIntensiveController@sampling',
            'title'=>'Shrimp Culture Semi-Intensive Sampling'
        ]);
    });
    Route::group(['prefix'=>'semi_intensive'],function (){
        Route::get('harvest',[
            'as'=>'shrimp_culture.client.semi_intensive.harvest',
            'uses'=>'SemiIntensiveController@harvest',
            'title'=>'Shrimp Culture Semi-Intensive Harvest'
        ]);
    });
    Route::group(['prefix'=>'extensive'],function (){
        Route::get('pond_management',[
            'as'=>'shrimp_culture.client.extensive.pond_management',
            'uses'=>'ClusterPerspectiveController@pondManagement',
            'title'=>'Shrimp Culture Extensive Pond Management'
        ]);
    });
    Route::group(['prefix'=>'extensive'],function (){
        Route::get('harvest',[
            'as'=>'shrimp_culture.client.extensive.harvest',
            'uses'=>'ClusterPerspectiveController@harvest',
            'title'=>'Shrimp Culture Extensive Harvest'
        ]);
    });
    Route::get('inventory',[
        'as'=>'shrimp_culture.client.inventory',
        'uses'=>'InventoryController@index',
        'title'=>'Shrimp Culture Client Inventory'
    ]);
    //Ajax Routes Start
    Route::get('get_pond_info',[
        'as'=>'shrimp_culture.client.semi_intensive.get_pond_info',
        'uses'=>'SemiIntensiveController@getPondInfo'
    ]);
    Route::get('get_feed_info',[
        'as'=>'shrimp_culture.client.semi_intensive.get_feed_info',
        'uses'=>'SemiIntensiveController@getFeedInfo'
    ]);
    Route::get('get_probiotics_minerals_others_info',[
        'as'=>'shrimp_culture.client.semi_intensive.get_probiotic_mineral_other_info',
        'uses'=>'SemiIntensiveController@getProbioticMineralOtherInfo'
    ]);
    Route::get('get_monitoring_parameter_info',[
        'as'=>'shrimp_culture.client.semi_intensive.get_monitoring_parameter_info',
        'uses'=>'SemiIntensiveController@getMonitoringParameterInfo'
    ]);
    //Ajax Routes End
//    Route::get('hatchery',[
//        'as'=>'client.hatchery.index',
//        'uses'=>'HatcheryController@index',
//        'title'=>'Hatchery List'
//    ]);
//    Route::get('cluster',[
//        'as'=>'client.cluster.index',
//        'uses'=>'ClusterController@index',
//        'title'=>'Cluster List'
//    ]);
//    Route::get('cluster_pond',[
//        'as'=>'client.cluster_pond.index',
//        'uses'=>'ClusterPondController@index',
//        'title'=>'Cluster Pond List'
//    ]);
//    Route::get('enter_to_enclosure',[
//        'as'=>'cluster_client.enter_to_enclosure',
//        'uses'=>'ClusterPondManagementController@enter_to_enclosure',
//        'title'=>'Cluster Enter to Enclosure'
//    ]);
//    Route::get('farm',[
//        'as'=>'client.farm.index',
//        'uses'=>'FarmController@index',
//        'title'=>'Farm List'
//    ]);
//    Route::get('farm/create',[
//        'as'=>'client.farm.create',
//        'uses'=>'FarmController@create',
//        'title'=>'Create Farm'
//    ]);
//    Route::post('farm/create',[
//        'as'=>'semi_intensive_client.farm.store',
//        'uses'=>'FarmController@store',
//        'title'=>'Store Farm'
//    ]);
//    Route::get('si_pond',[
//        'as'=>'client.si_pond.index',
//        'uses'=>'SemiIntensivePondController@index',
//        'title'=>'Semi-Intensive Pond List'
//    ]);
//    Route::get('si_pond/create',[
//        'as'=>'client.si_pond.create',
//        'uses'=>'SemiIntensivePondController@create',
//        'title'=>'Create Semi-Intensive Pond'
//    ]);
//    Route::post('si_pond/create',[
//        'as'=>'client.si_pond.store',
//        'uses'=>'SemiIntensivePondController@store',
//        'title'=>'Store Semi-Intensive Pond'
//    ]);
//    Route::get('feed',[
//        'as'=>'client.feed.index',
//        'uses'=>'FeedController@index',
//        'title'=>'Feed List'
//    ]);
//    Route::get('medicine',[
//        'as'=>'client.medicine.index',
//        'uses'=>'MedicineController@index',
//        'title'=>'Medicine List'
//    ]);
//    Route::get('chert',[
//        'as'=>'client.chert.index',
//        'uses'=>'ChertController@index',
//        'title'=>'Chert List'
//    ]);
//    Route::get('cluster_enter_to_enclosure',[
//        'as'=>'client.cluster.enter_to_enclosure',
//        'uses'=>'ClusterPondManagementController@enter_to_enclosure',
//        'title'=>'Cluster Enter to Enclosure'
//    ]);
//    Route::get('cluster_during_enclosure',[
//        'as'=>'client.cluster.during_enclosure',
//        'uses'=>'ClusterPondManagementController@during_enclosure',
//        'title'=>'Cluster During Enclosure'
//    ]);
//    Route::get('si_enter_to_enclosure',[
//        'as'=>'client.si.enter_to_enclosure',
//        'uses'=>'SemiIntensivePondManagementController@enter_to_enclosure',
//        'title'=>'Semi-Intensive Enter to Enclosure'
//    ]);
//    //Ajax route
//    Route::get('getPond',[
//        'as'=>'client.getPond',
//        'uses'=>'SemiIntensivePondManagementController@getPond',
//    ]);
//    //Ajax route
//    Route::get('si_during_enclosure',[
//        'as'=>'client.si.during_enclosure',
//        'uses'=>'SemiIntensivePondManagementController@during_enclosure',
//        'title'=>'Semi-Intensive During Enclosure'
//    ]);
});
Route::group(['prefix'=>'processing_plant','middleware'=>['auth','processing_plant_client']],function (){
    Route::get('dashboard',[
        'as'=>'shrimp_culture.processing_plant.dashboard',
        'uses'=>'ProcessingPlantController@index',
        'title'=>'Processing Plant Dashboard'
    ]);
    Route::get('profile',[
        'as'=>'shrimp_culture.processing_plant.profile',
        'uses'=>'ProcessingPlantController@profile',
        'title'=>'Processing Plant profile'
    ]);
    Route::get('{type_of_farming}/suppliers',[
        'as'=>'shrimp_culture.processing_plant.suppliers',
        'uses'=>'ProcessingPlantController@suppliers',
        'title'=>'Suppliers for Processing Plant'
    ]);
    Route::get('{type_of_farming}/clusters/{client_id}',[
        'as'=>'shrimp_culture.processing_plant.clusters',
        'uses'=>'ProcessingPlantController@clusters',
        'title'=>'Clusters for Processing Plant'
    ]);
    Route::get('{type_of_farming}/farms/{client_id}',[
        'as'=>'shrimp_culture.processing_plant.farms',
        'uses'=>'ProcessingPlantController@farms',
        'title'=>'Farms for Processing Plant'
    ]);
    Route::get('{type_of_farming}/{client_id}/ponds/{cluster_or_farm_id}',[
        'as'=>'shrimp_culture.processing_plant.ponds',
        'uses'=>'ProcessingPlantController@ponds',
        'title'=>'Processing Plant Ponds'
    ]);
    Route::get('{type_of_farming}/{client_id}/ponds_status/{pond_id}',[
        'as'=>'shrimp_culture.processing_plant.pond_status',
        'uses'=>'ProcessingPlantController@pondStatus',
        'title'=>' Pond Status for Processing Plant'
    ]);
});