<?php
Route::get('dashboard',[
    'as'=>'Dashboard',
    'uses'=>'DashboardController@index'
]);

// User
Route::get('user',[
    'as'=>'User Lists',
    'uses'=>'UserController@index'
]);
Route::get('user/create',[
    'as'=>'User Create',
    'uses'=>'UserController@create'
]);
Route::post('user/store',[
    'as'=>'store',
    'uses'=>'UserController@store'
]);

// Types Of Fish
Route::get('Types-of-fish',[
    'as'=>'Types Of Fish Lists',
    'uses'=>'TypesOfFishController@index'
]);
Route::get('Types-of-fish/create',[
    'as'=>'Types Of Fish Create',
    'uses'=>'TypesOfFishController@create'
]);
Route::post('Types-of-fish/store',[
    'as'=>'store',
    'uses'=>'TypesOfFishController@store'
]);

// Farms
Route::get('farms',[
    'as'=>'Farms Lists',
    'uses'=>'FarmsController@index'
]);
Route::get('farms/create',[
    'as'=>'Farms Create',
    'uses'=>'FarmsController@create'
]);
Route::post('farms/store',[
    'as'=>'store',
    'uses'=>'FarmsController@store'
]);

// Block Number
Route::get('blocks',[
    'as'=>'Blocks Lists',
    'uses'=>'BlocksController@index'
]);
Route::get('blocks/create',[
    'as'=>'Blocks Create',
    'uses'=>'BlocksController@create'
]);
Route::post('blocks/store',[
    'as'=>'store',
    'uses'=>'BlocksController@store'
]);

// Pond
Route::get('ponds',[
    'as'=>'Ponds Lists',
    'uses'=>'PondsController@index'
]);
Route::get('ponds/create',[
    'as'=>'Ponds Create',
    'uses'=>'PondsController@create'
]);
Route::post('ponds/store',[
    'as'=>'store',
    'uses'=>'PondsController@store'
]);
Route::get('staff/{slug}',[
    'as'=>'Staff List',
    'uses'=>'PondsController@staffList'
]);
Route::get('ponaStockList/{slug}',[
    'as'=>'Pona Stock Lists',
    'uses'=>'PondsController@ponaStockList'
]);
Route::get('waterParameterDetails/{slug}',[
    'as'=>'Water Parameter Lists Pond Wise',
    'uses'=>'PondsController@waterParameterDetails'
]);
Route::get('ponaSampling/{slug}',[
    'as'=>'Pona Sampling Lists Pond Wise',
    'uses'=>'PondsController@ponaSampling'
]);

// Inventory Management
Route::get('inventoryManagement',[
    'as'=>'WhiteFish.client.inventory_management',
    'uses'=>'InventoriesController@inventoryManagement'
]);
Route::get('inventoryManagement/create',[
    'as'=>'WhiteFish.client.inventory_management.create',
    'uses'=>'InventoriesController@inventoryManagementCreate'
]);
Route::post('inventoryManagement/store',[
    'as'=>'WhiteFish.client.inventory_management.store',
    'uses'=>'InventoriesController@inventoryManagementStore'
]);
// Inventory Stock Out
Route::get('inventoryStockOut',[
    'as'=>'WhiteFish.client.inventory_stock_out',
    'uses'=>'InventoriesController@inventoryStockOut'
]);
Route::get('inventoryStockOut/create',[
    'as'=>'WhiteFish.client.inventory_stock_out.create',
    'uses'=>'InventoriesController@inventoryStockOutCreate'
]);
Route::post('inventoryStockOut/store',[
    'as'=>'WhiteFish.client.inventory_stock_out.store',
    'uses'=>'InventoriesController@inventoryStockOutStore'
]);
// Inventory Brand
Route::get('inventoryBrand',[
    'as'=>'WhiteFish.client.inventory_brands',
    'uses'=>'InventoriesController@inventoryBrand'
]);
Route::get('inventoryBrand/create',[
    'as'=>'WhiteFish.client.inventory_brands.create',
    'uses'=>'InventoriesController@inventoryBrandCreate'
]);
Route::post('inventoryBrand/store',[
    'as'=>'WhiteFish.client.inventory_brands.store',
    'uses'=>'InventoriesController@inventoryBrandStore'
]);
// Inventory Type
Route::get('inventoryTypes',[
    'as'=>'WhiteFish.client.inventory_types',
    'uses'=>'InventoriesController@inventoryTypes'
]);
/*Route::get('inventoryTypes/create',[
    'as'=>'WhiteFish.client.inventory_types.create',
    'uses'=>'InventoriesController@inventoryTypesCreate'
]);
Route::post('inventoryTypes/store',[
    'as'=>'WhiteFish.client.inventory_types.store',
    'uses'=>'InventoriesController@inventoryTypesStore'
]);*/
// Inventoryes
Route::get('inventoryTypes/{id}',[
    'as'=>'WhiteFish.client.inventoryTypes',
    'uses'=>'InventoriesController@inventoryTypesSlug'
]);
Route::get('inventoryStockData/{id}',[
    'as'=>'WhiteFish.client.inventoryStockData',
    'uses'=>'InventoriesController@inventoryStockingData'
]);
Route::get('inventories',[
    'as'=>'WhiteFish.client.inventories',
    'uses'=>'InventoriesController@inventories'
]);
Route::get('inventories/create',[
    'as'=>'WhiteFish.client.inventories.create',
    'uses'=>'InventoriesController@inventoriesCreate'
]);
Route::post('inventories/store',[
    'as'=>'WhiteFish.client.inventories.store',
    'uses'=>'InventoriesController@inventoriesStore'
]);


// Pona
Route::get('pona',[
    'as'=>'WhiteFish.client.pona',
    'uses'=>'InventoriesController@ponaLists'
]);
Route::post('pona/search',[
    'as'=>'WhiteFish.client.pona.search',
    'uses'=>'InventoriesController@ponaSearch'
]);


/*Dashboard Search*/
Route::get('search_pond_by_block',[
    'as'=>'WhiteFish.client.search_pond_by_block',
    'uses'=>'DashboardController@search_pond_by_block'
]);

Route::get('search_fish_by_pond',[
    'as'=>'WhiteFish.client.search_fish_by_pond',
    'uses'=>'DashboardController@search_fish_by_pond'
]); 

Route::post('InventoryStore/search/{slug}',[
    'as'=>'WhiteFish.client.InventoryStore.search',
    'uses'=>'InventoriesController@InventoryStoreSearch'
]);

Route::get('investBreakdown',[
    'as'=>'WhiteFish.client.investBreakdown',
    'uses'=>'InventoriesController@investBreakdown'
]);
Route::get('harvestBreakdown',[
    'as'=>'WhiteFish.client.harvestBreakdown',
    'uses'=>'InventoriesController@harvestBreakdown'
]);

Route::get('dashboard/search',[
    'as'=>'WhiteFish.dashboard.search',
    'uses'=>'DashboardController@index'
]);

Route::get('getInventory',[
    'as' => 'WhiteFish.client.getInventory',
    'uses' => 'AjaxData@getInventory'
]);
?>