<?php
Route::get('dashboard',[
    'as'=>'',
    'uses'=>'DashboardController@index'
])->name('WhiteFish.client.dashboard');
Route::get('d',[
    'as'=>'',
    'uses'=>'DashboardController@d'
])->name('WhiteFish.client.d');

// User
Route::get('user',[
    'as'=>'',
    'uses'=>'UserController@index'
])->name('WhiteFish.client.user');
Route::get('user/create',[
    'as'=>'',
    'uses'=>'UserController@create'
])->name('WhiteFish.client.user.create');
Route::post('user/store',[
    'as'=>'',
    'uses'=>'UserController@store'
])->name('WhiteFish.client.user.store');

// Types Of Fish
Route::get('Types-of-fish',[
    'as'=>'',
    'uses'=>'TypesOfFishController@index'
])->name('WhiteFish.client.typesOfFish');
Route::get('Types-of-fish/create',[
    'as'=>'',
    'uses'=>'TypesOfFishController@create'
])->name('WhiteFish.client.typesOfFish.create');
Route::post('Types-of-fish/store',[
    'as'=>'',
    'uses'=>'TypesOfFishController@store'
])->name('WhiteFish.client.typesOfFish.store');

// Farms
Route::get('farms',[
    'as'=>'',
    'uses'=>'FarmsController@index'
])->name('WhiteFish.client.farms');
Route::get('farms/create',[
    'as'=>'',
    'uses'=>'FarmsController@create'
])->name('WhiteFish.client.farms.create');
Route::post('farms/store',[
    'as'=>'',
    'uses'=>'FarmsController@store'
])->name('WhiteFish.client.farms.store');

// Block Number
Route::get('blocks',[
    'as'=>'',
    'uses'=>'BlocksController@index'
])->name('WhiteFish.client.blocks');
Route::get('blocks/create',[
    'as'=>'',
    'uses'=>'BlocksController@create'
])->name('WhiteFish.client.blocks.create');
Route::post('blocks/store',[
    'as'=>'',
    'uses'=>'BlocksController@store'
])->name('WhiteFish.client.blocks.store');

// Pond
Route::get('ponds',[
    'as'=>'',
    'uses'=>'PondsController@index'
])->name('WhiteFish.client.ponds');
Route::get('ponds/create',[
    'as'=>'',
    'uses'=>'PondsController@create'
])->name('WhiteFish.client.ponds.create');
Route::post('ponds/store',[
    'as'=>'',
    'uses'=>'PondsController@store'
])->name('WhiteFish.client.ponds.store');
Route::get('staff/{slug}',[
    'as'=>'',
    'uses'=>'PondsController@staffList'
])->name('WhiteFish.client.staffList');
Route::get('ponaStockList/{slug}',[
    'as'=>'',
    'uses'=>'PondsController@ponaStockList'
])->name('WhiteFish.client.ponaStockList');
Route::get('waterParameterDetails/{slug}',[
    'as'=>'',
    'uses'=>'PondsController@waterParameterDetails'
])->name('WhiteFish.client.waterParameterDetails');
Route::get('ponaSampling/{slug}',[
    'as'=>'',
    'uses'=>'PondsController@ponaSampling'
])->name('WhiteFish.client.ponaSampling');

// Inventory Management
Route::get('inventoryManagement',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryManagement'
])->name('WhiteFish.client.inventory_management');
Route::get('inventoryManagement/create',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryManagementCreate'
])->name('WhiteFish.client.inventory_management.create');
Route::post('inventoryManagement/store',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryManagementStore'
])->name('WhiteFish.client.inventory_management.store');
// Inventory Stock Out
Route::get('inventoryStockOut',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryStockOut'
])->name('WhiteFish.client.inventory_stock_out');
Route::get('inventoryStockOut/create',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryStockOutCreate'
])->name('WhiteFish.client.inventory_stock_out.create');
Route::post('inventoryStockOut/store',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryStockOutStore'
])->name('WhiteFish.client.inventory_stock_out.store');
// Inventory Brand
Route::get('inventoryBrand',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryBrand'
])->name('WhiteFish.client.inventory_brands');
Route::get('inventoryBrand/create',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryBrandCreate'
])->name('WhiteFish.client.inventory_brands.create');
Route::post('inventoryBrand/store',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryBrandStore'
])->name('WhiteFish.client.inventory_brands.store');
// Inventory Type
Route::get('inventoryTypes',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryTypes'
])->name('WhiteFish.client.inventory_types');
/*Route::get('inventoryTypes/create',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryTypesCreate'
])->name('WhiteFish.client.inventory_types.create');
Route::post('inventoryTypes/store',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryTypesStore'
])->name('WhiteFish.client.inventory_types.store;*/
// Inventoryes
Route::get('inventoryTypes/{id}',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryTypesSlug'
])->name('WhiteFish.client.inventoryTypes');
Route::get('inventoryStockData/{id}',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoryStockingData'
])->name('WhiteFish.client.inventoryStockData');
Route::get('inventories',[
    'as'=>'',
    'uses'=>'InventoriesController@inventories'
])->name('WhiteFish.client.inventories');
Route::get('inventories/create',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoriesCreate'
])->name('WhiteFish.client.inventories.create');
Route::post('inventories/store',[
    'as'=>'',
    'uses'=>'InventoriesController@inventoriesStore'
])->name('WhiteFish.client.inventories.store');


// Pona
Route::get('pona',[
    'as'=>'',
    'uses'=>'InventoriesController@ponaLists'
])->name('WhiteFish.client.pona');
Route::post('pona/search',[
    'as'=>'',
    'uses'=>'InventoriesController@ponaSearch'
])->name('WhiteFish.client.pona.search');


/*Dashboard Search*/
Route::get('search_pond_by_block',[
    'as'=>'',
    'uses'=>'DashboardController@search_pond_by_block'
])->name('WhiteFish.client.search_pond_by_block');

Route::get('search_fish_by_pond',[
    'as'=>'',
    'uses'=>'DashboardController@search_fish_by_pond'
])->name('WhiteFish.client.search_fish_by_pond'); 

Route::post('InventoryStore/search/{slug}',[
    'as'=>'',
    'uses'=>'InventoriesController@InventoryStoreSearch'
])->name('WhiteFish.client.InventoryStore.search');

Route::get('investBreakdown',[
    'as'=>'',
    'uses'=>'InventoriesController@investBreakdown'
])->name('WhiteFish.client.investBreakdown');
Route::get('harvestBreakdown',[
    'as'=>'',
    'uses'=>'InventoriesController@harvestBreakdown'
])->name('WhiteFish.client.harvestBreakdown');

Route::get('dashboard/search',[
    'as'=>'',
    'uses'=>'DashboardController@index'
])->name('WhiteFish.dashboard.search');

Route::get('getInventory',[
    'as'=>'',
    'uses' => 'AjaxData@getInventory'
])->name('WhiteFish.client.getInventory');
?>