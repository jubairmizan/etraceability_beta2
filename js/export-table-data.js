/*Export Table Init*/

"use strict"; 

$(document).ready(function() {
	$('#example').DataTable( {
		dom: 'Bfrtip',
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	} );
    $('#feed_datatable').DataTable();
    $('#probiotic_datatable').DataTable();
    $('#ph_datatable').DataTable();
    $('#dissolved_oxygen_datatable').DataTable();
    $('#temperature_oxygen_datatable').DataTable();
    $('#parameter_datatable').DataTable();
} );