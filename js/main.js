/**
 * Created by Unifox Digital on 6/3/2018.
 */
/* Datetimepicker Init*/
$('.datetimepicker1').datetimepicker({
    useCurrent: true,
    format: 'YYYY-MM-DD',
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
    },
}).on('dp.show', function() {
    if($(this).data("DateTimePicker").date() === null)
        $(this).data("DateTimePicker").date(moment());
});