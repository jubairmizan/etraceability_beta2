<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class WfRelFish extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Pond Id 1
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 1,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 1,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 1,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 1,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 1,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 1,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 1,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 1,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //Pond Id 2
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 2,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 2,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 2,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 2,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 2,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 2,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 2,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 2,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //Pond Id 3
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 3,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 3,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 3,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 3,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 3,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 3,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 3,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 3,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //Pond Id 4
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 4,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 4,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 4,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 4,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 4,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 4,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 4,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 4,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //Pond Id 5
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 5,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 5,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 5,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 5,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 5,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 5,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 5,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 5,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //Pond Id 6
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 6,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 6,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 6,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 6,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 6,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 6,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 6,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 6,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //Pond Id 7
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 7,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 7,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 7,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 7,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 7,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 7,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 7,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 7,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 8,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 8,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 8,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 8,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 8,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 8,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 8,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 8,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 8,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 9,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 9,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 9,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 9,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 9,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 9,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 9,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 9,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 9,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 10,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 10,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 10,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 10,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 10,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 10,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 10,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 10,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 10,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 11,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 11,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 11,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 11,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 11,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 11,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 11,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 11,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 11,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 12,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 12,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 12,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 12,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 12,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 12,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 12,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 12,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 12,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 13,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 13,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 13,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 13,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 13,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 13,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 13,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 13,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 13,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 14,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 14,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 14,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 14,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 14,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 14,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 14,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 14,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 14,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 15,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 15,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 15,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 15,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 15,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 15,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 15,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 15,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 15,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 16,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 16,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 16,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 16,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 16,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 16,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 16,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 16,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 16,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 17,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 17,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 17,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 17,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 17,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 17,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 17,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 17,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 17,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 18,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 18,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 18,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 18,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 18,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 18,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 18,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 18,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 18,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 19,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 19,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 19,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 19,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 19,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 19,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 19,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 19,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 19,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 20,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 20,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 20,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 20,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 20,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 20,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 20,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 20,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 20,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 21,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 21,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 21,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 21,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 21,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 21,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 21,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 21,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 21,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 22,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 22,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 22,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 22,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 22,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 22,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 22,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 22,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 22,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 23,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 23,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 23,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 23,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 23,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 23,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 23,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 23,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 23,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 24,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 24,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 24,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 24,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 24,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 24,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 24,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 24,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 24,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 25,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 25,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 25,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 25,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 25,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 25,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 25,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 25,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 25,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 26,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 26,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 26,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 26,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 26,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 26,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 26,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 26,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 26,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 27,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 27,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 27,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 27,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 27,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 27,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 27,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 27,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 27,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 28,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 28,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 28,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 28,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 28,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 28,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 28,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 28,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 28,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 29,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 29,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 29,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 29,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 29,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 29,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 29,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 29,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 29,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 30,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 30,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 30,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 30,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 30,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 30,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 30,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 30,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 30,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 31,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 31,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 31,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 31,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 31,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 31,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 31,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 31,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 31,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 32,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 32,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 32,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 32,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 32,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 32,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 32,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 32,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 32,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 33,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 33,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 33,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 33,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 33,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 33,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 33,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 33,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 33,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 34,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 34,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 34,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 34,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 34,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 34,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 34,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 34,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 34,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 35,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 35,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 35,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 35,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 35,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 35,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 35,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 35,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 35,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 36,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 36,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 36,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 36,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 36,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 36,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 36,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 36,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 36,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 37,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 37,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 37,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 37,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 37,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 37,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 37,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 37,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 37,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 38,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 38,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 38,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 38,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 38,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 38,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 38,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 38,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 38,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 39,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 39,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 39,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 39,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 39,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 39,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 39,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 39,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 39,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 40,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 40,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 40,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 40,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 40,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 40,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 40,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 40,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 40,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 41,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 41,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 41,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 41,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 41,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 41,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 41,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 41,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 41,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 42,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 42,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 42,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 42,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 42,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 42,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 42,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 42,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 42,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 43,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 43,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 43,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 43,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 43,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 43,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 43,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 43,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 43,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 44,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 44,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 44,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 44,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 44,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 44,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 44,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 44,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 44,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 45,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 45,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 45,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 45,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 45,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 45,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 45,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 45,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 45,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 46,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 46,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 46,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 46,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 46,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 46,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 46,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 46,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 46,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 47,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 47,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 47,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 47,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 47,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 47,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 47,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 47,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 47,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 48,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 48,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 48,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 48,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 48,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 48,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 48,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 48,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 48,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 49,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 49,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 49,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 49,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 49,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 49,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 49,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 49,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 49,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 50,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 50,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 50,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 50,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 50,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 50,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 50,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 50,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 50,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 51,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 51,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 51,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 51,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 51,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 51,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 51,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 51,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 51,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 52,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 52,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 52,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 52,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 52,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 52,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 52,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 52,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 52,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 53,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 53,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 53,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 53,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 53,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 53,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 53,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 53,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 53,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 54,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 54,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 54,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 54,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 54,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 54,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 54,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 54,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 54,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 55,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 55,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 55,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 55,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 55,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 55,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 55,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 55,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 55,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 56,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 56,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 56,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 56,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 56,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 56,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 56,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 56,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 56,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 57,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 57,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 57,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 57,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 57,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 57,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 57,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 57,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 57,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 58,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 58,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 58,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 58,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 58,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 58,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 58,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 58,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 58,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 59,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 59,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 59,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 59,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 59,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 59,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 59,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 59,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 59,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 60,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 60,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 60,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 60,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 60,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 60,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 60,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 60,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 60,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 61,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 61,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 61,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 61,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 61,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 61,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 61,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 61,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 61,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 62,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 62,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 62,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 62,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 62,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 62,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 62,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 62,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 62,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 63,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 63,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 63,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 63,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 63,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 63,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 63,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 63,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 63,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 64,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 64,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 64,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 64,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 64,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 64,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 64,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 64,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 64,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 65,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 65,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 65,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 65,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 65,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 65,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 65,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 65,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 65,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 66,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 66,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 66,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 66,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 66,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 66,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 66,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 66,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 66,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 67,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 67,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 67,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 67,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 67,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 67,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 67,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 67,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 67,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 68,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 68,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 68,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 68,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 68,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 68,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 68,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 68,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 68,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 69,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 69,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 69,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 69,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 69,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 69,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 69,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 69,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 69,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 70,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 70,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 70,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 70,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 70,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 70,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 70,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 70,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 70,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 71,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 71,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 71,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 71,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 71,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 71,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 71,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 71,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 71,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 72,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 72,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 72,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 72,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 72,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 72,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 72,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 72,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 72,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 73,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 73,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 73,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 73,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 73,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 73,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 73,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 73,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 73,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 74,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 74,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 74,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 74,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 74,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 74,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 74,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 74,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 74,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 75,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 75,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 75,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 75,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 75,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 75,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 75,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 75,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 75,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 76,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 76,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 76,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 76,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 76,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 76,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 76,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 76,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 76,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 77,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 77,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 77,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 77,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 77,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 77,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 77,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 77,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 77,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 78,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 78,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 78,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 78,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 78,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 78,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 78,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 78,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 78,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 79,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 79,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 79,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 79,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 79,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 79,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 79,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 79,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 79,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 80,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 80,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 80,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 80,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 80,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 80,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 80,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 80,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 80,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 81,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 81,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 81,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 81,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 81,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 81,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 81,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 81,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 81,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 82,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 82,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 82,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 82,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 82,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 82,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 82,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 82,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 82,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 83,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 83,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 83,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 83,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 83,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 83,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 83,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 83,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 83,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 84,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 84,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 84,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 84,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 84,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 84,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 84,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 84,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 84,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 85,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 85,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 85,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 85,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 85,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 85,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 85,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 85,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 85,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 86,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 86,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 86,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 86,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 86,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 86,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 86,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 86,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 86,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 87,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 87,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 87,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 87,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 87,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 87,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 87,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 87,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 87,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 88,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 88,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 88,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 88,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 88,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 88,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 88,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 88,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 88,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 89,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 89,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 89,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 89,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 89,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 89,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 89,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 89,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 89,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 90,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 90,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 90,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 90,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 90,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 90,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 90,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 90,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 90,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 91,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 91,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 91,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 91,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 91,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 91,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 91,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 91,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 91,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 92,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 92,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 92,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 92,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 92,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 92,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 92,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 92,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 92,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 93,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 93,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 93,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 93,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 93,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 93,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 93,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 93,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 93,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 94,
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 94,
            'fish_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 94,
            'fish_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 94,
            'fish_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 94,
            'fish_id' => 4,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 94,
            'fish_id' => 5,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 94,
            'fish_id' => 6,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 94,
            'fish_id' => 7,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
            'org_id' => 1,
            'farm_id' => 1,
            'pond_id' => 94,
            'fish_id' => 8,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        //'pond_id' => 95,
        DB::table('wf_rel_pond_wise_fish')->insert([
        	'org_id' => 1,
        	'farm_id' => 1,
        	'pond_id' => 95,
        	'fish_id' => 1,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
        	'org_id' => 1,
        	'farm_id' => 1,
        	'pond_id' => 95,
        	'fish_id' => 2,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
        	'org_id' => 1,
        	'farm_id' => 1,
        	'pond_id' => 95,
        	'fish_id' => 3,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
        	'org_id' => 1,
        	'farm_id' => 1,
        	'pond_id' => 95,
        	'fish_id' => 4,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
        	'org_id' => 1,
        	'farm_id' => 1,
        	'pond_id' => 95,
        	'fish_id' => 5,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
        	'org_id' => 1,
        	'farm_id' => 1,
        	'pond_id' => 95,
        	'fish_id' => 6,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
        	'org_id' => 1,
        	'farm_id' => 1,
        	'pond_id' => 95,
        	'fish_id' => 7,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('wf_rel_pond_wise_fish')->insert([
        	'org_id' => 1,
        	'farm_id' => 1,
        	'pond_id' => 95,
        	'fish_id' => 8,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
