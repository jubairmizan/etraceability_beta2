<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiPLStockTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('si_pl_stock')->truncate();
        DB::table('si_pl_stock')->insert([
            'id' => '0',
            'hatchery_id'=>'1',
            'farm_id'=>'1',
            'farm_manager_id'=>'1',
            'pond_id'=>'1',
            'pl_quantity'=>'0',
            'pl_price'=>'0',
            'pl_age'=>'0',
        ]);
        DB::unprepared("UPDATE si_pl_stock SET id=0");
        DB::unprepared("ALTER TABLE si_pl_stock AUTO_INCREMENT=1;");
    }
}
