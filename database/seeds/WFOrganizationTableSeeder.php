<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WfOrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wf_organizations')->insert([
            'name'=>'Globe'
        ]);
        DB::table('wf_organizations')->insert([
            'name'=>'Index'
        ]);
    }
}
