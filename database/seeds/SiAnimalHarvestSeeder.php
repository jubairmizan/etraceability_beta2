<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiAnimalHarvestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('si_animal_harvest')->truncate();
        DB::table('si_animal_harvest')->insert([
            'id' => '0',
            'farm_id'=>'1',
            'farm_manager_id'=>'1',
            'pond_id'=>'1',
            'days_of_culture'=>'0',
        ]);
        DB::unprepared("UPDATE si_animal_harvest SET id=0");
        DB::unprepared("ALTER TABLE si_animal_harvest AUTO_INCREMENT=1;");
    }
}
