<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RelSiFarmClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rel_si_farm_client')->insert([
            'si_farm_id'=>'1',
            'client_id'=>'2',
        ]);
        DB::table('rel_si_farm_client')->insert([
            'si_farm_id'=>'2',
            'client_id'=>'8',
        ]);
        DB::table('rel_si_farm_client')->insert([
            'si_farm_id'=>'2',
            'client_id'=>'6',
        ]);
    }
}
