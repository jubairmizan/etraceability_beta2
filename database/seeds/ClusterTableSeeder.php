<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClusterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('clusters')->truncate();
        DB::table('clusters')->insert([
            'id' => '0',
            'name'=>'N/A',
            'cluster_manager_id'=>'1',
            'division_id'=>4,
            'district_id'=>39,
            'upazila_id'=>335,
            'union_id'=>4548
        ]);
        DB::unprepared("UPDATE clusters SET id=0");
        DB::unprepared("ALTER TABLE clusters AUTO_INCREMENT=1;");
        DB::table('clusters')->insert([
            'name'=>'Ratankhali',
            'cluster_manager_id'=>'5',
            'division_id'=>4,
            'district_id'=>39,
            'upazila_id'=>335,
            'union_id'=>4548,
            'post_office'=>'Dumuria',
            'post_code'=>'324',
            'status'=>1,
        ]);
    }
}
