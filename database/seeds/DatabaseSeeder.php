<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FarmingTypeTableSeeder::class);
        $this->call(UserTypeTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(HatcheryTableSeeder::class);
        $this->call(RelHatcheryClientSeeder::class);
        $this->call(SiFarmTableSeeder::class);
        $this->call(RelSiFarmClientSeeder::class);
        $this->call(SiPondTableSeeder::class);
        $this->call(RelSiPondManagerSeeder::class);
        $this->call(ClusterTableSeeder::class);
        $this->call(RelClusterClientSeeder::class);
        $this->call(ClusterPondTableSeeder::class);
        $this->call(RelClusterPondManagerSeeder::class);
        $this->call(InventoryBrandSeeder::class);
        $this->call(InventoryTypeSeeder::class);
        $this->call(InventorySeeder::class);
        $this->call(SiPLStockTableSeeder::class);
        $this->call(ClusterPLStockTableSeeder::class);
        $this->call(SiPondManagementSeeder::class);
        $this->call(ClusterPondManagementSeeder::class);
        $this->call(SiAnimalHarvestSeeder::class);
        $this->call(ClusterAnimalHarvestSeeder::class);
        $this->call(WfOrganizationTableSeeder::class);
        $this->call(WfFarmTableSeeder::class);
        $this->call(WfCultivationPeriodTableSeeder::class);
        $this->call(WfCultivationProcessTableSeeder::class);
        $this->call(WfTypesOfFishTableSeeder::class);   
        $this->call(WfPondBlocksTableSeeder::class);
        $this->call(WfPondsTableSeeder::class);
        $this->call(WfRelFish::class);   
        $this->call(WfDuringSeeder::class);
        $this->call(WfHarvestTableSeeder::class);
    }
}
