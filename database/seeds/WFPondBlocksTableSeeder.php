<?php

use Illuminate\Database\Seeder;

class WfPondBlocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wf_pond_blocks')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Block A'
        ]);
        DB::table('wf_pond_blocks')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Block B'
        ]);
        DB::table('wf_pond_blocks')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Block C'
        ]);
        DB::table('wf_pond_blocks')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Block D'
        ]);
        DB::table('wf_pond_blocks')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Block E'
        ]);
        DB::table('wf_pond_blocks')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Block F'
        ]);
        DB::table('wf_pond_blocks')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Block G'
        ]);
        DB::table('wf_pond_blocks')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Block Prince Kamal'
        ]);
        DB::table('wf_pond_blocks')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Block North Nursery'
        ]);
        DB::table('wf_pond_blocks')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Block East Nursery'
        ]);
    }
}
