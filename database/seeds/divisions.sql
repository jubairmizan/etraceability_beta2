INSERT INTO `divisions` (`id`, `bn_name`, `name`) VALUES
(1, 'বরিশাল','Barisal'),
(2, 'চট্টগ্রাম', 'Chittagong'),
(3, 'ঢাকা', 'Dhaka'),
(4, 'খুলনা', 'Khulna'),
(5, 'রাজশাহী', 'Rajshahi'),
(6, 'রংপুর', 'Rangpur'),
(7, 'সিলেট', 'Sylhet');