<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WfFarmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wf_farms')->insert([
            'name'=>'Globe',
            'org_id'=>1,
            'status'=>1
        ]);
    }
}
