<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WfHarvestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('wf_harvest')->truncate();
        DB::table('wf_harvest')->insert([
            'id' => '0',
            'org_id'=>'1',
            'user_id'=>'1',
            'farm_id'=>'1',
            'block_id'=>'1',
            'pond_id'=>'1',
            'fish_id'=>'1',
            'days_of_culture'=>'0',
        ]);
        DB::unprepared("UPDATE wf_harvest SET id=0");
        DB::unprepared("ALTER TABLE wf_harvest AUTO_INCREMENT=1;");
    }
}
