<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClusterAnimalHarvestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('cluster_animal_harvest')->truncate();
        DB::table('cluster_animal_harvest')->insert([
            'id' => '0',
            'pond_id'=>'1',
            'cluster_id'=>'1',
            'cluster_manager_id'=>'1'
        ]);
        DB::unprepared("UPDATE cluster_animal_harvest SET id=0");
        DB::unprepared("ALTER TABLE cluster_animal_harvest AUTO_INCREMENT=1;");
    }
}
