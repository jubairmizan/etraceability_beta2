<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WfCultivationPeriodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wf_cultivation_periods')->insert([
            'org_id'=>1,
            'name'=>'3 Months in a year'
        ]);
        DB::table('wf_cultivation_periods')->insert([
            'org_id'=>1,
            'name'=>'6 Months in a year'
        ]);
        DB::table('wf_cultivation_periods')->insert([
            'org_id'=>1,
            'name'=>'9 Months in a year'
        ]);
        DB::table('wf_cultivation_periods')->insert([
            'org_id'=>1,
            'name'=>'12 Months in a year'
        ]);
    }
}
