<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WfTypesOfFishTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wf_type_of_fishes')->insert([
            'org_id'=>1,
            'name'=>'Rui'
        ]);
        DB::table('wf_type_of_fishes')->insert([
            'org_id'=>1,
            'name'=>'Telapiaya'
        ]);
        DB::table('wf_type_of_fishes')->insert([
            'org_id'=>1,
            'name'=>'Silver Corpe'
        ]);
        DB::table('wf_type_of_fishes')->insert([
            'org_id'=>1,
            'name'=>'Mregel'
        ]);
        DB::table('wf_type_of_fishes')->insert([
            'org_id'=>1,
            'name'=>'Kali Baoush'
        ]);
        DB::table('wf_type_of_fishes')->insert([
            'org_id'=>1,
            'name'=>'Grash Courp'
        ]);
        DB::table('wf_type_of_fishes')->insert([
            'org_id'=>1,
            'name'=>'Shorputi'
        ]);
        DB::table('wf_type_of_fishes')->insert([
            'org_id'=>1,
            'name'=>'Pangash'
        ]);
    }
}
