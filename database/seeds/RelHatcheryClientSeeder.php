<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RelHatcheryClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rel_hatchery_client')->insert([
            'hatchery_id'=>'1',
            'client_id'=>'2',
        ]);
        DB::table('rel_hatchery_client')->insert([
            'hatchery_id'=>'2',
            'client_id'=>'8',
        ]);
    }
}
