<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InventoryBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('inventory_brands')->truncate();
        DB::table('inventory_brands')->insert([
            'id' => '0',
            'name'=>'N\A',
        ]);
        DB::unprepared("UPDATE inventory_brands SET id=0");
        DB::unprepared("ALTER TABLE inventory_brands AUTO_INCREMENT=1;");
        DB::table('inventory_brands')->insert([
            'name'=>'INVE',
        ]);
        DB::table('inventory_brands')->insert([
            'name'=>'Uni President',
        ]);
        DB::table('inventory_brands')->insert([
            'name'=>'CP',
        ]);
        DB::table('inventory_brands')->insert([
            'name'=>'FishTech',
        ]);
        DB::table('inventory_brands')->insert([
            'name'=>'PVS',
        ]);
        DB::table('inventory_brands')->insert([
            'name'=>'Generic',
        ]);
    }
}
