<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RelClusterClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rel_cluster_client')->insert([
            'cluster_id'=>'1',
            'client_id'=>'2',
        ]);
        DB::table('rel_cluster_client')->insert([
            'cluster_id'=>'1',
            'client_id'=>'6',
        ]);
    }
}
