<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WfDuringSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('wf_during_enclosure')->truncate();
        DB::table('wf_during_enclosure')->insert([
            'id' => '0',
            'org_id'=>'1',
            'pond_id'=>'1',
            'farm_id'=>'1',
            'block_id'=>'1',
        ]);
        DB::unprepared("UPDATE wf_during_enclosure SET id=0");
        DB::unprepared("ALTER TABLE wf_during_enclosure AUTO_INCREMENT=1;");
    }
}
