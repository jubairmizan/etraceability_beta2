<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InventoryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inventory_types')->insert([
            'name'=>'Feeds',
        ]);
        DB::table('inventory_types')->insert([
            'name'=>'Probiotic',
        ]);
        DB::table('inventory_types')->insert([
            'name'=>'Minerals',
        ]);
        DB::table('inventory_types')->insert([
            'name'=>'Other',
        ]);
        DB::table('inventory_types')->insert([
            'name'=>'Probiotic-Additive',
        ]);
        DB::table('inventory_types')->insert([
            'name'=>'Chert',
        ]);
    }
}
