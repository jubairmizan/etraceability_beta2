<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClusterPondManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('cluster_pond_management')->truncate();
        DB::table('cluster_pond_management')->insert([
            'id' => '0',
            'pond_id'=>'1',
            'cluster_id'=>'1',
            'cluster_manager_id'=>'1'
        ]);
        DB::unprepared("UPDATE cluster_pond_management SET id=0");
        DB::unprepared("ALTER TABLE cluster_pond_management AUTO_INCREMENT=1;");
    }
}
