<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HatcheryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hatcheries')->insert([
            'name'=>'MKA Hatchery',
            'license_number'=>'1234555',
            'division_id'=>1,
            'district_id'=>1,
            'upazila_id'=>1,
            'union_id'=>1,
            'status'=>1,
        ]);
        DB::table('hatcheries')->insert([
            'name'=>'Desh Bangla Hatchery',
            'license_number'=>'3232323',
            'division_id'=>4,
            'district_id'=>35,
            'upazila_id'=>205,
            'union_id'=>2543,
            'status'=>1,
        ]);
    }
}
