<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FarmingTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('farming_types')->insert([
            'name'=>'Cluster Farming'
        ]);
        DB::table('farming_types')->insert([
            'name'=>'Semi-Intensive'
        ]);
        DB::table('farming_types')->insert([
            'name'=>'Advanced Traditional'
        ]);
    }
}
