<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiPondTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP1123456789',
            'name'=>'1',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP2123456789',
            'name'=>'2',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP3123456789',
            'name'=>'3',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP4123456789',
            'name'=>'4',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP5123456789',
            'name'=>'5',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP6123456789',
            'name'=>'6',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP7123456789',
            'name'=>'7',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP8123456789',
            'name'=>'8',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP9123456789',
            'name'=>'9',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP1023456789',
            'name'=>'10',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP0123456789',
            'name'=>'11',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP1223456789',
            'name'=>'12',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP1123456700',
            'name'=>'13',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP2123456701',
            'name'=>'14',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP3123456702',
            'name'=>'15',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP4123456703',
            'name'=>'16',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP5123456704',
            'name'=>'17',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP6123456705',
            'name'=>'18',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'0',
            'name'=>'Not needed',
            'si_farm_id'=>'1',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP1120000700',
            'name'=>'01',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP2120000701',
            'name'=>'02',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP3120000702',
            'name'=>'03',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP4120000703',
            'name'=>'4A',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP5120000704',
            'name'=>'4B',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP6120000705',
            'name'=>'05',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP7120000706',
            'name'=>'06',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP1120000707',
            'name'=>'07',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP2120000708',
            'name'=>'08',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP3120000709',
            'name'=>'9A',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP4120000710',
            'name'=>'9B',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP5120000711',
            'name'=>'10',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP6120000712',
            'name'=>'11',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP6120000713',
            'name'=>'12',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP6120000714',
            'name'=>'13',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP6120000715',
            'name'=>'14',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP6120000716',
            'name'=>'15',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP6120000717',
            'name'=>'16',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP6120000718',
            'name'=>'17',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
        DB::table('si_ponds')->insert([
            'pond_id'=>'SP7120000719',
            'name'=>'18',
            'si_farm_id'=>'2',
            'area'=>'120',
            'depth'=>1,
            'type_of_soil'=>'Aintel',
            'farming_type_id'=>'2',
            'farming_cycle'=>'Yearly',
            'status'=>1,
        ]);
    }
}
