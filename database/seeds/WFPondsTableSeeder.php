<?php

use Illuminate\Database\Seeder;

class WfPondsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Block A Pond 13 Canel 1
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0001',
            'name'=>'Pond A-01',
            'pond_number'=>'01',
            'area'=>'2.8 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0002',
            'name'=>'Pond A-02',
            'pond_number'=>'02',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0003',
            'name'=>'Pond A-03',
            'pond_number'=>'03',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0004',
            'name'=>'Pond A-04',
            'pond_number'=>'04',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0005',
            'name'=>'Pond A-05',
            'pond_number'=>'05',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0006',
            'name'=>'Pond A-06',
            'pond_number'=>'06',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0007',
            'name'=>'Pond A-07',
            'pond_number'=>'07',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0008',
            'name'=>'Pond A-08',
            'pond_number'=>'08',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0009',
            'name'=>'Pond A-09',
            'pond_number'=>'09',
            'area'=>'2.8 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0010',
            'name'=>'Pond A-10',
            'pond_number'=>'10',
            'area'=>'1.7 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0011',
            'name'=>'Pond A-11',
            'pond_number'=>'11',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0012',
            'name'=>'Pond A-12',
            'pond_number'=>'12',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0013',
            'name'=>'Pond A-13',
            'pond_number'=>'13',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>1,
            'pond_id'=>'0014',
            'name'=>'Canal 01',
            'pond_number'=>'14',
            'area'=>'6 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0015',
            'name'=>'Pond B-01',
            'pond_number'=>'01',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0016',
            'name'=>'Pond B-02',
            'pond_number'=>'02',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0017',
            'name'=>'Pond B-03',
            'pond_number'=>'03',
            'area'=>'0.8 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0018',
            'name'=>'Pond B-04',
            'pond_number'=>'04',
            'area'=>'0.8 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0019',
            'name'=>'Pond B-05',
            'pond_number'=>'05',
            'area'=>'0.8 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0020',
            'name'=>'Pond B-06',
            'pond_number'=>'06',
            'area'=>'0.8 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0021',
            'name'=>'Pond B-7.1',
            'pond_number'=>'07',
            'area'=>'0.8 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0022',
            'name'=>'Pond B-7.2',
            'pond_number'=>'08',
            'area'=>'4 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0023',
            'name'=>'Pond B-08',
            'pond_number'=>'09',
            'area'=>'3 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0024',
            'name'=>'Pond B-09',
            'pond_number'=>'10',
            'area'=>'2.8 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0025',
            'name'=>'Pond B-10',
            'pond_number'=>'11',
            'area'=>'3 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0026',
            'name'=>'Pond B-11',
            'pond_number'=>'12',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0027',
            'name'=>'Pond B-12',
            'pond_number'=>'13',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0028',
            'name'=>'Pond B-13',
            'pond_number'=>'14',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0029',
            'name'=>'Pond B-14',
            'pond_number'=>'15',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0030',
            'name'=>'Pond B-15',
            'pond_number'=>'16',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0031',
            'name'=>'Pond B-16',
            'pond_number'=>'17',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0032',
            'name'=>'Pond B-17',
            'pond_number'=>'18',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0033',
            'name'=>'Pond B-18',
            'pond_number'=>'19',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0034',
            'name'=>'Pond B-19',
            'pond_number'=>'20',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>2,
            'pond_id'=>'0035',
            'name'=>'Pond B-20',
            'pond_number'=>'21',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0036',
            'name'=>'Pond C-01',
            'pond_number'=>'01',
            'area'=>'1.8 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0037',
            'name'=>'Pond C-02',
            'pond_number'=>'02',
            'area'=>'1.8 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0038',
            'name'=>'Pond C-03',
            'pond_number'=>'03',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0039',
            'name'=>'Pond C-04',
            'pond_number'=>'04',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0040',
            'name'=>'Pond C-05',
            'pond_number'=>'05',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0041',
            'name'=>'Pond C-06',
            'pond_number'=>'06',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0042',
            'name'=>'Pond C-07',
            'pond_number'=>'07',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0043',
            'name'=>'Pond C-08',
            'pond_number'=>'08',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0044',
            'name'=>'Pond C-09',
            'pond_number'=>'09',
            'area'=>'1.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0045',
            'name'=>'Pond C-10',
            'pond_number'=>'10',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0046',
            'name'=>'Pond C-11',
            'pond_number'=>'11',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0047',
            'name'=>'Pond C-12',
            'pond_number'=>'12',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0048',
            'name'=>'Pond C-13',
            'pond_number'=>'13',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0049',
            'name'=>'Pond C-14',
            'pond_number'=>'14',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0050',
            'name'=>'Pond C-15',
            'pond_number'=>'15',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>3,
            'pond_id'=>'0051',
            'name'=>'Canal 2',
            'pond_number'=>'16',
            'area'=>'6 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>4,
            'pond_id'=>'0052',
            'name'=>'Pond D-01',
            'pond_number'=>'01',
            'area'=>'2.7 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>4,
            'pond_id'=>'0053',
            'name'=>'Pond D-02',
            'pond_number'=>'02',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>4,
            'pond_id'=>'0054',
            'name'=>'Pond D-03',
            'pond_number'=>'03',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>4,
            'pond_id'=>'0055',
            'name'=>'Pond D-04',
            'pond_number'=>'04',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>4,
            'pond_id'=>'0056',
            'name'=>'Pond D-05',
            'pond_number'=>'05',
            'area'=>'1.6 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>4,
            'pond_id'=>'0057',
            'name'=>'Pond D-06',
            'pond_number'=>'06',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>4,
            'pond_id'=>'0058',
            'name'=>'Pond D-07',
            'pond_number'=>'07',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>4,
            'pond_id'=>'0059',
            'name'=>'Pond D-08',
            'pond_number'=>'08',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>4,
            'pond_id'=>'0060',
            'name'=>'Pond D-09',
            'pond_number'=>'09',
            'area'=>'2.9 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>5,
            'pond_id'=>'0061',
            'name'=>'Pond E-01',
            'pond_number'=>'01',
            'area'=>'4.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>5,
            'pond_id'=>'0062',
            'name'=>'Pond E-02',
            'pond_number'=>'02',
            'area'=>'3.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>5,
            'pond_id'=>'0063',
            'name'=>'Pond E-03',
            'pond_number'=>'03',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>5,
            'pond_id'=>'0064',
            'name'=>'Pond E-04',
            'pond_number'=>'04',
            'area'=>'1.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>5,
            'pond_id'=>'0065',
            'name'=>'Pond E-05',
            'pond_number'=>'05',
            'area'=>'4.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>5,
            'pond_id'=>'0066',
            'name'=>'Pond E-06',
            'pond_number'=>'06',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>5,
            'pond_id'=>'0067',
            'name'=>'Pond E-07',
            'pond_number'=>'07',
            'area'=>'1.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>5,
            'pond_id'=>'0068',
            'name'=>'Pond E-08',
            'pond_number'=>'08',
            'area'=>'2 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>5,
            'pond_id'=>'0069',
            'name'=>'Pond E-09',
            'pond_number'=>'09',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>5,
            'pond_id'=>'0070',
            'name'=>'Canal 3',
            'pond_number'=>'10',
            'area'=>'1.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>6,
            'pond_id'=>'0071',
            'name'=>'Pond F-01',
            'pond_number'=>'01',
            'area'=>'9 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>7,
            'pond_id'=>'0072',
            'name'=>'Pond G-01',
            'pond_number'=>'01',
            'area'=>'1.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>7,
            'pond_id'=>'0073',
            'name'=>'Pond G-02',
            'pond_number'=>'02',
            'area'=>'1 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>7,
            'pond_id'=>'0074',
            'name'=>'Pond G-03',
            'pond_number'=>'03',
            'area'=>'2.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>7,
            'pond_id'=>'0075',
            'name'=>'Pond G-04',
            'pond_number'=>'04',
            'area'=>'3.5 acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>7,
            'pond_id'=>'0076',
            'name'=>'Pond G-05',
            'pond_number'=>'05',
            'area'=>'40 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>7,
            'pond_id'=>'0077',
            'name'=>'Pond G-06',
            'pond_number'=>'06',
            'area'=>'20 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>7,
            'pond_id'=>'0078',
            'name'=>'Pond G-07',
            'pond_number'=>'07',
            'area'=>'30 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>7,
            'pond_id'=>'0079',
            'name'=>'Pond G-08',
            'pond_number'=>'08',
            'area'=>'20 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>7,
            'pond_id'=>'0080',
            'name'=>'Pond G-09',
            'pond_number'=>'09',
            'area'=>'40 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>8,
            'pond_id'=>'0081',
            'name'=>'Pond Prince Kamal',
            'pond_number'=>'01',
            'area'=>'10 Acre',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>9,
            'pond_id'=>'0082',
            'name'=>'Pond NN-01',
            'pond_number'=>'01',
            'area'=>'40 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>9,
            'pond_id'=>'0083',
            'name'=>'Pond NN-02',
            'pond_number'=>'02',
            'area'=>'40 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>9,
            'pond_id'=>'0084',
            'name'=>'Pond NN-03',
            'pond_number'=>'03',
            'area'=>'30 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>9,
            'pond_id'=>'0085',
            'name'=>'Pond NN-04',
            'pond_number'=>'04',
            'area'=>'30 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>9,
            'pond_id'=>'0086',
            'name'=>'Pond NN-05',
            'pond_number'=>'05',
            'area'=>'40 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>10,
            'pond_id'=>'0087',
            'name'=>'Pond EN-01',
            'pond_number'=>'01',
            'area'=>'30 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>10,
            'pond_id'=>'0088',
            'name'=>'Pond EN-02',
            'pond_number'=>'02',
            'area'=>'30 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>10,
            'pond_id'=>'0089',
            'name'=>'Pond EN-03',
            'pond_number'=>'03',
            'area'=>'30 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>10,
            'pond_id'=>'0090',
            'name'=>'Pond EN-04',
            'pond_number'=>'04',
            'area'=>'30 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>10,
            'pond_id'=>'0091',
            'name'=>'Pond EN-05',
            'pond_number'=>'05',
            'area'=>'40 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>10,
            'pond_id'=>'0092',
            'name'=>'Pond EN-06',
            'pond_number'=>'06',
            'area'=>'50 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>10,
            'pond_id'=>'0093',
            'name'=>'Pond EN-07',
            'pond_number'=>'07',
            'area'=>'40 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>10,
            'pond_id'=>'0094',
            'name'=>'Pond EN-08',
            'pond_number'=>'08',
            'area'=>'50 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
        DB::table('wf_ponds')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'block_id'=>10,
            'pond_id'=>'0095',
            'name'=>'Pond EN-09',
            'pond_number'=>'09',
            'area'=>'50 Decimal',
            'farming_type_id'=>3,
            'culvation_process_id'=>1,
            'cultivation_period_id'=>1,
        ]);
    }
}
