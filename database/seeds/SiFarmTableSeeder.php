<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiFarmTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('si_farms')->truncate();
        DB::table('si_farms')->insert([
            'id' => '0',
            'name'=>'N/A',
            'license_number'=>'00000',
            'area'=>'120',
            'farm_manager_id'=>'1',
            'division_id'=>1,
            'district_id'=>1,
            'upazila_id'=>1,
            'union_id'=>1,
        ]);
        DB::unprepared("UPDATE si_farms SET id=0");
        DB::unprepared("ALTER TABLE si_farms AUTO_INCREMENT=1;");
        DB::table('si_farms')->insert([
            'name'=>'MKA Shrimp Farm',
            'license_number'=>'1234555',
            'area'=>'120',
            'farm_manager_id'=>'4',
            'division_id'=>1,
            'district_id'=>1,
            'upazila_id'=>1,
            'union_id'=>1,
            'status'=>1,
        ]);
        DB::table('si_farms')->insert([
            'name'=>'Fahim Aqua Park',
            'license_number'=>'Fahim Aqua Park',
            'area'=>'120',
            'farm_manager_id'=>'4',
            'division_id'=>4,
            'district_id'=>35,
            'upazila_id'=>305,
            'union_id'=>2543,
            'status'=>1,
        ]);
    }
}
