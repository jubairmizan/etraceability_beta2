<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WfCultivationProcessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wf_cultivation_process')->insert([
            'org_id'=>1,
            'farm_id'=>1,
            'name'=>'Mixed'
        ]);
    }
}
