<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_types')->insert([
            'name'=>'Admin'
        ]);
        DB::table('user_types')->insert([
            'name'=>'Client'
        ]);
        DB::table('user_types')->insert([
            'name'=>'Buyer'
        ]);
        DB::table('user_types')->insert([
            'name'=>'Farmer'
        ]);
        DB::table('user_types')->insert([
            'name'=>'Hatchery Manager'
        ]);
        DB::table('user_types')->insert([
            'name'=>'Farm Manager'
        ]);
        DB::table('user_types')->insert([
            'name'=>'Cluster Manager'
        ]);
        DB::table('user_types')->insert([
            'name'=>'Collection Center Manager'
        ]);
        DB::table('user_types')->insert([
            'name'=>'Processing Plant Manager'
        ]);
        DB::table('user_types')->insert([
            'name'=>'Pond Supervisor'
        ]);
    }
}
