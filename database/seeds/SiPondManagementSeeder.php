<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SiPondManagementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('si_pond_management')->truncate();
        DB::table('si_pond_management')->insert([
            'id' => '0',
            'pond_id'=>'1',
            'farm_id'=>'1',
            'farm_manager_id'=>'1'
        ]);
        DB::unprepared("UPDATE si_pond_management SET id=0");
        DB::unprepared("ALTER TABLE si_pond_management AUTO_INCREMENT=1;");
    }
}
