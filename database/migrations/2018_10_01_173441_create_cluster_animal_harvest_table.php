<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClusterAnimalHarvestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cluster_animal_harvest', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cluster_id');
            $table->foreign('cluster_id')->references('id')->on('clusters');
            $table->unsignedInteger('cluster_manager_id');
            $table->foreign('cluster_manager_id')->references('id')->on('users');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('cluster_ponds');
            $table->enum('harvest_type',['Full','Partial'])->default('Full');
            $table->unsignedInteger('days_of_culture');
            $table->unsignedInteger('crop_number')->default(0);
            $table->float('total_feed')->nullable();
            $table->float('total_biomass')->nullable();
            $table->float('total_quantity')->nullable();
            $table->float('total_price')->nullable();
            $table->float('fcr')->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cluster_animal_harvest');
    }
}
