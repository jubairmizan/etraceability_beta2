<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_management', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inventory_id');
            $table->foreign('inventory_id')->references('id')->on('inventories');
            $table->unsignedInteger('client_id');
            $table->foreign('client_id')->references('id')->on('users');
            $table->unsignedInteger('org_id')->default(0);
            $table->unsignedInteger('si_farm_id');
            $table->foreign('si_farm_id')->references('id')->on('si_farms');
            $table->unsignedInteger('cluster_id');
            $table->foreign('cluster_id')->references('id')->on('clusters');
            $table->float('quantity');
            $table->float('remaining_quantity')->default('0');
            $table->float('price');
            $table->float('total_price');
            $table->float('total_price');
            $table->enum('unit',['KG','G','L','Bundle','Pieces'])->default('KG');
            $table->string('item_avg_consumption')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_management');
    }
}
