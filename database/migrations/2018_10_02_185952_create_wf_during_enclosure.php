<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWfDuringEnclosure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_during_enclosure', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('org_id');
            $table->foreign('org_id')->references('id')->on('wf_organizations');
            $table->unsignedInteger('farm_id');
            $table->foreign('farm_id')->references('id')->on('wf_farms');
            $table->unsignedInteger('block_id');
            $table->foreign('block_id')->references('id')->on('wf_pond_blocks');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('wf_ponds');
            $table->unsignedInteger('inventory_management_id');
            $table->foreign('inventory_management_id')->references('id')->on('inventory_management');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_during_enclosure');
    }
}
