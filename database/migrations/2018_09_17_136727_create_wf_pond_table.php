<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWFPondTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_ponds', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('org_id');
            $table->foreign('org_id')->references('id')->on('wf_organizations');
            $table->unsignedInteger('farm_id');
            $table->foreign('farm_id')->references('id')->on('wf_farms');
            $table->unsignedInteger('block_id');
            $table->foreign('block_id')->references('id')->on('wf_pond_blocks');
            $table->unsignedInteger('fisheries_officer_id')->nullable()->comment('Fisheries Officer Name');;
            $table->foreign('fisheries_officer_id')->references('id')->on('users');
            $table->unsignedInteger('pond_superviser_id')->nullable()->comment('Pond Manager');
            $table->foreign('pond_superviser_id')->references('id')->on('users');
            $table->string('pond_id')->unique();
            $table->string('name')->nullable();
            $table->string('pond_number');
            $table->string('area')->nullable();
            $table->date('establishment_date')->nullable();
            $table->unsignedInteger('farming_type_id')->comment('Cultivation Types');
            $table->foreign('farming_type_id')->references('id')->on('farming_types');
            $table->unsignedInteger('culvation_process_id')->comment('Cultivation Process');
            $table->foreign('culvation_process_id')->references('id')->on('wf_cultivation_process');
            $table->boolean('flood_free')->nullable()->default('1');
            $table->unsignedInteger('cultivation_period_id');
            $table->foreign('cultivation_period_id')->references('id')->on('wf_cultivation_periods');
            $table->boolean('animal_protection_facilities')->default('1');
            $table->boolean('weed_repression')->default('1');
            $table->boolean('border_repayment')->default('1');
            $table->boolean('remove_black_mud')->default('1');
            $table->boolean('dry_the_pond')->default('1');
            $table->boolean('soil_cultivation')->default('1');
            $table->boolean('bleaching_usage')->default('0');
            $table->boolean('lime_usage')->default('0');
            $table->string('water_depth')->nullable();
            $table->boolean('water_drainage')->default('1');
            $table->boolean('water_quality_test')->default('1');
            $table->string('food_supplies')->nullable();
            $table->string('water_source')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_ponds');
    }
}
