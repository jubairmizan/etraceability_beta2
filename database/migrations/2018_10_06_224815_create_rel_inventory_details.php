<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelInventoryDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rel_inventory_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inventory_details_id');
            $table->foreign('inventory_details_id')->references('id')->on('inventory_used_in_water_details');
            $table->unsignedInteger('inventory_management_id');
            $table->foreign('inventory_management_id')->references('id')->on('inventory_management');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rel_inventory_details');
    }
}
