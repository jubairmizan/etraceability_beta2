<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClusterPondManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cluster_pond_management', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('cluster_ponds');
            $table->unsignedInteger('cluster_id');
            $table->foreign('cluster_id')->references('id')->on('clusters');
            $table->unsignedInteger('cluster_manager_id');
            $table->foreign('cluster_manager_id')->references('id')->on('users');
            $table->unsignedInteger('harvest_tracking_id')->default(0);
            $table->unsignedInteger('harvest_counter')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cluster_pond_management');
    }
}
