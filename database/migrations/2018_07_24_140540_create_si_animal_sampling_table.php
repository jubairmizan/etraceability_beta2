<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiAnimalSamplingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('si_animal_sampling', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('hatchery_id');
            $table->foreign('hatchery_id')->references('id')->on('hatcheries');
            $table->unsignedInteger('farm_id');
            $table->foreign('farm_id')->references('id')->on('si_farms');
            $table->unsignedInteger('farm_manager_id');
            $table->foreign('farm_manager_id')->references('id')->on('users');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('si_ponds');
            $table->float('total_weight')->nullable();
            $table->unsignedInteger('number_of_sample')->nullable();
            $table->float('average_body_weight')->nullable();
            $table->float('current_biomass')->nullable();
            $table->unsignedInteger('survival_rate')->nullable();
            $table->unsignedInteger('days_of_culture')->default(0);
            $table->float('stocked_pl_quantity')->nullable();
            $table->float('stocked_pl_age')->nullable();
            $table->float('stocked_pl_price')->nullable();
            $table->foreign('si_pl_stock_id')->references('id')->on('si_pl_stock');
            $table->unsignedInteger('si_pl_stock_id');
            $table->unsignedInteger('harvest_tracking_id')->default(0);
            $table->unsignedInteger('harvest_counter')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('si_animal_sampling');
    }
}
