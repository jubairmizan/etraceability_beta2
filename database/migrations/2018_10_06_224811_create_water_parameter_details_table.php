<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaterParameterDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('water_parameter_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('si_pond_management_id');
            $table->foreign('si_pond_management_id')->references('id')->on('si_pond_management');
            $table->unsignedInteger('ct_pond_management_id');
            $table->foreign('ct_pond_management_id')->references('id')->on('cluster_pond_management');
            $table->unsignedInteger('wf_during_enclosure_id');
            $table->foreign('wf_during_enclosure_id')->references('id')->on('wf_during_enclosure');
            $table->float('alkalinity_hco3')->nullable();
            $table->float('carbonate_co3')->nullable();
            $table->float('calcium_ca')->nullable();
            $table->float('chlorine_cl2')->nullable();
            $table->float('magnesium_mg')->nullable();
            $table->float('potassium_k')->nullable();
            $table->float('ammonia_nh3')->nullable();
            $table->float('nitrate_no3')->nullable();
            $table->float('ammonium_nh4')->nullable();
            $table->float('salinity')->nullable();
            $table->string('transparency')->nullable();
            $table->string('plankton_type')->nullable();
            $table->float('green')->nullable();
            $table->float('yellow')->nullable();
            $table->float('vibrio_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('water_parameter_details');
    }
}
