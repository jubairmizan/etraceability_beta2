<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWfNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('org_id');
            $table->foreign('org_id')->references('id')->on('wf_organizations');
            $table->unsignedInteger('block_id');
            $table->foreign('block_id')->references('id')->on('wf_pond_blocks');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('wf_ponds');
            $table->enum('notificationType',['0','1','2','3'])->comment('1 = Invest, 2= Harvest')->default('0');
            $table->boolean('notification')->comment('0 = Notification End, 1= Notification Create');
            $table->unsignedInteger('inventory_used_in_water_details_id')->default('0');
            $table->unsignedInteger('harvest_id')->default('0');
            $table->unsignedInteger('created_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_notifications');
    }
}
