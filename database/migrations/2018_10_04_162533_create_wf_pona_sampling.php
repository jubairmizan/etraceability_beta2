<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWfPonaSampling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_pona_sampling', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('org_id');
            $table->foreign('org_id')->references('id')->on('wf_organizations');
            $table->unsignedInteger('farm_id');
            $table->foreign('farm_id')->references('id')->on('wf_farms');
            $table->unsignedInteger('block_id');
            $table->foreign('block_id')->references('id')->on('wf_pond_blocks');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('wf_ponds');
            $table->unsignedInteger('farm_manager_id');
            $table->foreign('farm_manager_id')->references('id')->on('users');
            $table->unsignedInteger('fish_id');
            $table->foreign('fish_id')->references('id')->on('wf_type_of_fishes');
            $table->string('number_of_sample')->nullable();
            $table->string('average_body_weight')->nullable();
            $table->string('current_biomass')->nullable();
            $table->string('survival_rate')->nullable();
            $table->string('total_weight')->nullable();
            $table->string('pl_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_pona_sampling');
    }
}
