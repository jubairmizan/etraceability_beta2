<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClusterAnimalSamplingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cluster_animal_sampling', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('hatchery_id');
            $table->foreign('hatchery_id')->references('id')->on('hatcheries');
            $table->unsignedInteger('cluster_id');
            $table->foreign('cluster_id')->references('id')->on('clusters');
            $table->unsignedInteger('cluster_manager_id');
            $table->foreign('cluster_manager_id')->references('id')->on('users');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('cluster_ponds');
            $table->float('total_weight')->nullable();
            $table->unsignedInteger('number_of_sample')->nullable();
            $table->float('average_body_weight')->nullable();
            $table->float('current_biomass')->nullable();
            $table->unsignedInteger('survival_rate')->nullable();
            $table->unsignedInteger('days_of_culture')->default(0);
            $table->float('stocked_pl_quantity')->nullable();
            $table->float('stocked_pl_age')->nullable();
            $table->float('stocked_pl_price')->nullable();
            $table->foreign('cluster_pl_stock_id')->references('id')->on('cluster_pl_stock');
            $table->unsignedInteger('cluster_pl_stock_id');
            $table->unsignedInteger('harvest_tracking_id')->default(0);
            $table->unsignedInteger('harvest_counter')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cluster_animal_sampling');
    }
}
