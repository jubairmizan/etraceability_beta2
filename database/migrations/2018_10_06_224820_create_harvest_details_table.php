<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHarvestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harvest_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('si_harvest_id');
            $table->foreign('si_harvest_id')->references('id')->on('si_animal_harvest');
            $table->unsignedInteger('ct_harvest_id');
            $table->foreign('ct_harvest_id')->references('id')->on('cluster_animal_harvest');
            $table->unsignedInteger('wf_harvest_id');
            $table->foreign('wf_harvest_id')->references('id')->on('wf_during_enclosure');
            $table->string('grade')->nullable();
            $table->float('quantity');
            $table->enum('unit',['KG','G','Pieces','Bundle'])->default('KG');
            $table->float('unit_price')->default(0);
            $table->float('total_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harvest_details');
    }
}
