<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelWFFarmManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_rel_farm_manager', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('org_id');
            $table->foreign('org_id')->references('id')->on('wf_organizations');
            $table->unsignedInteger('farm_id');
            $table->foreign('farm_id')->references('id')->on('wf_farms');
            $table->unsignedInteger('farm_manager_id')->comment('Fisheries Officer Name');
            $table->foreign('farm_manager_id')->references('id')->on('users');
            $table->tinyInteger('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_rel_farm_manager');
    }
}
