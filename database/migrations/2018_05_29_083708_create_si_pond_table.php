<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiPondTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('si_ponds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pond_id')->unique();
            $table->string('name');
            $table->unsignedInteger('si_farm_id');
            $table->foreign('si_farm_id')->references('id')->on('si_farms');
            $table->string('length')->nullable();
            $table->string('width')->nullable();
            $table->string('area');
            $table->string('depth');
            $table->enum('type_of_soil',['Aintel', 'Doash', 'Bele', 'Poli']);
            $table->unsignedInteger('farming_type_id');
            $table->foreign('farming_type_id')->references('id')->on('farming_types');
            $table->enum('farming_cycle',['Yearly', 'Half-Yearly']);
            $table->tinyInteger('reserve_status')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('si_ponds');
    }
}
