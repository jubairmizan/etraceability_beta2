<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WfHarvest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_harvest', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('org_id');
            $table->foreign('org_id')->references('id')->on('wf_organizations');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('farm_id');
            $table->foreign('farm_id')->references('id')->on('wf_farms');
            $table->unsignedInteger('block_id');
            $table->foreign('block_id')->references('id')->on('wf_pond_blocks');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('wf_ponds');;
            $table->unsignedInteger('fish_id');
            $table->foreign('fish_id')->references('id')->on('wf_type_of_fishes');
            $table->enum('harvest_type',['Full','Partial'])->default('Full');
            $table->unsignedInteger('days_of_culture');
            $table->unsignedInteger('crop_number')->default(1);
            $table->float('grade_type')->nullable();
            $table->float('unit_price')->nullable();
            $table->string('unit')->nullable();
            $table->float('total_feed')->nullable();
            $table->float('total_biomass')->nullable();
            $table->float('total_quantity')->nullable();
            $table->float('total_price')->nullable();
            $table->text('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_harvest');
    }
}
