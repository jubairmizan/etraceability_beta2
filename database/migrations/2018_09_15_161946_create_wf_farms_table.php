<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWFFarmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_farms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('org_id');
            $table->foreign('org_id')->references('id')->on('wf_organizations');
            /*$table->unsignedInteger('farm_manager_id')->comment('Fisheries Officer Name');
            $table->foreign('farm_manager_id')->references('id')->on('users');*/
            $table->string('area')->nullable();
            $table->unsignedInteger('division_id')->nullable();
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->unsignedInteger('district_id')->nullable();
            $table->foreign('district_id')->references('id')->on('districts');
            $table->unsignedInteger('upazila_id')->nullable();
            $table->foreign('upazila_id')->references('id')->on('upazilas');
            $table->unsignedInteger('union_id')->nullable();
            $table->foreign('union_id')->references('id')->on('unions');
            $table->tinyInteger('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_farms');
    }
}
