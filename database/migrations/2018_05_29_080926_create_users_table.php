<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cat_id')->default(0);
            $table->unsignedInteger('type_id');
            $table->foreign('type_id')->references('id')->on('user_types');
            $table->unsignedInteger('farming_type_id')->default(0);
            $table->unsignedInteger('org_id')->default(0);
            $table->string('name');
            $table->string('user_id')->unique();
            $table->string('password');
            $table->string('email')->unique();
            $table->string('cell_no')->unique();
            $table->string('nid')->unique()->nullable();
            $table->enum('gender',['male','female','other']);
            $table->date('dob')->nullable();
            $table->unsignedInteger('division_id');
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->unsignedInteger('district_id');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->unsignedInteger('upazila_id');
            $table->foreign('upazila_id')->references('id')->on('upazilas');
            $table->unsignedInteger('union_id');
            $table->foreign('union_id')->references('id')->on('unions');
            $table->string('mouja')->nullable();
            $table->string('post_office')->nullable();
            $table->string('post_code')->nullable();
            $table->string('ward_no')->nullable();
            $table->string('photo')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
