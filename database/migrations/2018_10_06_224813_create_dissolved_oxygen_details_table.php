<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDissolvedOxygenDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dissolved_oxygen_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('si_pond_management_id');
            $table->foreign('si_pond_management_id')->references('id')->on('si_pond_management');
            $table->unsignedInteger('ct_pond_management_id');
            $table->foreign('ct_pond_management_id')->references('id')->on('cluster_pond_management');
            $table->unsignedInteger('wf_during_enclosure_id');
            $table->foreign('wf_during_enclosure_id')->references('id')->on('wf_during_enclosure');
            $table->float('quantity');
            $table->enum('timing',['am','pm']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dissolved_oxygen_details');
    }
}
