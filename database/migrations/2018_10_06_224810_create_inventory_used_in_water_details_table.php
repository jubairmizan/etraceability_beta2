<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryUsedInWaterDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_used_in_water_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('si_pond_management_id');
            $table->foreign('si_pond_management_id')->references('id')->on('si_pond_management');
            $table->unsignedInteger('ct_pond_management_id');
            $table->foreign('ct_pond_management_id')->references('id')->on('cluster_pond_management');
            $table->unsignedInteger('wf_during_enclosure_id');
            $table->foreign('wf_during_enclosure_id')->references('id')->on('wf_during_enclosure');
            $table->unsignedInteger('inventory_id');
            $table->foreign('inventory_id')->references('id')->on('inventories');
            $table->unsignedInteger('inventory_management_id')->default(0);
            $table->float('quantity');
            $table->float('remaining_quantity')->default(0);
            $table->enum('unit',['KG','G','L'])->default('KG');
            $table->unsignedInteger('timing')->default(0);
            $table->string('price')->default(0);
            $table->string('total_price')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_used_in_water_details');
    }
}
