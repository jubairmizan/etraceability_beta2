<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWfPondWiseFish extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wf_rel_pond_wise_fish', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('org_id');
            $table->foreign('org_id')->references('id')->on('wf_organizations');
            $table->unsignedInteger('farm_id');
            $table->foreign('farm_id')->references('id')->on('wf_farms');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('wf_ponds');
            $table->unsignedInteger('fish_id');
            $table->foreign('fish_id')->references('id')->on('wf_type_of_fishes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wf_rel_pond_wise_fish');
    }
}
