<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiPlStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('si_pl_stock', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('hatchery_id');
            $table->foreign('hatchery_id')->references('id')->on('hatcheries');
            $table->unsignedInteger('farm_id');
            $table->foreign('farm_id')->references('id')->on('si_farms');
            $table->unsignedInteger('farm_manager_id');
            $table->foreign('farm_manager_id')->references('id')->on('users');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('si_ponds');
            $table->float('pl_quantity');
            $table->string('pl_age')->nullable();
            $table->float('pl_price');
            $table->float('average_weight')->nullable();
            $table->enum('shrimp_species',['Bagda','Golda'])->default('Bagda');
            $table->enum('status',['enclosure','harvested'])->default('enclosure');
            $table->unsignedInteger('harvest_tracking_id')->default(0);
            $table->unsignedInteger('harvest_counter')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('si_pl_stock');
    }
}
