<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHatcheryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hatcheries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('license_number');
            $table->unsignedInteger('division_id');
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->unsignedInteger('district_id');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->unsignedInteger('upazila_id');
            $table->foreign('upazila_id')->references('id')->on('upazilas');
            $table->unsignedInteger('union_id');
            $table->foreign('union_id')->references('id')->on('unions');
            $table->string('mouja')->nullable();
            $table->string('post_office')->nullable();
            $table->string('post_code')->nullable();
            $table->string('ward_no')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hatcheris');
    }
}
