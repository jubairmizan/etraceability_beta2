<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelSiPondManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rel_si_pond_manager', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('si_pond_id');
            $table->foreign('si_pond_id')->references('id')->on('si_ponds');
            $table->unsignedInteger('manager_id');
            $table->foreign('manager_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rel_si_pond_manager');
    }
}
