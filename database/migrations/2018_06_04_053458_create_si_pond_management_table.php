<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiPondManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('si_pond_management', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pond_id');
            $table->foreign('pond_id')->references('id')->on('si_ponds');
            $table->unsignedInteger('farm_id');
            $table->foreign('farm_id')->references('id')->on('si_farms');
            $table->unsignedInteger('farm_manager_id');
            $table->foreign('farm_manager_id')->references('id')->on('users');
            $table->unsignedInteger('harvest_tracking_id')->default(0);
            $table->unsignedInteger('harvest_counter')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('si_pond_management');
    }
}
