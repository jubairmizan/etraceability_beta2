<?php
/**
 * Created by PhpStorm.
 * User: Unifox Digital
 * Date: 6/3/2018
 * Time: 12:45 PM
 */
return [
    'user_types'=>[
        'admin'=>'Admin',
        'client'=>'Client',
        'buyer'=>'Buyer',
        'farmer'=>'Farmer',
        'hatchery_manager'=>'Hatchery Manager',
        'farm_manager'=>'Farm Manager',
        'cluster_manager'=>'Client Manager',
        'processing_plant_manager'=>'Processing Plant Manager'
    ],
    'genders'=>[
        'male'=>'Male',
        'female'=>'Female',
        'other'=>'Other'
    ]
];